# About Wildland Cargo for macOS

Wildland Cargo for macOS is a client application for
decentralized storage services provided by
[Wildland](https://wildland.io) project.

# Documentation links

For more technical information about the project see
our [technical documentation](tech) directory.

For information about Wildland Cargo swift SDK see
[SDK](https://gitlab.com/wildland/corex/sdk-apple) project
documentation.





