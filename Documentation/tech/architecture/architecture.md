% macOS Cargo client architecture

# Introduction

This document describes high-level macOS Cargo client
architecture.

Figure \ref{fig:context} shows the context diagram of the client.

![Client context\label{fig:context}](diagrams/cargo_context.png)

The app runs on macOS system but interfaces with cloud-provided
services and storage.

## Components and their roles
    
Internally Cargo client consists of several components, as
illustrated on figure \ref{fig:arch}.

![Client architecture\label{fig:arch}](diagrams/cargo_arch_fp.png)

There are several executables shipped within the client. Their
primary roles are discussed below.

### GUI (Cargo.app)

GUI is the main executable, which gets launched when user double
clicks on the application icon.

The GUI is responsible for interactive functionalities like:

- user onboarding,
- attaching new storage, 
- managing forest permissions,
- mounting / unmounting containers,
- configuring offline replication rules.

GUI is also responsible for registering the daemon (wlclientd) as
a launch item, to ensure that it is running through the user's
desktop session.

### File Provider extension

A Finder extension enabling exposing Wildland filesystem to
macOS.

### wlclientd

A daemon which maintains Wildland state and maintains
asynchronous, background tasks like data replication and conflict
resolution.


### CLI

This is an optional component, that may be developed to provide
some more advanced operations which are not exposed in the GUI.


## Communication between components

Components listed above share the same application group and use
XPC for communication. `wlclientd` accepts connections to an XPC
service and provides a set of specialized logical interfaces.

Currently the following interfaces are distinguished:

- **CTRL** - control interface, designated to perform
  Wildland-specific operations (examples are: mounting
  containers, user management, importing manifests)
  
- **FS** - file system interface, responsible for Wildland
  storage access, file system navigation and file I/O.
  

## Cross-platform library

Wildland Cargo uses [Wildland CoreX
SDK](https://gitlab.com/wildland/corex/sdk-apple) which packages
Wildand [cross platform
core](https://gitlab.com/wildland/corex/wildland-core) for usage
on Apple platforms.

The core life cycle is maintained within `wlclientd` process as
shown on figure ![Cargo app internal
design\label{fig:internal}](diagrams/cargo_app_arch.png).



