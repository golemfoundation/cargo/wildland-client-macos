// Use https://sequencediagram.org to update

title Progress Tracking Flow

participant FPE
participant FileProviderProxyActions
participant FileProviderManagerClientActions
participant GUI
participant Daemon


FPE->FileProviderProxyActions:FileProviderProxyActions

Daemon->FileProviderProxyActions:*private computed XPC property representing daemon: **FileProviderServiceProtocol**

note over FileProviderProxyActions, Daemon:uses **FileProviderManagerClientActions** methods (with calls on XPC *private instance **FileProviderServiceProtocol**)
FileProviderProxyActions->FileProviderManagerClientActions:continue on method (FileProviderProxyActions)\n

FileProviderManagerClientActions->Daemon:FileProviderServiceProtocol 

note over Daemon: starting sending progress - triggered by async stream

note right of Daemon: start*, ...*, end*\nare sended to\nall objects\nimplementing\n**FileProviderClientProtocol**\nthat are:\n- Daemon GUI\n- FPE


note over FileProviderManagerClientActions:consume XPC callbacks and yield stream continuation
Daemon->GUI:FileProviderClientProtocol (start*)\n
Daemon->FileProviderManagerClientActions:FileProviderClientProtocol (start*)

FileProviderManagerClientActions->FPE:FileProviderClientProtocol (stream progress update)
Daemon->GUI:FileProviderClientProtocol (...*)
Daemon->FileProviderManagerClientActions:FileProviderClientProtocol (...*)

FileProviderManagerClientActions->FPE:FileProviderClientProtocol (stream progress update)
Daemon->GUI:FileProviderClientProtocol (end*)

Daemon->FileProviderManagerClientActions:FileProviderClientProtocol (end*)

FileProviderManagerClientActions->FPE:FileProviderClientProtocol (stream operation completed update)

