#!/bin/sh
set -e
export_dia() {
	local name=$1
	local TMPNAM=`mktemp`.png
	local base=`basename $name .dia`
	dia -t png -e $TMPNAM $name 
	SIZ=`identify -format "%[width] %[height]" $TMPNAM |  awk '{printf "%dx%d", $1*2,$2*2}'` 
	rm $TMPNAM 
	dia -t png -e ${base}.png -s $SIZ $name 
}
for f in *.dia; do
	export_dia $f
done	
