Container management concepts
================

## Container management flow
 
Wildland `Cargo` uses `wlclientd` to manage users' containers. The management is possible when a user onboarding is complete, all the keys are storead in a `Local Secure Storage`, and at least free tier storage is available for container creation.

![Client context\label{fig:context}](../architecture/diagrams/cargo_containers.png)
<!-- ![Cargo container management
design\label{fig:internal}](/../diagrams/cargo_containers.png) -->

## Next operations are supported:
- List containers
- Create a container
- Remove a container
- Rename a container
- Mount a container
- List paths of a container
- Add a path to a container
- Remove a path from a container
- List storage template items
- List storage items of a container
- Add storage item to a container
- Remove storage item to a container

The operations create requests in `Cargo` through the XPC connection to `wlclientd`.

### 1. List containers:
`Cargo` request a list of all the available containers. The response provides all the existing containers or an empty list.
#### XPC call:
- `func getContainers(callback: @escaping (_ items: [Container]?, _ error: Error?) -> Void)`

### 2. Create container:
`Cargo` perform a request to create a container with a `StorageTemplate` object, a name for the container, and a path. The response gives a `Container` object with the details about the item.
#### XPC call:
- `func createContainer(for name: String, template: StorageTemplate, path: String, callback: @escaping (Error?) -> Void)`

### 3. Remove container:
`Cargo` perform a request to delete a container with a container object. The response returns an error if the operation fails; otherwise, nothing.
#### XPC call:
- `func delete(container: Container, callback: @escaping (Error?) -> Void)`

### 4. Rename container:
`Cargo` perform a request to create a container with a container object and a new name for this container. The response returns an error if the operation fails; otherwise, nothing.
#### XPC call:
- `func change(name: String, of container: Container, callback: @escaping (Error?) -> Void)`

### 5. Mount a container:
`Cargo` perform a request to mount a container with a container object and a parameter to use automount. The response returns an error if the operation fails; otherwise, nothing.
#### XPC call:
- `func mount(container: Container, with automount: Bool, callback: @escaping (Error?) -> Void)`

### 6. List paths of a container:
`Cargo` request a list of all the available paths fof the container object. The response provides all the existing path or an empty list and an error.
#### XPC call:
- `func getStorageTemplates(callback: @escaping (_ items: [StorageTemplate]?, _ error: Error?) -> Void)`

### 7. Add a path to a container:
`Cargo` perform a request to add a path to a container with a container object and a path value. The response returns an error if the operation fails; otherwise, nothing. After a successful path addition, `wlclientd` notify `File Provider extension` that there is a new path, and it must reload the list of file items for the working set and reflect these changes in the `Finder` app.
#### XPC call:
- `func add(path: String, to container: Container, callback: @escaping (Error?) -> Void)`

### 8. Remove a path from a container:
`Cargo` perform a request to remove a path to a container with a `Container` object and a path value. After a successful path removal, `wlclientd` notifies `File Provider extension` that the path is not available anymore. It must reload the list of file items for the working set and reflect these changes in the `Finder` app.
#### XPC call:
- `func remove(path: String, from container: Container, callback: @escaping (Error?) -> Void)`

### 9. List storage template items:
`Cargo` request a list of all the available storage template items. The response provides all the existing templates or an empty list.
#### XPC call:
- `func getStorageTemplates(callback: @escaping (_ items: [StorageTemplate]?, _ error: Error?) -> Void)`

### 10. List storage items of a container:
`Cargo` request a list of all the available storage items. The response provides all the existing storages or an empty list.
#### XPC call:
- `func getStorages(callback: @escaping (_ items: [Storage]?, _ error: Error?) -> Void)`

### 11. Add storage item to a container:
`Cargo` perform a request to add storage to an existing container with a `Container` object and a `StorageTemplate`. The response gives a `Storage` object with the details about the item.
#### XPC call:
- `func addStorage(to container: Container, from template: StorageTemplate, callback: @escaping (_ storage: Storage?, _ error: Error?) -> Void)`

### 12. Remove storage item to a container:
`Cargo` perform a request to remove storage to from an existing container with a `Container` object and a `Storage`. The response returns an error if the operation fails; otherwise, nothing.
#### XPC call:
- `func removeStorage(_ storage: Storage, from container: Container, callback: callback: @escaping (Error?) -> Void)`