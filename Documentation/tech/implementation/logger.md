Logging concepts
================

macOS wildland client uses [Unified Logging
System](https://developer.apple.com/documentation/os/logging) for
all the runtime logs. As there are several programs
constituting into WildlandClient.app, the following convention is
expected to be used for logging:

- all components log in the `io.wildland.client` subsystem

Each of main components sets its own logging category and logs
into it. More extensive hierarchies are allowed in justified
cases provided that there are no separate code units using the
same category.

All logger categories are expected to be listed here accompanied
by their component of origin and meaning.

- WildlandClient.app: `WildlandClient`
- wlclientd:
  - `wlclientd`: generic messages from the daemon
  - `CtrlService`: events logged by the daemon's
    XPC control interface
