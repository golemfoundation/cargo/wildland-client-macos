# Progress Tracking        

To ensure proper progress reporting in the File Provider Extension (FPE), a bidirectional XPC (Cross-Process Communication) connection is required. This means that the communication between the FPE and the Daemon needs to go both ways, allowing them to exchange information. Using methods with callbacks is not suitable because callbacks are one-time events and progress tracking requires periodic updates. However, callbacks can still be used for actions that trigger once per call.
    
Progress reporting is currently needed only for file operations. Therefore, the documentation will focus on the protocols involved, namely the `FileProviderServiceProtocol` and `FileProviderClientProtocol`. Mostly `FileProviderClientProtocol`.

# Communication  

In a bidirectional XPC communication, the FPE communicates with the Daemon through methods defined by the `FileProviderServiceProtocol`. To receive responses from the Daemon, the `FileProviderClientProtocol` is used. It's worth noting that these protocols combine other protocols. Not all actions require progress tracking, so only operations that need progress tracking implement the `FileProviderProgressProtocol`. Operations supporting progress tracking also implement the `FileProviderCancelProtocol`, which allows for canceling operations in progress.
**Notably, the FileProviderClientProtocol is implemented by two objects: the File Provider Extension (FPE) and the GUI.*

For implementation details, please refer to the code. Since progress reporting requires sequential execution, the implementation uses streams (Swift Concurrency).

![Client context\label{fig:context}](../architecture/diagrams/cargo_xpc_client_protocol.svg)

On the FPE side, the object responsible for establishing the XPC connection is named `FileProviderProxy`. It contains a property called `clientManager: FileProviderManagerClientActions`, which is an instance of an `Actor` conforming to the `FileProviderClientProtocol`. As the client manager handles operations on multiple tasks, it is crucial that it can handle operations from multiple threads and synchronize them to keep references to tasks in progress. It's important to mention that communication from the XPC occurs on a single thread per stabilized connection.

The lifecycle of the client manager is tightly related to the proxy object and is private, used only internally by the proxy.    

![Client context\label{fig:context}](../architecture/diagrams/cargo_progress_tracking.svg)
