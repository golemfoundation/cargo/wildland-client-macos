# Prepare system for the app reinstall

## Unload current application before running a new version

## File: [Light Cleanup](./lightCleanup.sh)

### Troubleshooting:
### If some issues appeare after the launching the new app version(daemon has incorrect version, File provider extension is not launched), then next steps could be required:

#### - Cleanup launch services DB
`/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister -kill -r -domain local -domain user`
####  - Reload file provider daemon:
`killall fileproviderd`

# Complete application uninstall

## Remove all the files created by the application

#### P.S. Keychain items will NOT be removed

## File: [Complete Cleanup](./completeCleanup.sh)
# Run scripts

### Open a terminal and run with a next command:
`<project_folder>/Documentation/tech/support/scriptName.sh`

### Troubleshooting:

####  - System might ask for the permissions to run script:
`chmod 755 <filename>.sh`
####  - To remove keys from the Keychain user should open `Keychain Access` application, search and remove all the the keys with name `io.wildland.Cargo`.
