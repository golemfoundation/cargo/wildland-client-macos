# Wildland Cargo for macOS - technical documentation

This directory contains technical documentation of Wildland Cargo
for macOS project.

## Table of contents

- [Architecture](architecture/architecture.md)
- [Implementation](implementation/implementation.md)
  - [Logger](implementation/logger.md)
  - [Container Management](implementation/containerManagement.md)
  - [Progress Tracking](implementation/progressTracking.md)
- [Support]()
  - [Uninstall](support/uninstall.md)
