#!/bin/sh
set -ex
build_num=`git rev-list --no-merges HEAD | wc -l | sed 's/ //g'`
echo $build_num
