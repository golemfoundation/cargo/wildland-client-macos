#!/usr/bin/env bash

commitHash=`git rev-parse HEAD`
/usr/libexec/PlistBuddy -c "Delete :ClientCommitHash" "${INFOPLIST_FILE}" || true
/usr/libexec/PlistBuddy -c "Add :ClientCommitHash string $commitHash" "${INFOPLIST_FILE}"

timestamp=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
/usr/libexec/PlistBuddy -c "Delete :ClientBuildTimestamp" "${INFOPLIST_FILE}" || true
/usr/libexec/PlistBuddy -c "Add :ClientBuildTimestamp string $timestamp" "${INFOPLIST_FILE}"

xcworkspaceDir="$(dirname "$PROJECT_DIR")"
packageResolvedPath="$xcworkspaceDir/WildlandClient.xcworkspace/xcshareddata/swiftpm/Package.resolved"
sdkParameters=`sed -n '/"identity" : "sdk-apple"/,/}/p' $packageResolvedPath`
sdkCommitHash=`echo $sdkParameters | sed 's/.*"revision" : "\(.*\)".*/\1/'`
/usr/libexec/PlistBuddy -c "Delete :SDKCommitHash" "${INFOPLIST_FILE}" || true
/usr/libexec/PlistBuddy -c "Add :SDKCommitHash string $sdkCommitHash" "${INFOPLIST_FILE}"

buildDir=`echo $BUILD_DIR | sed 's/\(Build\).*/\1/g'`
wildlandClientDir="$(dirname "$buildDir")"
sdkPackagePath="$wildlandClientDir/SourcePackages/checkouts/sdk-apple/Package.swift"
sdkUrl=`sed -n '/url: "/,/"/p' $sdkPackagePath`
sdkVersion=`echo $sdkUrl | sed 's/.*wildlandx.xcframework-\(.*\).zip.*/\1/'`
/usr/libexec/PlistBuddy -c "Delete :SDKVersion" "${INFOPLIST_FILE}" || true
/usr/libexec/PlistBuddy -c "Add :SDKVersion string $sdkVersion" "${INFOPLIST_FILE}"
