#!/bin/sh
set -e

SPARKLE_APPCAST_URL=${SPARKLE_APPCAST_URL:-"https://wildland.io/this/is/placeholder.html"}
SPARKLE_PUBLIC_KEY=${SPARKLE_PUBLIC_KEY:-"PLACEHOLDER_KEY"}

# Tool to read plist files
PLIST_EDITOR="/usr/libexec/PlistBuddy"

# Get the bundle version from the Info.plist file
INFO_PLIST=${1:-${BUILT_PRODUCTS_DIR}/${INFOPLIST_PATH}}

# Set the SUPublicEDKey
$PLIST_EDITOR -c "Set :SUPublicEDKey $SPARKLE_PUBLIC_KEY" "$INFO_PLIST"
# Set appcast.xml location
$PLIST_EDITOR -c "Set :SUFeedURL $SPARKLE_APPCAST_URL" "$INFO_PLIST"
