#!/bin/sh
set -e

# Tool to read plist files
PLIST_EDITOR="/usr/libexec/PlistBuddy"

# Get the bundle version from the Info.plist file
INFO_PLIST=${1:-${BUILT_PRODUCTS_DIR}/${INFOPLIST_PATH}}

# Fetch the current git commit count and use it as the bundle build version
BUILD_NUM=$(git rev-list --no-merges HEAD | wc -l | sed 's/ //g')

# Set the bundle build version from the Info.plist file
$PLIST_EDITOR -c "Set :CFBundleVersion $BUILD_NUM" "${INFO_PLIST}"

# Get current bundle version from the Info.plist file

APP_VERSION=$(cat ${SRCROOT}/../app_version.txt)
if [ -z "$APP_VERSION" ]; then
  echo "Failed to get app version"
  exit 1
fi
if ! [[ "$APP_VERSION" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "App version is not in format MAJOR.MINOR.PATCH"
  exit 1
fi
echo "App version is $APP_VERSION"

# Parse the version number from the plist file
MAJOR_VERSION=`echo $APP_VERSION | cut -d '.' -f 1`
MINOR_VERSION=`echo $APP_VERSION | cut -d '.' -f 2`

# Concatenate the version number
NEW_APP_VERSION="$MAJOR_VERSION.$MINOR_VERSION.$BUILD_NUM"

# Set the bundle version from the Info.plist file
$PLIST_EDITOR -c "Set :CFBundleShortVersionString $NEW_APP_VERSION" "${INFO_PLIST}"
