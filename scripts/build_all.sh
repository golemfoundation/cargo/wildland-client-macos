#!/bin/sh

set -ex

WL_BUILD_ROOT=${1:?"need build root path. Usually 'build' folder or another path to keep build intermediates"}

WL_PRODUCT_SCHEME="Cargo"
WL_XCARCHIVE="$WL_BUILD_ROOT/$WL_PRODUCT_SCHEME.xcarchive"
WL_EXPORT_DIR="$WL_BUILD_ROOT/Export"
WL_OUTPUT_DIR="$WL_BUILD_ROOT/Products"
CURRENT_DIR=$(dirname "$0")

VERSION=$(cat app_version.txt)
if [ -z "$VERSION" ]; then
  echo "Failed to get app version"
  exit 1
fi
if ! [[ "$VERSION" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "App version is not in format MAJOR.MINOR.PATCH"
  exit 1
fi
echo "App version is $VERSION"

BUILD_NUMBER=$("$CURRENT_DIR/get_build_no.sh")

echo "Starting build of $WL_PRODUCT_SCHEME. Version $VERSION.$BUILD_NUMBER. Build root: $WL_BUILD_ROOT. Output: $WL_OUTPUT_DIR"

rm -rf "$WL_BUILD_ROOT"
mkdir -p "$WL_BUILD_ROOT"
mkdir -p "$WL_OUTPUT_DIR"

echo "Compiling sources and making xcarchive"
xcodebuild archive \
  -workspace WildlandClient.xcworkspace \
  -scheme Cargo \
  -sdk macosx \
  -arch x86_64 -arch arm64 \
  -configuration Release \
  -archivePath "$WL_BUILD_ROOT/$WL_PRODUCT_SCHEME" \
  -derivedDataPath "$WL_BUILD_ROOT" \
  WL_VERSION=$VERSION \
  WL_BUILD_NUMBER="$BUILD_NUMBER" 

xcodebuild -exportArchive \
  -archivePath "$WL_XCARCHIVE" \
  -exportPath "$WL_EXPORT_DIR" \
  -exportOptionsPlist "$CURRENT_DIR/ExportOptions.plist" 

echo "Creating DMG"
WildlandClient/CargoDMG/create_dmg.sh \
  "$WL_EXPORT_DIR/$WL_PRODUCT_SCHEME.app" \
  "$WL_OUTPUT_DIR/$WL_PRODUCT_SCHEME.dmg"

echo "Copying products to output directory"
cp -R "$WL_XCARCHIVE/dSYMs" "$WL_OUTPUT_DIR"
