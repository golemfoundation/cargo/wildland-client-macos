#!/usr/bin/env bash

swiftlint --fix WildlandClient sdk-apple/Wildland
swiftformat \
  --swiftversion 5 \
  --indent 2 \
  --binarygrouping none \
  --decimalgrouping none \
  --hexgrouping none \
  --hexliteralcase lowercase \
  --octalgrouping none \
  --patternlet inline \
  --nospaceoperators \
  --stripunusedargs closure-only \
  --wraparguments before-first \
  --wrapparameters before-first \
  --wrapcollections before-first \
  --trimwhitespace nonblank-lines \
  --ranges no-space \
  --disable extensionAccessControl,blankLinesAtStartOfScope,wrapMultilineStatementBraces \
  WildlandClient sdk-apple/Wildland
