//
	

import SwiftUI

@main
struct WildlandCargo_iOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
