//
	

import FileProvider
import UniformTypeIdentifiers

class FileProviderItem: NSObject, NSFileProviderItem {

    // TODO: implement an initializer to create an item from your extension's backing model
    // TODO: implement the accessors to return the values from your extension's backing model
  
  private let _path: String
  init(path: String) {
    _path = path
  }
  
  var itemIdentifier: NSFileProviderItemIdentifier {
    return NSFileProviderItemIdentifier(_path)
  }
    
  var parentItemIdentifier: NSFileProviderItemIdentifier {
      return NSFileProviderItemIdentifier("")
  }
    
  var capabilities: NSFileProviderItemCapabilities {
    // TODO: implement capabilities
    return [.allowsReading, .allowsWriting, .allowsRenaming, .allowsReparenting, .allowsTrashing, .allowsDeleting]
  }
    
  var filename: String {
    return (_path as NSString).lastPathComponent
  }
    
  var contentType: UTType {
    return .folder //itemIdentifier == NSFileProviderItemIdentifier.rootContainer ? .folder : .plainText
  }
    
}
