#!/bin/bash

. ./common.sh
. ./tests.sh

EXITCODE=0
CARGO_DMG=${1:-""}

if [ -n "$CARGO_DMG" ] && [ ! -f "$CARGO_DMG" ]; then
  echo "Cargo DMG file not exists"
  exit 101
fi
echo "Remove old output directory"
rm -Rf out
echo "Fix SSH key privileges"
chmod 0600 "$SSHKEY"

echo "Starting VMs"
start_vms || exit 102
wait_for_vms

# WARNING ---== ACTUAL TEST CODE STARTS HERE ==----
if [ -n "$CARGO_DMG" ]; then
  echo "Installing Cargo"
  scp -i "$SSHKEY" "$CARGO_DMG" "$VM_USERNAME@$VM_IP_A:Cargo.dmg"
  ssh -i "$SSHKEY" "$VM_USERNAME@$VM_IP_A" "hdiutil mount Cargo.dmg && sudo cp -R /Volumes/Cargo/Cargo.app /Applications && hdiutil unmount /Volumes/Cargo"
  scp -i "$SSHKEY" "$CARGO_DMG" "$VM_USERNAME@$VM_IP_B:Cargo.dmg"
  ssh -i "$SSHKEY" "$VM_USERNAME@$VM_IP_B" "hdiutil mount Cargo.dmg && sudo cp -R /Volumes/Cargo/Cargo.app /Applications && hdiutil unmount /Volumes/Cargo"
  echo "Cargo install completed"
fi
echo "---- TEST START ----"
run_tests
EXITCODE=$?
echo "---- TEST END ----"
# WARNING ---== NO TEST LOGIC SHOULD HAPPEN BEYOND THIS POINT ==---

echo "Shutting down VM"
stop_vms || echo "VM already stopped, omiting stop command"
wait_for_no_vms
[ -n "$VM_NO_ROLLBACK" ] || rollback_vms
echo "All done"

if [ $EXITCODE -eq 0 ]; then
  echo "Tests Succeeded"
else
  echo "Tests Failed"
fi
exit $EXITCODE
