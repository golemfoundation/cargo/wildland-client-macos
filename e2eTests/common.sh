STDIR=${STDIR=$(pwd)}
SSHKEY="${STDIR}/ssh/demo"
VM_NAME=${VM_NAME:-"macOS_13.3.1"}
VM_IP_A="192.168.64.2"
VM_IP_B="192.168.64.3"
VM_USERNAME=demo
UTM_DIR=${UTM_DIR:-"${HOME}/UTM"}

start_vms() {
  open -a UTM
  until pgrep -q UTM; do
    sleep 1
  done
  sleep 5 # Dirty hack to be sure that UTM is up and running
  vm_start "${VM_NAME}_A" && vm_start "${VM_NAME}_B" || return 1
}

wait_for_vms() {
  wait_for_vm "$VM_IP_A" && wait_for_vm "$VM_IP_B" || return 1
}

wait_for_no_vms() {
  wait_for_no_vm "$VM_IP_A" && wait_for_no_vm "$VM_IP_B" || return 1
}

stop_vms() {
  vm_stop "${VM_NAME}_A" "$VM_IP_A" && vm_stop "${VM_NAME}_B" "$VM_IP_B" || return 1
}

rollback_vms() {
  vm_rollback "${VM_NAME}_A" && vm_rollback "${VM_NAME}_B" || return 1
}

vm_check_status() {
  local VM_NAME=$1
  local STATUS=$2
  VM_STATUS=$(utmctl status "$VM_NAME")
  if [ "$VM_STATUS" != "$STATUS" ]; then
    return 1
  fi
}

vm_start() {
  local VM_NAME=$1
  echo "Import VM $VM_NAME to UTM"
  open "$UTM_DIR/${VM_NAME}.utm"
  vm_check_status "$VM_NAME" "stopped" || return 1
  echo "Start VM $VM_NAME"
  utmctl start "$VM_NAME"
}

vm_stop() {
  local VM_NAME=$1
  local VM_IP=$2
  vm_check_status "$VM_NAME" "started" || return 1
  ssh -i "$SSHKEY" $VM_USERNAME@$VM_IP 'sudo shutdown -h now'
  until vm_check_status "$VM_NAME" "stopped"; do
    sleep 1
  done
}

vm_rollback() {
  local VM_NAME=$1
  vm_check_status "$VM_NAME" "stopped"
  pushd "$UTM_DIR" || return 1
  echo "Deleting of old VM $VM_NAME"
  rm -Rf "${VM_NAME}.utm"
  echo "Copying VM $VM_NAME image"
  cp -cR "${VM_NAME}.utm.backup" "${VM_NAME}.utm"
  echo "Rollback $VM_NAME completed"
  popd || return 1
}

wait_for_vm() {
  local VM_IP=$1
  echo "Waiting for VM to start"
  local gotit=0
  while [ "$gotit" != "1" ]; do
    sleep 1
    gotit=$(ssh -i "$SSHKEY" -oConnectTimeout=10 $VM_USERNAME@$VM_IP echo 1 2>/dev/null)
  done
}

wait_for_no_vm() {
  local VM_IP=$1
  echo "Waiting for VM to terminate"
  local stillthere=1
  while [ $stillthere -ne 0 ]; do
    ping -c1 -q $VM_IP >/dev/null 2>&1
    if [ $? -eq 2 ]; then
      stillthere=0
    else
      sleep 1
    fi
  done
}
