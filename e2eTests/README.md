# end2end tests

## Test environment

Test suite uses [UTM](https://getutm.app/) to host VMs (Virtual Machines). Macos hosting is possible only 
on Apple Silicon machines. Additionally only 2 VMs can be hosted simultaniously on one physical machine due to licensing limitations.

Gitlab runner working on macos system starts locally 2 VMs with macos to perform tests on them.
After tests both VMs are restarted to initial state.
After performing tests `out` directory is archived as GitLab artifact and contents of `out/report.xml` are used 
as JUnit compatible test report.

## Howto use it

### User

- Prepare env according to `docs` directory
- run `./run_test_suite.sh` if you don't want to have Cargo installed or `./run_test_suite.sh [CARGO_DMG_FILE]` to install Cargo during VM start

### Testers

Put all your code in `tests.sh` file, function `run_tests()`

### SSH to VMs

```shell
ssh -i ssh/demo demo@192.168.64.2 [...]
```

## Configuration details

VM IP addresses:

| VM   | IP Address   |
|------|--------------|
| VM A | 192.168.64.2 |
| VM B | 192.168.64.3 |
