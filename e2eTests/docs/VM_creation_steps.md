# VM installation and config steps

Look at [SmokeTests](../../SmokeTests/docs/VM_creation_steps.md)

## Changes:

- we take VM image from SmokeTests, create 2 copies with suffixes `_A` and `_B`
  ```shell
  cp -cR macOS_13.3.1.utm.backup macOS_13.3.1_A.utm
  cp -cR macOS_13.3.1.utm.backup macOS_13.3.1_B.utm
  ```
- load 2nd VM in UTM
  ```shell
  open macOS_13.3.1_A.utm
  open macOS_13.3.1_B.utm
  ```
- rename VMs
- change 2nd VM IP address from `192.168.64.2` to `192.168.64.3`
- change 2nd VM mac address
- login to both VMs to update ssh known hosts
- make backup of created VMs
  ```shell
  mv macOS_13.3.1_A.utm macOS_13.3.1_A.utm.backup
  mv macOS_13.3.1_B.utm macOS_13.3.1_B.utm.backup
  ```
- create images for 1st boot
  ```shell
  cp -cR macOS_13.3.1_A.utm.backup macOS_13.3.1_A.utm
  cp -cR macOS_13.3.1_B.utm.backup macOS_13.3.1_B.utm
    ```
