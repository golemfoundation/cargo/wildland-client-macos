//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import XCTest

final class FileOperationStorageTests: XCTestCase {

  // MARK: - Private

  private var sut: FileOperationStorage!

  // MARK: - Setup

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testBytesPerSecondReturnAvarageOfBytesPerSeconds() {
    // given
    sut = FileOperationStorage(
      identifier: "1",
      filename: "example.png",
      type: .transfer
    )

    [
      FileOperationMetadata(completedUnitCount: 0, totalUnitCount: 10, time: 0.0),
      FileOperationMetadata(completedUnitCount: 2, totalUnitCount: 10, time: 5.0),
      FileOperationMetadata(completedUnitCount: 4, totalUnitCount: 10, time: 9.0),
      FileOperationMetadata(completedUnitCount: 6, totalUnitCount: 10, time: 13.0),
      FileOperationMetadata(completedUnitCount: 8, totalUnitCount: 10, time: 20.0)
    ]
      .forEach { sut.setOperationMetadata($0) }

    // when
    let result = sut.bytesPerSecond

    // then
    XCTAssertEqual(result, 0.4, accuracy: 0.01)
  }

  func testFilenames() {
    // given
    let array = [
      FileOperationStorage.fixture(identifier: "1", filename: "example1.jpg"),
      FileOperationStorage.fixture(identifier: "2", filename: "example1.jpg"),
      FileOperationStorage.fixture(identifier: "3", filename: "example1.jpg")
    ]
    let expected = Array(repeating: "example1.jpg", count: 3)

    // when
    let result = array.filenames

    // then
    XCTAssertEqual(result, expected)
  }

  func testCurrentSize() {
    // given
    let array = [
      FileOperationStorage.fixture(operationMetadata: [
        .fixture(completedUnitCount: 0),
        .fixture(completedUnitCount: 2),
        .fixture(completedUnitCount: 5)
      ]),
      FileOperationStorage.fixture(operationMetadata: [.fixture(completedUnitCount: 3)]),
      FileOperationStorage.fixture(operationMetadata: [.fixture(completedUnitCount: 8)])
    ]

    // when
    let result = array.currentSize

    // then
    XCTAssertEqual(result, 16.0)
  }

  func testTotalSize() {
    // given
    let array = [
      FileOperationStorage.fixture(operationMetadata: [.fixture(totalUnitCount: 10)]),
      FileOperationStorage.fixture(operationMetadata: [.fixture(totalUnitCount: 15)]),
      FileOperationStorage.fixture(operationMetadata: [.fixture(totalUnitCount: 20)])
    ]

    // when
    let result = array.totalSize

    // then
    XCTAssertEqual(result, 45.0)
  }

  func testEstimation() {
    // given
    let array = [
      FileOperationStorage.fixture(
        operationMetadata: [
          .fixture(completedUnitCount: 0, totalUnitCount: 10, time: 0),
          .fixture(completedUnitCount: 8, totalUnitCount: 10, time: 2)
        ]
      ),
      FileOperationStorage.fixture(
        operationMetadata: [
          .fixture(completedUnitCount: 0, totalUnitCount: 20, time: 0),
          .fixture(completedUnitCount: 15, totalUnitCount: 20, time: 10)
        ]
      )
    ]

    // when
    let result = array.estimation

    // then
    XCTAssertEqual(result, 2.54, accuracy: 0.01)
  }
}
