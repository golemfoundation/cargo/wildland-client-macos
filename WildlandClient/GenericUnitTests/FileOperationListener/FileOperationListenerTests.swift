//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import XCTest

final class WildlandFileOperationListenerTests: XCTestCase {

  // MARK: - Private

  private var sut: WildlandFileOperationListener!

  // MARK: - Setup

  override func setUp() {
    sut = WildlandFileOperationListener()
    super.setUp()
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testOperationsCalledWhenStartOperation() {
    // given
    let spy = ValueSpy(publisher: sut.operationsPublisher)
    let result = FileOperationStorage.fixture(operationMetadata: [])
    let uuid = UUID()

    // when
    sut.startOperation(taskId: uuid, identifier: "1", filename: "examplePath.png", type: .transfer)

    // then
    XCTAssertEqual(spy.values, [[], [result]])
  }

  func testOperationsCalledWhenSetOperationMetadata() {
    // given
    let spy = ValueSpy(publisher: sut.operationsPublisher)
    let metadata = FileOperationMetadata.fixture(completedUnitCount: 7, time: 10.0)
    let uuid = UUID()
    sut.startOperation(taskId: uuid, identifier: "1", filename: "examplePath.png", type: .transfer)

    // when
    sut.setOperationMetadata(metadata, identifier: "1")

    // then
    let expected: [[FileOperationStorage]] = [
      [],
      [.fixture()],
      [.fixture(operationMetadata: [metadata])]
    ]
    XCTAssertEqual(spy.values, expected)
  }

  func testOperationsCalledWhenFinishOperation() {
    // given
    let spy = ValueSpy(publisher: sut.operationsPublisher)
    let metadata = FileOperationMetadata.fixture(completedUnitCount: 5, time: 10.0)
    let uuid = UUID()
    sut.startOperation(taskId: uuid, identifier: "1", filename: "examplePath.png", type: .transfer)
    sut.setOperationMetadata(metadata, identifier: "1")

    // when
    sut.finishOperation(uuid)

    // then
    let expected: [[FileOperationStorage]] = [[], [.fixture()], [.fixture(operationMetadata: [metadata])], []]
    XCTAssertEqual(spy.values, expected)
  }

  func testOperationsAreAggregated() {
    // given
    let spy = ValueSpy(publisher: sut.operationsPublisher)
    let metadata = FileOperationMetadata.fixture(completedUnitCount: 5, time: 10.0)
    sut.startOperation(taskId: UUID(), identifier: "1", filename: "examplePath.png", type: .transfer)
    sut.startOperation(taskId: UUID(), identifier: "2", filename: "examplePath2.png", type: .transfer)

    // when
    sut.setOperationMetadata(metadata, identifier: "1")
    sut.setOperationMetadata(metadata, identifier: "2")

    // then
    let expected: [[FileOperationStorage]] = [
      [],
      [.fixture()],
      [.fixture(), .fixture(identifier: "2", filename: "examplePath2.png")],
      [
        .fixture(operationMetadata: [metadata]),
        .fixture(identifier: "2", filename: "examplePath2.png")
      ],
      [
        .fixture(operationMetadata: [metadata]),
        .fixture(identifier: "2", filename: "examplePath2.png", operationMetadata: [metadata])
      ]
    ]
    XCTAssertEqual(spy.values, expected)
  }

  func testOperationsAreCleanedWhenAllOperationsFinished() {
    // given
    let spy = ValueSpy(publisher: sut.operationsPublisher)
    let uuid = UUID()
    let uuid2 = UUID()
    sut.startOperation(taskId: uuid, identifier: "1", filename: "examplePath.png", type: .transfer)
    sut.startOperation(taskId: uuid2, identifier: "2", filename: "examplePath2.png", type: .transfer)

    // when
    sut.finishOperation(uuid)
    sut.finishOperation(uuid2)

    // then
    let expected: [[FileOperationStorage]] = [
      [],
      [
        .fixture()
      ],
      [
        .fixture(),
        .fixture(identifier: "2", filename: "examplePath2.png")
      ],
      []
    ]
    XCTAssertEqual(spy.values, expected)
  }
}
