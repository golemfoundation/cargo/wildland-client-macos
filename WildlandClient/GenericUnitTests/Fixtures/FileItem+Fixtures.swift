//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon

extension FileItem {
  static func fixture(
    identifier: String,
    parentIdentifier: String,
    modificationDate: Date,
    size: UInt64 = .zero,
    type: FileProviderDomainService.Entry.EntryType = .file
  ) -> FileItem {
    let entry = FileProviderDomainService.Entry(
      name: "example name",
      identifier: identifier,
      parentIdentifier: parentIdentifier,
      revision: .zero,
      deleted: false,
      size: size,
      type: type,
      metadata: FileProviderDomainService.EntryMetadata(
        fileSystemFlags: nil,
        lastUsedDate: nil,
        creationDate: nil,
        contentModificationDate: modificationDate,
        validEntries: nil,
        owner: nil,
        recipients: nil
      )
    )
    return FileItem(entry, userPublicKey: "")
  }
}
