//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import WildlandCommon
import XCTest

final class FileItemProviderTests: XCTestCase {

  // MARK: - Private

  private var sut: FileItemProvider!
  private var fileItemStorage: FileItemStorageMock!
  private var fileItemCoreFacade: FileItemCoreFacadeMock!

  // MARK: - Setup

  override func setUp() {
    fileItemStorage = FileItemStorageMock()
    fileItemCoreFacade = FileItemCoreFacadeMock()
    sut = FileItemProvider(fileItemStorage: fileItemStorage, fileItemCoreFacade: fileItemCoreFacade)
    super.setUp()
  }

  override func tearDown() {
    fileItemStorage = nil
    fileItemCoreFacade = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testFileItemFromCache() async throws {
    // given
    let date = try self.date(from: "21-05-2023")
    let expected = FileItem.fixture(identifier: "1", parentIdentifier: "2", modificationDate: date)
    fileItemStorage.expectedFileItem = expected

    // when
    let fileItem = try await sut.fileItem(for: "1")

    // then
    XCTAssertEqual(fileItem, expected)
    XCTAssertEqual(fileItemStorage.fileItemCounter, 1)
    XCTAssertEqual(fileItemStorage.identifierSet, "1")
    XCTAssertEqual(fileItemCoreFacade.fileItemCounter, 0)
  }

  func testFileItemFromCore() async throws {
    // given
    let date = try self.date(from: "21-05-2023")
    let expected = FileItem.fixture(identifier: "1", parentIdentifier: "2", modificationDate: date)
    fileItemStorage.expectedFileItem = nil
    fileItemCoreFacade.expectedFileItem = expected

    // when
    let fileItem = try await sut.fileItem(for: "1")

    // then
    XCTAssertEqual(fileItem, expected)
    XCTAssertEqual(fileItemStorage.fileItemCounter, 1)
    XCTAssertEqual(fileItemStorage.identifierSet, "1")
    XCTAssertEqual(fileItemCoreFacade.fileItemCounter, 1)
    XCTAssertEqual(fileItemCoreFacade.identifierSet, "1")
  }

  func testFileItemThrowsErrorFromCore() async throws {
    // given
    fileItemStorage.expectedFileItem = nil
    fileItemCoreFacade.expectedFileItem = nil

    // when
    fileItemCoreFacade.fileItemError = ErrorStub.stub

    // then
    await XCTAssertThrowsError(try await sut.fileItem(for: "1"))
  }

  func testFileItemsFromCore() async throws {
    // given
    let date = try self.date(from: "21-05-2023")
    let expected1 = FileItem.fixture(identifier: "1", parentIdentifier: "3", modificationDate: date)
    let expected2 = FileItem.fixture(identifier: "2", parentIdentifier: "3", modificationDate: date)
    fileItemCoreFacade.expectedFileItems = [expected1, expected2]

    // when
    let fileItems = try await sut.fileItems(for: "3")

    // then
    XCTAssertEqual(fileItems, [expected1, expected2])
    XCTAssertEqual(fileItemStorage.storeCounter, 2)
    XCTAssertEqual(fileItemStorage.fileItemsSet, [expected1, expected2])
    XCTAssertEqual(fileItemCoreFacade.fileItemsCounter, 1)
    XCTAssertEqual(fileItemCoreFacade.parentIdentifierSet, "3")
  }

  func testFileItemsThrowsErrorFromCore() async throws {
    // given
    fileItemStorage.expectedFileItem = nil
    fileItemCoreFacade.expectedFileItem = nil

    // when
    fileItemCoreFacade.fileItemsError = ErrorStub.stub

    // then
    await XCTAssertThrowsError(try await sut.fileItems(for: "1"))
  }
}
