//
// Wildland Project
// SecureStorageTests.swift
// WildlandCommon
//
// Copyright © 2023 Golem Foundation
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import XCTest
@testable import CargoUI

final class OnboardingProgressProviderTests: XCTestCase {

  private let newSecureWordsPath: [OnboardingCoordinator.Stage] = [
    .setupCargo,
      .aboutWildlandIdentity,
      .createIdentity,
      .wildlandSecretPhraseIntro,
      .newSecureWords
  ]

  private let enterVerificationCodePath: [OnboardingCoordinator.Stage] = [
    .setupCargo,
    .aboutWildlandIdentity,
    .createIdentity,
    .wildlandSecretPhraseIntro,
    .newSecureWords,
    .confirmSecureWords,
    .nameDevice,
    .chooseCargoStorage,
    .getVerificationCode,
    .enterVerificationCode
  ]

  private let chooseCargoStorage: [OnboardingCoordinator.Stage] = [
    .setupCargo,
    .chooseIdentity,
    .nameDevice,
    .chooseCargoStorage
  ]

  func testProgressStages() {
    // given
    let sut = OnboardingProgressProvider()
    let parameters: [([OnboardingCoordinator.Stage], Double)] = [
      ([.setupCargo], 0.1),
      ([.setupCargo, .aboutWildlandIdentity], 0.18),
      ([.setupCargo, .chooseIdentity], 0.22),
      (newSecureWordsPath, 0.45),
      (enterVerificationCodePath, 0.90),
      (chooseCargoStorage, 0.57)
    ]

    // when
    parameters.forEach { parameters in
      let result = sut.progress(for: parameters.0)

      // then
      XCTAssertEqual(result, parameters.1, accuracy: 0.01, "stages: \(parameters.0)")
    }
  }
}
