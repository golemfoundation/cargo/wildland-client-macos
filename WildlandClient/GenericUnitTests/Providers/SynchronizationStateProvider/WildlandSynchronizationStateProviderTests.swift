//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import XCTest
import WildlandCommon

// swiftlint:disable:next type_name
final class WildlandSynchronizationStateProviderTests: XCTestCase {

  // MARK: - Private

  private var sut: WildlandSynchronizationStateProvider!
  private var fileProviderGlobalProgressObserver: FileProviderGlobalProgressObserverMock!
  private var fileOperationListener: FileOperationListenerMock!

  // MARK: - Setup

  override func setUp() {
    fileProviderGlobalProgressObserver = FileProviderGlobalProgressObserverMock()
    fileOperationListener = FileOperationListenerMock()
    sut = WildlandSynchronizationStateProvider(
      globalProgressObserver: fileProviderGlobalProgressObserver,
      fileOperationListener: fileOperationListener
    )
    super.setUp()
  }

  override func tearDown() {
    fileProviderGlobalProgressObserver = nil
    fileOperationListener = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testSyncedStatePublisherCalledOnInit() {
    // when
    let spy = ValueSpy(publisher: sut.statePublisher)

    // then
    XCTAssertEqual(spy.values, [.synced])
  }

  func testSyncingStatePublisherCalledInProgressFalseAndOperationsNotEmpty() {
    // given
    let spy = ValueSpy(publisher: sut.statePublisher)
    let syncProgress = SynchronizationState.SyncProgress(
      files: ["examplePath.png"],
      currentSize: Measurement(value: 0.0, unit: .bytes),
      totalSize: Measurement(value: 0.0, unit: .bytes),
      estimate: Measurement(value: 0.0, unit: .seconds)
    )

    // when
    fileOperationListener.publishOperations([.fixture()])
    fileProviderGlobalProgressObserver.publisherProgress(false)

    // then
    XCTAssertEqual(spy.values, [.synced, .syncing(syncProgress)])
  }

  func testSyncedStatePublisherCalledWhenOperationsEmptyAndInProgressTrue() {
    // given
    let spy = ValueSpy(publisher: sut.statePublisher)
    let syncProgress = SynchronizationState.SyncProgress(
      files: [],
      currentSize: Measurement(value: 0.0, unit: .bytes),
      totalSize: Measurement(value: 0.0, unit: .bytes),
      estimate: Measurement(value: 0.0, unit: .seconds)
    )

    // when
    fileOperationListener.publishOperations([])
    fileProviderGlobalProgressObserver.publisherProgress(true)

    // then
    XCTAssertEqual(spy.values, [.synced, .syncing(syncProgress)])
  }

  func testSyncingStatePublisherCalledWhenOperationsNotEmptyAndInProgressTrue() {
    // given
    let spy = ValueSpy(publisher: sut.statePublisher)
    let metadata = FileOperationMetadata.fixture(completedUnitCount: 5)
    let operation = FileOperationStorage.fixture(operationMetadata: [metadata])
    let syncProgress = SynchronizationState.SyncProgress(
      files: ["examplePath.png"],
      currentSize: Measurement(value: 5.0, unit: .bytes),
      totalSize: Measurement(value: 10.0, unit: .bytes),
      estimate: Measurement(value: 0.0, unit: .seconds)
    )

    // when
    fileOperationListener.publishOperations([operation])
    fileProviderGlobalProgressObserver.publisherProgress(true)

    // then
    XCTAssertEqual(spy.values, [.synced, .syncing(syncProgress)])
  }
}
