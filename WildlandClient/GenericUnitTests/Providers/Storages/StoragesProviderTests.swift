//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
@testable import WildlandCommon
import XCTest
import Combine

final class StoragesProviderTests: XCTestCase {

  // MARK: - Properties

  private var sut: StoragesProvider!
  private var userStoragesProvider: UserStoragesProviderStub!

  // MARK: - Setup

  override func setUp() {
    userStoragesProvider = UserStoragesProviderStub()
    sut = StoragesProvider(userApi: userStoragesProvider)
    super.setUp()
  }

  override func tearDown() {
    userStoragesProvider = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testGetStorages() throws {
    // given
    userStoragesProvider.storages = [.fixture()]

    // when
    let storages = try sut.getStorages()

    // then
    XCTAssertEqual(storages, [.fixture()])
  }

  func testGetStoragesCallsThresholdExceeded() throws {
    // given
    userStoragesProvider.storages = [.fixture(usedSpace: 86)]
    let valueSpy = ValueSpy(publisher: sut.thresholdExceededPublisher)

    // when
    _ = try sut.getStorages()

    // then
    XCTAssertEqual(valueSpy.values, [86])
  }

  func testGetStoragesCallsThresholdExceededNilWhenDismissed() throws {
    // given
    userStoragesProvider.storages = [.fixture(usedSpace: 86)]
    let valueSpy = ValueSpy(publisher: sut.thresholdExceededPublisher)

    // when
    _ = try sut.getStorages()
    sut.warningDismissed()

    // then
    XCTAssertEqual(valueSpy.values, [86, nil])
  }

  func testGetStoragesCallsThresholdExceededNilWhenDismissedAndCalledSecondTime() throws {
    // given
    userStoragesProvider.storages = [.fixture(usedSpace: 86)]
    let valueSpy = ValueSpy(publisher: sut.thresholdExceededPublisher)

    // when
    _ = try sut.getStorages()
    sut.warningDismissed()
    _ = try sut.getStorages()

    // then
    XCTAssertEqual(valueSpy.values, [86, nil, nil])
  }

  func testGetStoragesCallsThresholdExceededAgainWhenStateRestarted() throws {
    // given
    userStoragesProvider.storages = [.fixture(usedSpace: 86)]
    let valueSpy = ValueSpy(publisher: sut.thresholdExceededPublisher)

    // when
    _ = try sut.getStorages()
    userStoragesProvider.storages = [.fixture(usedSpace: 84)]
    _ = try sut.getStorages()
    userStoragesProvider.storages = [.fixture(usedSpace: 87)]
    _ = try sut.getStorages()

    // then
    XCTAssertEqual(valueSpy.values, [86, nil, 87])
  }
}
