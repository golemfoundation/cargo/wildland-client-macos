//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
@testable import WildlandCommon
import XCTest
import Combine

final class UpdaterControllerTests: XCTestCase {

  // MARK: - Properties

  private var sut: UpdaterController<ImmediateScheduler>!
  private var updaterType: UpdaterTypeMock.Type!
  private var bundle: BundleStub!
  private var userDriverMock: UserDriverMock!
  private var synchronizationStateProvider: SynchronizationStateProviderMock!
  private var versionChecker: VersionCheckerStub!
  private var fileWatcher: FileWatcherMock!
  private let fileWatcherQueue = DispatchQueue(label: "fileWatcherQueue")
  private lazy var event = FileWatcherEvent(FSEventStreamEventId(), bundle.bundlePath, FSEventStreamEventFlags())

  private let syncProgress = SynchronizationState.SyncProgress(
    files: ["examplePath.png"],
    currentSize: Measurement(value: 5.0, unit: .bytes),
    totalSize: Measurement(value: 10.0, unit: .bytes),
    estimate: Measurement(value: 0.0, unit: .seconds)
  )

  // MARK: - Setup

  override func setUp() {
    updaterType = UpdaterTypeMock.self
    bundle = BundleStub()
    bundle.expectedBundleURL = URL(filePath: "somePath/somePath/Cargo.app/somePath")
    userDriverMock = UserDriverMock()
    synchronizationStateProvider = SynchronizationStateProviderMock()
    versionChecker = VersionCheckerStub()
    fileWatcher = FileWatcherMock()
    sut = UpdaterController(
      updaterType: updaterType,
      bundle: bundle,
      userDriver: userDriverMock,
      synchronizationStateProvider: synchronizationStateProvider,
      fileWatcher: fileWatcher,
      versionChecker: versionChecker,
      debounceDueTime: .zero,
      queue: fileWatcherQueue,
      scheduler: .shared
    )
    super.setUp()
  }

  override func tearDown() {
    updaterType = nil
    bundle = nil
    userDriverMock = nil
    synchronizationStateProvider = nil
    versionChecker = nil
    fileWatcher = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testSetupAndStartOnInit() {
    XCTAssertNotNil(updaterType.updater?.hostBundleSet)
    XCTAssertNotNil(updaterType.updater?.applicationBundleSet)
    XCTAssertTrue(updaterType.updater?.userDriverSet === userDriverMock)
    XCTAssertNil(updaterType.updater?.delegateSet)
    XCTAssertEqual(updaterType.updater?.startCounter, 1)
  }

  func testCanCheckForUpdatesWhenUpdaterCanCheckForUpdates() {
    // given
    let valueSpy = ValueSpy(publisher: sut.canCheckForUpdatesPublisher)

    // when
    updaterType.updater?.callCanCheckForUpdates(true)

    // then
    XCTAssertEqual(valueSpy.values, [true])
  }

  func testCanInstallAndRelaunchCalledTrueWhenSyncedStatePublished() throws {
    // when
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(userDriverMock.canInstallAndRelaunchCounter, 1)
    XCTAssertTrue(try XCTUnwrap(userDriverMock.canInstallAndRelaunchSet))
  }

  func testCanInstallAndRelaunchCalledFalseWhenSyncingStatePublished() throws {
    // when
    synchronizationStateProvider.emit(state: .syncing(syncProgress))

    // then
    XCTAssertEqual(userDriverMock.canInstallAndRelaunchCounter, 1)
    XCTAssertFalse(try XCTUnwrap(userDriverMock.canInstallAndRelaunchSet))
  }

  func testAcknowledgementDismissed() {
    // when
    sut.acknowledgementDismissed()

    // then
    XCTAssertEqual(userDriverMock.acknowledgementDismissedCounter, 1)
  }

  func testCheckForUpdates() {
    // when
    sut.checkForUpdates()

    // then
    XCTAssertEqual(updaterType?.updater?.checkForUpdatesCounter, 1)
  }

  func testStartObservingPathsOnInit() {
    XCTAssertEqual(fileWatcher.startCounter, 1)
    XCTAssertEqual(fileWatcher.queueSet?.label, fileWatcherQueue.label)
    XCTAssertNotNil(fileWatcher.pathsSet)
  }

  func testFileWatcherCallbackSetOnInit() {
    XCTAssertNotNil(fileWatcher.callback)
  }

  func testNewAppVersionIdentifiedCalledWhenNewVersionCopied() {
    // given
    let valueSpy = ValueSpy(publisher: sut.newAppVersionIdentifiedPublisher)
    versionChecker.isVersionAvailable = true

    // when
    synchronizationStateProvider.emit(state: .synced)
    userDriverMock.callUpdateState(.dismiss)
    fileWatcher.call(event)

    // then
    XCTAssertEqual(valueSpy.values.count, 1)
  }

  func testNewAppVersionIdentifiedNotCalledWhenNewVersionItNotAvailable() {
    // given
    let valueSpy = ValueSpy(publisher: sut.newAppVersionIdentifiedPublisher)
    versionChecker.isVersionAvailable = false

    // when
    fileWatcher.call(event)
    synchronizationStateProvider.emit(state: .synced)
    userDriverMock.callUpdateState(.dismiss)

    // then
    XCTAssertEqual(valueSpy.values.count, .zero)
  }

  func testNewAppVersionIdentifiedNotCalledWhenSyncing() {
    // given
    let valueSpy = ValueSpy(publisher: sut.newAppVersionIdentifiedPublisher)
    versionChecker.isVersionAvailable = true

    // when
    synchronizationStateProvider.emit(state: .syncing(syncProgress))
    userDriverMock.callUpdateState(.dismiss)
    fileWatcher.call(event)

    // then
    XCTAssertEqual(valueSpy.values.count, .zero)
  }

  func testNewAppVersionIdentifiedCalledWhenSynced() {
    // given
    let valueSpy = ValueSpy(publisher: sut.newAppVersionIdentifiedPublisher)
    versionChecker.isVersionAvailable = true

    // when
    synchronizationStateProvider.emit(state: .syncing(syncProgress))
    userDriverMock.callUpdateState(.dismiss)
    fileWatcher.call(event)
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(valueSpy.values.count, 1)
  }
}
