//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import XCTest
import Sparkle
import Combine

final class UserDriverTests: XCTestCase {

  // MARK: - Properties

  private var sut: UserDriver!

  // MARK: - Setup

  override func setUp() {
    sut = UserDriver(decimalFormatter: .decimalCountFormatter)
    super.setUp()
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testShowReturnsUpdatePermissionResponse() async {
    // given
    let request = SPUUpdatePermissionRequest(systemProfile: [])

    // when
    let response = await sut.show(request)

    // then
    XCTAssertTrue(response.automaticUpdateChecks)
    XCTAssertNil(response.automaticUpdateDownloading)
    XCTAssertFalse(response.sendSystemProfile)
  }

  func testShowDownloadDidReceiveExpectedContentLengthCallsDownloadingUpdateState() {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)

    // when
    sut.showDownloadDidReceiveExpectedContentLength(1000)

    // then
    XCTAssertEqual(valueSpy.values, [.downloading(receivedFormattedBytes: "0 bytes", totalFormattedBytes: "1 KB")])
  }

  func testShowDownloadDidReceiveDataCallsDownloadingUpdateState() {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)
    let expected: [UpdateState] = [
      .downloading(receivedFormattedBytes: "0 bytes", totalFormattedBytes: "1 KB"),
      .downloading(receivedFormattedBytes: "100 bytes", totalFormattedBytes: "1 KB"),
      .downloading(receivedFormattedBytes: "150 bytes", totalFormattedBytes: "1 KB")
    ]

    // when
    sut.showDownloadDidReceiveExpectedContentLength(1000)
    sut.showDownloadDidReceiveData(ofLength: 100)
    sut.showDownloadDidReceiveData(ofLength: 50)

    // then
    XCTAssertEqual(valueSpy.values, expected)
  }

  func testShowDownloadDidStartExtractingUpdateCallsExtractingUpdateState() {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)

    // when
    sut.showDownloadDidStartExtractingUpdate()

    // then
    XCTAssertEqual(valueSpy.values, [.extracting(percent: .zero)])
  }

  func testShowExtractionReceivedProgressCallsExtractingUpdateState() {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)
    let expected: [UpdateState] = [
      .extracting(percent: 15),
      .extracting(percent: 32)
    ]

    // when
    sut.showExtractionReceivedProgress(0.15)
    sut.showExtractionReceivedProgress(0.32)

    // then
    XCTAssertEqual(valueSpy.values, expected)
  }

  func testAcknowledgementDismissedCalledWhenShowUpdateNotFoundWithError() {
    // given
    let expectation = XCTestExpectation()
    sut.showUpdateNotFoundWithError(ErrorStub.stub) {
      // then
      expectation.fulfill()
    }

    // when
    sut.acknowledgementDismissed()
    wait(for: [expectation], timeout: 0.5)
  }

  func testUpdateStateSubjectUpToDateCalledWhenShowUpdateNotFoundWithErrorUpToDate() {
    // given
    let upToDate = NSError(domain: "", code: 1001)
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)

    // when
    sut.showUpdateNotFoundWithError(upToDate) { }

    // then
    XCTAssertEqual(valueSpy.values, [.upToDate])
  }

  func testUpdateStateSubjectErrorCalledWhenShowUpdateNotFoundWithOtherErrorThenUpToDate() {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)
    let error = ErrorStub.stub

    // when
    sut.showUpdateNotFoundWithError(error) { }

    // then
    XCTAssertEqual(valueSpy.values, [.error(error)])
  }

  func testAcknowledgementDismissedCalledWhenShowUpdaterError() {
    // given
    let expectation = XCTestExpectation()
    sut.showUpdaterError(ErrorStub.stub) {
      // then
      expectation.fulfill()
    }

    // when
    sut.acknowledgementDismissed()
    wait(for: [expectation], timeout: 0.5)
  }

  func testUpdateStateSubjectErrorCalledWhenShowUpdateNotFoundWithError() {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)
    let error = ErrorStub.stub

    // when
    sut.showUpdaterError(error) { }

    // then
    XCTAssertEqual(valueSpy.values, [.error(error)])
  }

  func testShowReadyToInstallAndRelaunchReturnInstallWhenCanInstallAndRelaunchTrue() async {
    // given
    Just(true).subscribe(sut.canInstallAndRelaunchSubscriber)

    // when
    let result = await sut.showReadyToInstallAndRelaunch()

    // then
    XCTAssertEqual(result, .install)
  }

  func testUpdateStateDismissWhenCanInstallAndRelaunchTrue() async {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)
    Just(true).subscribe(sut.canInstallAndRelaunchSubscriber)

    // when
    _ = await sut.showReadyToInstallAndRelaunch()

    // then
    XCTAssertEqual(valueSpy.values, [.dismiss])
  }

  func testUpdateStateWaitForOperationsToComplateWhenCanInstallAndRelaunchFalse() async {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)
    Just(false).subscribe(sut.canInstallAndRelaunchSubscriber)

    // when
    _ = await sut.showReadyToInstallAndRelaunch()

    // then
    XCTAssertEqual(valueSpy.values, [.waitForOperationsToComplete])
  }

  func testShowInstallingUpdateCallsRetryTerminatingApplicationWhenApplicationTerminatedFalse() {
    // given
    let expectation = XCTestExpectation()

    // when
    sut.showInstallingUpdate(withApplicationTerminated: false, retryTerminatingApplication: {
      // then
      expectation.fulfill()
    })

    wait(for: [expectation], timeout: 0.5)
  }

  func testShowInstallingUpdateNotCallsRetryTerminatingApplicationWhenApplicationTerminatedTrue() {
    // given
    let expectation = XCTestExpectation()
    expectation.isInverted = true

    // when
    sut.showInstallingUpdate(withApplicationTerminated: true, retryTerminatingApplication: {
      // then
      expectation.fulfill()
    })

    wait(for: [expectation], timeout: 0.5)
  }

  func testShowUpdateInstalledAndRelaunched() {
    // given
    let expectation = XCTestExpectation()

    // when
    sut.showUpdateInstalledAndRelaunched(true) {
      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 0.5)
  }

  func testDismissUpdateInstallationCallsUpdateStateDismiss() {
    // given
    let valueSpy = ValueSpy(publisher: sut.updateStatePublisher)

    // when
    sut.dismissUpdateInstallation()

    // then
    XCTAssertEqual(valueSpy.values, [.dismiss])
  }
}
