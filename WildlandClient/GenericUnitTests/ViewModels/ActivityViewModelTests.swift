//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import Combine
@testable import CargoUI
@testable import WildlandCommon

final class ActivityViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: ActivityViewModel<ImmediateScheduler, ImmediateScheduler>!
  private var storagesProvider: StoragesProviderMock!
  private var pasteboard: PasteboardMock!
  private var userApi: PublicKeyProviderStub!
  private var synchronizationStateProvider: SynchronizationStateProviderMock!
  private var domainMounter: DomainMounterMock!
  private var activityMessageHandler: ActivityMessageHandlerMock!
  private let expectedStorageItems = [
    StorageItem(
      id: "1",
      title: "storage1",
      subtitle: "S3SC, 50 bytes of 100 bytes in use (50%)",
      usedBytes: Measurement<UnitInformationStorage>(value: 50, unit: .bytes),
      totalBytes: Measurement<UnitInformationStorage>(value: 100, unit: .bytes)
    ),
    StorageItem(
      id: "2",
      title: "storage2",
      subtitle: "S3SC, 10 bytes of 100 bytes in use (10%)",
      usedBytes: Measurement<UnitInformationStorage>(value: 10, unit: .bytes),
      totalBytes: Measurement<UnitInformationStorage>(value: 100, unit: .bytes)
    )
  ]
  private let storageRefreshingPeriod = 0.5
  private let expectedWarning = ActivityMessage(
    identifier: .storageAlmostFull,
    title: "Your storage is almost full",
    description: "You're using **86%** of your Cargo storage.\nYou might need to add more storage soon",
    isVisibleButton: true,
    type: .warning
  )
  private let updateMessage = ActivityMessage(
    identifier: .updateAvailable,
    title: "some title",
    description: "description",
    isVisibleButton: false,
    type: .update
  )

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    domainMounter = DomainMounterMock()
    synchronizationStateProvider = SynchronizationStateProviderMock()
    storagesProvider = StoragesProviderMock()
    storagesProvider.expectedStorages = [
      Storage(identifier: "1", type: "S3SC", name: "storage1", usedSpace: 50, totalSpace: 100),
      Storage(identifier: "2", type: "S3SC", name: "storage2", usedSpace: 10, totalSpace: 100)
    ]
    pasteboard = PasteboardMock()
    userApi = PublicKeyProviderStub(userPublicKey: "", publicKeyShort: "")
    activityMessageHandler = ActivityMessageHandlerMock()
    sut = ActivityViewModel(
      pasteboard: pasteboard,
      userApi: userApi,
      domainMounter: domainMounter,
      storagesProvider: storagesProvider,
      synchronizationStateProvider: synchronizationStateProvider,
      storageRefreshingPeriod: storageRefreshingPeriod,
      activityMessageHandler: activityMessageHandler,
      scheduler: .shared,
      fetchScheduler: .shared
    )
  }

  override func tearDown() {
    sut = nil
    storagesProvider = nil
    synchronizationStateProvider = nil
    userApi = nil
    pasteboard = nil
    activityMessageHandler = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testStoragesSetWhenSyncedData() {
    // given
    let spy = ValueSpy(publisher: sut.$storages)

    // when
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(spy.values, [[], expectedStorageItems])
  }

  func testStoragesSetWhenDomainMount() {
    // given
    let spy = ValueSpy(publisher: sut.$storages)

    // when
    domainMounter.callIsMountDomain()

    // then
    XCTAssertEqual(spy.values, [[], expectedStorageItems])
  }

  func testStoragesTimerCalledAfterStorageRefreshingPeriod() {
    expectation(
      publisher: sut.$storages,
      expectedCount: 2,
      when: { },
      then: { [weak self] values in
        XCTAssertEqual(values, [[], self?.expectedStorageItems])
      }
    )
  }

  func testCargoIsSyncSyncedTitle() {
    // given
    let spy = ValueSpy(publisher: sut.$synchronizationTitle)

    // when
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(spy.values, ["Cargo is synced", "Cargo is synced"])
  }

  func testSyncingTitleForSingleFile() {
    // given
    let spy = ValueSpy(publisher: sut.$synchronizationTitle)

    // when
    synchronizationStateProvider.emit(state: .syncing(progress()))

    // then
    XCTAssertEqual(spy.values, ["Cargo is synced", "Syncing"])
  }

  func testSyncStatusTitleForMultifiles() {
    // given
    let spy = ValueSpy(publisher: sut.$synchronizationTitle)

    // when
    synchronizationStateProvider.emit(state: .syncing(progress(files: ["file1", "file2"])))

    // then
    XCTAssertEqual(spy.values, ["Cargo is synced", "Syncing"])
  }

  func testSynchronizationFilesNilWhenSynced() {
    // given
    let spy = ValueSpy(publisher: sut.$synchronizationFiles)

    // when
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(spy.values, [nil, nil])
  }

  func testSynchronizationFilesSetWhenSyncingSingleFile() {
    // given
    let spy = ValueSpy(publisher: sut.$synchronizationFiles)

    // when
    synchronizationStateProvider.emit(state: .syncing(progress()))

    // then
    XCTAssertEqual(spy.values, [nil, "file1"])
  }

  func testSynchronizationFilesSetWhenSyncingMultifiles() {
    // given
    let spy = ValueSpy(publisher: sut.$synchronizationFiles)

    // when
    synchronizationStateProvider.emit(state: .syncing(progress(files: ["file1", "file2"])))

    // then
    XCTAssertEqual(spy.values, [nil, "multiple files"])
  }

  func testProgressZeroWhenSynced() {
    // given
    let spy = ValueSpy(publisher: sut.$progress)

    // when
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(spy.values, [.zero, .zero])
  }

  func testProgressSetWhenSyncing() {
    // given
    let spy = ValueSpy(publisher: sut.$progress)

    // when
    synchronizationStateProvider.emit(state: .syncing(progress()))

    // then
    XCTAssertEqual(spy.values, [.zero, 0.5])
  }

  func testMetricsNilWhenSynced() {
    // given
    let spy = ValueSpy(publisher: sut.$formattedMetrics)

    // when
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(spy.values, [nil, nil])
  }

  func testMetricsNilWhenSyncing() {
    // given
    let spy = ValueSpy(publisher: sut.$formattedMetrics)

    // when
    synchronizationStateProvider.emit(state: .syncing(progress()))

    // then
    XCTAssertEqual(spy.values, [nil, "10 bytes (10 secs)"])
  }

  func testOnHoverShow() {
    sut.updateCopyPopover(show: false)

    XCTAssertEqual(sut.copyPublicKeyPopoverState, .invisible)
  }

  func testOnHoverHide() {
    XCTAssertEqual(sut.copyPublicKeyPopoverState, .invisible)

    sut.updateCopyPopover(show: true)

    XCTAssertEqual(sut.copyPublicKeyPopoverState, .visible(WLStrings.ActivityView.tooltipPublicKeyCopy))
  }

  func testPopoverHide() {
    sut.copyPublicKey()

    sut.updateCopyPopover(show: false)

    XCTAssertEqual(sut.copyPublicKeyPopoverState, .invisible)
  }

  func testCopyPublicKey() {
    sut.copyPublicKey()

    XCTAssertEqual(sut.copyPublicKeyPopoverState, .visible(WLStrings.ActivityView.tooltipPublicKeyCopied))
  }

  func testStoredCorrectKeyCopy() {
    userApi.userPublicKey = "userPublicKey"

    let when: () -> Void = { [weak self] in
      self?.sut.copyPublicKey()
    }

    expectation(
      publisher: pasteboard.copiedStringPublisher,
      expectedCount: 1,
      when: when,
      then: { results in
        XCTAssertEqual(results.first, "userPublicKey")
      })
  }

  func testStoredEmptyKeyCopy() {
    let when: () -> Void = { [weak self] in
      self?.sut.copyPublicKey()
    }

    expectation(
      publisher: pasteboard.copiedStringPublisher,
      expectedCount: 1,
      when: when,
      then: { results in
        XCTAssertEqual(results.first, "")
      })
  }

  func testActivityMessageWhenActivityMessageEmittedByActivityMessageHandler() {
    // given
    let spy = ValueSpy(publisher: sut.$activityMessage)

    // when
    activityMessageHandler.callActivityMessage(expectedWarning)

    // then
    XCTAssertEqual(spy.values, [nil, expectedWarning])
  }

  func testWarningMessageDismissedCalledWhenClosePressed() {
    // when
    sut.messageClosePressed(message: expectedWarning)

    // then
    XCTAssertEqual(activityMessageHandler.messageClosePressedCounter, 1)
    XCTAssertEqual(activityMessageHandler.messageSet, expectedWarning)
  }

  func testDismissUpdate() {
    // when
    sut.messageClosePressed(message: updateMessage)

    // then
    XCTAssertEqual(activityMessageHandler.messageClosePressedCounter, 1)
    XCTAssertEqual(activityMessageHandler.messageSet, updateMessage)
  }

  func testUpdate() {
    // when
    sut.update()

    // then
    XCTAssertEqual(activityMessageHandler.updateCounter, 1)
  }

  func testUpdateStateEmittedByActivityMessageHandler() {
    // given
    let spy = ValueSpy(publisher: sut.$updateState)

    // when
    activityMessageHandler.callUpdateState(.newUpdateAvailable)

    // then
    XCTAssertEqual(spy.values, [.dismiss, .newUpdateAvailable])
  }

  // MARK: - Helpers

  private func progress(files: [String] = ["file1"]) -> SynchronizationState.SyncProgress {
    SynchronizationState.SyncProgress(
      files: files,
      currentSize: Measurement<UnitInformationStorage>(value: 5.0, unit: .bytes),
      totalSize: Measurement<UnitInformationStorage>(value: 10.0, unit: .bytes),
      estimate: Measurement<UnitDuration>(value: 10.0, unit: .minutes)
    )
  }
}
