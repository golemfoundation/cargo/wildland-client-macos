//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import wildlandx
@testable import CargoUI
@testable import WildlandCommon

@MainActor
final class GetVerificationCodeViewModelTests: XCTestCase {

  private var sut: GetVerificationCodeViewModel!
  private var userService: DaemonUserServiceMock!

  override func setUp() {
    super.setUp()
    userService = DaemonUserServiceMock()
    sut = GetVerificationCodeViewModel(userService: userService)
    sut.type = .freeFoundationTier
  }

  override func tearDown() {
    sut = nil
    userService = nil
    super.tearDown()
  }

  func testRequestFreeStorageCalledOnNextButtonClicked() async {
    // given
    sut.email = "valid@email.com"

    // when
    await sut.nextButtonClicked()

    // then
    XCTAssertEqual(userService.requestFreeStorageCounter, 1)
    XCTAssertEqual(userService.emailSet, "valid@email.com")
  }

  func testAlertNetworkConnectivityWhenNextButtonClickedAndRequestFreeStorageErrorNetworkConnectivityType() async {
    // given
    let exception = RustForFPEExceptionMock(with: .notAuthenticated, errorCategory: ErrorCategory_NetworkConnectivity)
    userService.error = WildlandError(exception)
    sut.email = "valid@email.com"

    // when
    await sut.nextButtonClicked()

    // then
    let expectedError = ViewActivityState.error(
      WLStrings.onboarding.generalError.networkError,
      imageType: .networkConnection,
      timeout: .seconds(3))
    XCTAssertEqual(sut.activityState, expectedError)
  }

  func testAlertNetworkConnectivityWhenNextButtonClickedAndRequestFreeStorageErrorDatabaseType() async {
    // given
    let exception = RustForFPEExceptionMock(with: .notAuthenticated, errorCategory: ErrorCategory_Database)
    userService.error = WildlandError(exception)
    sut.email = "valid@email.com"

    // when
    await sut.nextButtonClicked()

    // then
    let expectedError = ViewActivityState.error(
      WLStrings.onboarding.generalError.networkError,
      imageType: .networkConnection,
      timeout: .seconds(3))
    XCTAssertEqual(sut.activityState, expectedError)
  }

  func testAlertGenericErrorWhenNextButtonClickedAndRequestFreeStorageGenericError() async {
    // given
    userService.error = ErrorStub.stub
    sut.email = "valid@email.com"

    // when
    await sut.nextButtonClicked()

    // then
    XCTAssertEqual(sut.activityState, .error(WLStrings.Alert.errorGenericMessage))
  }
}
