//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
import Foundation
import XCTest
import Combine

@MainActor
final class PreferencesSceneModelTests: XCTestCase {

  private var sut: PreferencesSceneModel!
  private var userService: DaemonUserServiceMock!

  override func setUp() {
    userService = DaemonUserServiceMock()
    sut = PreferencesSceneModel(daemonUserService: userService, logger: .logger)
  }

  override func tearDown() {
    userService = nil
    sut = nil
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testUserOnboarded() async {
    // given
    let valueSpy = ValueSpy(publisher: sut.$displayPreferences)
    userService.availabilityState = .readyForUse

    // when
    await sut.fetchAvailabilityState()

    // then
    XCTAssertEqual(valueSpy.values, [false, true])
  }

  func testUserNotOnboarded() async {
    // given
    let valueSpy = ValueSpy(publisher: sut.$displayPreferences)
    userService.availabilityState = .storageNotGranted

    // when
    await sut.fetchAvailabilityState()

    // then
    XCTAssertEqual(valueSpy.values, [false, false])
  }
}
