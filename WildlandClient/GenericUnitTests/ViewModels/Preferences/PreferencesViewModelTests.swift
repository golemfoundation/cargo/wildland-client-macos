//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
import Combine
import XCTest

@MainActor
final class PreferencesViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: PreferencesViewModel<ImmediateScheduler>!
  private var publicKeyProvider: PublicKeyProviderStub!
  private var userService: DaemonUserServiceMock!
  private var storagesProvider: StoragesProviderMock!
  private lazy var generalViewModel = PreferencesGeneralViewModel<RunLoop>(
    publicKeyProvider: publicKeyProvider,
    userService: userService,
    scheduler: .main,
    logger: .logger
  )

  // MARK: - Setup

  override func setUp() {
    publicKeyProvider = PublicKeyProviderStub(
      userPublicKey: "examplePublicKey",
      publicKeyShort: "exampleShortPublicKey"
    )
    userService = DaemonUserServiceMock()
    storagesProvider = StoragesProviderMock()
    sut = PreferencesViewModel(
      scheduler: .shared,
      publicKeyProvider: publicKeyProvider,
      userService: userService,
      storagesProvider: storagesProvider,
      logger: .logger
    )
  }

  override func tearDown() {
    sut = nil
    publicKeyProvider = nil
    userService = nil
    storagesProvider = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testSelectedOptionOnInit() {
    // when
    let valueSpy = ValueSpy(publisher: sut.$selectedOption)

    // then
    XCTAssertEqual(valueSpy.values, [.general])
  }

  func testGeneralConentViewTypeOnInit() {
    // when
    let valueSpy = ValueSpy(publisher: sut.$contentViewType)

    // then
    XCTAssertEqual(valueSpy.values, [.general(generalViewModel)])
  }

  func testStoragesConentViewTypeWhenStorageSelectedOption() {
    // given
    let valueSpy = ValueSpy(publisher: sut.$contentViewType)
    let viewModel = PreferencesStorageViewModel(storagesProvider: storagesProvider, logger: .logger)

    // when
    sut.selectedOption = .storage

    // then
    XCTAssertEqual(valueSpy.values, [.general(generalViewModel), .storages(viewModel)])
  }

  func testDefaultOptions() {
    XCTAssertEqual(sut.options, PreferencesOptions.allCases)
  }
}

extension ContentViewType: Equatable {
  public typealias Content = ContentViewType<GeneralViewModel, StorageViewModel>
  public static func == (lhs: Content, rhs: Content) -> Bool {
    switch (lhs, rhs) {
    case (.general, .general):
      return true
    case (.storages, .storages):
      return true
    default:
      return false
    }
  }
}
