//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
import WildlandCommon
import XCTest
import Combine

@MainActor
final class PreferencesGeneralViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: PreferencesGeneralViewModel<ImmediateScheduler>!
  private var publicKeyProvider: PublicKeyProviderStub!
  private var userService: DaemonUserServiceMock!
  private var appServiceType: SMAppServiceMock.Type!
  private var appService: SMAppServiceMock!
  private var settings: SettingsMock!
  private var pasteboard: PasteboardMock!
  private var workspace: NSWorkspaceMock!
  private let rootPath = URL(filePath: "/Documents/Cargo")

  // MARK: - Setup

  override func setUp() {
    publicKeyProvider = PublicKeyProviderStub(
      userPublicKey: "examplePublicKey",
      publicKeyShort: "exampleShortPublicKey"
    )
    userService = DaemonUserServiceMock(expectedRootCargoUrl: rootPath)
    appService = SMAppServiceMock()
    appServiceType = SMAppServiceMock.self
    appServiceType.appService = appService
    settings = SettingsMock(
      startCargoOnLogin: true,
      appearanceColorScheme: .automatic
    )
    pasteboard = PasteboardMock()
    workspace = NSWorkspaceMock()
    sut = PreferencesGeneralViewModel(
      publicKeyProvider: publicKeyProvider,
      userService: userService,
      pasteboardProvider: pasteboard,
      workspace: workspace,
      appServiceType: appServiceType,
      settings: settings,
      scheduler: .shared,
      logger: .logger
    )
  }

  override func tearDown() {
    appServiceType.cleanup()
    appServiceType = nil
    appService = nil
    publicKeyProvider = nil
    workspace = nil
    userService = nil
    pasteboard = nil
    settings = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testSetupRootCargoUrlOnInit() {
    expectation(
      publisher: sut.$rootPath,
      expectedCount: 2,
      when: { },
      then: { [weak self] values in
        XCTAssertEqual(values, ["", self?.rootPath.path()])
      }
    )
  }

  func testLaunchCargoOnLoginSetTrueOnInit() {
    // when
    let valueSpy = ValueSpy(publisher: sut.$launchCargoOnLogin)

    // then
    XCTAssertEqual(valueSpy.values, [true])
  }

  func testSelectedItemSetAutomaticOnInit() {
    // given
    let expected = DropdownItem(id: "automatic", title: "Follow OS")

    // when
    let valueSpy = ValueSpy(publisher: sut.$selectedItem)

    // then
    XCTAssertEqual(valueSpy.values, [expected])
  }

  func testShortPublicKeyOnInit() {
    XCTAssertEqual(sut.publicKey, "exampleShortPublicKey")
  }

  func testLaunchCargoOnLoginTrueCallsRegister() {
    // when
    sut.launchCargoOnLogin = true

    // then
    XCTAssertEqual(appServiceType.loginItemCounter, 1)
    XCTAssertEqual(appServiceType.identifierSet, "io.wildland.Cargo")
    XCTAssertEqual(appService.registerCounter, 1)
  }

  func testLaunchCargoOnLoginTrueSetSettings() {
    // when
    sut.launchCargoOnLogin = true

    // then
    XCTAssertEqual(settings.updateStartCargoOnLoginCounter, 1)
    XCTAssertTrue(settings.enableSet ?? false)
  }

  func testLaunchCargoOnLoginFalseCallsUnregister() {
    // when
    sut.launchCargoOnLogin = false

    // then
    XCTAssertEqual(appServiceType.loginItemCounter, 1)
    XCTAssertEqual(appServiceType.identifierSet, "io.wildland.Cargo")
    XCTAssertEqual(appService.unregisterCounter, 1)
  }

  func testLaunchCargoOnLoginFalseSetSettings() {
    // when
    sut.launchCargoOnLogin = false

    // then
    XCTAssertEqual(settings.updateStartCargoOnLoginCounter, 1)
    XCTAssertFalse(settings.enableSet ?? true)
  }

  func testSelectedDarkItemUpdateColorScheme() {
    validateUpdateColorScheme(
      expectedOption: .dark,
      selectedItem: DropdownItem(id: "dark", title: "dark")
    )
  }

  func testSelectedLightItemUpdateColorScheme() {
    validateUpdateColorScheme(
      expectedOption: .light,
      selectedItem: DropdownItem(id: "light", title: "light")
    )
  }

  func testSelectedAutomaticItemUpdateColorScheme() {
    validateUpdateColorScheme(
      expectedOption: .automatic,
      selectedItem: DropdownItem(id: "automatic", title: "automatic")
    )
  }

  private func validateUpdateColorScheme(expectedOption: Settings.ColorThemeOption, selectedItem: DropdownItem) {
    // when
    sut.selectedItem = selectedItem

    // then
    XCTAssertEqual(settings.updateColorSchemeCounter, 1)
    XCTAssertEqual(settings.optionSet, expectedOption)
  }

  func testSelectedLightItemCallsUserService() {
    // given
    let expectation = XCTestExpectation(description: "Set theme was called")
    userService.setThemeExpectation = expectation

    // when
    sut.selectedItem = DropdownItem(id: "light", title: "light")

    // then
    wait(for: [expectation], timeout: 1.0)
    XCTAssertEqual(userService.setThemeCounter, 1)
    XCTAssertEqual(userService.themeOptionSet, .light)
  }

  func testCopyPublicKeyCopyToPasteboard() {
    let when: () -> Void = { [weak self] in
      self?.sut.copyPublicKey()
    }

    expectation(
      publisher: pasteboard.copiedStringPublisher,
      expectedCount: 1,
      when: when,
      then: { results in
        XCTAssertEqual(results, ["examplePublicKey"])
      })
  }

  func testCopyPublicKeySetPressedTooltipState() {
    // given
    let valueSpy = ValueSpy(publisher: sut.$publicKeyTooltipState)

    // when
    sut.copyPublicKey()

    // then
    XCTAssertEqual(valueSpy.values, [.dismiss, .pressed("Copied!")])
  }

  func testOnCopyButtonHoverFalseSetDismissTooltipState() {
    // given
    let valueSpy = ValueSpy(publisher: sut.$publicKeyTooltipState)

    // when
    sut.onCopyButtonHover(isHover: false)

    // then
    XCTAssertEqual(valueSpy.values, [.dismiss, .dismiss])
  }

  func testOnCopyButtonHoverTrueSetHoverTooltipState() {
    // given
    let valueSpy = ValueSpy(publisher: sut.$publicKeyTooltipState)

    // when
    sut.onCopyButtonHover(isHover: true)

    // then
    XCTAssertEqual(valueSpy.values, [.dismiss, .hover("Copy Public key")])
  }

  func testOpenFinderSelectNilFileWithRootUrl() {
    // when
    sut.openFinder()

    // then
    XCTAssertEqual(workspace.selectFileCounter, 1)
    XCTAssertNil(workspace.fullPathSet)
  }

  func testDeviceNameSetThenDisplayNameForSharingNotDisabled() {
    // given
    let spy = ValueSpy(publisher: sut.$displayNameForSharingDisabled)

    // when
    sut.displayName = "Test name"

    // then
    XCTAssertEqual(spy.values, [true, false])
  }

  func testDeviceNameSetThenDisplayNameForSharingDisabled() {
    // given
    let spy = ValueSpy(publisher: sut.$displayNameForSharingDisabled)

    // when
    sut.displayName = "Test name"
    sut.displayName = ""

    // then
    XCTAssertEqual(spy.values, [true, false, true])
  }

  func testDeviceNameSetEmptyThenShouldDisplayNameForSharingFalse() {
    // given
    let spy = ValueSpy(publisher: sut.$shouldDisplayNameForSharing)

    // when
    sut.shouldDisplayNameForSharing = true
    sut.displayName = ""

    // then
    XCTAssertEqual(spy.values, [false, true, false])
  }

  func testDeviceNameSave() {
    // when
    sut.displayName = "Test name"

    // then
    XCTAssertEqual(settings.displayName, "Test name")
  }

  func testDeviceNameSharing() {
    // when
    sut.shouldDisplayNameForSharing = true

    // then
    XCTAssertEqual(settings.displayNameForSharingCounter, 1)
    XCTAssertTrue(settings.displayNameForSharingSet ?? false)
  }
}
