//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
import XCTest

@MainActor
final class PreferencesStorageViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: PreferencesStorageViewModel!
  private var storagesProvider: StoragesProviderMock!

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    storagesProvider = StoragesProviderMock()
    sut = PreferencesStorageViewModel(
      storagesProvider: storagesProvider,
      configuration: .once,
      logger: .logger
    )
  }

  override func tearDown() {
    storagesProvider = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testIsLoadingWhenFetchData() async {
    // given
    let valueSpy = ValueSpy(publisher: sut.$isLoading)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(valueSpy.values, [false, true, false])
  }

  func testStoragesWhenDataFetched() async {
    // given

    let valueSpy = ValueSpy(publisher: sut.$storages)
    storagesProvider.expectedStorages = [.fixture(usedSpace: 86)]
    let expected = StorageItem(
      id: "1",
      title: "storage1",
      subtitle: "S3SC, 86 bytes of 100 bytes in use (86%)",
      usedBytes: Measurement<UnitInformationStorage>(value: 86, unit: .bytes),
      totalBytes: Measurement<UnitInformationStorage>(value: 100, unit: .bytes)
    )

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(valueSpy.values, [[], [expected]])
  }

  func testStoragesWhenFetchDataWithError() async {
    // given

    let valueSpy = ValueSpy(publisher: sut.$storages)
    storagesProvider.storagesError = ErrorStub.stub

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(valueSpy.values, [[], []])
  }
}
