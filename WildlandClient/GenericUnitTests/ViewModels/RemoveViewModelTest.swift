//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import XCTest
@testable import CargoUI

final class RemoveViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: RemoveViewModel!

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    sut = RemoveViewModel(publicKey: "public key", alias: "alias")
  }

  override func tearDown() {
    sut = nil
  }

  // MARK: - Tests

  func testShouldRemovePublisherWhenOnCancel() {
    // given
    let valueSpy = ValueSpy(publisher: sut.shouldRemovePublisher)

    // when
    sut.onCancel()

    // then
    XCTAssertEqual(valueSpy.values, [false])
  }

  func testShouldRemovePublisherWhenOnRemove() {
    // given
    let valueSpy = ValueSpy(publisher: sut.shouldRemovePublisher)

    // when
    sut.onRemove()

    // then
    XCTAssertEqual(valueSpy.values, [true])
  }

  func testIdentityInitializationAliasNil() {
    // given
    let alias: String? = nil
    let publicKey = "public key"

    // when
    sut = RemoveViewModel(publicKey: publicKey, alias: alias)

    // then
    XCTAssertEqual(sut.identity, publicKey)
  }

  func testIdentityInitializationAliasEmpty() {
    // given
    let alias: String? = ""
    let publicKey = "public key"

    // when
    sut = RemoveViewModel(publicKey: publicKey, alias: alias)

    // then
    XCTAssertEqual(sut.identity, publicKey)
  }

  func testIdentityInitializationAliasNotNil() {
    // given
    let alias = "alias"
    let publicKey = "public key"

    // when
    sut = RemoveViewModel(publicKey: publicKey, alias: alias)

    // then
    XCTAssertEqual(sut.identity, alias)
  }
}
