//
// Wildland Project
// SecureStorageTests.swift
// WildlandCommon
//
// Copyright © 2022 Golem Foundation
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import XCTest
import WildlandCommon
@testable import CargoUI

@MainActor
final class ContainerPropertiesViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: ContainerPropertiesViewModel!
  private var fileItemProvider: FileItemProviderMock!
  private var thumbnailProvider: ThumbnailProviderMock!
  private let fileItem = FileItem.fixture(
    identifier: "Mock identifier",
    parentIdentifier: "Parent identifier",
    modificationDate: Date(),
    size: 1234567
  )
  private let parentFileItem = FileItem.fixture(
    identifier: "Parent identifier",
    parentIdentifier: "Parent identifier",
    modificationDate: Date(),
    size: 3213112,
    type: .folder
  )

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    fileItemProvider = FileItemProviderMock()
    thumbnailProvider = ThumbnailProviderMock()
    sut = ContainerPropertiesViewModel(
      identifier: "Mock identifier",
      dependecies: ContainerPropertiesViewModelDependecies(
        pathResolvable: PathResolvableStub(),
        fileItemProvider: fileItemProvider,
        userNotifications: UserNotificationsMock(),
        thumbnailProvider: thumbnailProvider,
        storagesProvider: StoragesProviderMock()
      ),
      logger: .logger
    )
  }

  override func tearDown() {
    fileItemProvider = nil
    thumbnailProvider = nil
    sut = nil
    super.tearDown()
  }

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testIsLoadingSetWhenFetchContainerInformation() async {
    // given
    let spy = ValueSpy(publisher: sut.$isLoading)
    fileItemProvider.expectedFileItem = fileItem

    // when
    await sut.fetchContainerInformation()

    // then
    XCTAssertEqual(spy.values, [false, true, false])
  }

  func testContainerInfoSetWhenFetchContainerInformation() async {
    // given
    let spy = ValueSpy(publisher: sut.$containerInfo)
    let image = NSImage()
    fileItemProvider.expectedFileItem = fileItem
    thumbnailProvider.expectedImage = image
    let expected = ContainerInfo(
      name: "example name",
      properties: "1,2 MB",
      icon: image
    )

    // when
    await sut.fetchContainerInformation()

    // then
    XCTAssertEqual(spy.values, [.placeholder, expected])
  }

  func testContainerInfoSetForFolderTwoFilesWhenFetchContainerInformation() async {
    // given
    let spy = ValueSpy(publisher: sut.$containerInfo)
    let image = NSImage()
    fileItemProvider.expectedFileItem = parentFileItem
    fileItemProvider.expectedFileItems = [fileItem, fileItem]
    thumbnailProvider.expectedImage = image
    let expected = ContainerInfo(
      name: "example name",
      properties: "3,2 MB, 2 files",
      icon: image
    )

    // when
    await sut.fetchContainerInformation()

    // then
    XCTAssertEqual(spy.values, [.placeholder, expected])
  }

  func testContainerInfoSetForFolderOneFileWhenFetchContainerInformation() async {
    // given
    let spy = ValueSpy(publisher: sut.$containerInfo)
    let image = NSImage()
    fileItemProvider.expectedFileItem = parentFileItem
    fileItemProvider.expectedFileItems = [fileItem]
    thumbnailProvider.expectedImage = image
    let expected = ContainerInfo(
      name: "example name",
      properties: "3,2 MB, 1 file",
      icon: image
    )

    // when
    await sut.fetchContainerInformation()

    // then
    XCTAssertEqual(spy.values, [.placeholder, expected])
  }

  func testIsHeaderRedactedSetWhenFetchContainerInformationWithError() async {
    // given
    let spy = ValueSpy(publisher: sut.$isHeaderRedacting)

    // when
    await sut.fetchContainerInformation()

    // then
    XCTAssertEqual(spy.values, [false, true])
  }
}
