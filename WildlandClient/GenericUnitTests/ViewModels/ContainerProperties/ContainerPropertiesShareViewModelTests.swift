//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
@testable import CargoUI

@MainActor
final class ContainerPropertiesShareViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: ContainerPropertiesShareViewModel<RemoveViewModel>!
  private var userNotifications: UserNotificationsMock!
  private var pathResolver: PathResolvableStub!
  private var pasteboard: PasteboardMock!
  private var shareApi: ShareOwnerProviderStub!
  private var settings: SettingsMock!

  private let deeplinkUrl = "cargo-daemon://share?body=copied%20message"
  private let deeplinkUrlWithUserAlias = "cargo-daemon://share?body=copied%20message&userAlias=example%20name"

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    pathResolver = PathResolvableStub()
    pasteboard = PasteboardMock()
    shareApi = ShareOwnerProviderStub()
    userNotifications = UserNotificationsMock()
    settings = SettingsMock(startCargoOnLogin: true, appearanceColorScheme: .automatic)
    sut = ContainerPropertiesShareViewModel<RemoveViewModel>(
      identifier: "testIdentifier",
      pathResolvable: pathResolver,
      pasteboard: pasteboard,
      shareApi: shareApi,
      userNotifications: userNotifications,
      settings: settings,
      logger: .logger
    )
  }

  override func tearDown() {
    pathResolver = nil
    pasteboard = nil
    shareApi = nil
    userNotifications = nil
    settings = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testCopyLinkPressed() async {
    // given
    let spy = ValueSpy(publisher: pasteboard.copiedStringPublisher)
    shareApi.shareMessage = "copied message"

    // when
    await sut.copyLinkPressed()

    // then
    XCTAssertEqual(spy.values, [deeplinkUrl])
  }

  func testCopyLinkPressedWithUserAliasAndDisplayNameForSharingFalse() async {
    // given
    let spy = ValueSpy(publisher: pasteboard.copiedStringPublisher)
    shareApi.shareMessage = "copied message"
    settings.updateDisplayNameForSharing(false)
    settings.updateDisplayName("example name")

    // when
    await sut.copyLinkPressed()

    // then
    XCTAssertEqual(spy.values, [deeplinkUrl])
  }

  func testCopyLinkPressedWithUserAliasAndDisplayNameForSharingTrue() async {
    // given
    let spy = ValueSpy(publisher: pasteboard.copiedStringPublisher)
    shareApi.shareMessage = "copied message"
    settings.updateDisplayNameForSharing(true)
    settings.updateDisplayName("example name")

    // when
    await sut.copyLinkPressed()

    // then
    XCTAssertEqual(spy.values, [deeplinkUrlWithUserAlias])
  }

  func testCopyLinkPressedWithPathFailure() async {
    // given
    let spy = ValueSpy(publisher: pasteboard.copiedStringPublisher)
    pathResolver.throwPathError = true

    // when
    await sut.copyLinkPressed()

    // then
    XCTAssertEqual(spy.values, [])
  }

  func testCopyLinkPressedWithShareFailure() async {
    // given
    let spy = ValueSpy(publisher: pasteboard.copiedStringPublisher)
    shareApi.throwShareError = true

    // when
    await sut.copyLinkPressed()

    // then
    XCTAssertEqual(spy.values, [])
  }

  func testShowNotificationWhenCopyLinkPressed() async {
    // given
    shareApi.shareMessage = "some message"

    // when
    await sut.copyLinkPressed()

    // then
    XCTAssertEqual(userNotifications.showNotificationCounter, 1)
    XCTAssertEqual(userNotifications.categorySet, .shareLinkCopied)
  }

  func testCopyLinkPressedWithGetAllRecipientsFailure() async {
    // given
    shareApi.throwGetAllRecipientsError = true

    // when
    await sut.copyLinkPressed()

    // then
    XCTAssertEqual(sut.shareItems, [])
  }

  func testCopyLinkPressedGetAllRecipients() async {
    // given
    let expectedKeys = ["key1", "key2"]
    shareApi.publicKeys = expectedKeys

    // when
    await sut.copyLinkPressed()

    // then
    let items = expectedKeys.map {
      ContainerPropertiesShareItem(publicKey: $0, userDefaults: UserDefaultsMock())
    }
    XCTAssertEqual(sut.shareItems, items)
  }

  func testOnAppearGetAllRecipients() async {
    // given
    let expectedKeys = ["key1", "key2"]
    shareApi.publicKeys = expectedKeys

    // when
    await sut.onAppear()

    // then
    let items = expectedKeys.map {
      ContainerPropertiesShareItem(publicKey: $0, userDefaults: UserDefaultsMock())
    }
    XCTAssertEqual(sut.shareItems, items)
  }

  func testOnAppearGetAllRecipientsFailure() async {
    // given
    shareApi.throwGetAllRecipientsError = true

    // when
    await sut.onAppear()

    // then
    XCTAssertEqual(sut.shareItems, [])
  }
}
