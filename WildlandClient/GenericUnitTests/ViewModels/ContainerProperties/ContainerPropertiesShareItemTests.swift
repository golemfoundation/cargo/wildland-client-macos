//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import Combine
@testable import CargoUI

final class ContainerPropertiesShareItemTests: XCTestCase {

  // MARK: - Properties

  private var sut: ContainerPropertiesShareItem!
  private var userDefaultsMock: UserDefaultsMock!
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    userDefaultsMock = UserDefaultsMock()
    sut = ContainerPropertiesShareItem(publicKey: "test key", userDefaults: userDefaultsMock)
  }

  override func tearDown() {
    sut = nil
    userDefaultsMock = nil
    cancellables.removeAll()
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testSettingNameSavesToUserDefaults() {
    // given
    let spy = ValueSpy(publisher: sut.$name)
    let alias = "expected alias"

    // when
    sut.name = alias

    // then
    XCTAssertEqual(spy.values, ["", alias])
  }

  func testInitReadsFromUserDefaults() {
    // given
    let alias = "expected alias"
    let key = "existing key"

    // when
    userDefaultsMock.set(alias, forKey: key)
    let sut = ContainerPropertiesShareItem(
      publicKey: key,
      userDefaults: userDefaultsMock
    )

    // then
    XCTAssertEqual(sut.name, alias)
  }

  func testEditIfNeeded() {
    // given
    let key = "test key"
    let spy = ValueSpy(publisher: sut.publicKeyAddedPublisher)

    // when
    sut.editIfNeeded(publicKey: key)

    // then
    XCTAssertEqual(spy.values.count, 1)
  }

  func testDoNotEditIfNeeded() {
    // given
    let key = "test key 2"
    let spy = ValueSpy(publisher: sut.publicKeyAddedPublisher)

    // when
    sut.editIfNeeded(publicKey: key)

    // given
    XCTAssertEqual(spy.values.count, 0)
  }
}
