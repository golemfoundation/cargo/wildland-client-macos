//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import WildlandCommon
@testable import CargoUI

@MainActor
final class ContainerPropertiesStorageViewModelTests: XCTestCase {

  // MARK: - Properties

  private var sut: ContainerPropertiesStorageViewModel!
  private var storagesProvider: StoragesProviderMock!
  private var pathResolvable: PathResolvableStub!

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    storagesProvider = StoragesProviderMock()
    pathResolvable = PathResolvableStub()
    sut = ContainerPropertiesStorageViewModel(
      identifier: "identifier",
      storagesProvider: storagesProvider,
      pathResolvable: pathResolvable,
      configuration: .once,
      logger: .logger
    )
  }

  override func tearDown() {
    storagesProvider = nil
    pathResolvable = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testStorageItemsSetWhenFetchData() async {
    // given
    storagesProvider.expectedStorages = [.fixture(usedSpace: 41)]
    let spy = ValueSpy(publisher: sut.$storageItems)
    let expected = StorageItem(
      id: "1",
      title: "storage1",
      subtitle: "S3SC, 41 bytes of 100 bytes in use (41%)",
      usedBytes: Measurement<UnitInformationStorage>(value: 41, unit: .bytes),
      totalBytes: Measurement<UnitInformationStorage>(value: 100, unit: .bytes)
    )

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [.placeholder, [expected]])
  }

  func testStorageItemsEmptySetWhenFetchDataWithError() async {
    // given
    storagesProvider.storagesError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$storageItems)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [.placeholder, []])
  }

  func testPathSetWhenFetchData() async {
    // given
    storagesProvider.expectedStorages = [.fixture(usedSpace: 41)]
    pathResolvable.path = "/A/B/C"

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(storagesProvider.pathSet, "/A/B/C")
  }

  func testIsLoadingWhenFetchData() async {
    // given
    storagesProvider.expectedStorages = [.fixture(usedSpace: 41)]
    let spy = ValueSpy(publisher: sut.$isLoading)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [false, true, false])
  }

  func testIsLoadingWhenFetchDataWithError() async {
    // given
    storagesProvider.storagesError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$isLoading)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [false, true, false])
  }
}
