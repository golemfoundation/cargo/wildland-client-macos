//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
@testable import WildlandCommon
@testable import CargoUI
import FileProvider.NSFileProviderItem

@MainActor
final class ReceiveShareViewModelTests: XCTestCase {

  // MARK: - Properties

  private var shareReceiverProvider: ShareReceiverProviderMock!
  private var urlProvider: TemporaryUrlProviderMock!
  private var workspace: NSWorkspaceMock!
  private var thumbnailProvider: ThumbnailProviderMock!
  private var sut: ReceiveShareViewModel!

  private let previewMessage = PreviewShareMessage(
    fileName: "exampleFileName.jpg",
    size: 100000,
    publicKey: "examplePublicKey"
  )
  private let sharingMessage = SharingMessage(body: "example body", userAlias: "Bob")

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    shareReceiverProvider = ShareReceiverProviderMock()
    urlProvider = TemporaryUrlProviderMock()
    workspace = NSWorkspaceMock()
    thumbnailProvider = ThumbnailProviderMock()
    sut = ReceiveShareViewModel(
      sharingMessage: sharingMessage,
      shareReceiverProvider: shareReceiverProvider,
      urlProvider: urlProvider,
      workspace: workspace,
      thumbnailProvider: thumbnailProvider
    )
  }

  override func tearDown() {
    shareReceiverProvider = nil
    urlProvider = nil
    workspace = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testIsLoadingCalledWhenFetchData() async {
    // given
    let spy = ValueSpy(publisher: sut.$isLoading)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [false, true, false])
  }

  func testIsLoadingCalledWhenFetchDataWithError() async {
    // given
    shareReceiverProvider.prepareSharePreviewMessageError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$isLoading)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [false, true, false])
  }

  func testIsContentRedactingCalledWhenFetchData() async {
    // given
    shareReceiverProvider.previewMessage = previewMessage
    let spy = ValueSpy(publisher: sut.$isContentRedacting)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [true, true, false])
  }

  func testIsContentRedactingCalledWhenFetchDataWithError() async {
    // given
    shareReceiverProvider.prepareSharePreviewMessageError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$isContentRedacting)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [true, true])
  }

  func testInfoViewTypeCalledWhenFetchData() async {
    // given
    shareReceiverProvider.previewMessage = previewMessage
    let spy = ValueSpy(publisher: sut.$infoViewType)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [nil, nil])
  }

  func testInfoViewTypeCalledWhenFetchDataWithError() async {
    // given
    shareReceiverProvider.prepareSharePreviewMessageError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$infoViewType)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [nil, nil, .error(ErrorStub.stub.localizedDescription)])
  }

  func testShareInfoSetWhenFetchData() async {
    // given
    shareReceiverProvider.previewMessage = previewMessage
    let spy = ValueSpy(publisher: sut.$shareInfo)
    let someImage = NSImage()
    thumbnailProvider.expectedImage = someImage
    let expectedShareInfo = ShareInfo(
      senderName: "Bob",
      senderPublicKey: "examplePublicKey",
      sharedContentName: "exampleFileName",
      sharedContentSize: "100 KB",
      sharedContentType: "jpg",
      image: someImage
    )

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [.placeholder, expectedShareInfo])
  }

  func testHeaderTitleWhenFetchData() async throws {
    // given
    shareReceiverProvider.previewMessage = previewMessage

    var expectedAttributedString = AttributedString("Bob shared a file with you")
    let rangeOfBoldedText = try XCTUnwrap(expectedAttributedString.range(of: "Bob"))
    expectedAttributedString[rangeOfBoldedText].font = .displaySmall

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(sut.headerTitle, expectedAttributedString)
  }

  func testShareInfoNotSetWhenFetchDataWithError() async {
    // given
    shareReceiverProvider.prepareSharePreviewMessageError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$shareInfo)

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(spy.values, [.placeholder])
  }

  func testPrepareSharePreviewMessageCalledWhenFetchData() async {
    // given
    shareReceiverProvider.previewMessage = previewMessage

    // when
    await sut.fetchData()

    // then
    XCTAssertEqual(shareReceiverProvider.prepareSharePreviewMessageCounter, 1)
    XCTAssertEqual(shareReceiverProvider.sharingMessageSet, sharingMessage)
  }

  func testInfoViewTypeCalledWhenMainButtonPressed() async {
    // given
    urlProvider.userVisibleURL = URL(filePath: "/")
    let spy = ValueSpy(publisher: sut.$infoViewType)

    // when
    await sut.mainButtonPressed()

    // then
    XCTAssertEqual(spy.values, [nil, .loading("Adding to Cargo..."), .info("Added to Cargo")])
  }

  func testInfoViewTypeCalledWhenMainButtonPressedWithError() async {
    // given
    shareReceiverProvider.mountShareContainerError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$infoViewType)

    // when
    await sut.mainButtonPressed()

    // then
    XCTAssertEqual(spy.values, [nil, .loading("Adding to Cargo..."), .error(ErrorStub.stub.localizedDescription)])
  }

  func testInfoViewTypeCalledWhenMainButtonPressedWithUrlProviderError() async {
    // given
    urlProvider.getUserVisibleURLError = ErrorStub.stub
    let spy = ValueSpy(publisher: sut.$infoViewType)
    let expected: [InfoViewType?] = [
      nil,
      .loading("Adding to Cargo..."),
      .info("Added to Cargo"),
      .error(ErrorStub.stub.localizedDescription)
    ]

    // when
    await sut.mainButtonPressed()

    // then
    XCTAssertEqual(spy.values, expected)
  }

  func testMountShareContainerCalledWhenMainButtonPressed() async {
    // given
    urlProvider.userVisibleURL = URL(filePath: "/")

    // when
    await sut.mainButtonPressed()

    // then
    XCTAssertEqual(shareReceiverProvider.mountShareContainerCounter, 1)
    XCTAssertEqual(shareReceiverProvider.sharingMessageSet, sharingMessage)
    XCTAssertEqual(shareReceiverProvider.pathSet, "/Shared")
  }

  func testGetUserVisibleURLCalledWhenMainButtonPressed() async {
    // given
    urlProvider.userVisibleURL = URL(filePath: "/")

    // when
    await sut.mainButtonPressed()

    // then
    XCTAssertEqual(urlProvider.getUserVisibleURLCounter, 1)
    XCTAssertEqual(urlProvider.itemIdentifierSet, NSFileProviderItemIdentifier.rootContainer)
  }

  func testSelectFileCalledWhenMainButtonPressed() async {
    // given
    urlProvider.userVisibleURL = URL(filePath: "/")

    // when
    await sut.mainButtonPressed()

    // then
    XCTAssertEqual(workspace.selectFileCounter, 1)
    XCTAssertNil(workspace.fullPathSet)
    XCTAssertEqual(workspace.rootFullPathSet, "/Shared")
  }

  func testClosedActionCalledWhenMainButtonPressed() async {
    // given
    let spy = ValueSpy(publisher: sut.closePublisher)
    urlProvider.userVisibleURL = URL(filePath: "/")

    // when
    await sut.mainButtonPressed()

    // then
    XCTAssertEqual(spy.values.count, 1)
  }

  func testClosedActionCalledWhenCancelAction() async {
    // given
    let spy = ValueSpy(publisher: sut.closePublisher)
    urlProvider.userVisibleURL = URL(filePath: "/")

    // when
    sut.cancelAction()

    // then
    XCTAssertEqual(spy.values.count, 1)
  }
}
