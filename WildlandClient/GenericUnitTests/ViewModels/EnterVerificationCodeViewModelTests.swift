//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import wildlandx
@testable import CargoUI
@testable import WildlandCommon

@MainActor
final class EnterVerificationCodeViewModelTests: XCTestCase {

  private var sut: EnterVerificationCodeViewModel!
  private var userService: DaemonUserServiceMock!
  private var controlService: DaemonControlServiceMock!

  override func setUp() {
    super.setUp()
    userService = DaemonUserServiceMock()
    controlService = DaemonControlServiceMock()

    sut = EnterVerificationCodeViewModel(
      numberOfWords: 10,
      email: "example@email.com",
      userService: userService,
      controlService: controlService
    )
  }

  override func tearDown() {
    sut = nil
    userService = nil
    super.tearDown()
  }

  func testWordsAreSetOnInitialization() {
    // given
    let expected = Array(repeating: EnterVerificationCodeViewModel.Word(), count: 10)

    // then
    XCTAssertEqual(sut.words, expected)
  }

  func testCurrentSymbolIndexSetWhenWordsSet() {
    expectation(
      publisher: sut.$currentSymbolIndex,
      expectedCount: 2,
      when: { [weak self] in
        self?.fillWords(by: "ABC")
      },
      then: { results in
        XCTAssertEqual(results, [0, 3])
      }
    )
  }

  func testCurrentSymbolIndexSetOnVerificationCodeTextFieldAppeared() {
    expectation(
      publisher: sut.$currentSymbolIndex,
      expectedCount: 2,
      when: { [weak self] in
        self?.sut.verificationCodeTextFieldAppeared()
      },
      then: { results in
        XCTAssertEqual(results, [0, 0])
      }
    )
  }

  func testRequestFreeStorageCalledOnResendCodeClicked() async {
    // when
    await sut.resendCodeClicked()

    // then
    XCTAssertEqual(userService.requestFreeStorageCounter, 1)
    XCTAssertEqual(userService.emailSet, "example@email.com")
  }

  func testErrorLabelCodeWasResentOnResendCodeClicked() async {
    // when
    await sut.resendCodeClicked()

    // then
    XCTAssertEqual(sut.activityState, .success(WLStrings.onboarding.enterVerificationCode.infoResend))
  }

  func testIsEditableFieldWhenZeroWordsFilled() {
    // when
    fillWords(by: "")

    // then
    verifyEditableField(isEditableIndex: .zero)
  }

  func testIsEditableFieldWhenThreeWordsFilled() {
    // when
    fillWords(by: "abc")

    // then
    verifyEditableField(isEditableIndex: 3)
  }

  func testIsEditableFieldWhenAllWordsFilled() {
    // when
    fillWords(by: "abcdefghij")

    // then
    verifyEditableField(isEditableIndex: 9)
  }

  private func verifyEditableField(isEditableIndex: Int) {
    (0..<sut.words.count).forEach { index in
      let result = sut.isEditableField(at: index)
      if index == isEditableIndex {
        XCTAssertTrue(result)
      } else {
        XCTAssertFalse(result)
      }
    }
  }

  func testWordAt8DeletedWhenLastFieldEmpty() {
    // given
    fillWords(by: "abcdefghi")

    // when
    sut.deleted(at: 9, string: "")

    // then
    XCTAssertEqual(code, "ABCDEFGH")
  }

  func testWordAt9DeletedWhenLastFieldFilled() {
    // given
    fillWords(by: "abcdefghij")

    // when
    sut.deleted(at: 9, string: "j")

    // then
    XCTAssertEqual(code, "ABCDEFGHI")
  }

  func testIsVerifiedWhenFilledCorrectWords() {
    expectation(
      publisher: sut.$isVerified,
      expectedCount: 2,
      when: { [weak self] in
        self?.fillWords(by: "abcdefghij")
      },
      then: { [weak self] results in
        XCTAssertEqual(self?.userService.verifyEmailUsingCodeCounter, 1)
        XCTAssertEqual(self?.userService.codeSet, "ABCDEFGHIJ")
        XCTAssertEqual(results, [false, true])
      }
    )
  }

  func testActivityStateWhenFilledCorrectWords() {
    expectation(
      publisher: sut.$activityState,
      expectedCount: 6,
      when: { [weak self] in
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        let expected: [ViewActivityState] = [
          .idle,
          .idle,
          .loading(WLStrings.onboarding.enterVerificationCode.infoVerification),
          .success(WLStrings.onboarding.enterVerificationCode.infoVerified),
          .loading(WLStrings.onboarding.enterVerificationCode.mountingContainers),
          .idle
        ]
        XCTAssertEqual(results, expected)
      }
    )
  }

  func testCodeVerificationAfterFillingCode() {
    expectation(
      publisher: sut.$isVerified,
      expectedCount: 2,
      when: { [weak self] in
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        XCTAssertEqual(results, [false, true])
      }
    )
  }

  func testMountingAfterCodeVerified() {
    expectation(
      publisher: sut.$isCompleted,
      expectedCount: 2,
      when: { [weak self] in
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        XCTAssertEqual(results, [false, true])
      }
    )
  }

  func testActivityStateWhenFillWordsWithoutSufficientCharacters() {
    expectation(
      publisher: sut.$activityState,
      expectedCount: 1,
      when: { [weak self] in
        self?.fillWords(by: "abcdefghi")
      },
      then: { [weak self] results in
        XCTAssertEqual(self?.userService.verifyEmailUsingCodeCounter, .zero)
        XCTAssertNil(self?.userService.codeSet)
        XCTAssertEqual(results, [.idle])
      }
    )
  }

  func testActiveStateTypeVerifiedWhenFilledCorrectWords() {
    expectation(
      publisher: sut.$activityState,
      expectedCount: 3,
      when: { [weak self] in
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        let expected: [ViewActivityState] = [
          .idle,
          .idle,
          .loading(WLStrings.onboarding.enterVerificationCode.infoVerification)
        ]
        XCTAssertEqual(results, expected)
      }
    )
  }

  func testActiveStateInvalidCodeWhenFilledWordsAndVerifyEmailUsingCodeThrowsError() {
    expectation(
      publisher: sut.$activityState,
      expectedCount: 4,
      when: { [weak self] in
        let exception = RustExceptionMock(with: .notAuthenticated, errorCategory: ErrorCategory_Auth)
        self?.userService.error = WildlandError(exception)
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        let expected: [ViewActivityState] = [
          .idle,
          .idle,
          .loading(WLStrings.onboarding.enterVerificationCode.infoVerification),
          .error(WLStrings.onboarding.enterVerificationCode.infoVerificationFailed)
        ]
        XCTAssertEqual(results, expected)
      }
    )
  }

  func testIsVerifiedWhenRetryClickedAndVerifyEmailUsingCode() {
    expectation(
      publisher: sut.$isVerified,
      expectedCount: 3,
      when: { [weak self] in
        let exception = RustExceptionMock(with: .notAuthenticated, errorCategory: ErrorCategory_Auth)
        self?.userService.error = WildlandError(exception)
        self?.fillWords(by: "abcdefghij")
        self?.userService.error = nil
        self?.sut.retryClicked()
      },
      then: { results in
        XCTAssertEqual(results, [false, true, true])
      }
    )
  }

  func testActivityStateWhenFilledWordsAndVerifyEmailUsingCodeThrowsNetworkConnectivity() {
    expectation(
      publisher: sut.$activityState,
      expectedCount: 4,
      when: { [weak self] in
        let exception = RustExceptionMock(with: .notAuthenticated, errorCategory: ErrorCategory_NetworkConnectivity)
        self?.userService.error = WildlandError(exception)
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        XCTAssertEqual(
          results,
          [
            .idle,
            .idle,
            .loading(WLStrings.onboarding.enterVerificationCode.infoVerification),
            .error(WLStrings.onboarding.generalError.networkError, imageType: .networkConnection, timeout: .seconds(3))
          ]
        )
      }
    )
  }

  func testActivityStateWhenFilledWordsAndVerifyEmailUsingCodeThrowsDatabase() {
    expectation(
      publisher: sut.$activityState,
      expectedCount: 4,
      when: { [weak self] in
        let exception = RustExceptionMock(with: .notAuthenticated, errorCategory: ErrorCategory_Database)
        self?.userService.error = WildlandError(exception)
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        XCTAssertEqual(
          results,
          [
            .idle,
            .idle,
            .loading(WLStrings.onboarding.enterVerificationCode.infoVerification),
            .error(WLStrings.onboarding.generalError.networkError, imageType: .networkConnection, timeout: .seconds(3))
          ]
        )
      }
    )
  }

  func testVerifyEmailCodeCounterWhenRetryClickedAndVerifyEmailUsingCodeThrowsNetworkDatabase() {
    expectation(
      publisher: userService.$verifyEmailUsingCodeCounter,
      expectedCount: 3,
      when: { [weak self] in
        let exception = RustExceptionMock(with: .notAuthenticated, errorCategory: ErrorCategory_Database)
        self?.userService.error = WildlandError(exception)
        self?.fillWords(by: "abcdefghij")
        self?.sut.retryClicked()
      },
      then: { [weak self] results in
        XCTAssertEqual(self?.userService.codeSet, "ABCDEFGHIJ")
        XCTAssertEqual(results, [0, 1, 2])
      }
    )
  }

  func testActiveStateErrorWhenFillsWordsAndVerifyEmailUsingCodeGenericThrowsSomeError() {
    let message = WLStrings.Alert.errorGenericMessage
    expectation(
      publisher: sut.$activityState,
      expectedCount: 4,
      when: { [weak self] in
        self?.userService.error = ErrorStub.stub
        self?.fillWords(by: "abcdefghij")
      },
      then: { results in
        XCTAssertEqual(results, [
          .idle,
          .idle,
          .loading(WLStrings.onboarding.enterVerificationCode.infoVerification),
          .error(message)
        ])
      }
    )
  }

  func testWordsSetWhenPasteStringAndCorrectCode() {
    // when
    sut.pasteString(value: "ABC1234567")

    // then
    XCTAssertEqual(code, "ABC1234567")
  }

  func testWordsNotSetWhenPasteStringAndWrongCharacter() {
    // when
    sut.pasteString(value: "ABC123456@")

    // then
    XCTAssertEqual(code, "")
  }

  func testWordsNotSetWhenPasteStringAndWordsCountLessThen10() {
    // when
    sut.pasteString(value: "ABC123456")

    // then
    XCTAssertEqual(code, "")
  }

  // MARK: - Helpers
  private var code: String {
    sut.words.map { $0.value }.joined()
  }

  private func fillWords(by string: String) {
    sut.words = (0..<10).map { index in
      var words = ""
      if index < string.count {
        words = String(string[string.index(string.startIndex, offsetBy: index)])
      }
      return EnterVerificationCodeViewModel.Word(value: words)
    }
  }
}
