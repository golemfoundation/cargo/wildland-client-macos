//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
import XCTest
import Combine

@MainActor
final class ConfirmWildlandWordsViewModelTests: XCTestCase {

  private var viewModel: ConfirmWildlandWordsViewModel<ImmediateScheduler>!
  private var daemonUserService: DaemonUserServiceMock!
  private var phrases: [String]!
  // In the project, we limit the number of attempts to 3, but we check here that words are never repeated consecutively.
  // If it works for 10, then it also works for 3
  private let numberOfAttempts = 10
  private let defaultSelectedWord = "------"

  override func setUp() async throws {
    daemonUserService = DaemonUserServiceMock()

    let generatedMnemonic = try await daemonUserService.generateMnemonic()
    let mnemonicWords = generatedMnemonic.enumerated().map(MnemonicWord.init)

    phrases = generatedMnemonic
    viewModel = ConfirmWildlandWordsViewModel(
      scheduler: .shared,
      mnemonic: mnemonicWords,
      numberOfAttempts: numberOfAttempts,
      delayBetweenAnswer: .zero,
      service: daemonUserService
    )
  }

  override func tearDown() {
    daemonUserService = nil
    phrases = nil
    viewModel = nil
    super.tearDown()
  }

  func testDeinitSuccess() {
    // given
    weak var weakViewModel = viewModel

    // when
    viewModel = nil

    // then
    XCTAssertNil(weakViewModel)
  }

  func testWordClickedSuccessCallsSelectedWord() throws {
    // given
    let indexToConfirm = try XCTUnwrap(viewModel.indexToConfirm)
    let selectedIndex = indexToConfirm
    let valueSpy = ValueSpy(publisher: viewModel.$selectedWord)

    // when
    viewModel.wordClicked(indexToConfirm)

    // then
    XCTAssertEqual(valueSpy.values, [defaultSelectedWord, phrases[selectedIndex], defaultSelectedWord])
  }

  func testWordClickedSuccessCallsDisabled() throws {
    // given
    let indexToConfirm = try XCTUnwrap(viewModel.indexToConfirm)
    let valueSpy = ValueSpy(publisher: viewModel.$disabled)

    // when
    viewModel.wordClicked(indexToConfirm)

    // then
    XCTAssertEqual(valueSpy.values, [false, true, false])
  }

  func testWordClickedFailureCallsWrongSelectedIndex() throws {
    // given
    let indexToConfirm = try XCTUnwrap(viewModel.indexToConfirm)
    let wrappedWrongPhraseWithIndex = viewModel.shuffledPhrasesWithIndexes
      .filter { $0.index != indexToConfirm }
      .randomElement()
    let wrongPhraseWithIndex = try XCTUnwrap(wrappedWrongPhraseWithIndex)
    let indexOfSelectedButton = viewModel.shuffledPhrasesWithIndexes.firstIndex(where: { $0 == wrongPhraseWithIndex })
    let valueSpy = ValueSpy(publisher: viewModel.wrongSelectedIndexPublisher)

    // when
    viewModel.wordClicked(wrongPhraseWithIndex.index)

    // then
    XCTAssertEqual(valueSpy.values, [indexOfSelectedButton])
  }

  func testWordClickedFailureCallsSetDefaultStateOfSelectedWord() throws {
    // given
    let indexToConfirm = try XCTUnwrap(viewModel.indexToConfirm)
    let wrappedWrongPhraseWithIndex = viewModel.shuffledPhrasesWithIndexes
      .filter { $0.index != indexToConfirm }
      .randomElement()
    let wrongPhraseWithIndex = try XCTUnwrap(wrappedWrongPhraseWithIndex)
    let valueSpy = ValueSpy(publisher: viewModel.$selectedWord)

    // when
    viewModel.wordClicked(wrongPhraseWithIndex.index)

    // then
    XCTAssertEqual(valueSpy.values, [defaultSelectedWord, defaultSelectedWord])
  }

  func testAllAttemptsFulfilledCallsOnNext() throws {
    let when = { [weak self] in
      guard let self else { return }
      (0..<self.numberOfAttempts).forEach { _ in
        if let indexToConfirm = self.viewModel.indexToConfirm {
          self.viewModel.wordClicked(indexToConfirm)
        }
      }
    }

    expectation(
      publisher: viewModel.onNext.eraseToAnyPublisher(),
      expectedCount: 1,
      when: when,
      then: { results in
        XCTAssertEqual(results.count, 1)
      })
  }

  func testAllAttemptsFulfilledCallsCreateMnemonic() throws {
    let when = { [weak self] in
      guard let self else { return }
      (0..<self.numberOfAttempts).forEach { _ in
        if let indexToConfirm = self.viewModel.indexToConfirm {
          self.viewModel.wordClicked(indexToConfirm)
        }
      }
    }

    expectation(
      publisher: viewModel.onNext.eraseToAnyPublisher(),
      expectedCount: 1,
      when: when,
      then: { [weak self] _ in
        XCTAssertEqual(self?.daemonUserService.createMnemonicCounter, 1)
        XCTAssertEqual(self?.daemonUserService.wordsSet, self?.phrases)
      })
  }

  /// This test method checks whether there is any repetition in phrases, one after another.
  /// It does not mimic user actions, but rather tests the sequence of phrases in the `correctlySelectedWordPublisher` stream.
  func testIfNoRepeatsInPhrasesToConfirm() throws {
    // given
    let valueSpy = ValueSpy(publisher: viewModel.$selectedWord)

    // when
    (0..<numberOfAttempts).forEach { _ in
      if let indexToConfirm = self.viewModel.indexToConfirm {
        viewModel.wordClicked(indexToConfirm)
      }
    }

    // then
    var result = valueSpy.values
    result.removeAll(where: { $0 == defaultSelectedWord })
    XCTAssertEqual(Set(result).count, 9)
  }
}
