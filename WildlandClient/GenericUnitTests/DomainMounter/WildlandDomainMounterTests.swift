//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import XCTest
import WildlandCommon

final class WildlandDomainMounterTests: XCTestCase {

  // MARK: - Private

  private var sut: WildlandDomainMounter!
  private var fileProviderManager: FileProviderManagerMock.Type!
  private var userAvailabilityProvider: UserAvailabilityStub!
  private var containerProvider: ContainerProviderMock!

  // MARK: - Setup

  override func setUp() {
    fileProviderManager = FileProviderManagerMock.self
    userAvailabilityProvider = UserAvailabilityStub(availabilityState: .readyForUse)
    containerProvider = ContainerProviderMock()
    sut = WildlandDomainMounter(
      fileProviderManagerType: fileProviderManager,
      userAvailabilityProvider: userAvailabilityProvider,
      containerProvider: containerProvider
    )
    super.setUp()
  }

  override func tearDown() {
    fileProviderManager.cleanup()
    fileProviderManager = nil
    userAvailabilityProvider = nil
    containerProvider = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testMountIfAvailableTrueWhenReadyForUseAndErrorNil() {
    // given
    fileProviderManager.completionHandlerError = nil

    // when
    let when: () -> Void = { [weak self] in
      self?.sut.mountIfAvailable()
    }

    expectation(
      publisher: sut.isMountDomainPublisher,
      expectedCount: 1,
      when: when,
      then: { [weak self] results in
        XCTAssertEqual(results.count, 1)
        XCTAssertNil(self?.fileProviderManager.domainSet)
        XCTAssertEqual(self?.fileProviderManager.addCounter, 0)
        XCTAssertEqual(self?.containerProvider.mountAllContainersCounter, 0)
      }
    )
  }

  func testMountDomainTrueWhenStorageGrantedButNotMountedAndErrorNil() {
    // given
    userAvailabilityProvider.userAvailabilityState = .storageGrantedButNotMounted

    fileProviderManager.addDomainHandler = { [weak self] in
      self?.userAvailabilityProvider.userAvailabilityState = .readyForUse
    }

    // when
    let when: () -> Void = { [weak self] in
      self?.sut.mountDomain { error in
        XCTAssertNil(error)
      }
    }

    expectation(
      publisher: sut.isMountDomainPublisher,
      expectedCount: 1,
      when: when,
      then: { [weak self]  results in
        XCTAssertEqual(results.count, 1)
        XCTAssertEqual(self?.fileProviderManager.domainSet, fileProviderDomain)
        XCTAssertEqual(self?.fileProviderManager.addCounter, 1)
        XCTAssertEqual(self?.containerProvider.mountAllContainersCounter, 1)
      }
    )
  }

  func testMountIfAvailableFalseWhenReadyForUseAndError() {
    // given
    let expectedError = ErrorStub.stub
    userAvailabilityProvider.error = expectedError

    // when
    let when: () -> Void = { [weak self] in
      self?.sut.mountIfAvailable()
    }

    expectation(
      publisher: sut.isMountDomainPublisher,
      expectedCount: 0,
      when: when,
      then: { [weak self] results in
        XCTAssertEqual(results.count, 0)
        XCTAssertNil(self?.fileProviderManager.domainSet)
        XCTAssertEqual(self?.fileProviderManager.addCounter, 0)
        XCTAssertEqual(self?.containerProvider.mountAllContainersCounter, 0)
      },
      thenError: { error in
        XCTAssertEqual(WildlandError(expectedError).localizedDescription, error?.localizedDescription)
      }
    )
  }

  func testMountDomainFalseWhenStorageSetAndError() {
    // given
    let valueSpy = ValueSpy(publisher: sut.isMountDomainPublisher)
    fileProviderManager.completionHandlerError = ErrorStub.stub
    userAvailabilityProvider.userAvailabilityState = .storageGrantedButNotMounted
    let expectation = XCTestExpectation()

    // when
    sut.mountDomain { error in
      XCTAssertNotNil(error)
      expectation.fulfill()
    }

    // then
    wait(for: [expectation], timeout: 0.5)
    XCTAssertEqual(valueSpy.values.count, 0)
    XCTAssertEqual(fileProviderManager.domainSet, fileProviderDomain)
    XCTAssertEqual(fileProviderManager.addCounter, 1)
    XCTAssertEqual(containerProvider.mountAllContainersCounter, 1)
  }

  func testMountIfAvailableWhenStorageNotGranted() {
    // given
    userAvailabilityProvider.userAvailabilityState = .storageNotGranted

    // when
    let when: () -> Void = { [weak self] in
      self?.sut.mountIfAvailable()
    }

    expectation(
      publisher: sut.isMountDomainPublisher,
      expectedCount: 0,
      when: when,
      then: { results in
        XCTAssertEqual(results.count, 0)
        XCTAssertNil(self.fileProviderManager.domainSet)
        XCTAssertEqual(self.fileProviderManager.addCounter, .zero)
        XCTAssertEqual(self.containerProvider.mountAllContainersCounter, .zero)
      }
    )
  }

  func testMountDomainFalseWhenStorageNotGranted() {
    // given
    let valueSpy = ValueSpy(publisher: sut.isMountDomainPublisher)
    let expectation = XCTestExpectation()
    userAvailabilityProvider.userAvailabilityState =  .storageNotGranted

    // when
    sut.mountDomain { error in
      XCTAssertNil(error)
      expectation.fulfill()
    }

    // then
    wait(for: [expectation], timeout: 0.5)
    XCTAssertEqual(valueSpy.values.count, 0)
    XCTAssertNil(fileProviderManager.domainSet)
    XCTAssertEqual(fileProviderManager.addCounter, .zero)
    XCTAssertEqual(containerProvider.mountAllContainersCounter, .zero)
  }

  func testMountIfAvailableWhenError() {
    // given
    let valueSpy = ValueSpy(publisher: sut.isMountDomainPublisher)
    let expectation = XCTestExpectation()
    userAvailabilityProvider.error = ErrorStub.stub

    // when
    sut.mountDomain { error in
      XCTAssertNotNil(error)
      expectation.fulfill()
    }
    // then
    wait(for: [expectation], timeout: 0.5)
    XCTAssertEqual(valueSpy.values.count, 0)
    XCTAssertNil(fileProviderManager.domainSet)
    XCTAssertEqual(fileProviderManager.addCounter, .zero)
    XCTAssertEqual(containerProvider.mountAllContainersCounter, .zero)
  }
}
