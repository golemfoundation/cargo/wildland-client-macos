//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
import XCTest
import WildlandCommon

final class AboutPanelProviderTests: XCTestCase {

  // MARK: - Properties

  private var bundle: BundleStub!
  private var application: ApplicationMock!

  // MARK: - Setup

  override func setUp() {
    bundle = BundleStub()
    bundle.informations = [
      "ClientCommitHash": "1234567890",
      "ClientBuildTimestamp": "2023-04-06T15:35:17Z",
      "SDKVersion": "e5a02495",
      "SDKCommitHash": "0987654321",
      "CFBundleName": "Example name",
      "CFBundleShortVersionString": "0.1.599",
      "CFBundleVersion": "599"
    ]
    application = ApplicationMock()
    super.setUp()
  }

  override func tearDown() {
    bundle = nil
    application = nil
    super.tearDown()
  }

  private var expectedCredits: NSAttributedString {
    let aboutAppInformation = """
    Commit: 1234567
    Build time: 2023-04-06T15:35:17Z
    SDK version: e5a02495
    SDK commit: 0987654
    """
    return NSAttributedString(
      string: aboutAppInformation,
      attributes: [
        .paragraphStyle: NSParagraphStyle.paragraph(alignment: .center),
        .font: NSFont.textSRegular
      ]
    )
  }

  private var expectedVersions: [NSApplication.AboutPanelOptionKey: String] {
    [NSApplication.AboutPanelOptionKey.applicationVersion: "0.1.599", NSApplication.AboutPanelOptionKey.version: ""]
  }

  // MARK: - Tests

  func testShowAboutPanelCallsOrderFrontStandardAboutPanel() {
    // given
    let sut = AboutPanelProvider(bundle: bundle, application: application)
    let appInfo = AppInfo(bundle: bundle)

    // when
    sut.showAboutPanel(appInfo: appInfo)

    // then
    XCTAssertEqual(application.orderFrontStandardAboutPanelCounter, 1)

    let creditsResult = application.optionsDictionarySet?[NSApplication.AboutPanelOptionKey.credits]
    XCTAssertEqual(creditsResult as? NSAttributedString, expectedCredits)

    let copyrightKey = NSApplication.AboutPanelOptionKey(rawValue: "Copyright")
    let copyrightResult = application.optionsDictionarySet?[copyrightKey]
    XCTAssertEqual(copyrightResult as? String, "© 2023 Golem Foundation")

    let versionResult = application.optionsDictionarySet?[NSApplication.AboutPanelOptionKey.version]
    let versionExpected = expectedVersions[NSApplication.AboutPanelOptionKey.version]
    XCTAssertEqual(versionResult as? String, versionExpected)

    let appVersionResult = application.optionsDictionarySet?[NSApplication.AboutPanelOptionKey.applicationVersion]
    let appVersionExpected = expectedVersions[NSApplication.AboutPanelOptionKey.applicationVersion]
    XCTAssertEqual(appVersionResult as? String, appVersionExpected)
  }
}
