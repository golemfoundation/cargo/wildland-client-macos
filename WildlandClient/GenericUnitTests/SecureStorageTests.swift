//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import Foundation
import WildlandCommon
import wildlandx

final class SecureKeyStorageTests: XCTestCase {

  private let key = "aKey"
  private let value = "aValue"

  private var secureKeyStorage: SecureKeychainAccessKeyStorage!

  override func setUp() {
    let storage = SecureKeychainAccessKeyStorage.KeychainType(
      service: Defaults.BundleID.mainApp,
      accessGroup: Defaults.applicationGroupID
    )
      .synchronizable(false)
      .accessibility(.afterFirstUnlockThisDeviceOnly)
    secureKeyStorage = SecureKeychainAccessKeyStorage(storage: storage)
  }

  override func tearDown() {
    secureKeyStorage.clearAllKeys()
  }

  // MARK: - Test storage

  func testLSSInsert() {
    _ = secureKeyStorage.insert(RustString(key), RustString(value))
    let keyExists = secureKeyStorage.containsKey(RustString(key)).unwrap()
    XCTAssertTrue(keyExists, "Key should exist in the storage")
  }

  func testLSSGet() {
    _ = secureKeyStorage.insert(RustString(key), RustString(value))
    let valueIsEqual = secureKeyStorage.get(RustString(key)).unwrap().swiftOptional()?.toString() == value
    XCTAssertTrue(valueIsEqual, "Value must be equal to the stored one")
  }

  func testLSSRemove() {
    _ = secureKeyStorage.insert(RustString(key), RustString(value))
    let keyExists = secureKeyStorage.containsKey(RustString(key)).unwrap()
    XCTAssertTrue(keyExists, "Key should exist in the storage")
    _ = secureKeyStorage.remove(RustString(key))
    let keyNotExists = secureKeyStorage.containsKey(RustString(key)).unwrap()
    XCTAssertFalse(keyNotExists)
  }

  func testLSSClear() {
    _ = secureKeyStorage.insert(RustString(key), RustString(value))
    let keyExists = secureKeyStorage.containsKey(RustString(key)).unwrap()
    XCTAssertTrue(keyExists, "Key should exist in the storage")
    secureKeyStorage.clearAllKeys()
    XCTAssertTrue(secureKeyStorage.allKeys.count == 0, "Storage must be empty")
  }

  func testLSSGetAll() {
    XCTAssertTrue(secureKeyStorage.allKeys.count == 0, "Storage must be empty before the test")
    for i in 0..<10 {
      let index = "\(i)"
      let itemKey = key + index
      let itemValue = value + index
      _ = secureKeyStorage.insert(RustString(itemKey), RustString(itemValue))
      let keyExists = secureKeyStorage.containsKey(RustString(itemKey)).unwrap()
      XCTAssertTrue(keyExists, "Key should exist in the storage")
    }
    XCTAssertTrue(secureKeyStorage.allKeys.count == 10, "Failed to store all the keys")
  }
}
