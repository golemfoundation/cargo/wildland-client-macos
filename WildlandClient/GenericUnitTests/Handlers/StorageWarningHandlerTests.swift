//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import Combine
import CargoUI
import XCTest

final class StorageWarningHandlerTests: XCTestCase {
  private var sut: StorageWarningHandler<ImmediateScheduler>!

  override func setUp() {
    super.setUp()
    sut = StorageWarningHandler(scheduler: .shared)
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  func testDeinitSuccess() {
    // given
    weak var weakSut = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakSut)
  }

  func testErrorObserver() {
    // given
    let errorPublisher = Just((error: ErrorStub.stub, filename: "filename") as WarningHandler.Content?)
      .eraseToAnyPublisher()

    // when
    XCTAssertFalse(sut.presenter.isPresented.wrappedValue)

    errorPublisher
      .compactMap({ $0 })
      .receive(subscriber: sut.errorSubscriber)

    // then
    XCTAssertTrue(sut.presenter.isPresented.wrappedValue)
  }

  func testDismissPublisher() {
    let when = { [weak self] in
      guard let self else { return }
      self.sut.presenter.isPresented.wrappedValue = true
    }

    expectation(
      publisher: sut.dismissPublisher,
      expectedCount: 1,
      when: when,
      then: { [weak self] results in
        XCTAssertEqual(results.count, 1)
        XCTAssertEqual(self?.sut.presenter.isPresented.wrappedValue, false)
      }
    )
  }
}
