//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
@testable import WildlandCommon
import XCTest
import Combine
import wildlandx
import FileProvider

final class StatusBarIconHandlerTests: XCTestCase {

  // MARK: - Properties

  private var sut: StatusBarIconHandler<ImmediateScheduler>!
  private var storagesProvider: StoragesProviderMock!
  private var synchronizationStateProvider: SynchronizationStateProviderMock!
  private var popover: PopoverMock!
  private var updaterController: UpdaterControllerMock!
  private let syncProgress = SynchronizationState.SyncProgress(
    files: ["examplePath.png"],
    currentSize: Measurement(value: 5.0, unit: .bytes),
    totalSize: Measurement(value: 10.0, unit: .bytes),
    estimate: Measurement(value: 0.0, unit: .seconds)
  )

  // MARK: - Setup

  override func setUp() {
    storagesProvider = StoragesProviderMock()
    synchronizationStateProvider = SynchronizationStateProviderMock()
    popover = PopoverMock()
    updaterController = UpdaterControllerMock()
    sut = StatusBarIconHandler(
      storagesProvider: storagesProvider,
      synchronizationStateProvider: synchronizationStateProvider,
      updaterController: updaterController,
      scheduler: .shared
    )
    sut.popover = popover
    super.setUp()
  }

  override func tearDown() {
    storagesProvider = nil
    synchronizationStateProvider = nil
    updaterController = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testSyncingIconWhenErrorNilThresholdFalseAndSyncing() {
    // when
    updaterController.callUpdateStateSubject(.dismiss)
    Just(nil).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(nil)
    synchronizationStateProvider.emit(state: .syncing(syncProgress))

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.inProgress.icon)
  }

  func testSyncedIconWhenErrorNilThresholdFalseAndSynchronized() {
    // when
    updaterController.callUpdateStateSubject(.dismiss)
    Just(nil).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(nil)
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.synchronized.icon)
  }

  func testErrorIconWhenErrorNilThresholdTrueAndSynchronized() {
    // when
    updaterController.callUpdateStateSubject(.dismiss)
    Just(nil).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(87)
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.error.icon)
  }

  func testSyncedIconWhenErrorDifferentThenInsufficientQuotaThresholdFalseAndSynchronized() {
    // when
    updaterController.callUpdateStateSubject(.dismiss)
    Just(ErrorStub.stub).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(nil)
    synchronizationStateProvider.emit(state: .synced)

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.synchronized.icon)
  }

  func testErrorIconWhenErrorInsufficientQuotaThresholdFalseAndSyncing() {
    // given
    let exception = NSError(domain: "Mock domain", code: NSFileProviderError.insufficientQuota.rawValue)
    let error = WildlandError(exception)

    // when
    updaterController.callUpdateStateSubject(.dismiss)
    Just(error).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(nil)
    synchronizationStateProvider.emit(state: .syncing(syncProgress))

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.error.icon)
  }

  func testErrorIconWhenErrorUpdateApp() {
    // when
    Just(nil).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(nil)
    synchronizationStateProvider.emit(state: .synced)
    updaterController.callUpdateStateSubject(.error(ErrorStub.stub))

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.error.icon)
  }

  func testSyncingIconWhenDownloadingUpdate() {
    // when
    Just(nil).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(nil)
    synchronizationStateProvider.emit(state: .synced)
    updaterController.callUpdateStateSubject(.downloading(receivedFormattedBytes: "", totalFormattedBytes: ""))

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.inProgress.icon)
  }

  func testSyncingIconWhenExtractingUpdate() {
    // when
    Just(nil).subscribe(sut.errorSubscriber)
    storagesProvider.sendThresholdExceededEvent(nil)
    synchronizationStateProvider.emit(state: .synced)
    updaterController.callUpdateStateSubject(.extracting(percent: 24))

    // then
    XCTAssertEqual(popover.statusBarIconImage, StatusAppIcon.inProgress.icon)
  }
}
