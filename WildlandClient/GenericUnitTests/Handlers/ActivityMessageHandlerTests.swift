//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
@testable import WildlandCommon
import Combine
import XCTest

final class ActivityMessageHandlerTests: XCTestCase {

  // MARK: - Properties

  private var sut: ActivityMessageHandler!
  private var updaterController: UpdaterControllerMock!
  private var storagesProvider: StoragesProviderMock!
  private var userNotifications: UserNotificationsMock!

  private let expectedWarning = ActivityMessage(
    identifier: .storageAlmostFull,
    title: "Your storage is almost full",
    description: "You're using **86%** of your Cargo storage.\nYou might need to add more storage soon",
    isVisibleButton: true,
    type: .warning
  )

  private let expectedUpdateError = ActivityMessage(
    identifier: .updateError,
    title: WLStrings.ActivityView.updateErrorTitle,
    description: ErrorStub.stub.localizedDescription,
    isVisibleButton: true,
    type: .warning
  )

  private let updateMessage = ActivityMessage(
    identifier: .updateAvailable,
    title: "update title",
    description: "update description",
    isVisibleButton: false,
    type: .update
  )

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    storagesProvider = StoragesProviderMock()
    updaterController = UpdaterControllerMock()
    userNotifications = UserNotificationsMock()
    sut = ActivityMessageHandler(
      storagesProvider: storagesProvider,
      updaterController: updaterController,
      userNotifications: userNotifications
    )
  }

  override func tearDown() {
    storagesProvider = nil
    updaterController = nil
    userNotifications = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakSut = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakSut)
  }

  func testUpdateCallsInstall() {
    // when
    sut.update()

    // then
    XCTAssertEqual(updaterController.updateUserChoiceSet, .install)
    XCTAssertEqual(updaterController.updateUserChoiceSubjectCounter, 1)
  }

  func testDimissUpdateCallsDismiss() {
    // when
    sut.messageClosePressed(message: updateMessage)

    // then
    XCTAssertEqual(updaterController.updateUserChoiceSet, .dismiss)
    XCTAssertEqual(updaterController.updateUserChoiceSubjectCounter, 1)
  }

  func testUpdateStatePublisherWhenUpdateState() {
    // given
    let spy = ValueSpy(publisher: sut.updateStatePublisher)

    // when
    updaterController.callUpdateStateSubject(.newUpdateAvailable)

    // then
    XCTAssertEqual(spy.values, [.dismiss, .newUpdateAvailable])
  }

  func testWarningMessageShowedWhenPercentUsageNotNilAndUpdateErrorNil() {
    // given
    let spy = ValueSpy(publisher: sut.activityMessagePublisher)

    // when
    storagesProvider.sendThresholdExceededEvent(86)

    // then
    XCTAssertEqual(spy.values, [expectedWarning])
  }

  func testUpdateErrorMessageShowedWhenUpdateStateErrorInsteadWarrningMessage() {
    // given
    let spy = ValueSpy(publisher: sut.activityMessagePublisher)

    // when
    storagesProvider.sendThresholdExceededEvent(86)
    updaterController.callUpdateStateSubject(.error(ErrorStub.stub))
    storagesProvider.sendThresholdExceededEvent(86)

    // then
    XCTAssertEqual(spy.values, [expectedWarning, expectedUpdateError, expectedUpdateError])
  }

  func testWarningMessageDismissedCalledWhenClosePressed() {
    // when
    sut.messageClosePressed(message: expectedWarning)

    // then
    XCTAssertEqual(storagesProvider.warningDismissedCounter, 1)
  }

  func testAcknowledgementDismissedCalledWhenClosePressed() {
    // when
    sut.messageClosePressed(message: expectedUpdateError)

    // then
    XCTAssertEqual(updaterController.acknowledgementDismissedCounter, 1)
  }

  func testActivityMessageShowedAgainWhenStorageUsedDroppedBelow85PercentAndBumpAgain() {
    // given
    let spy = ValueSpy(publisher: sut.activityMessagePublisher)

    // when
    storagesProvider.sendThresholdExceededEvent(86)
    storagesProvider.sendThresholdExceededEvent(nil)
    storagesProvider.sendThresholdExceededEvent(86)

    // then
    XCTAssertEqual(spy.values, [expectedWarning, nil, expectedWarning])
  }

  func testShowNotificationWhenUpdateStateNewUpdateAvailable() {
    // when
    updaterController.callUpdateStateSubject(.newUpdateAvailable)

    // then
    XCTAssertEqual(userNotifications.showNotificationCounter, 1)
    XCTAssertEqual(userNotifications.categorySet, .appUpdate)
  }

  func testShowNotificationNotCalledWhenUpdateStateOtherThenNewUpdateAvailable() {
    // when
    updaterController.callUpdateStateSubject(.upToDate)

    // then
    XCTAssertEqual(userNotifications.showNotificationCounter, 0)
    XCTAssertNil(userNotifications.categorySet)
  }

  func testUpdateUserChoiceCalledWithInstallWhenNotificationResponseApproved() {
    // given
    let response = UserNotificationResponse(category: .appUpdate, action: .approved)

    // when
    userNotifications.callNotificationResponse(response)

    // then
    XCTAssertEqual(updaterController.updateUserChoiceSubjectCounter, 1)
    XCTAssertEqual(updaterController.updateUserChoiceSet, .install)
  }

  func testUpdateUserChoiceCalledWithInstallWhenNotificationResponseDeclined() {
    // given
    let response = UserNotificationResponse(category: .appUpdate, action: .declined)

    // when
    userNotifications.callNotificationResponse(response)

    // then
    XCTAssertEqual(updaterController.updateUserChoiceSubjectCounter, 1)
    XCTAssertEqual(updaterController.updateUserChoiceSet, .dismiss)
  }
}
