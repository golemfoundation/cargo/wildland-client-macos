//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import XCTest

final class FileProviderActionTests: XCTestCase {

  // MARK: - Properties

  private var sut: FileProviderAction!

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    sut = FileProviderAction()
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testPerformActionContainerPropertiesCallsShowContainerProperties() {
    // given
    let spy = ValueSpy(publisher: sut.showContainerPropertiesPublisher)

    // when
    sut.performAction(action: .containerProperties, identifiers: ["1", "2"], completion: { _ in })

    // then
    XCTAssertEqual(spy.values, ["1"])
  }

  func testPerformActionContainerPropertiesCallSevictItems() {
    // given
    let spy = ValueSpy(publisher: sut.evictItemsPublisher)

    // when
    sut.performAction(action: .makeOnlineOnly, identifiers: ["1", "2"], completion: { _ in })

    // then
    XCTAssertEqual(spy.values, [["1", "2"]])
  }
}
