//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import Foundation
import XCTest

final class FileProviderGlobalProgressObserverTests: XCTestCase {

  // MARK: - Properties
  private var sut: FileProviderGlobalProgressObserver!
  private var fileProviderManager: FileProviderManagerMock!

  override func tearDown() {
    fileProviderManager = nil
    sut = nil
  }

  func testGlobalProgressCalledOnInitTwoTimes() {
    // when
    setupGlobalProgressObserver()

    // then
    XCTAssertEqual(fileProviderManager.globalProgressCount, 2)
  }

  func testGlobalProgressCalledForDownloadingAndUploading() {
    // when
    setupGlobalProgressObserver()

    // then
    XCTAssertEqual(fileProviderManager.kindsSet, [.downloading, .uploading])
  }

  func testInProgressPublisherCalledWhenAllProgressesNotFinished() {
    // given
    setupGlobalProgressObserver(isFinishedDownloading: false, isFinishedUploading: false)

    // when
    let valueSpy = ValueSpy(publisher: sut.inProgressPublisher)

    // then
    XCTAssertEqual(valueSpy.values, [true])
  }

  func testInProgressPublisherCalledWhenAllProgressesFinished() {
    // given
    setupGlobalProgressObserver(isFinishedDownloading: true, isFinishedUploading: true)

    // when
    let valueSpy = ValueSpy(publisher: sut.inProgressPublisher)

    // then
    XCTAssertEqual(valueSpy.values, [false])
  }

  func testInProgressPublisherCalledWhenOneProgressFinished() {
    // given
    setupGlobalProgressObserver(isFinishedDownloading: false, isFinishedUploading: true)

    // when
    let valueSpy = ValueSpy(publisher: sut.inProgressPublisher)

    // then
    XCTAssertEqual(valueSpy.values, [true])
  }

  func testInProgressPublisherCalledSecondTimeWhenDownloadingFinished() {
    // given
    let progresses = setupGlobalProgressObserver(isFinishedDownloading: false, isFinishedUploading: true)

    // when
    let valueSpy = ValueSpy(publisher: sut.inProgressPublisher)
    progresses[.downloading]?.completedUnitCount = 10

    // then
    XCTAssertEqual(valueSpy.values, [true, false])
  }

  // MARK: - Helpers

  @discardableResult
  private func setupGlobalProgressObserver(isFinishedDownloading: Bool = true,
                                           isFinishedUploading: Bool = true) -> [Progress.FileOperationKind: Progress] {
    let downloadingProgress = Progress(totalUnitCount: 10)
    downloadingProgress.completedUnitCount = isFinishedDownloading ? 10 : 5
    let uploadingProgress = Progress(totalUnitCount: 10)
    uploadingProgress.completedUnitCount = isFinishedUploading ? 10 : 5

    fileProviderManager = FileProviderManagerMock(
      progresses: [
        .downloading: downloadingProgress,
        .uploading: uploadingProgress
      ]
    )
    sut = FileProviderGlobalProgressObserver(fileProviderManager: fileProviderManager)

    return [
      .downloading: downloadingProgress,
        .uploading: uploadingProgress
    ]
  }
}
