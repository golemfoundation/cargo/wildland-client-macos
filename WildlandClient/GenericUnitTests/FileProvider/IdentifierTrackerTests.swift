//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import XCTest
import WildlandCommon

final class IdentifierTrackerTests: XCTestCase {

  private var sut: IdentifierTracker!
  private var fileProviderSignalEnumerator: FileProviderSignalEnumeratorMock!
  private var fileItemProvider: FileItemProviderMock!
  private var fileItemStorage: FileItemStorageMock!

  // MARK: - Configuration

  override func setUp() {
    super.setUp()

    fileProviderSignalEnumerator = FileProviderSignalEnumeratorMock()
    fileItemProvider = FileItemProviderMock()
    fileItemStorage = FileItemStorageMock()

    sut = IdentifierTracker(
      fileProviderManager: fileProviderSignalEnumerator,
      fileItemProvider: fileItemProvider,
      fileItemStorage: fileItemStorage
    )
  }

  override func tearDown() {
    fileProviderSignalEnumerator = nil
    fileItemProvider = nil
    fileItemStorage = nil
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testEmptyResultIfNonObserved() async {
    // when
    let updates = await sut.itemUpdates(anchorData: Data())

    // then
    XCTAssertEqual(fileProviderSignalEnumerator.signalEnumeratorCounter, .zero)
    XCTAssertEqual(updates.addedOrUpdatedItems, [], "Should not be any added items")
    XCTAssertEqual(updates.deletedIdentifiers, [], "Should not be any removed files")
  }

  func testOneUpdatedItemOfObservedIdentifier() async throws {
    // given
    await sut.startObserving(identifier: "1")

    let itemDate = try date(from: "18-05-2023")
    fileItemStorage.expectedFileItems = [
      .fixture(identifier: "2", parentIdentifier: "1", modificationDate: itemDate)
    ]

    let newDate = try date(from: "19-05-2023")
    let updatedItem = FileItem.fixture(identifier: "2", parentIdentifier: "1", modificationDate: newDate)

    fileItemProvider.expectedFileItems = [updatedItem]

    // when
    await sut.startContentUpdate(with: .zero, repeatable: false)

    // then
    XCTAssertEqual(fileProviderSignalEnumerator.signalEnumeratorCounter, 1)
    let updates = await sut.itemUpdates(anchorData: Data())
    XCTAssertEqual(updates.deletedIdentifiers, [], "Should not be any removed files")
    XCTAssertEqual(updates.addedOrUpdatedItems, [updatedItem], "Should contain one item")
  }

  func testTwoUpdatedItemsOfObservedIdentifier() async throws {
    // given
    await sut.startObserving(identifier: "1")

    let itemDate = try date(from: "18-05-2023")
    fileItemStorage.expectedFileItems = [
      .fixture(identifier: "2", parentIdentifier: "1", modificationDate: itemDate),
      .fixture(identifier: "3", parentIdentifier: "1", modificationDate: itemDate)
    ]

    let updatedDate = try date(from: "19-05-2023")
    let firstUpdatedItem = FileItem.fixture(identifier: "2", parentIdentifier: "1", modificationDate: updatedDate)
    let secondUpdatedItem = FileItem.fixture(identifier: "3", parentIdentifier: "1", modificationDate: updatedDate)

    fileItemProvider.expectedFileItems = [firstUpdatedItem, secondUpdatedItem]

    // when
    await sut.startContentUpdate(with: .zero, repeatable: false)

    // then
    XCTAssertEqual(fileProviderSignalEnumerator.signalEnumeratorCounter, 1)
    let updates = await sut.itemUpdates(anchorData: Data())
    XCTAssertEqual(updates.deletedIdentifiers, [], "Should not be any removed files")
    let updated = updates.addedOrUpdatedItems.sorted(by: { $0.identifier < $1.identifier })
    XCTAssertEqual(updated, [firstUpdatedItem, secondUpdatedItem], "Should contain two item")
  }

  func testTwoUpdatedItemsFromTwoObservedIdentifiers() async throws {
    // given
    await sut.startObserving(identifier: "1")
    await sut.startObserving(identifier: "2")

    let itemDate = try date(from: "18-05-2023")
    fileItemStorage.expectedFileItems = [
      .fixture(identifier: "3", parentIdentifier: "1", modificationDate: itemDate),
      .fixture(identifier: "4", parentIdentifier: "2", modificationDate: itemDate)
    ]

    let updatedDate = try date(from: "19-05-2023")
    let firstUpdatedItem = FileItem.fixture(identifier: "3", parentIdentifier: "1", modificationDate: updatedDate)
    let secondUpdatedItem = FileItem.fixture(identifier: "4", parentIdentifier: "2", modificationDate: updatedDate)

    fileItemProvider.expectedFileItems = [firstUpdatedItem, secondUpdatedItem]

    // when
    await sut.startContentUpdate(with: .zero, repeatable: false)

    // then
    XCTAssertEqual(fileProviderSignalEnumerator.signalEnumeratorCounter, 1)
    let updates = await sut.itemUpdates(anchorData: Data())
    XCTAssertEqual(updates.deletedIdentifiers, [], "Should not be any removed files")
    let updated = updates.addedOrUpdatedItems.sorted(by: { $0.identifier < $1.identifier })
    XCTAssertEqual(updated, [firstUpdatedItem, secondUpdatedItem], "Should contain two item")
  }

  func testOneDeletedItemOfObservedIdentifier() async throws {
    // given
    await sut.startObserving(identifier: "1")

    let itemDate = try date(from: "18-05-2023")
    fileItemStorage.expectedFileItems = [
      .fixture(identifier: "2", parentIdentifier: "1", modificationDate: itemDate)
    ]

    fileItemProvider.expectedFileItems = []

    // when
    await sut.startContentUpdate(with: .zero, repeatable: false)

    // then
    XCTAssertEqual(fileProviderSignalEnumerator.signalEnumeratorCounter, 1)
    let updates = await sut.itemUpdates(anchorData: Data())
    XCTAssertEqual(updates.deletedIdentifiers, ["2"], "Item should be removed")
    XCTAssertEqual(updates.addedOrUpdatedItems, [], "Should contain zero item")
  }

  func testTwoDeletedItemOfTwoObservedIdentifiers() async throws {
    // given
    await sut.startObserving(identifier: "1")
    await sut.startObserving(identifier: "2")

    let itemDate = try date(from: "18-05-2023")
    fileItemStorage.expectedFileItems = [
      .fixture(identifier: "3", parentIdentifier: "1", modificationDate: itemDate),
      .fixture(identifier: "4", parentIdentifier: "2", modificationDate: itemDate)
    ]

    fileItemProvider.expectedFileItems = [FileItem]()

    // when
    await sut.startContentUpdate(with: .zero, repeatable: false)

    // then
    XCTAssertEqual(fileProviderSignalEnumerator.signalEnumeratorCounter, 1)
    let updates = await sut.itemUpdates(anchorData: Data())
    let deleted = updates.deletedIdentifiers.sorted(by: { $0 < $1 })
    XCTAssertEqual(deleted, ["3", "4"], "Items should be removed")
    XCTAssertEqual(updates.addedOrUpdatedItems, [], "Should contain zero item")
  }

  func testOneAddedItemOfObservedIdentifier() async throws {
    // given
    await sut.startObserving(identifier: "1")

    let itemDate = try date(from: "18-05-2023")
    let firstItem = FileItem.fixture(identifier: "2", parentIdentifier: "1", modificationDate: itemDate)

    fileItemProvider.expectedFileItems = [firstItem]

    // when
    await sut.startContentUpdate(with: .zero, repeatable: false)

    // then
    XCTAssertEqual(fileProviderSignalEnumerator.signalEnumeratorCounter, 1)
    let updates = await sut.itemUpdates(anchorData: Data())
    XCTAssertEqual(updates.deletedIdentifiers, [], "Should contain zero identifiers")
    XCTAssertEqual(updates.addedOrUpdatedItems, [firstItem], "Should contain added item")
  }
}
