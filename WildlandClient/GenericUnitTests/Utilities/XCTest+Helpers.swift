//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import Combine

extension XCTest {

  func XCTAssertThrowsError<T: Sendable>(_ expression: @autoclosure () async throws -> T, _ message: String = "function should throw error") async {
    do {
      _ = try await expression()
      XCTFail(message)
    } catch {}
  }
}

extension XCTestCase {

  func expectation<T, Failure: Error>(
    publisher: AnyPublisher<T, Failure>,
    expectedCount: Int,
    when: () -> Void,
    then: @escaping ([T]) -> Void,
    thenError: ((Error?) -> Void)? = nil,
    method name: String = #function) {
      let expectation = XCTestExpectation(description: name)
      if expectedCount == 0 {
        expectation.isInverted = true
      } else {
        expectation.expectedFulfillmentCount = expectedCount
      }
      var cancellables = Set<AnyCancellable>()
      var results = [T]()

      publisher
        .sink(
          receiveCompletion: { completion in
            guard case .failure(let error) = completion else { return }
            thenError?(error)
          },
          receiveValue: { value in
            results.append(value)
            expectation.fulfill()
          })
        .store(in: &cancellables)

      when()
      wait(for: [expectation], timeout: 1.0)
      then(results)
    }

  func expectation<T>(
    publisher: Published<T>.Publisher,
    expectedCount: Int,
    when: () -> Void,
    then: @escaping ([T]) -> Void,
    method name: String = #function) {
      expectation(
        publisher: publisher.eraseToAnyPublisher(),
        expectedCount: expectedCount,
        when: when,
        then: then,
        method: name
      )
    }

  func verifyResults<T: Equatable>(_ results: [T], expected: [T]) {
    XCTAssertEqual(results.count, expected.count)

    var mutableResults = results
    var mutableExpected = expected

    while !mutableResults.isEmpty {
      let element = mutableResults.removeFirst()
      if let index = mutableExpected.firstIndex(of: element) {
        mutableExpected.remove(at: index)
      } else {
        XCTFail("Element not found: \(element)")
      }
    }
  }
}

private enum DateError: Error {
  case badFormat
}

extension XCTestCase {
  func date(from string: String) throws -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    guard let date = dateFormatter.date(from: string) else { throw DateError.badFormat }
    return date
  }
}
