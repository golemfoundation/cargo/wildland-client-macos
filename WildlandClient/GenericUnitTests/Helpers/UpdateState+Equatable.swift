//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

extension UpdateState: Equatable {
  public static func == (lhs: UpdateState, rhs: UpdateState) -> Bool {
    switch (lhs, rhs) {
    case (.dismiss, .dismiss):
      return true
    case (.upToDate, .upToDate):
      return true
    case (.newUpdateAvailable, .newUpdateAvailable):
      return true
    case (.downloading(let lhsReceived, let lhsTotal), .downloading(let rhsReceived, let rhsTotal)):
      return lhsReceived == rhsReceived && lhsTotal == rhsTotal
    case (.extracting(let lhsPercent), .extracting(let rhsPercent)):
      return lhsPercent == rhsPercent
    case (.waitForOperationsToComplete, .waitForOperationsToComplete):
      return true
    case (.error(let lhsError), .error(let rhsError)):
      return lhsError.localizedDescription == rhsError.localizedDescription
    default:
      return false
    }
  }
}
