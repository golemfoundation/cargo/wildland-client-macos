//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI

extension InfoViewType: Equatable {
  public static func == (lhs: InfoViewType, rhs: InfoViewType) -> Bool {
    switch (lhs, rhs) {
    case (.info(let lhsInfo), .info(let rhsInfo)):
      return lhsInfo == rhsInfo
    case (.error(let lhsText, let lhsImage), .error(let rhsText, let rhsImage)):
      return lhsText == rhsText && lhsImage == rhsImage
    case (.loading(let lhsText), .loading(let rhsText)):
      return lhsText == rhsText
    default:
      return false
    }
  }
}
