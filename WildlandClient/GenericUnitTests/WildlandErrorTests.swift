//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import FileProvider
import wildlandx
import XCTest

class WildlandErrorTests: XCTestCase {

  func testRustForFileProviderExtensionErrorAdaptee() {
    // given
    let expectedUnderlyingErrorCode = -10034
    let expectedErrorCategory: ErrorCategory = ErrorCategory_General

    let fileProviderErrorDomain = NSError(domain: NSFileProviderErrorDomain, code: expectedUnderlyingErrorCode)
    let mockRustException = RustForFPEExceptionMock(
      fileProviderExtensionError: fileProviderErrorDomain,
      errorCategory: expectedErrorCategory
    )

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertEqual(sut.underlyingError.domain, NSFileProviderErrorDomain)
    XCTAssertEqual(sut.underlyingError.code, expectedUnderlyingErrorCode)
    XCTAssertEqual(ErrorCategory(UInt32(sut.code)), expectedErrorCategory)
  }

  func testInitWithError() {
    // given
    let nonRustError = ErrorStub.stub

    // when
    let sut = WildlandError(nonRustError)

    // then
    XCTAssertEqual(sut.domain, sut.underlyingError.domain)
    XCTAssertEqual(sut.code, sut.underlyingError.code)
    XCTAssertEqual(sut, sut.underlyingError)
  }

  func testInitWithNSError() {
    // given
    let nsError = NSError(domain: "Mock domain", code: 1001)

    // when
    let sut = WildlandError(nsError)

    // then
    XCTAssertEqual(sut.domain, nsError.domain)
    XCTAssertEqual(sut.code, nsError.code)
    XCTAssertEqual(sut, sut.underlyingError)
  }

  func testIsUserNotAuthenticated() {
    // given
    let mockRustException = RustForFPEExceptionMock(
      with: .notAuthenticated,
      errorCategory: ErrorCategory_Logical
    )

    // when
    let sut = WildlandError(mockRustException )

    // then
    XCTAssertTrue(sut.isUserNotAuthenticated)
  }

  func testIsUserNotAuthenticatedButWrongCategory() {
    // given
    let mockRustException = RustForFPEExceptionMock(
      with: .notAuthenticated,
      errorCategory: ErrorCategory_General
    )

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertFalse(sut.isUserNotAuthenticated)
  }

  func testIsCancelledByUser() {
    // given
    let error = NSError(domain: NSCocoaErrorDomain, code: NSUserCancelledError)
    let mockRustException = RustForFPEExceptionMock(
      fileProviderExtensionError: error,
      errorCategory: ErrorCategory_Logical
    )

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertTrue(sut.isCancelledByUser)
  }

  func testIsnotCancelledByUserBecauseWrongDomain() {
    // given
    let error = NSError(domain: "Mock domain", code: NSUserCancelledError)
    let mockRustException = RustForFPEExceptionMock(
      fileProviderExtensionError: error,
      errorCategory: ErrorCategory_Logical
    )

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertFalse(sut.isCancelledByUser)
  }

  func testIsNotCancelledByUserBecauseWrongCategory() {
    // given
    let error = NSError(domain: "Mock domain", code: NSFileProviderError.notAuthenticated.rawValue)
    let mockRustException = RustForFPEExceptionMock(fileProviderExtensionError: error)

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertFalse(sut.isCancelledByUser)
  }

  func testIsInsufficientQuota() {
    // given
    let error = NSError(domain: "Mock domain", code: NSFileProviderError.insufficientQuota.rawValue)
    let mockRustException = RustForFPEExceptionMock(fileProviderExtensionError: error)

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertTrue(sut.isInsufficientQuota)
  }

  func testShouldSetServerUnreachableErrorForProperCategory() {
    // given
    let expectedErrorCategory = ErrorCategory_NetworkConnectivity
    let mockRustException = RustExceptionMock(with: .serverUnreachable, errorCategory: expectedErrorCategory)

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertEqual(sut.underlyingError.code, NSFileProviderError.Code.serverUnreachable.rawValue)
    XCTAssertEqual(sut.underlyingError.domain, NSFileProviderErrorDomain)
    XCTAssertEqual(ErrorCategory(UInt32(sut.code)), expectedErrorCategory)
  }

  func testShouldSetDefaultErrorForUnsupportedCategory() {
    // given
    let expectedErrorCategory = ErrorCategory_General
    let mockRustException = RustExceptionMock(with: .serverUnreachable, errorCategory: expectedErrorCategory)

    // when
    let sut = WildlandError(mockRustException)

    // then
    XCTAssertEqual(sut.underlyingError.code, NSXPCConnectionReplyInvalid)
    XCTAssertEqual(sut.underlyingError.domain, NSCocoaErrorDomain)
    XCTAssertEqual(ErrorCategory(UInt32(sut.code)), expectedErrorCategory)
  }
}
