//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import WildlandCommon
import XCTest

final class FileItemStorageTests: XCTestCase {

  // MARK: - Private

  private var sut: FileItemStorage!

  // MARK: - Setup

  override func setUp() {
    sut = FileItemStorage()
    super.setUp()
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testStoreNewFileItem() async {
    // given
    let expected = FileItem.fixture(identifier: "1", parentIdentifier: "2", modificationDate: Date())

    // when
    await sut.store(expected)

    // then
    let fileItem = await sut.fileItem(for: "1")
    XCTAssertEqual(fileItem, expected)
  }

  func testStoreExistingFileItem() async throws {
    // given
    await sut.store(
      .fixture(identifier: "1", parentIdentifier: "2", modificationDate: try date(from: "12-05-2003"))
    )
    let expectedDate = try date(from: "13-05-2003")
    let expected = FileItem.fixture(identifier: "1", parentIdentifier: "2", modificationDate: expectedDate)

    // when
    await sut.store(expected)

    // then
    let fileItem = await sut.fileItem(for: "1")
    XCTAssertEqual(fileItem, expected)
  }

  func testRemoveFileItem() async throws {
    // given
    await sut.store(
      .fixture(identifier: "1", parentIdentifier: "2", modificationDate: try date(from: "12-05-2003"))
    )

    // when
    await sut.remove(identifier: "1")

    // then
    let fileItem = await sut.fileItem(for: "1")
    XCTAssertNil(fileItem)
  }
}
