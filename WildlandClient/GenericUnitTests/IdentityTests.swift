//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

// swiftlint:disable line_length

import XCTest
import WildlandCommon
import wildlandx

class IdentityTests: XCTestCase {

  private var sut: UserApiProviderType!

  override func setUp() {
    super.setUp()
    sut = UserApiProvider()
    WildlandX.storage.clearAllKeys()
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  func testCreateIdentity() throws {
    let words = ["deal", "industry", "bean", "moral", "gauge", "faith", "occur", "pear", "one", "emotion", "empty", "menu"]
    let mnemonicWords = words.compactMap { $0.isEmpty ? nil : RustString($0) }
    guard let recovered = try? sut.createMnemonic(words: mnemonicWords) else {
      assert(false, "Failed to recover user mnemonic")
      return
    }
    XCTAssertTrue(try recovered.words == words)
  }

  func testRandomIdentity() throws {
    guard let mnemonic = try? sut.generateMnemonic() else {
      assert(false, "Failed to create a user mnemonic")
      return
    }
    let words = try mnemonic.words
    XCTAssertEqual(12, words.count)

    let mnemonicWords = words.compactMap { $0.isEmpty ? nil : RustString($0) }
    guard let recovered = try? sut.createMnemonic(words: mnemonicWords) else {
      assert(false, "Failed to recover the mnemonic from words")
      return
    }
    XCTAssertEqual(words, try recovered.words)
  }

  func testCreateUserFromGeneratedMnemonic() throws {
    guard let mnemonic = try? sut.generateMnemonic() else {
      assert(false, "Failed to create a user mnemonic")
      return
    }

    let user = try! sut.createUser(from: mnemonic, deviceName: "Windows device")
    assert(!user.stringify().toString().isEmpty, "User created: \(user.stringify())")
  }

  func testCreateUserFromCustomSeedWords() throws {
    let words = ["deal", "industry", "bean", "moral", "gauge", "faith", "occur", "pear", "one", "emotion", "empty", "menu"]
    let mnemonicWords = words.compactMap { $0.isEmpty ? nil : RustString($0) }
    guard let mnemonic = try? sut.createMnemonic(words: mnemonicWords) else {
      assert(false, "Failed to recover the mnemonic from words")
      return
    }

    let user = try! sut.createUser(from: mnemonic, deviceName: "Mac device")
    assert(!user.stringify().toString().isEmpty, "User created: \(user.stringify())")
  }
}

// swiftlint:enable line_length
