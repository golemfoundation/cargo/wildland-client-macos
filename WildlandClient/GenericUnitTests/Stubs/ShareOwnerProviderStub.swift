//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

final class ShareOwnerProviderStub: ShareOwnerProvider {

  // MARK: - Properties

  var shareMessage = "Mock shareMessage"
  var throwShareError = false
  var throwGetAllRecipientsError = false
  var publicKeys = ["9449619a4b95dfa023d32bf4746b414fd45d71d67e801146f3fa9bcfd75ef10c"]

  // MARK: - Stub

  func share(path: String, publicKey: String) throws -> String {
    guard throwShareError == false else {
      throw ErrorStub.stub
    }
    return shareMessage
  }

  func getAllRecipients(for identifier: String) throws -> [String] {
    guard throwGetAllRecipientsError == false else {
      throw ErrorStub.stub
    }
    return publicKeys
  }
}
