//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import XCTest

final class DeeplinkTests: XCTestCase {

  // MARK: - Tests

  func testShareDeeplinkUrl() {
    // given
    let sharingMessage = SharingMessage(body: "exampleBody", userAlias: "exampleAlias")
    let deeplink = Deeplink.share(sharingMessage)

    // when
    let url = deeplink.url

    // then
    XCTAssertEqual(url.scheme, "cargo-daemon")
    XCTAssertEqual(url.host(), "share")
    XCTAssertEqual(url.query(), "body=exampleBody&userAlias=exampleAlias")
  }

  func testShareDeeplinkFromUrlComponents() throws {
    // given
    let sharingMessage = SharingMessage(body: "exampleBody", userAlias: "exampleAlias")
    let expectedDeeplink = Deeplink.share(sharingMessage)
    let url = try XCTUnwrap(URL(string: "cargo-daemon://share?body=exampleBody&userAlias=exampleAlias"))
    let components = try XCTUnwrap(URLComponents(url: url, resolvingAgainstBaseURL: true))

    // when
    let deeplink = Deeplink(urlComponents: components)

    // then
    XCTAssertEqual(deeplink, expectedDeeplink)
  }
}
