//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import CargoUI
import Combine
import WildlandCommon

final class DomainMounterMock: DomainMounter {

  // MARK: - Properties

  lazy var isMountDomainPublisher = isMountDomainSubject.eraseToAnyPublisher()
  private let isMountDomainSubject = PassthroughSubject<Void, Error>()

  private(set) var disconnectDomainCounter = Int.zero
  private(set) var mountIfAvailableCounter = Int.zero

  // MARK: - Mock

  func disconnectDomain() async throws {
    disconnectDomainCounter += 1
  }

  func mountIfAvailable() {
    mountIfAvailableCounter += 1
  }

  // MARK: - Public

  func callIsMountDomain() {
    isMountDomainSubject.send()
  }
}
