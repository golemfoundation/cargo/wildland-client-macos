//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import FileProvider

final class TemporaryUrlProviderMock: TemporaryUrlProvider {

  // MARK: - Properties

  var directoryURL: URL?
  var temporaryDirectoryURLError: Error?
  private(set) var temporaryDirectoryURLCounter = Int.zero

  var userVisibleURL: URL?
  var getUserVisibleURLError: Error?
  private(set) var getUserVisibleURLCounter = Int.zero
  private(set) var itemIdentifierSet: NSFileProviderItemIdentifier?

  // MARK: - Public

  func temporaryDirectoryURL() throws -> URL {
    temporaryDirectoryURLCounter += 1
    if let temporaryDirectoryURLError { throw temporaryDirectoryURLError }
    guard let directoryURL else { throw ErrorStub.mockValueNotProvided }
    return directoryURL
  }

  func getUserVisibleURL(for itemIdentifier: NSFileProviderItemIdentifier) async throws -> URL {
    getUserVisibleURLCounter += 1
    itemIdentifierSet = itemIdentifier
    if let getUserVisibleURLError { throw getUserVisibleURLError }
    guard let userVisibleURL else { throw ErrorStub.mockValueNotProvided }
    return userVisibleURL
  }
}
