//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import WildlandCommon
import XCTest

final class DaemonUserServiceMock: DaemonUserService {

  // MARK: - Properties

  var error: Error?
  var rootCargoUrlError: Error?
  var setThemeExpectation: XCTestExpectation?
  var availabilityState: UserAvailabilityState = .readyForUse
  private let words: [String]
  private let expectedRootCargoUrl: URL
  private(set) var requestFreeStorageCounter = Int.zero
  private(set) var emailSet: String?
  @Published private(set) var verifyEmailUsingCodeCounter = Int.zero
  private(set) var codeSet: String?
  private(set) var createUserCounter = Int.zero
  private(set) var deviceNameSet: String?
  private(set) var createMnemonicCounter = Int.zero
  private(set) var wordsSet: [String]?
  private(set) var generateMnemonicCounter = Int.zero
  private(set) var rootCargoUrlCounter = Int.zero
  private(set) var setThemeCounter = Int.zero
  private(set) var themeOptionSet: Settings.ColorThemeOption?

  init(words: [String] = .wordsFixture, expectedRootCargoUrl: URL = URL(filePath: "/Documents/Cargo")) {
    self.words = words
    self.expectedRootCargoUrl = expectedRootCargoUrl
  }

  // MARK: - Mocks

  var rootCargoUrl: URL {
    get async throws {
      rootCargoUrlCounter += 1
      if let rootCargoUrlError {
        throw rootCargoUrlError
      }
      return expectedRootCargoUrl
    }
  }

  func verifyEmail(with code: String) async throws {
    codeSet = code
    verifyEmailUsingCodeCounter += 1
    if let error { throw error }
  }

  func requestFreeTierStorage(for email: String) async throws {
    emailSet = email
    requestFreeStorageCounter += 1
    if let error { throw error }
  }

  func createMnemonic(using words: [String]) async -> Result<Void, Error> {
    createMnemonicCounter += 1
    wordsSet = words
    if let error {
      return .failure(error)
    } else {
      return .success(())
    }
  }

  func generateMnemonic() async throws -> [String] {
    generateMnemonicCounter += 1
    if let error { throw error }
    return words
  }

  func createUser(with deviceName: String) async throws {
    createUserCounter += 1
    deviceNameSet = deviceName
    throw ErrorStub.stub
  }

  func setTheme(_ themeOption: Settings.ColorThemeOption) async throws {
    themeOptionSet = themeOption
    setThemeCounter += 1
    setThemeExpectation?.fulfill()
  }
}

private extension [String] {
  static let wordsFixture = [
    "today", "devote", "pledge", "slogan", "equal", "nature", "spend", "magnet", "pilot", "issue", "travel", "audit"
  ]
}
