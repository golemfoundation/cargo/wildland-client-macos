//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

final class UserDefaultsMock: UserDefaultsProviderType {

  // MARK: - Properties

  var storage = [String: Any]()

  // MARK: - Mock

  func set(_ value: Any?, forKey defaultName: String) {
    storage[defaultName] = value
  }

  func object(forKey defaultName: String) -> Any? {
    storage[defaultName]
  }

  func string(forKey defaultName: String) -> String? {
    storage[defaultName] as? String
  }
}
