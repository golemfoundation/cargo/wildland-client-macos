//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import FileProvider
@testable import wlclientd
@testable import WildlandCommon

final class FileProviderManagerMock: GlobalProgressFileProviderManager, FileProviderManagerType {

  // MARK: - Properties

  static var addDomainHandler: (() -> Void)?
  private(set) var globalProgressCount = Int.zero
  private(set) var kindsSet = [Progress.FileOperationKind]()
  private let progresses: [Progress.FileOperationKind: Progress]
  static var completionHandlerError: Error?
  static private(set) var domainSet: NSFileProviderDomain?
  static private(set) var addCounter = Int.zero

  // MARK: - Initialization

  init(progresses: [Progress.FileOperationKind: Progress]) {
    self.progresses = progresses
  }

  // MARK: - GlobalProgressFileProviderManager

  func globalProgress(for kind: Progress.FileOperationKind) -> Progress {
    globalProgressCount += 1
    kindsSet.append(kind)
    return progresses[kind] ?? Progress()
  }

  // MARK: - FileProviderManagerType

  static func add(_ domain: NSFileProviderDomain, completionHandler: @escaping (Error?) -> Void) {
    addCounter += 1
    domainSet = domain
    addDomainHandler?()
    completionHandler(completionHandlerError)
  }

  static func cleanup() {
    domainSet = nil
    completionHandlerError = nil
    addCounter = .zero
  }
}
