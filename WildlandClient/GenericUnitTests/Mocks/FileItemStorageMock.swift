//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import WildlandCommon

final class FileItemStorageMock: FileItemStorageType {

  // MARK: - Properties

  var expectedFileItem: FileItem?
  var expectedFileItems: [FileItem] = []
  private(set) var identifierSet: String?
  private(set) var fileItemCounter = Int.zero
  private(set) var fileItemsSet: [FileItem] = []
  private(set) var storeCounter = Int.zero
  private(set) var removeCounter = Int.zero
  private(set) var fileItemsCounter = Int.zero
  private(set) var parentIdentifierSet: String?

  // MARK: - Mock

  func fileItem(for identifier: String) async -> FileItem? {
    fileItemCounter += 1
    identifierSet = identifier
    return expectedFileItem
  }

  func store(_ fileItem: FileItem) async {
    storeCounter += 1
    fileItemsSet.append(fileItem)
  }

  func remove(identifier: String) async {
    removeCounter += 1
    identifierSet = identifier
  }

  func fileItems(for parentIdentifier: String) async -> [FileItem] {
    fileItemsCounter += 1
    parentIdentifierSet = parentIdentifier
    return expectedFileItems
  }
}
