//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import Combine
import UserNotifications

final class UserNotificationsMock: UserNotificationsType {

  // MARK: - Properties

  lazy private(set) var notificationResponsePublisher = notificationResponseSubject.eraseToAnyPublisher()
  private let notificationResponseSubject = PassthroughSubject<UserNotificationResponse, Never>()

  private(set) var requestNotificationAuthorizationCounter = Int.zero
  private(set) var categorySet: UserNotifications.Category?
  private(set) var showNotificationCounter = Int.zero
  private(set) var authorizationStatusCounter = Int.zero
  private(set) var completionSet: ((UNAuthorizationStatus) -> Void)?

  // MARK: - Mock

  func requestNotificationAuthorization() {
    requestNotificationAuthorizationCounter += 1
  }

  func showNotification(category: UserNotifications.Category) {
    showNotificationCounter += 1
    categorySet = category
  }

  func authorizationStatus(completion: @escaping (UNAuthorizationStatus) -> Void) {
    authorizationStatusCounter += 1
    completionSet = completion
  }

  // MARK: - Helpers

  func callNotificationResponse(_ response: UserNotificationResponse) {
    notificationResponseSubject.send(response)
  }
}
