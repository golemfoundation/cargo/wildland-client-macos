//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import Foundation
import wildlandx
import FileProvider

struct RustForFPEExceptionMock: RustExceptionBase, FileProviderExtensionErrorAdaptee {
  let fileProviderExtensionError: NSError
  let errorCategory: ErrorCategory

  func reason() -> RustString { RustString("Mock reason") }
  func category() -> ErrorCategory { errorCategory }

  init(fileProviderExtensionError: NSError = ErrorStub.stub as NSError, errorCategory: ErrorCategory = ErrorCategory_General) {
    self.fileProviderExtensionError = fileProviderExtensionError
    self.errorCategory = errorCategory
  }

  init(with code: NSFileProviderError.Code, errorCategory: ErrorCategory) {
    self.fileProviderExtensionError = NSError(domain: "Mock domain", code: code.rawValue)
    self.errorCategory = errorCategory
  }
}

struct RustExceptionMock: RustExceptionBase {
  let fileProviderExtensionError: NSError
  let errorCategory: ErrorCategory

  func reason() -> RustString { RustString("Mock reason") }
  func category() -> ErrorCategory { errorCategory }

  init(fileProviderExtensionError: NSError = ErrorStub.stub as NSError, errorCategory: ErrorCategory = ErrorCategory_General) {
    self.fileProviderExtensionError = fileProviderExtensionError
    self.errorCategory = errorCategory
  }

  init(with code: NSFileProviderError.Code, errorCategory: ErrorCategory) {
    self.fileProviderExtensionError = NSError(domain: "Mock domain", code: code.rawValue)
    self.errorCategory = errorCategory
  }
}
