//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import Combine

final class UpdaterControllerMock: UpdaterControllerType {

  // MARK: - Properties

  private(set) lazy var updateStatePublisher = updateStateSubject.eraseToAnyPublisher()
  private let updateStateSubject = PassthroughSubject<UpdateState, Never>()

  private(set) lazy var updateUserChoiceSubscriber = AnySubscriber(updateUserChoiceSubject)
  private let updateUserChoiceSubject = PassthroughSubject<UpdateUserChoice, Never>()

  private(set) lazy var canCheckForUpdatesPublisher = canCheckForUpdatesSubject.eraseToAnyPublisher()
  private let canCheckForUpdatesSubject = PassthroughSubject<Bool, Never>()

  private(set) lazy var newAppVersionIdentifiedPublisher = newAppVersionIdentifiedSubject.eraseToAnyPublisher()
  private let newAppVersionIdentifiedSubject = PassthroughSubject<Void, Never>()

  private(set) var checkForUpdatesCounter = Int.zero
  private(set) var acknowledgementDismissedCounter = Int.zero
  private(set) var updateUserChoiceSet: UpdateUserChoice?
  private(set) var updateUserChoiceSubjectCounter = Int.zero

  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  init() {
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    updateUserChoiceSubject
      .sink { [weak self] updateUserChoice in
        self?.updateUserChoiceSet = updateUserChoice
        self?.updateUserChoiceSubjectCounter += 1
      }
      .store(in: &cancellables)
  }

  // MARK: - Mock

  func checkForUpdates() {
    checkForUpdatesCounter += 1
  }

  func acknowledgementDismissed() {
    acknowledgementDismissedCounter += 1
  }

  // MARK: - Helpers

  func callUpdateStateSubject(_ updateState: UpdateState) {
    updateStateSubject.send(updateState)
  }
}
