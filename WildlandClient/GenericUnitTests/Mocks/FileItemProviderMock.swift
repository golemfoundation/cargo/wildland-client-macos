//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import WildlandCommon

final class FileItemProviderMock: FileItemProviderType {

  enum MockError: Error {
    case expectedFileItemNotSet
    case expectedFileItemsNotSet
  }

  // MARK: - Properties

  var expectedFileItems: [FileItem]?
  var fileItemsError: Error?
  var expectedFileItem: FileItem?
  var fileItemError: Error?

  private(set) var fileItemsCounter = Int.zero
  private(set) var parentIdentifierSet: String?
  private(set) var createFileItemCounter = Int.zero
  private(set) var pathSet: String?
  private(set) var updateFileItemCounter = Int.zero
  private(set) var identifierSet: String?
  private(set) var fileItemCounter = Int.zero
  private(set) var medataDataSet: Metadata?

  // MARK: - Mock

  func fileItem(for identifier: String, path: String?, metadata: Metadata?) async throws -> FileItem {
    fileItemCounter += 1
    identifierSet = identifier
    pathSet = path
    medataDataSet = metadata
    guard let expectedFileItem else { throw MockError.expectedFileItemNotSet }
    return expectedFileItem
  }

  func fileItems(for parentIdentifier: String) async throws -> [FileItem] {
    fileItemsCounter += 1
    parentIdentifierSet = parentIdentifier
    if let fileItemsError { throw fileItemsError }
    guard let expectedFileItems else { throw MockError.expectedFileItemsNotSet }
    return expectedFileItems
  }

  func createFileItem(path: String) async throws -> FileItem {
    createFileItemCounter += 1
    pathSet = path
    if let fileItemError { throw fileItemError }
    guard let expectedFileItem else { throw MockError.expectedFileItemNotSet }
    return expectedFileItem
  }

  func updateFileItem(identifier: String) async throws -> FileItem {
    updateFileItemCounter += 1
    identifierSet = identifier
    if let fileItemError { throw fileItemError }
    guard let expectedFileItem else { throw MockError.expectedFileItemNotSet }
    return expectedFileItem
  }
}
