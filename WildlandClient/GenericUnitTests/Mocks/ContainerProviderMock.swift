//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import wlclientd
import wildlandx
import WildlandCommon

final class ContainerProviderMock: ContainerProviderType {

  // MARK: - Properties

  var createContainerError: Error?
  var fileContainer: FileContainer
  var containersError: Error?
  var fileContainers: [FileContainer]
  var mountError: Error?
  var mountAllContainersError: Error?
  var unmountError: Error?

  private(set) var templateSet: StorageTemplate?
  private(set) var nameSet: String?
  private(set) var pathSet: String?
  private(set) var createContainerCounter = Int.zero
  private(set) var getContainersCounter = Int.zero
  private(set) var containerSet: FileContainer?
  private(set) var mountCounter = Int.zero
  private(set) var mountAllContainersCounter = Int.zero
  private(set) var unmountCounter = Int.zero

  // MARK: - Initialization

  init(fileContainer: FileContainer = .fixture, fileContainers: [FileContainer] = [.fixture, .fixture]) {
    self.fileContainer = fileContainer
    self.fileContainers = fileContainers
  }

  // MARK: - ContainerProviderType

  func createContainer(from template: StorageTemplate, name: String, path: String) throws -> FileContainer {
    createContainerCounter += 1
    templateSet = template
    nameSet = name
    pathSet = path
    if let createContainerError { throw createContainerError }
    return fileContainer
  }

  func getContainers() throws -> [FileContainer] {
    getContainersCounter += 1
    if let containersError { throw containersError }
    return fileContainers
  }

  func mount(container: FileContainer) throws {
    mountCounter += 1
    containerSet = container
    if let mountError { throw mountError }
  }

  func mountAllContainers() throws {
    mountAllContainersCounter += 1
    if let mountAllContainersError { throw mountAllContainersError }
  }

  func unmount(container: FileContainer) throws {
    unmountCounter += 1
    containerSet = container
    if let unmountError { throw unmountError }
  }
}
