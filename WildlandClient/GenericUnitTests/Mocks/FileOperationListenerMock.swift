//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import wlclientd
import WildlandCommon
import Combine

final class FileOperationListenerMock: FileOperationListener {

  // MARK: - Properties

  lazy private(set) var operationsPublisher = operationsSubject.eraseToAnyPublisher()
  lazy private(set) var errorPublisher = latestErrorSubject.eraseToAnyPublisher()
  private let operationsSubject = PassthroughSubject<[FileOperationStorage], Never>()
  private let latestErrorSubject = PassthroughSubject<Content?, Never>()

  private(set) var setOperationMetadataCount = Int.zero
  private(set) var metadataSet: FileOperationMetadata?
  private(set) var identifierSet: String?
  private(set) var uuid: UUID?
  private(set) var startOperationCount = Int.zero
  private(set) var finishOperationCount = Int.zero
  private(set) var typeSet: OperationType?

  // MARK: - Public

  func startOperation(taskId: UUID, identifier: String, filename: String, type: OperationType) {
    startOperationCount += 1
    uuid = taskId
    identifierSet = identifier
    typeSet = type
  }

  func setOperationMetadata(_ metadata: FileOperationMetadata, identifier: String) {
    setOperationMetadataCount += 1
    metadataSet = metadata
    identifierSet = identifier
  }

  func finishOperation(_ taskId: UUID, input: FileOperationFinishInput) {
    finishOperationCount += 1
    uuid = taskId
  }

  func publishOperations(_ operations: [FileOperationStorage]) {
    operationsSubject.send(operations)
  }
}
