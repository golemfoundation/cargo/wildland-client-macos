//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import ServiceManagement

final class SMAppServiceMock: SMAppService {

  // MARK: - Properties

  var registerError: Error?
  var unregisterError: Error?
  static var appService: SMAppServiceMock?
  private(set) static var loginItemCounter = Int.zero
  private(set) static var identifierSet: String?
  private(set) var registerCounter = Int.zero
  private(set) var unregisterCounter = Int.zero

  // MARK: - Mock

  override class func loginItem(identifier: String) -> SMAppServiceMock {
    loginItemCounter += 1
    identifierSet = identifier
    return appService ?? SMAppServiceMock()
  }

  override func register() throws {
    registerCounter += 1
    guard let registerError else { return }
    throw registerError
  }

  override func unregister() throws {
    unregisterCounter += 1
    guard let unregisterError else { return }
    throw unregisterError
  }

  // MARK: - Helpers

  static func cleanup() {
    loginItemCounter = .zero
    identifierSet = nil
    appService = nil
  }
}
