//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon

final class SettingsMock: SettingsType {

  // MARK: - Properties

  let startCargoOnLogin: Bool?
  let appearanceColorScheme: Settings.ColorThemeOption
  private(set) var updateColorSchemeCounter = Int.zero
  private(set) var optionSet: Settings.ColorThemeOption?
  private(set) var updateStartCargoOnLoginCounter = Int.zero
  private(set) var enableSet: Bool?
  private(set) var displayName: String?
  private(set) var shouldDisplayNameForSharing: Bool?
  private(set) var displayNameForSharingCounter = Int.zero
  private(set) var displayNameForSharingSet: Bool?

  // MARK: - Initialization

  init(startCargoOnLogin: Bool?, appearanceColorScheme: Settings.ColorThemeOption) {
    self.startCargoOnLogin = startCargoOnLogin
    self.appearanceColorScheme = appearanceColorScheme
  }

  // MARK: - Mock

  func updateColorScheme(with option: Settings.ColorThemeOption) {
    updateColorSchemeCounter += 1
    optionSet = option
  }

  func updateStartCargoOnLogin(_ enable: Bool) {
    updateStartCargoOnLoginCounter += 1
    enableSet = enable
  }

  func updateDisplayName(_ value: String) {
    displayName = value
  }

  func updateDisplayNameForSharing(_ enable: Bool) {
    shouldDisplayNameForSharing = enable
    displayNameForSharingCounter += 1
    displayNameForSharingSet = enable
  }
}
