//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import Combine

final class StoragesProviderMock: StoragesProviderType {

  // MARK: - Properties

  var expectedStorages: [Storage] = []
  var storagesError: Error?

  private(set) var getStoragesCounter = Int.zero
  private(set) var pathSet: String?
  private(set) var warningDismissedCounter = Int.zero
  private(set) lazy var thresholdExceededPublisher = thresholdExceededSubject.eraseToAnyPublisher()
  private let thresholdExceededSubject = PassthroughSubject<Int?, Never>()

  // MARK: - Stub

  func getStorages(path: String?) throws -> [Storage] {
    getStoragesCounter += 1
    pathSet = path
    if let storagesError { throw storagesError }
    return expectedStorages
  }

  func sendThresholdExceededEvent(_ percentUsage: Int?) {
    thresholdExceededSubject.send(percentUsage)
  }

  func warningDismissed() {
    warningDismissedCounter += 1
  }
}
