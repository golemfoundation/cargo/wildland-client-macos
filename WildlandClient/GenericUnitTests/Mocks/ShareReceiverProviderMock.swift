//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

final class ShareReceiverProviderMock: ShareReceiverProviderType {

  // MARK: - Properties

  var mountShareContainerError: Error?
  private(set) var mountShareContainerCounter = Int.zero
  private(set) var sharingMessageSet: SharingMessage?
  private(set) var pathSet: String?

  var previewMessage: PreviewShareMessage?
  var prepareSharePreviewMessageError: Error?
  private(set) var prepareSharePreviewMessageCounter = Int.zero

  // MARK: - Public

  func mountShareContainer(sharingMessage: SharingMessage, path: String) async throws {
    mountShareContainerCounter += 1
    sharingMessageSet = sharingMessage
    pathSet = path
    if let mountShareContainerError { throw mountShareContainerError }
  }

  func prepareSharePreviewMessage(sharingMessage: SharingMessage) async throws -> PreviewShareMessage {
    prepareSharePreviewMessageCounter += 1
    sharingMessageSet = sharingMessage
    if let prepareSharePreviewMessageError { throw prepareSharePreviewMessageError }
    guard let previewMessage else { throw ErrorStub.mockValueNotProvided }
    return previewMessage
  }
}
