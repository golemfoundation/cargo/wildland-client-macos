//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import CargoUI
import Combine
import WildlandCommon

final class ActivityMessageHandlerMock: ActivityMessageHandlerType {

  // MARK: - Properties

  lazy private(set) var activityMessagePublisher = activityMessageSubject.eraseToAnyPublisher()
  private let activityMessageSubject = PassthroughSubject<ActivityMessage?, Never>()

  lazy private(set) var updateStatePublisher = updateStateSubject.eraseToAnyPublisher()
  private let updateStateSubject = PassthroughSubject<UpdateState, Never>()

  private(set) var updateCounter = Int.zero
  private(set) var messageClosePressedCounter = Int.zero
  private(set) var messageSet: ActivityMessage?

  // MARK: - ActivityMessageHandlerType

  func update() {
    updateCounter += 1
  }

  func messageClosePressed(message: ActivityMessage) {
    messageClosePressedCounter += 1
    messageSet = message
  }

  // MARK: - Helpers

  func callUpdateState(_ updateState: UpdateState) {
    updateStateSubject.send(updateState)
  }

  func callActivityMessage(_ message: ActivityMessage?) {
    activityMessageSubject.send(message)
  }
}
