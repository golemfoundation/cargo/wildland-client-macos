//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import Sparkle
import Combine

final class UserDriverMock: NSObject, UserDriverType {

  // MARK: - Properties

  lazy private(set) var updateStatePublisher = updateStateSubject.eraseToAnyPublisher()
  private let updateStateSubject = PassthroughSubject<UpdateState, Never>()

  lazy private(set) var updateUserChoiceSubscriber = AnySubscriber(updateUserChoiceSubject)
  private let updateUserChoiceSubject = PassthroughSubject<UpdateUserChoice, Never>()

  lazy private(set) var canInstallAndRelaunchSubscriber = AnySubscriber(canInstallAndRelaunchSubject)
  private let canInstallAndRelaunchSubject = PassthroughSubject<Bool, Never>()

  private(set) var updateUserChoiceCounter = Int.zero
  private(set) var updateUserChoiceSet: UpdateUserChoice?

  private(set) var canInstallAndRelaunchCounter = Int.zero
  private(set) var canInstallAndRelaunchSet: Bool?

  private(set) var acknowledgementDismissedCounter = Int.zero

  private(set) var showCounter = Int.zero
  private(set) var requestSet: SPUUpdatePermissionRequest?

  private(set) var showUserInitiatedUpdateCheckCounter = Int.zero
  private(set) var cancellationSet: (() -> Void)?

  private(set) var showUpdateFoundCounter = Int.zero
  private(set) var appcastItemSet: SUAppcastItem?
  private(set) var stateSet: SPUUserUpdateState?

  private(set) var showUpdateReleaseNotesCounter = Int.zero
  private(set) var downloadDataSet: SPUDownloadData?

  private(set) var showUpdateReleaseNotesFailedErrorCounter = Int.zero
  private(set) var errorSet: Error?

  private(set) var showUpdateNotFoundWithErrorCounter = Int.zero
  private(set) var acknowledgementSet: (() -> Void)?

  private(set) var showUpdaterErrorCounter = Int.zero
  private(set) var showDownloadInitiatedCounter = Int.zero

  private(set) var showDownloadDidReceiveCounter = Int.zero
  private(set) var expectedContentLengthSet: UInt64?

  private(set) var showDownloadDidReceiveDataCounter = Int.zero
  private(set) var lengthSet: UInt64?

  private(set) var showExtractingUpdateDidStartCounter = Int.zero

  private(set) var showExtractionReceivedProgressCounter = Int.zero
  private(set) var progressSet: Double?

  private(set) var showReadyToInstallAndRelaunchCounter = Int.zero

  private(set) var showInstallingUpdateCounter = Int.zero
  private(set) var applicationTerminatedSet: Bool?
  private(set) var retryTerminatingApplicationSet: (() -> Void)?

  private(set) var showUpdateInstalledAndRelaunchedCounter = Int.zero
  private(set) var relaunchedSet: Bool?

  private(set) var showUpdateInFocusCounter = Int.zero
  private(set) var dismissUpdateInstallationCounter = Int.zero

  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  override init() {
    super.init()
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    updateUserChoiceSubject
      .sink { [weak self] updateUserChoice in
        self?.updateUserChoiceCounter += 1
        self?.updateUserChoiceSet = updateUserChoice
      }
      .store(in: &cancellables)

    canInstallAndRelaunchSubject
      .sink { [weak self] canInstallAndRelaunch in
        self?.canInstallAndRelaunchCounter += 1
        self?.canInstallAndRelaunchSet = canInstallAndRelaunch
      }
      .store(in: &cancellables)
  }

  // MARK: - Mock

  func acknowledgementDismissed() {
    acknowledgementDismissedCounter += 1
  }

  // MARK: - SPUUserDriver

  func show(_ request: SPUUpdatePermissionRequest) async -> SUUpdatePermissionResponse {
    showCounter += 1
    requestSet = request
    return SUUpdatePermissionResponse(
      automaticUpdateChecks: true,
      automaticUpdateDownloading: nil,
      sendSystemProfile: false
    )
  }

  func showUserInitiatedUpdateCheck(cancellation: @escaping () -> Void) {
    showUserInitiatedUpdateCheckCounter += 1
    cancellationSet = cancellation
  }

  func showUpdateFound(with appcastItem: SUAppcastItem, state: SPUUserUpdateState) async -> SPUUserUpdateChoice {
    showUpdateFoundCounter += 1
    appcastItemSet = appcastItem
    stateSet = state
    return .install
  }

  func showUpdateReleaseNotes(with downloadData: SPUDownloadData) {
    showUpdateReleaseNotesCounter += 1
    downloadDataSet = downloadData
  }

  func showUpdateReleaseNotesFailedToDownloadWithError(_ error: Error) {
    showUpdateReleaseNotesFailedErrorCounter += 1
    errorSet = error
  }

  func showUpdateNotFoundWithError(_ error: Error, acknowledgement: @escaping () -> Void) {
    showUpdateNotFoundWithErrorCounter += 1
    errorSet = error
    acknowledgementSet = acknowledgement
  }

  func showUpdaterError(_ error: Error, acknowledgement: @escaping () -> Void) {
    showUpdaterErrorCounter += 1
    errorSet = error
    acknowledgementSet = acknowledgement
  }

  func showDownloadInitiated(cancellation: @escaping () -> Void) {
    showDownloadInitiatedCounter += 1
    cancellationSet = cancellation
  }

  func showDownloadDidReceiveExpectedContentLength(_ expectedContentLength: UInt64) {
    showDownloadDidReceiveCounter += 1
    expectedContentLengthSet = expectedContentLength
  }

  func showDownloadDidReceiveData(ofLength length: UInt64) {
    showDownloadDidReceiveDataCounter += 1
    lengthSet = length
  }

  func showDownloadDidStartExtractingUpdate() {
    showExtractingUpdateDidStartCounter += 1
  }

  func showExtractionReceivedProgress(_ progress: Double) {
    showExtractionReceivedProgressCounter += 1
    progressSet = progress
  }

  func showReadyToInstallAndRelaunch() async -> SPUUserUpdateChoice {
    showReadyToInstallAndRelaunchCounter += 1
    return .install
  }

  func showInstallingUpdate(withApplicationTerminated applicationTerminated: Bool, retryTerminatingApplication: @escaping () -> Void) {
    showInstallingUpdateCounter = 1
    applicationTerminatedSet = applicationTerminated
    retryTerminatingApplicationSet = retryTerminatingApplication
  }

  func showUpdateInstalledAndRelaunched(_ relaunched: Bool, acknowledgement: @escaping () -> Void) {
    showUpdateInstalledAndRelaunchedCounter += 1
    relaunchedSet = relaunched
    acknowledgementSet = acknowledgement
  }

  func showUpdateInFocus() {
    showUpdateInFocusCounter += 1
  }

  func dismissUpdateInstallation() {
    dismissUpdateInstallationCounter += 1
  }

  // MARK: - Helpers

  func callUpdateState(_ state: UpdateState) {
    updateStateSubject.send(state)
  }
}
