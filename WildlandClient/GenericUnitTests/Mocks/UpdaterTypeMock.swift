//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@testable import WildlandCommon
import Combine
import Sparkle

final class UpdaterTypeMock: UpdaterType {

  // MARK: - Properties

  lazy private(set) var canCheckForUpdatesPublisher = canCheckForUpdatesSubject.eraseToAnyPublisher()
  private let canCheckForUpdatesSubject = PassthroughSubject<Bool, Never>()

  private(set) var initCounter = Int.zero
  private(set) var hostBundleSet: Bundle?
  private(set) var applicationBundleSet: Bundle?
  private(set) var userDriverSet: SPUUserDriver?
  private(set) var delegateSet: SPUUpdaterDelegate?
  private(set) var checkForUpdatesCounter = Int.zero
  private(set) var startCounter = Int.zero

  static private(set) weak var updater: UpdaterTypeMock?

  // MARK: - Initialization

  init(hostBundle: Bundle, applicationBundle: Bundle, userDriver: SPUUserDriver, delegate: SPUUpdaterDelegate?) {
    initCounter += 1
    hostBundleSet = hostBundle
    applicationBundleSet = applicationBundle
    userDriverSet = userDriver
    delegateSet = delegate

    Self.updater = self
  }

  // MARK: - Mock

  func checkForUpdates() {
    checkForUpdatesCounter += 1
  }

  func start() throws {
    startCounter += 1
  }

  // MARK: - Helpers

  func callCanCheckForUpdates(_ canCheckUpdate: Bool) {
    canCheckForUpdatesSubject.send(canCheckUpdate)
  }
}
