//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

// swiftlint:disable line_length
// swiftlint:disable type_name

import Foundation

public enum WLStrings {
  enum button {
    static let cancel = "Cancel"
    static let next = "Next"
    static let back = "Back"
    static let generate = "Generate"
    static let quickTour = "Quick tour"
    static let getCode = "Get code"
    static let ok = "OK"
  }

  enum onboarding {
    enum setupCargo {
      static let title = "Set up Wildland Cargo"
      static let description = "To get started, you can create a brand new Wildland identity or add another device to a previously created identity."

      enum itemCreateNew {
        static let title = "Create a new Wildland identity"
        static let description = "Choose this option if you are new to Cargo"
      }

      enum itemLinkExisting {
        static let title = "Link device to an existing identity"
        static let description = "Choose this option to install Cargo on another device"
      }
    }

    enum aboutWildlandIdentities {
      static let title = "About Wildland identities"
      static let description = """
      You can create a Wildland identity and securely store a secret phrase to manage your identity.\n
      Alternatively, if you're familiar with Ethereum, you can use your wallet to create and manage your identity.
      """
    }

    enum createIdentity {
      static let title = "Create a Wildland identity"
      static let description = "Generate and securely store a 12 word secret phrase to secure your Wildland Cargo identity."

      enum itemWildland {
        static let title = "Use Wildland for identity"
        static let description = "Generate and store a secret phrase "
      }

      enum itemEthereum {
        static let title = "Use Ethereum for identity"
        static let description = "Derive an identity from a public key"
      }
    }

    enum chooseIdentity {
      static let title = "Manage your Wildland identity"
      static let description = "You can manage Wildland Cargo with a Wildland secret phrase or use your Ethereum wallet for identity."

      enum itemWildland {
        static let title = "Use Wildland identity"
        static let description = "Create a new identity or link this device"
      }

      enum itemEthereum {
        static let title = "Use Ethereum identity"
        static let description = "Create a new identity or link this device"
      }
    }

    enum wildlandIntro {
      static let title = "Manage your Wildland identity"
      static let description = "Create a new secret phrase to secure your Wildland Cargo, or link this device to an existing Wildland identity."

      enum itemCreateNewIdentity {
        static let title = "Create a new Wildland identity"
        static let description = "Generate a new secret phrase now"
      }

      enum itemLinkIdentityToDevice {
        static let title = "Link your identity to this device"
        static let description = "Set up this device using an existing identity"
      }
    }

    enum wildlandSecretPhaseIntro {
      static let newUserTitle = "Keep your secret phrase safe"
      static let newUserDescription = "On the next screen Cargo will generate and display your 12 word secret phrase."
      static let newUserText = "Please do not view your phrase in a public place or where someone else can see your screen."

      static let existingUserTitle = newUserTitle
      static let existingUserDescription = "On the next screen your 12 word secret phrase may be visible on screen."
      static let existingUserText = "Please do not enter your phrase in a public place or where someone else can see your screen "
    }

    enum newWildlandWords {
      static let title = "Secure your 12 word passphrase"
      static let description = "The secret phrase below is the master key to your Wildland Cargo.\nThe Wildland team cannot recover it, so write it down and keep it safe."
      static let buttonGenerate = "Confirm words"
      static let buttonDownload = "Download as text file"
      static let checkbox = "I have written down or otherwise securely stored my secret phrase"
    }

    enum comfirmWildlandWords {
      static let title = "Confirm your 12 word passphrase"
      static let description = "The phrase below is your key to Wildland Cargo. The Wildland team cannot recover it, so write it down and keep it safe. Click the highlighted tiles until you have seen and written down all 12 words."
      static let buttonForgot = "I forgot my words"
    }

    enum enterWildlandWords {
      static let title = "Enter your 12 word secret phrase"
      static let description = "To link this device to your Wildland identity, please enter your secret phrase. Paste from the clipboard or enter each word in order."
      static let infoVerified = "Secret phrase verified"
      static let infoVerificationFailed = "Secret phrase incorrect. Please try again"
      static let buttonVerify = "Verify phrase"
      static let buttonGetStarted = "Get started"
    }

    enum setYourOwnStorage {
      static let title = "Set up your own storage"
      static let descriptionBottomTop = "If you want to use a specific storage\nsolution like S3, IPFS, or Dropbox,\nyou can do so with via the Terminal."
      static let descriptionBottomStart = "See"
      static let descriptionBottomLink = "this page"
      static let descriptionBottomEnd = "in the Wildland docs."
    }

    enum nameYourDevice {
      static let title = "Name your device"
      static let description = "You can give your device a memorable name for use in Cargo or just use the device’s current name."
    }

    enum chooseCargoStorage {
      static let title = "Choose your Cargo storage"
      static let description = "You can either use a free storage tier sponsored by the Golem Foundation or set up your own storage backend."
      static let agreeCheckboxTitle = "I agree to the free tier"
      static let agreeCheckboxLink = "terms & conditions"

      enum itemFreeFoundationTier {
        static let title = "Free Foundation tier"
        static let description = "500GB, requires email verification"
      }

      enum itemSelfSetup {
        static let title = "Self set up"
        static let description = "Requires Terminal familiarity"
      }
    }

    enum getVerificationCode {
      static let title = "Get a verification code"
      static let description = "Please enter your email address so we can verify you are human in order to give you the free Cargo storage tier. "
      static let tip = "We won’t use your email for anything except for sending you a code to verify you are not a bot."
      static let invalidEmail = "This doesn’t look like a valid email address"
    }

    enum enterVerificationCode {
      static let title = "Enter your verification code"
      static let description = "Please enter the 10 digit verification code. If you didn’t get a code, please check your spam folder before requesting a new code."
      static let infoVerified = "Code verified"
      static let infoVerificationFailed = "Code incorrect. Please try again"
      static let resendCodeText = "Didn’t get an email?"
      static let resendCodeLink = "Resend verification code"

      enum alertInvalidCode {
        static let title = "Invalid code"
        static let message = "Please, check is the code is correct"
      }

      enum alertStorageCreated {
        static let title = "Storage was created"
        static let message = "Please enjoy the best storage service"
      }

      enum alertСodeResent {
        static let title = "Resend request was sent"
        static let message = "Please check your email"
      }
    }
  }
}

// swiftlint:enable line_length
// swiftlint:enable type_name
