//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol AccessiblityIdentifiable {
  func identifier(_ commonComponentIdentifier: CommonComponentIdentifier, value: String?) -> String
  func identifier(_ componentIdentifier: String, value: String?) -> String
}

extension AccessiblityIdentifiable {

  func identifier(_ commonComponentIdentifier: CommonComponentIdentifier, value: String? = nil) -> String {
    identifier(commonComponentIdentifier.rawValue, value: value)
  }

  func identifier(_ componentIdentifier: String, value: String? = nil) -> String {
    [
      String(describing: Self.self),
      componentIdentifier,
      value
    ]
      .compactMap { $0?.firstUppercased }
      .joined(separator: ".")
  }
}
