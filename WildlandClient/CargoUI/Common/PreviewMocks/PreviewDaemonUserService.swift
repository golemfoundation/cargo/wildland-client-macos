//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

#if DEBUG

final class PreviewDaemonUserService: DaemonUserService {
  var availabilityState: UserAvailabilityState { .readyForUse }
  var rootCargoUrl: URL { URL(filePath: "") }
  func createUser(with deviceName: String) async throws { }
  func requestFreeTierStorage(for email: String) async throws { }
  func verifyEmail(with code: String) async throws { }
  func createMnemonic(using words: [String]) async -> Result<Void, Error> { .success(()) }
  func generateMnemonic() async throws -> [String] { [] }
  func setTheme(_ themeOption: Settings.ColorThemeOption) async throws { }
}

#endif
