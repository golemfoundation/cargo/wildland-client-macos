//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

extension View {
  func adjustHeight(maxHeight: CGFloat? = nil) -> some View {
    modifier(HeightAdjustingModifier(maxHeight: maxHeight))
  }
}

private struct HeightAdjustingModifier: ViewModifier {

  // MARK: - Properties

  @State var height: CGFloat = 0
  let maxHeight: CGFloat?

  // MARK: - ViewModifier

  func body(content: Content) -> some View {
    content
      .onPreferenceChange(HeightPreferenceKey.self) { value in
        self.height = value
      }
      .frame(height: min(height, maxHeight ?? height))
  }
}
