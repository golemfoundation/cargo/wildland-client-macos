//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

/// A `View` extension to handle copy and paste via the system clipboard.
/// This extension is useful in contexts where standard copy-paste shortcuts do not work, such as in a daemon.
extension View {
  func pasteboard(text: Binding<String>) -> some View {
    modifier(CaptureKeyEventsModifier(text: text))
  }
}

private struct CaptureKeyEventsModifier: ViewModifier {

  // MARK: - Properties

  @Binding private var text: String

  // MARK: - Initialization

  init(text: Binding<String>) {
    self._text = text
  }

  // MARK: - Body

  func body(content: Content) -> some View {
    content
      .background(
        CaptureView(sharedText: $text)
      )
  }
}

private struct CaptureView: NSViewRepresentable {

  // MARK: - Properties

  @Binding private var sharedText: String

  // MARK: - Initialization

  init(sharedText: Binding<String>) {
    _sharedText = sharedText
  }

  // MARK: - Public

  func makeNSView(context: Context) -> PlaceholderNSTextField {
    let textField = PlaceholderNSTextField(sharedText: $sharedText)
    textField.isEditable = false
    textField.isBezeled = false
    textField.backgroundColor = .clear
    return textField
  }

  func updateNSView(_ nsView: PlaceholderNSTextField, context: Context) {}
}

private final class PlaceholderNSTextField: NSTextField {

  // MARK: - Properties

  private let sharedText: Binding<String>
  private let pasteboardProvider: PasteboardProviderType

  // MARK: - Initialization

  init(sharedText: Binding<String>, pasteboardProvider: PasteboardProviderType = NSPasteboard.general) {
    self.sharedText = sharedText
    self.pasteboardProvider = pasteboardProvider
    super.init(frame: .zero)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Public

  override func performKeyEquivalent(with event: NSEvent) -> Bool {
    let eventFlag = event.modifierFlags.rawValue & NSEvent.ModifierFlags.deviceIndependentFlagsMask.rawValue
    guard eventFlag == NSEvent.ModifierFlags.command.rawValue,
          let characters = event.charactersIgnoringModifiers?.lowercased() else { return false }

    switch characters {
    case "c":
      pasteboardProvider.copy(string: sharedText.wrappedValue)
    case "v":
      if let pasteboardContent = pasteboardProvider.paste() {
        sharedText.wrappedValue = pasteboardContent
      }
    default:
      return false
    }

    return true
  }
}
