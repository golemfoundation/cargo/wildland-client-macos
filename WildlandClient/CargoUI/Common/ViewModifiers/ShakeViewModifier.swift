//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/// A custom `ViewModifier` that adds a shaking animation to a view.
struct ShakeViewModifier: ViewModifier {
  /// A binding to a `CGFloat` value that controls the amount of shake.
  @Binding var animationAmount: CGFloat

  init(animationAmount: Binding<CGFloat>) {
    _animationAmount = animationAmount
  }

  func body(content: Content) -> some View {
    content
      .modifier(Shake(animatableData: animationAmount))
      .animation(.linear(duration: 0.5), value: animationAmount)
  }
}

private struct Shake: GeometryEffect {

  let amount: CGFloat = 8
  let shakesPerUnit = 4
  var animatableData: CGFloat

  func effectValue(size: CGSize) -> ProjectionTransform {
    ProjectionTransform(
      CGAffineTransform(
        translationX: amount * sin(animatableData * .pi * CGFloat(shakesPerUnit)), y: 0
      )
    )
  }
}
