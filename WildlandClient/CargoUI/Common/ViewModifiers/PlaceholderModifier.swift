//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

extension View {
  func placeholder(animate: Bool, isRedacted: Bool = false) -> some View {
    modifier(
      PlaceholderModifier(
        animate: animate,
        isRedacted: isRedacted
      )
    )
  }
}

private struct PlaceholderModifier: ViewModifier {

  // MARK: - Properties

  let animate: Bool
  let isRedacted: Bool
  private let shift = 0.3
  private let animation = Animation.linear(duration: 2.0).repeatForever(autoreverses: false)
  @State private var isAnimating = false

  private let gradient = Gradient(colors: [
    .white.opacity(0.3),
    .white,
    .white.opacity(0.3)
  ])

  private var startPoint: UnitPoint {
    isAnimating ? UnitPoint(x: 1.0, y: 1.0) : UnitPoint(x: -shift, y: -shift)
  }

  private var endPoint: UnitPoint {
    isAnimating ? UnitPoint(x: 1.0 + shift, y: 1.0 + shift) : UnitPoint(x: .zero, y: .zero)
  }

  private var linearGradient: LinearGradient {
    LinearGradient(
      gradient: gradient,
      startPoint: startPoint,
      endPoint: endPoint
    )
  }

  // MARK: - ViewModifier

  func body(content: Content) -> some View {
    if animate {
      content
        .redacted(reason: .placeholder)
        .mask(linearGradient)
        .onAppear {
          withAnimation(animation) {
            isAnimating = true
          }
        }
    } else {
      content
        .redacted(reason: isRedacted ? .placeholder : [])
        .onAppear {
          isAnimating = false
        }
    }
  }
}
