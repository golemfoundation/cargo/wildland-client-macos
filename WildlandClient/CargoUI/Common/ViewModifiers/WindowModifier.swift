//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

public extension View {

  // A View modifier that provides the ability to access its window from the environment.
  func window() -> some View {
    modifier(WindowModifier())
  }

  // A View modifier that triggers an action when the View's window is closing.
  // This could be triggered either programmatically or manually by a user closing the window.
  func onWindowClose(perform action: @escaping (NSWindow?) -> Void) -> some View {
    modifier(WindowModifier(onClose: action))
  }
}

private struct WindowModifier: ViewModifier {

  // MARK: - Properties

  @State var window: () -> NSWindow? = { nil }
  let onClose: ((NSWindow?) -> Void)?

  // MARK: - Initialization

  init(onClose: ((NSWindow?) -> Void)? = nil) {
    self.onClose = onClose
  }

  func body(content: Content) -> some View {
    content
      .background(
        WindowObserver(binding: $window, onClose: onClose)
      )
      .environment(\.window, window)
  }
}

private struct WindowObserver: NSViewRepresentable {

  // MARK: - Properties

  private let windowAccessorView: WindowAccessorView
  private let onClose: ((NSWindow?) -> Void)?

  // MARK: - Initialization

  init(binding: Binding<() -> NSWindow?>, onClose: ((NSWindow?) -> Void)? = nil) {
    self.onClose = onClose
    windowAccessorView = WindowAccessorView(binding: binding)
  }

  // MARK: - Public

  func makeNSView(context: Context) -> NSView {
    windowAccessorView
  }

  func updateNSView(_ nsView: NSView, context: Context) {
    nsView.window?.shiftWindowButtonsAsync()
    nsView.window?.delegate = context.coordinator
  }

  func makeCoordinator() -> WindowDelegate {
    WindowDelegate(onClose)
  }
}

private class WindowDelegate: NSObject, NSWindowDelegate {

  // MARK: - Private

  private let closeAction: ((NSWindow?) -> Void)?

  // MARK: - Initialization

  init(_ onClose: ((NSWindow?) -> Void)?) {
    self.closeAction = onClose
  }

  // MARK: - NSWindowDelegate

  func windowWillClose(_ notification: Notification) {
    closeAction?(notification.object as? NSWindow)
  }
}

private class WindowAccessorView: NSView {

  // MARK: - Properties

  @Binding private var windowBinding: () -> NSWindow?
  private var isWindowsSet: Bool = false

  // MARK: - Initialization

  init(binding: Binding<() -> NSWindow?>) {
    self._windowBinding = binding
    super.init(frame: .zero)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Public

  override func viewWillMove(toWindow newWindow: NSWindow?) {
    super.viewWillMove(toWindow: newWindow)
    guard newWindow != nil else { return }
    setupWindowsIfNotSet()
  }

  // MARK: - Private

  private func setupWindowsIfNotSet() {
    guard !isWindowsSet else { return }
    isWindowsSet = true

    windowBinding = { [weak self] in
      self?.window
    }
  }
}

private extension NSWindow {
  func shiftWindowButtonsAsync() {
    Task { [weak self] in
      self?.shiftWindowButtons()
    }
  }
}
