//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

public extension View {
  func colorScheme() -> some View {
    modifier(ColorSchemeViewModifier())
  }
}

private struct ColorSchemeViewModifier: ViewModifier {

  @AppStorage(AppStorageIdentifiers.isDarkMode.rawValue, store: .sharedDefaults) var isDarkMode: Bool?
  @Environment(\.colorScheme) var colorScheme

  public func body(content: Content) -> some View {
    if let isDarkMode = isDarkMode {
      let scheme: ColorScheme = isDarkMode ? .dark : .light
      content
        .preferredColorScheme(scheme)
        .environment(\.colorScheme, scheme)
    } else {
      if NSApp.effectiveAppearance.bestMatch(from: [.darkAqua]) != nil {
        content
          .environment(\.colorScheme, .dark)
      } else if NSApp.effectiveAppearance.bestMatch(from: [.aqua]) != nil {
        content
          .environment(\.colorScheme, .light)
      } else {
        content
          .preferredColorScheme(nil)
      }
    }
  }

  public init() {}
}
