//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

public extension EnvironmentValues {
   var appInfo: AppInfo {
     get { self[AppInfoKey.self] }
     set { self[AppInfoKey.self] = newValue }
   }
 }

struct AppInfoKey: EnvironmentKey {
    public static let defaultValue: AppInfo = AppInfo()
 }

public struct AppInfo {

  // MARK: - Properties

  @InfoDictionaryValue(key: "ClientCommitHash") public var commitHash
  @InfoDictionaryValue(key: "ClientBuildTimestamp") public var buildTimestamp
  @InfoDictionaryValue(key: "SDKVersion") public var sdkVersion
  @InfoDictionaryValue(key: "SDKCommitHash") public var sdkCommitHash
  @InfoDictionaryValue(key: "CFBundleName") public var appName
  @InfoDictionaryValue(key: "CFBundleShortVersionString") public var appVersion

  public let copyrightKey = WLStrings.aboutPanel.about.copyrightKey
  public let copyrightValue = WLStrings.aboutPanel.about.copyrightValue

  public var aboutAppInformation: String {
    """
    \(WLStrings.aboutPanel.about.commitKey): \(commitHash.prefix(7))
    \(WLStrings.aboutPanel.about.buildTimeKey): \(buildTimestamp)
    \(WLStrings.aboutPanel.about.sdkVersionKey): \(sdkVersion)
    \(WLStrings.aboutPanel.about.sdkCommitKey): \(sdkCommitHash.prefix(7))
    """
  }

  // MARK: - Initialization

  public init(bundle: Bundle = .main) {
    _commitHash.bundle = bundle
    _buildTimestamp.bundle = bundle
    _sdkVersion.bundle = bundle
    _sdkCommitHash.bundle = bundle
    _appVersion.bundle = bundle
  }
}
