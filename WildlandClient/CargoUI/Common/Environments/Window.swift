//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

// This extension adds a new environment value to the `EnvironmentValues` type.
// It uses the `WindowKey` type as a key to associate the `window` property with the environment value.
// It adds a computed property `window` to `EnvironmentValues`, which you can use to get or set the `NSWindow` instance associated with the current environment.
// Note that the `window` property will return nil unless you use the `.window()` modifier on a view.
public extension EnvironmentValues {

  // MARK: - Properties

  var window: () -> NSWindow? {
    get { self[WindowKey.self] }
    set { self[WindowKey.self] = newValue }
  }
}

private struct WindowKey: EnvironmentKey {

  // MARK: - Properties

  public static let defaultValue: () -> NSWindow? = { nil }
}
