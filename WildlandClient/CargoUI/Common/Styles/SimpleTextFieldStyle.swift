//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct SimpleTextFieldStyle: TextFieldStyle {

  // MARK: - Properties

  var placeholder: String
  var isVisiblePlaceholder: Bool
  var isInvalid: Bool = false
  var isFocusedOnAppear: Bool = true
  var overlayColor: Color = Colors.mint.swiftUIColor
  var backgroundColor: Color = Color.Onboarding.backgroundForm
  private let horizontalPadding = 10.0
  @FocusState private var focused: Bool

  // MARK: - TextFieldStyle

  func _body(configuration: TextField<_Label>) -> some View {
    configuration
      .textFieldStyle(PlainTextFieldStyle())
      .focused($focused)
      .font(.bodyText)
      .foregroundColor(.text)
      .frame(minHeight: 38)
      .padding(.horizontal, horizontalPadding)
      .background(background)
      .overlay(textFieldOverlay)
      .placeholder(isVisible: isVisiblePlaceholder) { placeholderView }
      .onAppear { focused = isFocusedOnAppear }
  }

  @ViewBuilder private var textFieldOverlay: some View {
    let color = isInvalid ? Colors.warning.swiftUIColor : overlayColor
    RoundedRectangle(cornerRadius: 4).stroke(color, lineWidth: 1)
  }

  private var background: some View {
    backgroundColor
      .cornerRadius(4.0)
  }

  private var placeholderView: some View {
    Text(placeholder)
      .font(.bodyText)
      .foregroundColor(Color(light: Colors.cloud, dark: Colors.granite))
      .padding(.horizontal, horizontalPadding)
  }
}
