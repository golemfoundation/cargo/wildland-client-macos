//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/// A custom `ButtonStyle` for a mnemonic passphrase
struct MnemonicButtonStyle: ButtonStyle {

  func makeBody(configuration: Configuration) -> some View {
    PassphraseButton(configuration: configuration)
  }

  private struct PassphraseButton: View {
    let configuration: MnemonicButtonStyle.Configuration

    var body: some View {
      HStack {
        configuration.label
          .padding(.leading, 16.0)
          .font(.textMMedium)
          .foregroundColor(.text)
        Spacer()
      }
      .frame(height: 32)
      .background(background)
    }

    private var background: some View {
      Color.Onboarding.backgroundBox
        .opacity(configuration.isPressed ? 1.0 : 0.8)
        .cornerRadius(4.0)
    }
  }
}
