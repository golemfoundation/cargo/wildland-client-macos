//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

public protocol Presentable {
  init(@ViewBuilder view: () -> any View, isPresented: Binding<Bool>, configuration: WindowConfiguration)
}

public struct WindowConfiguration {
  let contentRect: NSRect
  let styleMask: NSWindow.StyleMask
  let `defer`: Bool
}

extension WindowConfiguration {
  static let `default` = WindowConfiguration(
    contentRect: .zero,
    styleMask: [
      .nonactivatingPanel,
      .titled,
      .resizable,
      .closable,
      .fullSizeContentView
    ],
    defer: false
  )
}
