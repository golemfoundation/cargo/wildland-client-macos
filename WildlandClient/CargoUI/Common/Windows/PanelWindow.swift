//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa
import SwiftUI
import Combine

public final class PanelWindow: NSPanel, Presentable {

  // MARK: - Properties

  @Binding var isPresented: Bool

  // MARK: - Initialization

  convenience public init(view: () -> any View, isPresented: Binding<Bool>, configuration: WindowConfiguration) {
    self.init(view: view, contentRect: configuration.contentRect, isPresented: isPresented)
  }

  public init(view: () -> any View,
              contentRect: NSRect,
              backing: NSWindow.BackingStoreType = .buffered,
              defer flag: Bool = false,
              isPresented: Binding<Bool>) {
    _isPresented = isPresented
    super.init(contentRect: contentRect,
               styleMask: [.nonactivatingPanel,
                           .titled,
                           .resizable,
                           .closable,
                           .fullSizeContentView],
               backing: backing,
               defer: flag
    )
    isFloatingPanel = true
    animationBehavior = .alertPanel
    contentView = NSHostingView(rootView: view().eraseToAnyView())
    center()
  }

  // MARK: - Public

  public override func layoutIfNeeded() {
    super.layoutIfNeeded()
    shiftWindowButtons()
  }

  public override func close() {
    super.close()
    isPresented = false
  }
}
