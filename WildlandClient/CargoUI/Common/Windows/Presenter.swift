//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import Combine

/// Class that serves as a bridge between SwiftUI views and an AppKit environment. It acts as a presenter for an alert.
///
/// Note: While `PanelWindow` is a subclass of `NSPanel` and could be used directly with SwiftUI via its binding,
/// `Presenter` enables the use of `PanelWindow` (which is designed for SwiftUI) within a AppKit-based application.
final public class Presenter<Window: NSWindow & Presentable> {

  // MARK: - Properties

  lazy public private(set) var dismissPublisher = dismissSubject.eraseToAnyPublisher()
  private var view: (() -> any View)?
  private let dismissSubject = PassthroughSubject<Void, Never>()
  private var configuration: WindowConfiguration?

  public var isPresented: Binding<Bool> {
    Binding(
      get: { self.view != nil },
      set: { newValue in
        if newValue, let content = self.view {
          let alert = Window(
            view: content,
            isPresented: self.isPresented,
            configuration: self.configuration ?? .default
          )
          alert.makeKeyAndOrderFront(nil)
        } else {
          self.view = nil
          self.dismissSubject.send()
        }
      }
    )
  }

  // MARK: - Initialization

  public init() {}

  // MARK: - Public

  public func present(configuration: WindowConfiguration? = nil, _ view: @escaping () -> any View) {
    self.configuration = configuration
    self.view = view
    self.isPresented.wrappedValue = true
  }
}
