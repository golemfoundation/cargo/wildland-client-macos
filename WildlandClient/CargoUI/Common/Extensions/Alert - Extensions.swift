//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

extension Alert {
  static func genericErrorAlert(_ text: String) -> Alert {
    var message = WLStrings.Alert.errorGenericMessage
    #if DEBUG
    message += " \(text)"
    #endif
    return Alert(
      title: Text(WLStrings.Alert.errorGenericTitle),
      message: Text(message),
      dismissButton: .default(Text(WLStrings.button.ok))
    )
  }

  static func connectivityErrorAlert(retryAction: @escaping () -> Void) -> Alert {
    Alert(
      title: Text(WLStrings.Alert.connectivityTitle),
      message: Text(WLStrings.Alert.connectivityMessage),
      primaryButton: .default(Text(WLStrings.Alert.retryButtonTitle), action: retryAction),
      secondaryButton: .cancel(Text(WLStrings.button.ok))
    )
  }
}
