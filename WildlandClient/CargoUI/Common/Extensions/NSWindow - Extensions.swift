//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit.NSWindow
import SwiftUI

extension NSWindow {
  convenience init<Content: View>(_ rootView: Content) {
    self.init()
    contentView = NSHostingView(rootView: rootView)
  }

  public func shiftWindowButtons() {
    offsetWindowsButtonsBy(NSPoint(x: 15, y: -1), space: 20.0)
  }

  private func offsetWindowsButtonsBy(_ newOffset: NSPoint, space: Double) {
    standardWindowButton(.closeButton)?.setFrameOrigin(NSPoint(x: newOffset.x, y: newOffset.y))
    standardWindowButton(.miniaturizeButton)?.setFrameOrigin(NSPoint(x: newOffset.x + space, y: newOffset.y))
    standardWindowButton(.zoomButton)?.setFrameOrigin(NSPoint(x: newOffset.x + 2 * space, y: newOffset.y))
  }
}
