//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

public extension View {

  func `if`<Content: View>(_ conditional: Bool, content: (Self) -> Content) -> some View {
    if conditional {
      return AnyView(content(self))
    } else {
      return AnyView(self)
    }
  }

  func ifLet<Content: View, OptionalValue>(_ optionalValue: OptionalValue?, content: (Self, OptionalValue) -> Content) -> some View {
    if let optionalValue {
      return AnyView(content(self, optionalValue))
    } else {
      return AnyView(self)
    }
  }

  func hovered(_ hovered: Binding<Bool>, color: Color) -> some View {
    onHover { hovered.wrappedValue = $0 }
      .background(hovered.wrappedValue ? color : Color.clear)
  }

  func setProperty<T>(_ keyPath: WritableKeyPath<Self, T>, _ value: T) -> Self {
    var copy = self
    copy[keyPath: keyPath] = value
    return copy
  }

  func setProperty<V: View>(_ keyPath: WritableKeyPath<Self, AnyView?>, @ViewBuilder body: @escaping () -> V) -> Self {
    setProperty(keyPath, AnyView(LazyView(content: body)))
  }

  func eraseToAnyView() -> AnyView {
    AnyView(self)
  }
}

extension View {
  func shake(count: Binding<CGFloat>) -> some View {
    modifier(ShakeViewModifier(animationAmount: count))
  }
}

public extension Image {
  func resized(to dimension: CGFloat) -> some View {
    resizable()
      .scaledToFit()
      .frame(width: dimension, height: dimension)
  }
}

public extension View {

  func alert<T>(using value: Binding<T?>, content: (T) -> Alert) -> some View {
    let isPresented = Binding<Bool>(
      get: { value.wrappedValue != nil },
      set: { _ in value.wrappedValue = nil }
    )
    return alert(isPresented: isPresented) {
      content(value.wrappedValue!)
    }
  }

  func placeholder<Content: View>(isVisible: Bool, @ViewBuilder placeholder: () -> Content) -> some View {
    overlay(alignment: .leading) {
      placeholder()
        .opacity(isVisible ? 1.0 : .zero)
    }
  }
}
