//
// Wildland Project
// Wildland CargoUI
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

public extension Color {
  enum Modal {
    public static let windowBackground = Color(light: Colors.pureWhite, dark: Colors.gloom)
    public static let text = Color(light: Colors.mainText, dark: Colors.cloud)
  }
}

public extension Color {
  enum Activity {
    public static let background = Color(light: Colors.snow, dark: Colors.obsidian)
    static let contentBackground = Color(light: Colors.pureWhite, dark: Colors.styx)
    static let subtitle = Color(light: Colors.silver, dark: Colors.cloud)
    static let headerButtonBackground = Color(light: Colors.sleet, dark: Colors.onyx)
    static let popoverBackground = Color(light: Colors.night, dark: Colors.pureWhite)
    static let popoverTitleBackground = Color(light: Colors.pureWhite, dark: Colors.night)
  }
}

extension Color {
  enum Onboarding {
    static let background = Color(light: Colors.snow, dark: Colors.obsidian)
    static let backgroundForm = Color(light: Colors.pureWhite, dark: Colors.gloom)
    static let backgroundBox = Color(light: Colors.dust, dark: Colors.gloom)
  }
}

public extension Color {
  enum Preferences {
    public static let itemBackground = Color(light: Colors.pureWhite, dark: Colors.gloom)
    public static let itemBorder = Color(light: Colors.dust, dark: Colors.dusk)
  }
}
