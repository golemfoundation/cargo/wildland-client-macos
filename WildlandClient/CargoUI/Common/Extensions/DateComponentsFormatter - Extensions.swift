//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public extension DateComponentsFormatter {

  convenience init(units: NSCalendar.Unit, unitsStyle: UnitsStyle) {
    self.init()
    self.allowedUnits = units
    self.unitsStyle = unitsStyle
  }

  static let estimateFormatter = DateComponentsFormatter(
    units: [.hour, .minute, .second],
    unitsStyle: .short
  )
}
