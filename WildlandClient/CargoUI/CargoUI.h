//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#import <Foundation/Foundation.h>

//! Project version number for WildlandUI.
FOUNDATION_EXPORT double WildlandUIVersionNumber;

//! Project version string for WildlandUI.
FOUNDATION_EXPORT const unsigned char WildlandUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WildlandUI/PublicHeader.h>

