//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

#if DEBUG
final class ActivityViewModelStub: ActivityViewModelType, ActivityFooterViewModel {

  // MARK: - Properties

  var storages: [StorageItem]
  var isExtended: Bool
  var isCopyPopoverPresented: Bool
  var progress: Double
  var formattedMetrics: String?
  var synchronizationTitle: String
  var synchronizationFiles: String?
  var activityMessage: ActivityMessage?
  var copyPublicKeyPopoverState: ButtonCopyPopoverState
  var updateState: UpdateState

  // MARK: - Initialization

  init(storages: [StorageItem] = [],
       isExtended: Bool = false,
       isCopyPopoverPresented: Bool = false,
       progress: Double = .zero,
       formattedMetrics: String? = nil,
       synchronizationTitle: String = "",
       synchronizationFiles: String? = nil,
       activityMessage: ActivityMessage? = nil,
       copyPublicKeyPopoverState: ButtonCopyPopoverState = .invisible,
       updateState: UpdateState = .dismiss) {
    self.storages = storages
    self.isExtended = isExtended
    self.progress = progress
    self.formattedMetrics = formattedMetrics
    self.synchronizationTitle = synchronizationTitle
    self.synchronizationFiles = synchronizationFiles
    self.isCopyPopoverPresented = isCopyPopoverPresented
    self.activityMessage = activityMessage
    self.copyPublicKeyPopoverState = copyPublicKeyPopoverState
    self.updateState = updateState
  }

  func copyPublicKey() { }
  func updateCopyPopover(show: Bool) { }
  func messageClosePressed(message: ActivityMessage) { }
  func update() { }
}
#endif
