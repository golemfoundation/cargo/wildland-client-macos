//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#if DEBUG
import Foundation
import Combine

final class ReceiveShareViewModelStub: ReceiveShareViewModelType {

  // MARK: - Properties

  let shareInfo: ShareInfo
  let headerTitle: AttributedString
  let isContentRedacting: Bool
  let isLoading: Bool
  let infoViewType: InfoViewType?
  let windowIdentifier: String
  var closePublisher: AnyPublisher<Void, Never> { Just(()).eraseToAnyPublisher() }

  // MARK: - Initialization

  init(
    shareInfo: ShareInfo = ShareInfo(
      senderName: "Bob",
      senderPublicKey: "0xc5a...82kcb",
      sharedContentName: "Sponsor Booth details",
      sharedContentSize: "16.56 MB",
      sharedContentType: "PDF",
      image: nil
    ),
    headerTitle: AttributedString = AttributedString("Bob shared a file with you"),
    isContentRedacting: Bool = false,
    isLoading: Bool = false,
    infoViewType: InfoViewType? = nil,
    windowIdentifier: String = "windowIdentifier"
  ) {
    self.shareInfo = shareInfo
    self.headerTitle = headerTitle
    self.isContentRedacting = isContentRedacting
    self.isLoading = isLoading
    self.infoViewType = infoViewType
    self.windowIdentifier = windowIdentifier
  }

  // MARK: - Public

  func fetchData() async { }
  func mainButtonPressed() async { }
  func cancelAction() { }
}
#endif
