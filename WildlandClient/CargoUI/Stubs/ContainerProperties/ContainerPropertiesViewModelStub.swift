//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

final class ContainerPropertiesViewModelStub: ContainerPropertiesViewModelType {

  typealias ShareViewModel = ContainerPropertiesShareViewModelStub
  typealias RemoveViewModelType = RemoveViewModel
  typealias StorageViewModel = ContainerPropertiesStorageViewModelStub

  // MARK: - Properties

  let options: [ContainerPropertiesOptions]
  var selectedOption: ContainerPropertiesOptions
  var isLoading: Bool
  let isHeaderRedacting: Bool
  let containerInfo: ContainerInfo
  let identifier: String
  let shareViewModel: ContainerPropertiesShareViewModelStub
  let storageViewModel: ContainerPropertiesStorageViewModelStub

  // MARK: - Initialization

  init(
    options: [ContainerPropertiesOptions],
    selectedOption: ContainerPropertiesOptions,
    isLoading: Bool,
    isHeaderRedacting: Bool,
    containerInfo: ContainerInfo,
    identifier: String,
    shareViewModel: ContainerPropertiesShareViewModelStub,
    storageViewModel: ContainerPropertiesStorageViewModelStub
  ) {
    self.options = options
    self.selectedOption = selectedOption
    self.isLoading = isLoading
    self.isHeaderRedacting = isHeaderRedacting
    self.containerInfo = containerInfo
    self.identifier = identifier
    self.shareViewModel = shareViewModel
    self.storageViewModel = storageViewModel
  }

  // MARK: - Public

  func fetchContainerInformation() async { }
  func headerButtonPressed() { }
}
