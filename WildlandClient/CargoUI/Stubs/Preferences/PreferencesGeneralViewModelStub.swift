//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#if DEBUG
final class PreferencesGeneralViewModelStub: PreferencesGeneralViewModelType {

  // MARK: - Properties

  let publicKey: String
  let rootPath: String
  var selectedItem: DropdownItem
  let items: [DropdownItem]
  var launchCargoOnLogin: Bool = false
  var displayName: String
  var shouldDisplayNameForSharing: Bool
  var isDropdownShown: Bool
  var publicKeyTooltipState: TooltipState
  let displayNameForSharingDisabled: Bool

  // MARK: - Initialization

  init(
    publicKey: String = "0x71c7656ec7ab88b098defb751b7401b5f6d8976f",
    rootPath: String = "file://example/root/path",
    selectedItem: DropdownItem,
    items: [DropdownItem],
    launchCargoOnLogin: Bool,
    displayName: String,
    shouldDisplayNameForSharing: Bool,
    isDropdownShown: Bool,
    publicKeyTooltipState: TooltipState,
    displayNameForSharingDisabled: Bool
  ) {
    self.publicKey = publicKey
    self.rootPath = rootPath
    self.selectedItem = selectedItem
    self.items = items
    self.launchCargoOnLogin = launchCargoOnLogin
    self.displayName = displayName
    self.shouldDisplayNameForSharing = shouldDisplayNameForSharing
    self.isDropdownShown = isDropdownShown
    self.publicKeyTooltipState = publicKeyTooltipState
    self.displayNameForSharingDisabled = displayNameForSharingDisabled
  }

  // MARK: - Public

  func copyPublicKey() { }
  func openFinder() { }
  func onCopyButtonHover(isHover: Bool) { }
}
#endif
