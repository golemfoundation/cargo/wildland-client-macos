//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

public struct WLLineProgressView: View {
  var value: Double
  var emptyColor = Color(light: Colors.dust, dark: Colors.dusk)
  var filledColor = Colors.mint.swiftUIColor

  public init<V: BinaryFloatingPoint>(value: V) {
    self.value = Double(value)
  }

  public var body: some View {
    ZStack(alignment: .leading) {
      emptyColor
      GeometryReader { geometry in
        Rectangle()
          .frame(width: min(value * geometry.size.width, geometry.size.width))
          .foregroundColor(filledColor)
      }
    }
  }

  public func colors(empty: Color? = nil, filled: Color? = nil) -> Self {
    var copy = self
    if let empty = empty {
      copy.emptyColor = empty
    }
    if let filled = filled {
      copy.filledColor = filled
    }
    return copy
  }
}

#if DEBUG

struct LineProgressView_Previews: PreviewProvider {
  static var previews: some View {
    WLLineProgressView(value: 0.3)
      .frame(height: 4)
      .padding()
  }
}

#endif
