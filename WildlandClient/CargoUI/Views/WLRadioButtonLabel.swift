//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

public struct WLRadioButtonLabel: View {
  let text: String
  let hint: String

  private var isDisabled: Bool

  public init(text: String,
              hint: String,
              isDisabled: Bool = false) {
    self.text = text
    self.hint = hint
    self.isDisabled = isDisabled
  }

  public var body: some View {
    VStack(alignment: .leading, spacing: 1) {
      Text(text)
        .font(.textLMedium)
        .frame(minHeight: 16)
        .foregroundColor(.text)
        .opacity(isDisabled ? 0.6 : 1.0)
      Text(hint)
        .font(.textSRegular)
        .frame(minHeight: 16)
        .foregroundColor(
          Color(light: Colors.mainText, dark: Colors.cloud)
            .opacity(0.6)
        )
        .opacity(isDisabled ? 0.6 : 1.0)
    }
  }
}

#if DEBUG

struct WLRadioButtonLabel_Previews: PreviewProvider {
  static var previews: some View {
    WLRadioButtonLabel(text: "Main text", hint: "Small text")
  }
}

#endif
