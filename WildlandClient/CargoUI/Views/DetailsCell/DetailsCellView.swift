//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct DetailsCellView<Content: View>: View {

  // MARK: - Properties

  enum ViewStyle {
    case constant(text: String)
    case editable(text: Binding<String>)
  }

  private let title: String?
  private let placeholder: String?
  private let textOpacity: Double
  private let trailingContent: () -> Content
  private let insets: EdgeInsets
  private let showTooltipText: String?
  private let viewStyle: ViewStyle

  @State private var tooltipState: TooltipState = .dismiss
  @State private var isTooltipHovered = false
  @FocusState private var isTextFieldFocused: Bool

  // MARK: - Initialization

  init(title: String? = nil,
       placeholder: String? = nil,
       textOpacity: Double = 1.0,
       showTooltipText: String? = nil,
       insets: EdgeInsets = EdgeInsets(top: 4.0, leading: 12.0, bottom: 4.0, trailing: 12.0),
       viewStyle: ViewStyle,
       @ViewBuilder trailingContent: @escaping () -> Content) {
    self.title = title
    self.textOpacity = textOpacity
    self.insets = insets
    self.trailingContent = trailingContent
    self.showTooltipText = showTooltipText
    self.placeholder = placeholder
    self.viewStyle = viewStyle
  }

  // MARK: - Views

  var body: some View {
    HStack(spacing: .zero) {
      leadingContent
      Spacer(minLength: 16.0)
      trailingContent()
    }
    .padding(insets)
    .frame(maxHeight: 40.0)
    .background(
      ZStack {
        Color.formBackground
        RoundedRectangle(cornerRadius: 4.0)
          .stroke(Color.formBorder, lineWidth: 1.0)
      }
    )
  }

  // MARK: - Private

  private var leadingContent: some View {
    HStack {
      VStack(alignment: .leading, spacing: 1.0) {
        switch viewStyle {
        case .constant(let text):
          titleLabel
          contentLabel(text)
        case .editable(let text):
          if !isTextFieldFocused {
            titleLabel
          }
          contentTextField(text)
        }
      }
      .opacity(textOpacity)
      .foregroundColor(.text)

      if showTooltipText?.isEmpty == false {
        tooltip
      }
    }
  }

  private var titleLabel: some View {
    if let title {
      return Text(title)
        .font(Fonts.Inter.semiBold.swiftUIFont(size: 8.0))
        .lineLimit(1)
        .eraseToAnyView()
    }
    return EmptyView()
      .eraseToAnyView()
  }

  private func contentLabel(_ text: String) -> some View {
    Text(text)
      .font(.textMRegular)
      .lineLimit(1)
  }

  private func contentTextField(_ text: Binding<String>) -> some View {
    TextField(placeholder ?? "", text: text)
      .textFieldStyle(PreferencesDetailsTextFieldStyle())
      .frame(height: isTextFieldFocused ? 38 : 15)
      .onChange(of: text.wrappedValue) { _ in
        isTextFieldFocused = true
      }
      .focused($isTextFieldFocused)
      .onSubmit {
        isTextFieldFocused = false
      }
  }

  private var tooltip: some View {
    tooltipImage
      .onHover { onHover in
        updateTooltip(isVisible: onHover)
        isTooltipHovered = onHover
      }
      .tooltip(position: .bottom, state: tooltipState, showArrow: false)
  }

  private var tooltipImage: Image {
    isTooltipHovered
    ? Images.preferencesQuestionButtonHovered.swiftUIImage
    : Images.preferencesQuestionButton.swiftUIImage
  }

  private func updateTooltip(isVisible: Bool) {
    tooltipState = isVisible
    ? .hover(showTooltipText ?? "")
    : .dismiss
  }
}
