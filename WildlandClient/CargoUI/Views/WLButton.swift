//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import SwiftUI

public struct WLButton: View {
  public struct Kind: Equatable {
    public var font: SwiftUI.Font
    public var foreground: UIStates
    public var background: UIStates
    public var border: UIStates
    public var horizontalPadding: CGFloat
  }

  let title: String
  let kind: Kind
  let action: () -> Void
  public var horizontalPadding: CGFloat?
  public var height: CGFloat = 40.0
  @Binding private var isLoading: Bool

  public init(
    title: String,
    kind: Kind,
    isLoading: Binding<Bool> = .constant(false),
    action: @escaping () -> Void
  ) {
    self.title = title
    self.kind = kind
    self.action = action
    self._isLoading = isLoading
  }

  public var body: some View {
    Button(action: action) {
      buttonContent
    }
    .buttonStyle(
      WLButtonStyle(
        foreground: kind.foreground,
        background: kind.background,
        border: kind.border
      )
    )
  }

  private var buttonContent: some View {
    HStack(spacing: 10.0) {
      circleProgressViewIfLoading
      Text(title)
        .font(kind.font)
    }
    .frame(height: height)
    .padding(.horizontal, horizontalPadding ?? kind.horizontalPadding)
    .contentShape(Rectangle())
  }

  @ViewBuilder private var circleProgressViewIfLoading: some View {
    if isLoading {
      CircleProgressView(
        strokeBackgroundColor: Color.colors(
          light: Colors.pureWhite.swiftUIColor.opacity(0.2),
          dark: Colors.onyx.swiftUIColor.opacity(0.2)
        ),
        strokeForegroundColor: Color.colors(
          light: Colors.snow.swiftUIColor,
          dark: Colors.onyx.swiftUIColor
        )
      )
      .frame(width: 16.0, height: 16.0)
    }
  }
}

extension WLButton.Kind {
  static var main: WLButton.Kind {
    .init(
      font: .textLSemiBold,
      foreground: .primaryForeground,
      background: .primaryBackground,
      border: .clear,
      horizontalPadding: 36.0
    )
  }

  static var fallback: WLButton.Kind {
    .init(
      font: .textLSemiBold,
      foreground: .fallbackForeground,
      background: .clear,
      border: .clear,
      horizontalPadding: 19.0
    )
  }

  static var warning: WLButton.Kind {
    .init(
      font: .textLSemiBold,
      foreground: .warningForeground,
      background: .warningBackground,
      border: .clear,
      horizontalPadding: 19.0
    )
  }
}

extension UIStates {
  static let primaryBackground = UIStates(
    default: Colors.grass.swiftUIColor,
    hover: Color(red: 21.0 / 255.0, green: 180.0 / 255.0, blue: 131.0 / 255.0),
    pressed: Color(red: 20.0 / 255.0, green: 168.0 / 255.0, blue: 122.0 / 255.0)
  )

  static let primaryForeground = UIStates(
    default: Color(light: Colors.pureWhite, dark: Colors.onyx),
    hover: Color(light: Colors.pureWhite, dark: Colors.onyx),
    pressed: Color(light: Colors.pureWhite, dark: Colors.onyx)
  )

  static let fallbackForeground = UIStates(
    default: Color(light: Colors.granite, dark: Colors.silver),
    hover: Color(light: Colors.mainText, dark: Colors.cloud),
    pressed: Color(light: Colors.dusk, dark: Colors.dust)
  )

  static let warningForeground = UIStates(
    default: Color(light: Colors.pureWhite, dark: Colors.onyx),
    hover: Color(light: Colors.pureWhite, dark: Colors.onyx),
    pressed: Color(light: Colors.pureWhite, dark: Colors.onyx)
  )

  static let warningBackground = UIStates(
    default: Colors.warning.swiftUIColor,
    hover: Color(hex: "#F44237"),
    pressed: Color(hex: "#E53227")
  )
}

// TODO: - Should update Figma with proper names (instead of using hex values)
private extension Color {
  init(hex: String) {
    var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
    hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
    var rgb: UInt64 = 0
    Scanner(string: hexSanitized).scanHexInt64(&rgb)

    let red = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
    let green = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
    let blue = CGFloat(rgb & 0x0000FF) / 255.0

    self.init(
      red: red,
      green: green,
      blue: blue
    )
  }
}

#if DEBUG

struct WLButton_Previews: PreviewProvider {
  static var previews: some View {
    VStack {
      WLButton(title: "Click Me", kind: .main) {}
      WLButton(title: "Click Me", kind: .fallback) {}
      WLButton(title: "Click Me", kind: .warning) {}
    }
  }
}

#endif
