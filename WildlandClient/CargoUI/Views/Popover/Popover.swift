//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

public protocol PopoverIconConfiguration: AnyObject {
  var statusBarIconImage: NSImage? { get set }
}

public protocol PopoverAction {
  func showContentIfPossible()
}

public final class Popover: NSObject, PopoverIconConfiguration, PopoverAction {

  // MARK: - Properties

  public var isPopoverEnabled: (() -> Bool)?
  public var statusBarIconImage: NSImage? {
    didSet {
      item.button?.image = statusBarIconImage
    }
  }
  private(set) var item: NSStatusItem
  private var menu: NSMenu
  private let popover = NSPopover()

  // MARK: - Public

  public init(
    with items: [NSMenuItem],
    statusBarIconImage: NSImage?,
    contentController viewController: NSViewController
  ) {
    menu = NSMenu()
    items.forEach(menu.addItem)
    item = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
    super.init()

    configureStatusBarButton(with: statusBarIconImage)
    configurePopover(contentViewController: viewController)
  }

  // MARK: - Public

  public func showContentIfPossible() {
    guard isPopoverEnabled?() == true, let sender = item.button else { return }
    handleLeftClick(sender)
  }

  // MARK: - Private

  private func configureStatusBarButton(with image: NSImage?) {
    statusBarIconImage = image
    item.button?.target = self
    item.button?.action = #selector(handleStatusItemAction)
    item.button?.sendAction(on: [.leftMouseDown, .rightMouseUp])
  }

  @objc private func handleStatusItemAction(_ sender: NSStatusBarButton) {
    guard isPopoverEnabled?() == true else { return }

    if let event = NSApp.currentEvent, event.isRightClickUp {
      handleRightClick(sender, event: event)
    } else {
      handleLeftClick(sender)
    }
  }

  private func handleLeftClick(_ sender: NSStatusBarButton) {
    if popover.isShown {
      popover.performClose(sender)
    } else {
      popover.show(relativeTo: sender.bounds, of: sender, preferredEdge: NSRectEdge.minY)
    }
  }

  private func handleRightClick(_ sender: NSStatusBarButton, event: NSEvent) {
    guard let button = item.button else { return }
    let buttonRect = button.window?.convertToScreen(button.frame) ?? .zero
    menu.popUp(positioning: menu.item(at: .zero), at: buttonRect.origin, in: nil)
  }

  private func configurePopover(contentViewController: NSViewController) {
    popover.behavior = .transient
    popover.contentViewController = contentViewController
    popover.animates = true
  }
}

private extension NSEvent {
  var isRightClickUp: Bool {
    let rightClick = type == .rightMouseUp
    let controlClick = modifierFlags.contains(.control)
    return rightClick || controlClick
  }
}
