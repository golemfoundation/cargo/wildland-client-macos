//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct DropdownItem: Identifiable, Equatable {
  let id: String
  let title: String
}

struct DropdownField: View {

  // MARK: - Properties

  let items: [DropdownItem]
  @Binding var selectedItem: DropdownItem
  @Binding var isExpanded: Bool
  @State private var isHovering = false
  private let cellHeight: Double = 28.0
  private let width: Double = 132.0

  private var offsetY: Double {
    guard let selectedIndex = items.firstIndex(of: selectedItem) else { return .zero }
    let middle = items.count / 2
    let shift = items.count % 2 == .zero ? 0.5 : .zero
    let multiplier = Double(middle - selectedIndex) - shift
    return cellHeight * multiplier
  }

  private var foregroundColor: Color {
    isHovering
    ? .text
    : Color(light: Colors.granite, dark: Colors.silver)
  }

  private var icon: Image {
    isHovering
    ? Images.dropdownTriggerArrowHover.swiftUIImage
    : Images.dropdownTriggerArrowDefault.swiftUIImage
  }

  // MARK: - Views

  var body: some View {
    HStack(spacing: 16.0) {
      Divider()
        .frame(width: 1.0, height: 24.0)
        .overlay(Color(light: Colors.dust, dark: Colors.charcoal))
      dropdownField
    }
    .frame(width: width)
    .contentShape(Rectangle())
    .onTapGesture {
      isExpanded.toggle()
    }
    .onHover { isHovering in
      self.isHovering = isHovering
    }
    .overlay(dropdownIfNeeded)
  }

  private var dropdownField: some View {
    HStack(spacing: .zero) {
      Text(selectedItem.title)
        .font(.textMRegular)
        .foregroundColor(foregroundColor)
      Spacer()
      icon
        .padding(.trailing, 8.0)
    }
  }

  @ViewBuilder private var dropdownIfNeeded: some View {
    if isExpanded {
      Dropdown(
        isExpanded: $isExpanded,
        selectedItem: $selectedItem,
        items: items,
        cellHeight: cellHeight
      )
      .offset(CGSize(width: .zero, height: offsetY))
      .frame(width: width)
    }
  }
}
