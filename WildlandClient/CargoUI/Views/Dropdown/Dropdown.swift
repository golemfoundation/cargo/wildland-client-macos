//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct Dropdown: View {

  // MARK: - Properties

  @Binding var isExpanded: Bool
  @Binding var selectedItem: DropdownItem
  let items: [DropdownItem]
  let cellHeight: Double

  // MARK: - Views

  var body: some View {
    content
      .padding(.vertical, 4.0)
      .background(Color(light: Colors.pureWhite, dark: Colors.onyx))
      .cornerRadius(4.0)
      .shadow(color: Colors.pureBlack.swiftUIColor.opacity(0.3), radius: 3.0, x: .zero, y: .zero)
  }

  private var content: some View {
    VStack(alignment: .leading, spacing: .zero) {
      ForEach(items, id: \.title) { item in
        dropdownItemView(item)
      }
    }
  }

  private func dropdownItemView(_ item: DropdownItem) -> some View {
    DropdownItemView(
      item: item,
      isSelected: item == selectedItem,
      height: cellHeight
    )
    .onTapGesture {
      selectedItem = item
      isExpanded = false
    }
  }
}
