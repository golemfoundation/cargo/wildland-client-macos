//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct DropdownItemView: View {

  // MARK: - Properties

  let item: DropdownItem
  let isSelected: Bool
  let height: Double
  @State private var isHovering = false

  private var foregroundColor: Color {
    guard isSelected || isHovering else {
      return Color(light: Colors.ash, dark: Colors.silver)
    }
    return .text
  }

  private var backgroundColor: Color {
    guard isHovering else {
      return .clear
    }
    return Color(light: Colors.ice, dark: Colors.gloom)
  }

  // MARK: - Views

  var body: some View {
    HStack(spacing: 14.0) {
      Divider()
        .frame(width: 2.0, height: height)
        .overlay(Color.text)
        .opacity(isSelected ? 1.0 : .zero)
      Text(item.title)
        .foregroundColor(foregroundColor)
      Spacer()
    }
    .background(backgroundColor)
    .onHover { isHovering in
      self.isHovering = isHovering
    }
  }
}
