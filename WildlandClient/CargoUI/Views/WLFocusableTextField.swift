//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import SwiftUI
import WildlandCommon

public struct WLFocusableTextField: NSViewRepresentable {

  public init(
    stringValue: Binding<String>,
    onChange: (() -> Void)? = nil,
    onDelete: ((String) -> Void)? = nil,
    onCommit: (() -> Void)? = nil,
    onPaste: ((String?) -> Void)? = nil,
    onTabKeystroke: (() -> Void)? = nil
  ) {
    self.stringValue = stringValue
    self.onChange = onChange
    self.onDelete = onDelete
    self.onCommit = onCommit
    self.onPaste = onPaste
    self.onTabKeystroke = onTabKeystroke
  }

  private var stringValue: Binding<String>

  public var placeholder: String = ""
  public var font: NSFont?
  public var textColor: Color?
  public var placeholderColor: Color?
  public var autoFocus = false
  public var alignment: NSTextAlignment = .center
  public var tag: Int = 0
  public var isEditable: Bool = true
  public var formatter: Formatter?
  public var focusTag: Binding<Int>?
  public var onChange: (() -> Void)?
  public var onDelete: ((String) -> Void)?
  public var onCommit: (() -> Void)?
  public var onPaste: ((String?) -> Void)?
  public var onTabKeystroke: (() -> Void)?

  @State private var didFocus = false

  public func makeNSView(context: Context) -> NSTextField {
    let textField = FocusAwareTextField()
    textField.stringValue = stringValue.wrappedValue
    textField.delegate = context.coordinator
    textField.alignment = alignment
    textField.isBordered = false
    textField.drawsBackground = false
    textField.tag = tag
    textField.focusRingType = .none
    textField.onPaste = onPaste
    textField.font = font
    textField.isEditable = isEditable
    textField.formatter = formatter

    if let textColor {
      textField.textColor = NSColor(textColor)
    }

    setPlaceholderIfNeeded(textField: textField)

    return textField
  }

  public func updateNSView(_ nsView: NSTextField, context: Context) {
    nsView.stringValue = stringValue.wrappedValue
    nsView.isEditable = isEditable
    setPlaceholderIfNeeded(textField: nsView)

    if let textField = nsView as? FocusAwareTextField {
      if focusTag?.wrappedValue == tag {
        if textField.isFirstResponder() == false {
          textField.becomeFirstResponder()
        }
      }

      if textField.onFocusChange == nil {
        textField.onFocusChange = { _ in
          DispatchQueue.main.delay(with: .tenthOfASecond) {
            self.focusTag?.wrappedValue = self.tag
          }
        }
      }
    }
  }

  private func setPlaceholderIfNeeded(textField: NSTextField) {
    if let placeholderColor, let font {
      let attributes: [NSAttributedString.Key: Any] = [
        .foregroundColor: NSColor(placeholderColor),
        .font: font
      ]
      let attributedPlaceholder = NSAttributedString(string: placeholder, attributes: attributes)
      textField.placeholderAttributedString = attributedPlaceholder
    } else {
      textField.placeholderString = placeholder
    }
  }

  public func makeCoordinator() -> Coordinator {
    Coordinator(with: self)
  }

  public class Coordinator: NSObject, NSTextFieldDelegate {
    let parent: WLFocusableTextField

    init(with parent: WLFocusableTextField) {
      self.parent = parent

      super.init()

      NotificationCenter.default.addObserver(
        self,
        selector: #selector(handleAppDidBecomeActive(notification:)),
        name: NSApplication.didBecomeActiveNotification,
        object: nil
      )
    }

    @objc
    func handleAppDidBecomeActive(notification: Notification) {
      if parent.autoFocus && !parent.didFocus {
        DispatchQueue.main.delay(with: .twentiethOfASecond) {
          self.parent.didFocus = false
        }
      }
    }

    // MARK: - NSTextFieldDelegate Methods

    public func controlTextDidChange(_ obj: Notification) {
      guard let textField = obj.object as? NSTextField else { return }
      parent.stringValue.wrappedValue = textField.stringValue
      parent.onChange?()
    }

    public func control(_ control: NSControl, textShouldEndEditing fieldEditor: NSText) -> Bool {
      parent.onCommit?()
      return true
    }

    public func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
      switch commandSelector {
      case #selector(NSStandardKeyBindingResponding.insertTab):
        parent.onTabKeystroke?()
        return true
      case #selector(NSStandardKeyBindingResponding.insertNewline):
        parent.onCommit?()
        return true
      case #selector(NSStandardKeyBindingResponding.deleteBackward):
        deleteText(in: textView)
        parent.onDelete?(textView.string)
        return true
      default:
        return false
      }
    }

    private func deleteText(in textView: NSTextView) {
      let text = textView.string
      let cursor = textView.selectedRange()
      var range: Range<String.Index>?
      var selectionRange: NSRange?

      if cursor.length > .zero {
        range = Range(cursor, in: text)
        selectionRange = NSRange(location: cursor.location, length: .zero)
      } else if cursor.location > .zero,
                let start = text.index(text.startIndex, offsetBy: cursor.location - 1, limitedBy: text.endIndex),
                let end = text.index(text.startIndex, offsetBy: cursor.location, limitedBy: text.endIndex) {
        range = start..<end
        selectionRange = NSRange(location: cursor.location - 1, length: .zero)
      }

      guard let range, let selectionRange else { return }
      parent.stringValue.wrappedValue.removeSubrange(range)
      textView.setSelectedRange(selectionRange)
    }
  }
}
