//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit

class FocusAwareTextField: NSTextField {
  var onFocusChange: ((Bool) -> Void)?
  var onPaste: ((String?) -> Void)?

  func isFirstResponder() -> Bool {
    guard let window = window else { return false }

    guard let textView = window.firstResponder as? NSTextView else { return false }

    return textView.delegate === self
  }

  @discardableResult
  override func becomeFirstResponder() -> Bool {
    guard super.becomeFirstResponder() else {
      return false
    }
    if let editor = currentEditor() {
      editor.moveToEndOfLine(self)
    }
    return true
  }

  override func mouseDown(with event: NSEvent) {
    super.mouseDown(with: event)

    guard isEditable else { return }
    onFocusChange?(true)
  }

  private let commandKey = NSEvent.ModifierFlags.command.rawValue
  private let commandShiftKey = NSEvent.ModifierFlags.command.rawValue | NSEvent.ModifierFlags.shift.rawValue

  // swiftlint:disable:next cyclomatic_complexity
  override func performKeyEquivalent(with event: NSEvent) -> Bool {
    guard event.type == NSEvent.EventType.keyDown else { return super.performKeyEquivalent(with: event) }

    let receivedCommandKey = event.modifierFlags.rawValue & NSEvent.ModifierFlags.deviceIndependentFlagsMask.rawValue
    if receivedCommandKey == commandKey {
      switch event.charactersIgnoringModifiers! {
      case "x":
        if NSApp.sendAction(#selector(NSText.cut(_:)), to: nil, from: self) { return true }
      case "c":
        if NSApp.sendAction(#selector(NSText.copy(_:)), to: nil, from: self) { return true }
      case "v":
        onPaste?(NSPasteboard.general.string(forType: NSPasteboard.PasteboardType.string))
        return true
      case "z":
        if NSApp.sendAction(Selector(("undo:")), to: nil, from: self) { return true }
      case "a":
        if NSApp.sendAction(#selector(NSResponder.selectAll(_:)), to: nil, from: self) { return true }
      default:
        break
      }
    } else if receivedCommandKey == commandShiftKey {
      if event.charactersIgnoringModifiers == "Z" {
        if NSApp.sendAction(Selector(("redo:")), to: nil, from: self) { return true }
      }
    }
    return super.performKeyEquivalent(with: event)
  }
}
