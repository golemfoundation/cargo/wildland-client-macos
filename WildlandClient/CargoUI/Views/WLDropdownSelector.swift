//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import SwiftUI

public struct WLDropdownOption: Hashable {
  public let key: String
  public let value: String

  public init(key: String, value: String) {
    self.key = key
    self.value = value
  }

  public static func == (lhs: Self, rhs: Self) -> Bool {
    return lhs.key == rhs.key
  }
}

public struct WLDropdownSelector: View {
  public init(
    selectedOption: WLDropdownOption? = nil,
    contentType: WLDropdownType = .text,
    placeholder: String = "",
    options: [WLDropdownOption],
    displaySelectionImage: Bool = true,
    didOptionSelected: ((WLDropdownOption) -> Void)? = nil,
    didMenuDisplayed: ((Bool) -> Void)? = nil
  ) {
    self.selectedOption = selectedOption
    self.contentType = contentType
    self.placeholder = placeholder
    self.options = options
    self.displaySelectionImage = displaySelectionImage
    self.didOptionSelected = didOptionSelected
    self.didMenuDisplayed = didMenuDisplayed
  }

  public enum WLDropdownType {
    case text
    case image
  }

  @State private var shouldShowDropdown = false {
    didSet {
      didMenuDisplayed?(shouldShowDropdown)
    }
  }

  @State var selectedOption: WLDropdownOption?

  var contentType: WLDropdownType = .text
  var placeholder: String = ""
  var options: [WLDropdownOption]
  var displaySelectionImage: Bool = true
  var didOptionSelected: ((_ option: WLDropdownOption) -> Void)?
  var didMenuDisplayed: ((_ displayed: Bool) -> Void)?

  private let buttonSize: CGFloat = 32

  public var body: some View {
    switch self.contentType {
    case .text:
      textContent()
    case .image:
      imageContent()
    }
  }

  func imageContent() -> some View {
    HStack(spacing: 8) {
      Spacer()

      Button(action: {
        self.shouldShowDropdown.toggle()
      }) {
        Images.preferencesMoreButton.swiftUIImage
          .resizable()
          .frame(width: self.buttonSize, height: self.buttonSize)
          .padding(.all, 6)
          .foregroundColor(Color(light: Colors.pureWhite, dark: Colors.gloom))
          .alignmentGuide(.trailing) { d in d[.leading] }
      }
      .buttonStyle(.borderless)
    }.overlay(
      VStack {
        if self.shouldShowDropdown {
          Spacer(minLength: self.buttonSize / 2 + 5)

          WLDropdown(
            options: self.options,
            selectedOption: self.$selectedOption.wrappedValue ?? self.options[0],
            displaySelectionImage: self.displaySelectionImage,
            onOptionSelected: { option in
              self.shouldShowDropdown = false
              self.selectedOption = option
              self.didOptionSelected?(option)
            }
          )
        }
      }, alignment: .topLeading
    )
  }

  func textContent() -> some View {
    Button(action: {
      self.shouldShowDropdown.toggle()
    }) {
      HStack {
        Text(self.selectedOption?.value ?? self.placeholder)
          .font(.textMRegular)
          .foregroundColor(.text)
          .padding(.leading, 14)

        Spacer()

        let image: Image = self.shouldShowDropdown
        ? Images.preferencesDisclosureIconDown.swiftUIImage
        : Images.preferencesDisclosureIconUp.swiftUIImage
        image
          .resizable()
          .frame(width: 24, height: 24)
          .foregroundColor(Color.black)
          .padding(.trailing, 10)
      }
    }
    .buttonStyle(.borderless)
    .overlay(
      VStack {
        if self.shouldShowDropdown {
          Spacer(minLength: self.buttonSize / 2 + 5)

          WLDropdown(
            options: self.options,
            selectedOption: self.$selectedOption.wrappedValue ?? self.options[0],
            displaySelectionImage: self.displaySelectionImage,
            onOptionSelected: { option in
              self.shouldShowDropdown = false
              self.selectedOption = option
              self.didOptionSelected?(option)
            }
          )
        }
      }, alignment: .topLeading
    )
  }
}

// MARK: - Private

private struct WLDropdownRow: View {
  var option: WLDropdownOption
  var onOptionSelected: ((_ option: WLDropdownOption) -> Void)?
  var isOptionSelected: Bool
  var displaySelectionImage: Bool

  var body: some View {
    Button(action: {
      if let onOptionSelected = self.onOptionSelected {
        onOptionSelected(self.option)
      }
    }) {
      HStack {
        Text(self.option.value)
          .font(.textMRegular)
          .foregroundColor(.text)

        Spacer()

        if self.isOptionSelected, self.displaySelectionImage {
          Images.preferencesRowCheckmark.swiftUIImage
            .resizable()
            .frame(width: 24, height: 24)
            .foregroundColor(.text)
            .padding(.trailing, 10)
        }
      }
    }
    .buttonStyle(.borderless)
    .frame(height: 28)
  }
}

private struct WLDropdown: View {
  var options: [WLDropdownOption]
  var selectedOption: WLDropdownOption
  var displaySelectionImage: Bool
  var onOptionSelected: ((_ option: WLDropdownOption) -> Void)?

  var body: some View {
    ScrollView {
      VStack(alignment: .leading, spacing: 0) {
        ForEach(self.options, id: \.self) { option in
          WLDropdownRow(
            option: option,
            onOptionSelected: self.onOptionSelected,
            isOptionSelected: option == self.selectedOption,
            displaySelectionImage: displaySelectionImage
          )
        }
      }
    }
    .frame(minHeight: CGFloat(self.options.count) * 28)
    .padding(.vertical, 4)
    .padding(.leading, 14)
    .background(Color(light: Colors.pureWhite, dark: Colors.onyx))
    .cornerRadius(5)
  }
}

#if DEBUG

struct WLDropdownSelector_Previews: PreviewProvider {
  static let options: [WLDropdownOption] = [
    WLDropdownOption(key: "dark", value: "Dark theme"),
    WLDropdownOption(key: "light", value: "Light theme"),
    WLDropdownOption(key: "automatic", value: "Follow OS")
  ]

  static var previews: some View {
    Group {
      WLDropdownSelector(
        selectedOption: WLDropdownOption(key: "dark", value: "Dark theme"),
        contentType: .image,
        placeholder: "Color theme",
        options: self.options,
        didOptionSelected: { option in
          print(option)
        }
      )
      .frame(height: 200)
    }
  }
}

#endif
