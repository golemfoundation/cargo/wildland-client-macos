//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

final class FocusableTextFieldFormatter: Formatter {

  private let maxLength: UInt
  private let validCharacters: CharacterSet

  init(maxLength: UInt, validCharacters: CharacterSet) {
    self.maxLength = maxLength
    self.validCharacters = validCharacters
    super.init()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func string(for obj: Any?) -> String? {
    obj as? String
  }

  override func getObjectValue(_ obj: AutoreleasingUnsafeMutablePointer<AnyObject?>?,
                               for string: String,
                               errorDescription error: AutoreleasingUnsafeMutablePointer<NSString?>?) -> Bool {
    obj?.pointee = string as AnyObject
    return true
  }

  override func isPartialStringValid(_ partialStringPtr: AutoreleasingUnsafeMutablePointer<NSString>,
                                     proposedSelectedRange proposedSelRangePtr: NSRangePointer?,
                                     originalString origString: String,
                                     originalSelectedRange origSelRange: NSRange,
                                     errorDescription error: AutoreleasingUnsafeMutablePointer<NSString?>?) -> Bool {
    partialStringPtr.pointee.length <= maxLength && isValid(partialStringPtr.pointee as String)
  }

  // MARK: - Private

  private func isValid(_ value: String) -> Bool {
    String(value.unicodeScalars.filter(validCharacters.contains)) == value
  }
}
