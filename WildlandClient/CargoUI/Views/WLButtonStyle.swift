//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import SwiftUI

struct WLButtonStyle: ButtonStyle {

  // MARK: - Properties

  let foreground: UIStates
  let background: UIStates
  let border: UIStates
  @State private var isHovered: Bool = false
  @Environment(\.isEnabled) private var isEnabled

  // MARK: - ButtonStyle

  func makeBody(configuration: Configuration) -> some View {
    configuration
      .label
      .foregroundColor(color(for: foreground, isPressed: configuration.isPressed))
      .background(color(for: background, isPressed: configuration.isPressed))
      .opacity(1.0)
      .onHover { isHovered = $0 }
      .background(
        RoundedRectangle(cornerRadius: 4.0)
          .stroke(color(for: border, isPressed: configuration.isPressed), lineWidth: 1.0)
      )
      .cornerRadius(4.0)
  }

  // MARK: - Private

  private func color(for state: UIStates, isPressed: Bool) -> Color {
    guard isEnabled else { return state.default.opacity(0.5) }
    guard !isPressed else { return state.pressed }
    guard !isHovered else { return state.hover }
    return state.default
  }
}
