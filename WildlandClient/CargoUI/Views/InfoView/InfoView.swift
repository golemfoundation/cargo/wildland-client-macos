//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct InfoView: View {

  // MARK: - Properties

  let image: Image
  let color: Color
  let text: String
  let animation: Bool

  // MARK: - Views

  var body: some View {
    HStack(spacing: 12) {
      switch animation {
      case true:
        CircleProgressView()
          .frame(width: 16, height: 16)
          .foregroundColor(color)
      case false:
        image
          .resizable()
          .frame(width: 16, height: 16)
          .foregroundColor(color)
      }
      Text(text)
        .font(.bodyText)
        .foregroundColor(.text)
    }
  }

  // MARK: - Public

  static func view(for type: InfoViewType) -> some View {
    switch type {
    case .info(let text):
      return InfoView(
        image: Image(systemName: "checkmark.circle.fill"),
        color: Colors.grass.swiftUIColor,
        text: text,
        animation: false
      )
    case .error(let text, let image):
      return InfoView(
        image: image ?? Image(systemName: "exclamationmark.circle.fill"),
        color: Colors.warning.swiftUIColor,
        text: text,
        animation: false
      )
    case .loading(let text):
      return InfoView(
        image: Image(systemName: "slowmo"),
        color: Colors.grass.swiftUIColor,
        text: text,
        animation: true
      )
    }
  }
}

#if DEBUG

struct InfoView_Previews: PreviewProvider {

  static var previews: some View {
    InfoView.view(for: .loading("Loading the data"))
    InfoView.view(for: .error("Failed sending code",
                              image: Image(systemName: "exclamationmark.circle.fill")))
    InfoView.view(for: .info("Code sent"))
  }
}

#endif
