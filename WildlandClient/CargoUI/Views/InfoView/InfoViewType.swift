//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

enum InfoViewType {
  case info(String)
  case error(_ text: String, image: Image? = nil)
  case loading(String)
}

extension InfoViewType {
  var isError: Bool {
    guard case .error = self else { return false }
    return true
  }

  var isLoading: Bool {
    guard case .loading = self else { return false }
    return true
  }
}
