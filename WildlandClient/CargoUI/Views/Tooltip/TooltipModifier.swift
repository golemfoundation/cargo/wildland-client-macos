//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

extension View {
  func tooltip(position: TooltipPosition, state: TooltipState, showArrow: Bool = true) -> some View {
    modifier(TooltipModifier(position: position, state: state, showArrow: showArrow))
  }
}

struct TooltipModifier: ViewModifier {

  // MARK: - Properties

  let position: TooltipPosition
  let state: TooltipState
  let showArrow: Bool

  private var overlayAlignment: Alignment {
    switch position {
    case .top:
      return .top
    case .bottom:
      return .bottom
    }
  }

  private var alignmentGuide: VerticalAlignment {
    switch position {
    case .top:
      return .top
    case .bottom:
      return .bottom
    }
  }

  private var guideAlignmentGuide: VerticalAlignment {
    switch position {
    case .top:
      return .bottom
    case .bottom:
      return .top
    }
  }

  // MARK: - ViewModifier

  func body(content: Content) -> some View {
    content
      .overlay(alignment: overlayAlignment, content: {
        tooltipIfNotDismissed
          .alignmentGuide(alignmentGuide) { guide in guide[guideAlignmentGuide] }
      })
  }

  @ViewBuilder private var tooltipIfNotDismissed: some View {
    if case .dismiss = state {
      EmptyView()
    } else {
      Tooltip(position: position, state: state, showArrow: showArrow)
    }
  }
}
