//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

enum TooltipState: Equatable {
  case hover(String)
  case pressed(String)
  case dismiss
}

extension TooltipState {
  var isHover: Bool {
    guard case .dismiss = self else { return true }
    return false
  }
}

enum TooltipPosition {
  case top
  case bottom
}

struct Tooltip: View {

  // MARK: - Properties

  let position: TooltipPosition
  let state: TooltipState
  let showArrow: Bool

  // MARK: - Views

  var body: some View {
    content
  }

  @ViewBuilder private var content: some View {
    VStack(spacing: .zero) {
      messageView
      imageView
    }
  }

  @ViewBuilder private var imageView: some View {
    if showArrow {
      Images.tooltipArrow.swiftUIImage
        .rotationEffect(imageRotation)
    }
  }

  private var imageRotation: Angle {
    switch position {
    case .top:
      return .degrees(180.0)
    case .bottom:
      return .zero
    }
  }

  private var messageView: some View {
    let message: String
    switch state {
    case .hover(let string):
      message = string
    case .pressed(let string):
      message = string
    case .dismiss:
      message = ""
    }
    return Text(message)
      .font(.textSRegular)
      .foregroundColor(Color(light: Colors.pureWhite, dark: Colors.night))
      .padding(textPadding)
      .background(backgroundView)
      .lineSpacing(lineSpacing)
      .fixedSize()
  }

  private var textPadding: EdgeInsets {
    showArrow
    ? EdgeInsets(top: 4.0, leading: 8.0, bottom: 4.0, trailing: 8.0)
    : EdgeInsets(top: 6.0, leading: 4.0, bottom: 6.0, trailing: 8.0)
  }

  private var lineSpacing: CGFloat {
    showArrow ? 0 : 2
  }

  private var backgroundView: some View {
    Color(light: Colors.night, dark: Colors.pureWhite)
      .cornerRadius(4.0)
  }
}
