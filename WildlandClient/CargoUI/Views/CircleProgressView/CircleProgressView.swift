//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct CircleProgressView: View {

  // MARK: - Properties

  @State private var isRotating = false
  var strokeBackgroundColor = Colors.mint.swiftUIColor.opacity(0.2)
  var strokeForegroundColor = Colors.mint.swiftUIColor

  // MARK: - Views

  var body: some View {
    VStack {
      ZStack {
        Circle()
          .stroke(strokeBackgroundColor, style: StrokeStyle(lineWidth: 2, lineCap: .round))
        Circle()
          .trim(from: 0, to: 0.8)
          .stroke(strokeForegroundColor, style: StrokeStyle(lineWidth: 2, lineCap: .round))
          .rotationEffect(isRotating ? .degrees(360) : .degrees(0))
      }
      .onAppear { isRotating = true }
      .onDisappear { isRotating = false }
      .rotationEffect(.degrees(isRotating ? 360 : 0))
      .animation(.linear(duration: 2.0).repeatForever(autoreverses: false), value: isRotating)
    }
  }
}
