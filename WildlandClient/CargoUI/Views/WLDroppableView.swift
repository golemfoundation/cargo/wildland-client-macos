//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import UniformTypeIdentifiers

struct WLDropResultError: LocalizedError {
  var errorDescription: String?

  init(_ value: String) {
    errorDescription = value
  }
}

struct WLDroppableView: View {
  let dropDelegate: WLDropViewDelegate

  var body: some View {
    return Rectangle()
      .onDrop(
        of: [UTType.fileURL, UTType.utf8PlainText],
        delegate: dropDelegate
      )
  }
}

struct WLDropViewDelegate: DropDelegate {

  var dropHandler: ((Result<Data?, Error>) -> Void)?

  func validateDrop(info: DropInfo) -> Bool {
    return info.hasItemsConforming(to: [UTType.fileURL, UTType.utf8PlainText])
  }

  func performDrop(info: DropInfo) -> Bool {
    if info.hasItemsConforming(to: [UTType.fileURL]) {
      return parseFileURL(for: info)
    } else if info.hasItemsConforming(to: [UTType.utf8PlainText]) {
      return parsePlainText(for: info)
    }
    return false
  }

  func dropUpdated(info: DropInfo) -> DropProposal? {
    return DropProposal(operation: .copy)
  }

  // MARK: - Private

  private func submitResult(with result: Result<Data?, Error>) {
    DispatchQueue.main.async {
      dropHandler?(result)
    }
  }

  private func parseFileURL(for info: DropInfo) -> Bool {
    guard let item = info.itemProviders(for: [UTType.fileURL]).first else {
      return false
    }

    item.loadItem(forTypeIdentifier: UTType.fileURL.identifier, options: nil) { urlData, error in
      guard error == nil else {
        submitResult(with: .failure(WLDropResultError("Failed to parse text")))
        return
      }
      guard let urlData = urlData as? Data else {
        submitResult(with: .failure(WLDropResultError("Failed to parse file")))
        return
      }
      guard let fileURL = URL(dataRepresentation: urlData, relativeTo: nil) else {
        submitResult(with: .failure(WLDropResultError("Failed to parse file")))
        return
      }
      do {
        let fileData = try Data(contentsOf: fileURL)
        submitResult(with: .success(fileData))
      } catch {
        submitResult(with: .failure(WLDropResultError("Failed to parse file")))
      }
    }
    return true
  }

  private func parsePlainText(for info: DropInfo) -> Bool {
    guard let item = info.itemProviders(for: [UTType.utf8PlainText]).first else {
      return false
    }

    item.loadItem(forTypeIdentifier: UTType.utf8PlainText.identifier, options: nil) { textData, error in
      guard error == nil else {
        submitResult(with: .failure(WLDropResultError("Failed to parse text")))
        return
      }
      guard let textData = textData as? Data else {
        submitResult(with: .failure(WLDropResultError("Failed to parse text")))
        return
      }

      submitResult(with: .success(textData))
    }

    return true
  }
}
