// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif
#if canImport(SwiftUI)
  import SwiftUI
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Images {
  internal static let activityMessageClose = ImageAsset(name: "Activity.Message.Close")
  internal static let activityViewArrowBottom = ImageAsset(name: "ActivityView.Arrow.Bottom")
  internal static let activityViewArrowTop = ImageAsset(name: "ActivityView.Arrow.Top")
  internal static let activityViewUpdateIcon = ImageAsset(name: "ActivityView.Update.Icon")
  internal static let activityViewWarning = ImageAsset(name: "ActivityView.Warning")
  internal static let containerPropertiesAvatar = ImageAsset(name: "ContainerProperties.Avatar")
  internal static let containerPropertiesContainerGreenIcon = ImageAsset(name: "ContainerProperties.ContainerGreenIcon")
  internal static let containerPropertiesEdit = ImageAsset(name: "ContainerProperties.Edit")
  internal static let containerPropertiesMoreIcon = ImageAsset(name: "ContainerProperties.MoreIcon")
  internal static let containerPropertiesStorageIcon = ImageAsset(name: "ContainerProperties.StorageIcon")
  internal static let dropdownTriggerArrowDefault = ImageAsset(name: "Dropdown.Trigger.Arrow.Default")
  internal static let dropdownTriggerArrowHover = ImageAsset(name: "Dropdown.Trigger.Arrow.Hover")
  internal static let errorNetworkConnection = ImageAsset(name: "Error.NetworkConnection")
  internal static let identityCheckmark = ImageAsset(name: "Identity.Checkmark")
  internal static let identityContainers = ImageAsset(name: "Identity.Containers")
  internal static let identityKeys = ImageAsset(name: "Identity.Keys")
  internal static let identityLaptop = ImageAsset(name: "Identity.Laptop")
  internal static let secretPhaseIntroShield = ImageAsset(name: "SecretPhaseIntro.Shield")
  internal static let setupCargoScrews = ImageAsset(name: "SetupCargo.Screws")
  internal static let popoverAvatarPlaceholder = ImageAsset(name: "Popover.AvatarPlaceholder")
  internal static let popoverDefaultFileIcon = ImageAsset(name: "Popover.DefaultFileIcon")
  internal static let popoverDriveIcon = ImageAsset(name: "Popover.DriveIcon")
  internal static let popoverPublicKeyHovered = ImageAsset(name: "Popover.PublicKeyHovered")
  internal static let popoverPublicKeyRegular = ImageAsset(name: "Popover.PublicKeyRegular")
  internal static let popoverTopArrow = ImageAsset(name: "Popover.TopArrow")
  internal static let preferencesDisclosureIconDown = ImageAsset(name: "Preferences.DisclosureIconDown")
  internal static let preferencesDisclosureIconUp = ImageAsset(name: "Preferences.DisclosureIconUp")
  internal static let preferencesGeneralAvatar = ImageAsset(name: "Preferences.General.Avatar")
  internal static let preferencesGeneralCopyDefault = ImageAsset(name: "Preferences.General.Copy.Default")
  internal static let preferencesGeneralCopyHovered = ImageAsset(name: "Preferences.General.Copy.Hovered")
  internal static let preferencesMoreButton = ImageAsset(name: "Preferences.MoreButton")
  internal static let preferencesQuestionButtonHovered = ImageAsset(name: "Preferences.QuestionButton.Hovered")
  internal static let preferencesQuestionButton = ImageAsset(name: "Preferences.QuestionButton")
  internal static let preferencesRowCheckmark = ImageAsset(name: "Preferences.RowCheckmark")
  internal static let preferencesStorageIcon = ImageAsset(name: "Preferences.Storage.Icon")
  internal static let preferencesToggleOff = ImageAsset(name: "Preferences.ToggleOff")
  internal static let preferencesToggleOn = ImageAsset(name: "Preferences.ToggleOn")
  internal static let quotaStorage = ImageAsset(name: "Quota.Storage")
  internal static let shareReceiveAvatar = ImageAsset(name: "Share.Receive.Avatar")
  internal static let shareReceiveFile = ImageAsset(name: "Share.Receive.File")
  internal static let tooltipArrow = ImageAsset(name: "Tooltip.Arrow")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, macOS 10.7, *)
  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }

  #if os(iOS) || os(tvOS)
  @available(iOS 8.0, tvOS 9.0, *)
  internal func image(compatibleWith traitCollection: UITraitCollection) -> Image {
    let bundle = BundleToken.bundle
    guard let result = Image(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
  #endif

  #if canImport(SwiftUI)
  @available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
  internal var swiftUIImage: SwiftUI.Image {
    SwiftUI.Image(asset: self)
  }
  #endif
}

internal extension ImageAsset.Image {
  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, *)
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

#if canImport(SwiftUI)
@available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
internal extension SwiftUI.Image {
  init(asset: ImageAsset) {
    let bundle = BundleToken.bundle
    self.init(asset.name, bundle: bundle)
  }

  init(asset: ImageAsset, label: Text) {
    let bundle = BundleToken.bundle
    self.init(asset.name, bundle: bundle, label: label)
  }

  init(decorative asset: ImageAsset) {
    let bundle = BundleToken.bundle
    self.init(decorative: asset.name, bundle: bundle)
  }
}
#endif

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
