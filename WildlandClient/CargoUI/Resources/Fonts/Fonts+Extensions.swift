//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import AppKit.NSFont

public extension SwiftUI.Font {
  static let displayLarge = Fonts.Inter.semiBold.swiftUIFont(size: 20.0)
  static let displayMedium = Fonts.Inter.semiBold.swiftUIFont(size: 18.0)
  static let displaySmall = Fonts.Inter.semiBold.swiftUIFont(size: 16.0)
  static let displaySmallRegular = Fonts.Inter.regular.swiftUIFont(size: 16.0)
  static let textLSemiBold = Fonts.Inter.semiBold.swiftUIFont(size: 14.0)
  static let textLMedium = Fonts.Inter.medium.swiftUIFont(size: 14.0)
  static let bodyText = Fonts.Inter.regular.swiftUIFont(size: 14.0)
  static let textMSemiBold = Fonts.Inter.semiBold.swiftUIFont(size: 12.0)
  static let textMMedium = Fonts.Inter.medium.swiftUIFont(size: 12.0)
  static let textMRegular = Fonts.Inter.regular.swiftUIFont(size: 12.0)
  static let textSSemiBold = Fonts.Inter.semiBold.swiftUIFont(size: 10.0)
  static let textSRegular = Fonts.Inter.regular.swiftUIFont(size: 10.0)
}

public extension NSFont {
  static let bodyText = Fonts.Inter.regular.font(size: 14.0)
  static let textSRegular = Fonts.Inter.regular.font(size: 10.0)
}
