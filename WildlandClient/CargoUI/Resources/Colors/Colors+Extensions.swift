//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import AppKit.NSColor

public extension Color {

  private static let darkModes = [
    NSAppearance.Name.darkAqua,
    NSAppearance.Name.vibrantDark,
    NSAppearance.Name.accessibilityHighContrastDarkAqua,
    NSAppearance.Name.accessibilityHighContrastVibrantDark
  ]

  static func colors(light: Color, dark: Color) -> Color {
    let nsColor = NSColor(name: nil) { appearance in
      Color.darkModes.contains(appearance.name) ? NSColor(dark) : NSColor(light)
    }
    return Color(nsColor: nsColor)
  }

  init(light: ColorAsset, dark: ColorAsset) {
    let nsColor = NSColor(name: nil) { appearance in
      Color.darkModes.contains(appearance.name) ? NSColor(dark.swiftUIColor) : NSColor(light.swiftUIColor)
    }
    self.init(nsColor: nsColor)
  }
}

public extension Color {
  static let windowBackground = Color(light: Colors.pureWhite, dark: Colors.obsidian)
  static let formBackground = Color(light: Colors.snow, dark: Colors.gloom)
  static let formBorder = Color(light: Colors.dust, dark: Colors.dusk)
  static let text = Color(light: Colors.mainText, dark: Colors.cloud)
}
