//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct PreferencesGeneralView<ViewModel: PreferencesGeneralViewModelType>: View {

  // MARK: - Properties

  @ObservedObject private(set) var viewModel: ViewModel

  private var copyPublicKeyButtonImage: Image {
    guard viewModel.publicKeyTooltipState.isHover else { return Images.preferencesGeneralCopyDefault.swiftUIImage }
    return Images.preferencesGeneralCopyHovered.swiftUIImage
  }

  // MARK: - Views

  var body: some View {
    VStack(spacing: 16.0) {
      publicKeyView
      displayNameView
      useDisplayNameForSharing
        .zIndex(2)
      startOnLoginView
      themeView
        .zIndex(1)
      filesFolderView
    }
  }

  private var publicKeyView: some View {
    HStack(spacing: 16.0) {
      Images.preferencesGeneralAvatar.swiftUIImage
      DetailsCellView(
        title: WLStrings.Preferences.General.publicKeyValue,
        textOpacity: 0.6,
        viewStyle: .constant(text: viewModel.publicKey),
        trailingContent: { copyPublicKeyButton }
      )
    }
  }

  @ViewBuilder private var copyPublicKeyButton: some View {
    Button(action: viewModel.copyPublicKey) {
      copyPublicKeyButtonImage
    }
    .buttonStyle(.borderless)
    .onHover(perform: viewModel.onCopyButtonHover)
    .tooltip(position: .top, state: viewModel.publicKeyTooltipState)
  }

  private var displayNameView: some View {
    DetailsCellView(
      title: WLStrings.Preferences.General.displayName,
      placeholder: WLStrings.Preferences.General.displayNamePlaceholder,
      insets: EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10),
      viewStyle: .editable(text: $viewModel.displayName),
      trailingContent: { EmptyView() })
  }

  private var useDisplayNameForSharing: some View {
    DetailsCellView(
      showTooltipText: WLStrings.Preferences.General.shouldDisplayNameForSharingTooltip,
      viewStyle: .constant(text: WLStrings.Preferences.General.useDisplayNameForSharing),
      trailingContent: { displayNameForSharingSwitch }
    )
    .disabled(viewModel.displayNameForSharingDisabled)
  }

  private var startOnLoginView: some View {
    DetailsCellView(
      viewStyle: .constant(text: WLStrings.Preferences.General.onStart),
      trailingContent: { startOnLoginSwitch }
    )
  }

  private var startOnLoginSwitch: some View {
    Toggle("", isOn: $viewModel.launchCargoOnLogin)
      .toggleStyle(WLToggleStyle())
      .labelsHidden()
  }

  private var displayNameForSharingSwitch: some View {
    Toggle("", isOn: $viewModel.shouldDisplayNameForSharing)
      .toggleStyle(WLToggleStyle())
      .labelsHidden()
  }

  private var themeView: some View {
    DetailsCellView(
      insets: EdgeInsets(
        top: 4.0,
        leading: 12.0,
        bottom: 4.0,
        trailing: .zero
      ),
      viewStyle: .constant(text: WLStrings.Preferences.General.uiTheme),
      trailingContent: { themeDropdownView }
    )
  }

  private var themeDropdownView: some View {
    DropdownField(
      items: viewModel.items,
      selectedItem: $viewModel.selectedItem,
      isExpanded: $viewModel.isDropdownShown
    )
  }

  private var filesFolderView: some View {
    DetailsCellView(
      title: WLStrings.Preferences.General.filesFolder,
      viewStyle: .constant(text: viewModel.rootPath),
      trailingContent: { openFinderButton }
    )
  }

  private var openFinderButton: some View {
    Button(WLStrings.Preferences.General.openFinder, action: viewModel.openFinder)
      .font(.textSRegular)
      .foregroundColor(Colors.sky.swiftUIColor)
      .buttonStyle(.borderless)
  }
}

#if DEBUG
struct PreferencesGeneralView_Previews: PreviewProvider {
  static var previews: some View {
    PreferencesGeneralView(
      viewModel: PreferencesGeneralViewModelStub(
        selectedItem: DropdownItem(id: "1", title: "example"),
        items: [
          DropdownItem(id: "1", title: "example"),
          DropdownItem(id: "2", title: "example2")
        ],
        launchCargoOnLogin: true,
        displayName: "User name",
        shouldDisplayNameForSharing: true,
        isDropdownShown: true,
        publicKeyTooltipState: .pressed("message"),
        displayNameForSharingDisabled: false
      )
    )
  }
}
#endif
