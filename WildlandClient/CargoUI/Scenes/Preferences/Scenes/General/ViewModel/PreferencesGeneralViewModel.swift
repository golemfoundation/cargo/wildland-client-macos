//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import AppKit.NSPasteboard
import Combine
import WildlandCommon
import ServiceManagement
import os

@MainActor
protocol PreferencesGeneralViewModelType: ObservableObject {
  var publicKey: String { get }
  var rootPath: String { get }
  var items: [DropdownItem] { get }
  var publicKeyTooltipState: TooltipState { get }
  var launchCargoOnLogin: Bool { get set }
  var displayName: String { get set }
  var shouldDisplayNameForSharing: Bool { get set }
  var selectedItem: DropdownItem { get set }
  var isDropdownShown: Bool { get set }
  var displayNameForSharingDisabled: Bool { get }

  func copyPublicKey()
  func openFinder()
  func onCopyButtonHover(isHover: Bool)
}

final class PreferencesGeneralViewModel<S: Scheduler>: PreferencesGeneralViewModelType {

  // MARK: - Properties

  var publicKey: String {
    do {
      return try publicKeyProvider.publicKeyShort
    } catch {
      logger.publicKey(failure: error)
      return ""
    }
  }

  let items: [DropdownItem] = Settings.ColorThemeOption.allCases.map(DropdownItem.init)

  var displayName: String {
    didSet { displayNameSubject.send(displayName) }
  }
  private let displayNameSubject = PassthroughSubject<String, Never>()

  @Published var selectedItem: DropdownItem
  @Published var isDropdownShown: Bool = false
  @Published var launchCargoOnLogin: Bool
  @Published var shouldDisplayNameForSharing: Bool
  @Published var rootPath: String = ""
  @Published var publicKeyTooltipState: TooltipState = .dismiss
  @Published var displayNameForSharingDisabled: Bool

  private let publicKeyProvider: UserPublicKeyProviderType
  private let pasteboardProvider: PasteboardProviderType
  private let userService: DaemonUserService
  private let settings: SettingsType
  private let workspace: NSWorkspace
  private let appServiceType: SMAppService.Type
  private let logger: Logger
  private let scheduler: S
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  init(publicKeyProvider: UserPublicKeyProviderType,
       userService: DaemonUserService,
       pasteboardProvider: PasteboardProviderType = NSPasteboard.general,
       workspace: NSWorkspace = .shared,
       appServiceType: SMAppService.Type = SMAppService.self,
       settings: SettingsType = Settings(),
       scheduler: S,
       logger: Logger) {
    self.publicKeyProvider = publicKeyProvider
    self.userService = userService
    self.pasteboardProvider = pasteboardProvider
    self.workspace = workspace
    self.appServiceType = appServiceType
    self.settings = settings
    self.selectedItem = DropdownItem(settings.appearanceColorScheme)
    self.logger = logger
    self.launchCargoOnLogin = settings.startCargoOnLogin ?? false
    self.displayName = settings.displayName ?? ""
    self.shouldDisplayNameForSharing = settings.shouldDisplayNameForSharing ?? false
    self.displayNameForSharingDisabled = settings.displayName?.isEmpty ?? true
    self.scheduler = scheduler
    self.setupRootCargoUrl()
    self.setupBindings()
  }

  // MARK: - Setup

  private func setupRootCargoUrl() {
    Task { [weak self] in
      guard let self else { return }
      do {
        self.rootPath = try await self.userService.rootCargoUrl.path()
      } catch {
        self.logger.rootCargoUrl(failure: error)
      }
    }
  }

  private func setupBindings() {
    $launchCargoOnLogin
      .dropFirst()
      .sink { [weak self] enable in
        self?.setupStartCargoOnLogin(enable)
      }
      .store(in: &cancellables)

    displayNameSubject
      .debounce(for: .seconds(0.5), scheduler: scheduler)
      .sink { [weak self] value in
        self?.setupDisplayName(value)
      }
      .store(in: &cancellables)

    let emptyDisplayNamePublisher = displayNameSubject
      .map { $0.isEmpty }
      .removeDuplicates()
      .share()

    emptyDisplayNamePublisher
      .assign(to: &$displayNameForSharingDisabled)

    emptyDisplayNamePublisher
      .filter { $0 }
      .map { !$0 }
      .assign(to: &$shouldDisplayNameForSharing)

    $shouldDisplayNameForSharing
      .dropFirst()
      .sink { [weak self] enable in
        self?.setupDisplayNameForSharing(enable)
      }
      .store(in: &cancellables)

    $selectedItem
      .dropFirst()
      .map(\.id)
      .compactMap(Settings.ColorThemeOption.init)
      .sink { [weak self] colorThemeOption in
        self?.updateColorScheme(with: colorThemeOption)
      }
      .store(in: &cancellables)
  }

  // MARK: - Public

  func copyPublicKey() {
    copyPublicKeyToPasteboard()
  }

  func openFinder() {
    workspace.selectFile(nil, inFileViewerRootedAtPath: rootPath)
  }

  func onCopyButtonHover(isHover: Bool) {
    publicKeyTooltipState = isHover ? .hover(WLStrings.Preferences.General.copy) : .dismiss
  }

  // MARK: - Private

  private func copyPublicKeyToPasteboard() {
    do {
      pasteboardProvider.copy(string: try publicKeyProvider.userPublicKey)
      publicKeyTooltipState = .pressed(WLStrings.Preferences.General.copied)
    } catch {
      logger.publicKey(failure: error)
    }
  }

  private func setupStartCargoOnLogin(_ enable: Bool) {
    settings.updateStartCargoOnLogin(enable)
    do {
      try appServiceType.enableService(identifier: Defaults.BundleID.mainApp, enable)
    } catch {
      logger.enableService(failure: error)
    }
  }

  private func setupDisplayName(_ value: String) {
    settings.updateDisplayName(value)
  }

  private func setupDisplayNameForSharing(_ enable: Bool) {
    settings.updateDisplayNameForSharing(enable)
  }

  private func updateColorScheme(with themeOption: Settings.ColorThemeOption) {
    settings.updateColorScheme(with: themeOption)
    Task {
      do {
        try await userService.setTheme(themeOption)
      } catch {
        logger.updateColorScheme(failure: error)
      }
    }
  }
}

private extension DropdownItem {
  init(_ themeOption: Settings.ColorThemeOption) {
    self.init(id: themeOption.rawValue, title: themeOption.title)
  }
}

private extension Settings.ColorThemeOption {
  var title: String {
    switch self {
    case .dark:
      return WLStrings.ColorTheme.dark
    case .light:
      return WLStrings.ColorTheme.light
    case .automatic:
      return WLStrings.ColorTheme.automatic
    }
  }
}

private extension Logger {

  func rootCargoUrl(file: String = #file, method: String = #function, failure: Error) {
    error(file: file, method: method, failure: failure)
  }

  func publicKey(file: String = #file, method: String = #function, failure: Error) {
    error(file: file, method: method, failure: failure)
  }

  func enableService(file: String = #file, method: String = #function, failure: Error) {
    error(file: file, method: method, failure: failure)
  }

  func updateColorScheme(file: String = #file, method: String = #function, failure: Error) {
    error(file: file, method: method, failure: failure)
  }
}
