//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import os

@MainActor
protocol PreferencesStorageViewModelType: ObservableObject {
  var storages: [StorageItem] { get }
  var isLoading: Bool { get }
  func fetchData() async
  func stopFetchingData()
}

final class PreferencesStorageViewModel: PreferencesStorageViewModelType {

  // MARK: - Properties

  @Published private(set) public var storages: [StorageItem] = []
  @Published private(set) public var isLoading: Bool = false

  private let storagesProvider: StoragesProviderType
  private let decimalFormatter: ByteCountFormatter
  private var configuration: StoragesFetchingConfiguration
  private let logger: Logger
  private var canFetchData = false

  // MARK: - Initialization

  init(
    decimalFormatter: ByteCountFormatter = .decimalCountFormatter,
    storagesProvider: StoragesProviderType,
    configuration: StoragesFetchingConfiguration = .repeatable(duration: 15),
    logger: Logger
  ) {
    self.decimalFormatter = decimalFormatter
    self.storagesProvider = storagesProvider
    self.configuration = configuration
    self.logger = logger
  }

  // MARK: - Public

  func fetchData() async {
    canFetchData = true
    repeat {
      await fetchAndSetStorages()
      try? await Task.sleep(for: .seconds(configuration.duration))
    } while configuration.repeatable && canFetchData
  }

  func stopFetchingData() {
    canFetchData = false
  }

  // MARK: - Private

  private func fetchAndSetStorages() async {
    if storages.isEmpty { isLoading = true }
    do {
      storages = try await fetchStorages()
    } catch {
      logger.fetchStorages(failure: error)
      storages = []
    }
    isLoading = false
  }

  private func fetchStorages() async throws -> [StorageItem] {
    let storages: [Storage] = try await Task.detached(priority: .high) { [weak self] in
      guard let self else { return [] }
      return try self.storagesProvider.getStorages()
    }.value
    return storages
      .map { StorageItem($0, formatter: decimalFormatter) }
  }
}

private extension Logger {
  func fetchStorages(file: String = #file, method: String = #function, failure: Error) {
    error(file: file, method: method, failure: failure)
  }
}
