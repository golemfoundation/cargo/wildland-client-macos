//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct StoragesPlaceholderView: View {

  // MARK: - Properties

  let count: Int
  @State private var isShimmering = false

  private let gradient = LinearGradient(
    gradient: Gradient(stops: [
      .init(color: Color.clear, location: 0),
      .init(color: Color.clear, location: 0.35),
      .init(color: Color.white, location: 0.5),
      .init(color: Color.clear, location: 0.65),
      .init(color: Color.clear, location: 1)
    ]),
    startPoint: .leading,
    endPoint: .trailing
  )

  // MARK: - Views

  var body: some View {
    VStack(spacing: .zero) {
      ForEach(0..<count, id: \.self) { _ in
        placeholderContainer
      }
    }
    .onAppear(perform: {
      isShimmering.toggle()
    })
  }

  private var placeholderContainer: some View {
    VStack(spacing: .zero) {
      placeholder
        .padding(
          EdgeInsets(
            top: 20.0,
            leading: 4.0,
            bottom: 20.0,
            trailing: 4.0
          )
        )
      Divider()
        .formDivider
    }
  }

  private var placeholder: some View {
    placeholderModel(color: Color(light: Colors.sleet, dark: Colors.onyx))
      .overlay(
        placeholderModel(color: Color(light: Colors.dust, dark: Colors.styx))
          .mask(
            placeholderModel(color: Color.clear)
              .foregroundColor(.clear)
              .background(gradient)
              .offset(x: isShimmering ? 300 : -300)
              .animation(Animation.linear(duration: 2).repeatForever(autoreverses: false), value: isShimmering)
          )
      )
      .frame(
        minWidth: .zero,
        maxWidth: .infinity,
        alignment: .topLeading
      )
  }

  private func placeholderModel(color: Color) -> some View {
    HStack(alignment: .center, spacing: 16.0) {
      RectangleView(color: color)
        .frame(width: 40.0, height: 40.0)
      VStack(alignment: .leading, spacing: 4.0) {
        RectangleView(color: color)
          .frame(width: 100.0, height: 20.0)
        RectangleView(color: color)
          .frame(width: 180.0, height: 16.0)
        RectangleView(color: color, cornerRadius: .zero)
          .frame(height: 8.0)
      }
    }
  }

  private func RectangleView(color: Color, cornerRadius: CGFloat = 4.0) -> some View {
    Rectangle()
      .fill(color)
      .cornerRadius(cornerRadius)
  }
}
