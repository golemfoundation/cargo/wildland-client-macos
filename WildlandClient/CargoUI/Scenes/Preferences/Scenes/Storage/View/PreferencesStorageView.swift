//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct PreferencesStorageView<ViewModel: PreferencesStorageViewModelType>: View {

  // MARK: - Properties

  @ObservedObject var viewModel: ViewModel

  // MARK: - Views

  var body: some View {
    VStack(spacing: .zero) {
      Divider()
        .formDivider
      contentView
    }
    .task {
      await viewModel.fetchData()
    }
    .onDisappear {
      viewModel.stopFetchingData()
    }
  }

  @ViewBuilder private var contentView: some View {
    ZStack(alignment: .top) {
      StoragesPlaceholderView(count: 2)
        .opacity(viewModel.isLoading ? 1.0 : .zero)
      storageItems
        .opacity(viewModel.isLoading ? .zero : 1.0)
    }
    .animation(Animation.linear(duration: 0.3), value: viewModel.isLoading)
  }

  private var storageItems: some View {
    VStack(spacing: .zero) {
      ForEach(viewModel.storages, content: storageItemContainer)
    }
  }

  private func storageItemContainer(_ item: StorageItem) -> some View {
    VStack(spacing: .zero) {
      storageItem(item)
        .padding(.vertical, 20.0)
      Divider()
        .formDivider
    }
  }

  private func storageItem(_ item: StorageItem) -> some View {
    HStack(alignment: .center, spacing: 12.0) {
      Images.preferencesStorageIcon.swiftUIImage
      VStack(alignment: .leading, spacing: .zero) {
        Text(item.title)
          .foregroundColor(.text)
          .font(.textMMedium)
          .frame(height: 20.0)
        Text(item.subtitle)
          .foregroundColor(Color(light: Colors.silver, dark: Colors.granite))
          .font(.textMRegular)
          .frame(height: 20.0)
        ProgressView(value: item.usedBytes.value, total: item.totalBytes.value)
          .progressViewStyle(ProgressStyle())
          .padding(.top, 4.0)
      }
    }
  }
}

#if DEBUG
struct PreferencesStorageView_Previews: PreviewProvider {
  static var previews: some View {
    PreferencesStorageView(
      viewModel: PreferencesStorageViewModelStub(
        storages: [
          StorageItem(
            id: "1",
            title: "storage1",
            subtitle: "S3SC, 50 bytes of 100 bytes in use (50%)",
            usedBytes: Measurement<UnitInformationStorage>(value: 50, unit: .bytes),
            totalBytes: Measurement<UnitInformationStorage>(value: 100, unit: .bytes)
          )
        ],
        isLoading: false
      )
    )
  }
}
#endif
