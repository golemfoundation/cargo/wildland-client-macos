//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import Combine
import os

@MainActor
protocol PreferencesViewModelType: ObservableObject {
  var options: [PreferencesOptions] { get }
  var selectedOption: PreferencesOptions { get set }

  associatedtype GeneralViewModel: PreferencesGeneralViewModelType
  associatedtype StorageViewModel: PreferencesStorageViewModelType
  var contentViewType: ContentViewType<GeneralViewModel, StorageViewModel> { get }
}

enum ContentViewType<
  GeneralViewModel: PreferencesGeneralViewModelType,
  StorageViewModel: PreferencesStorageViewModelType> {
  case general(GeneralViewModel)
  case storages(StorageViewModel)
}

final class PreferencesViewModel<S: Scheduler>: PreferencesViewModelType {

  // MARK: - Properties

  let options: [PreferencesOptions] = PreferencesOptions.allCases
  @Published var selectedOption: PreferencesOptions
  @Published private(set) var contentViewType: ContentViewType<
    PreferencesGeneralViewModel<RunLoop>,
    PreferencesStorageViewModel
  >

  private let publicKeyProvider: UserPublicKeyProviderType
  private let userService: DaemonUserService
  private let storagesProvider: StoragesProviderType
  private let logger: Logger
  private let scheduler: S

  private var generalViewModel: PreferencesGeneralViewModel<RunLoop> {
    PreferencesGeneralViewModel(
      publicKeyProvider: publicKeyProvider,
      userService: userService,
      scheduler: RunLoop.main,
      logger: logger
    )
  }

  private var storageViewModel: PreferencesStorageViewModel {
    PreferencesStorageViewModel(
      storagesProvider: storagesProvider,
      logger: logger
    )
  }

  // MARK: - Initialization

  init(
    scheduler: S = DispatchQueue.main,
    publicKeyProvider: UserPublicKeyProviderType,
    userService: DaemonUserService,
    storagesProvider: StoragesProviderType,
    logger: Logger
  ) {
    self.scheduler = scheduler
    self.publicKeyProvider = publicKeyProvider
    self.userService = userService
    self.storagesProvider = storagesProvider
    self.contentViewType = .general(
      PreferencesGeneralViewModel(
        publicKeyProvider: publicKeyProvider,
        userService: userService,
        scheduler: RunLoop.main,
        logger: logger
      )
    )
    self.selectedOption = .general
    self.logger = logger
    self.setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    $selectedOption
      .dropFirst()
      .compactMap { [weak self] in
        self?.contentViewType(for: $0)
      }
      .receive(on: scheduler)
      .assign(to: &$contentViewType)
  }

  // MARK: - Private

  private func contentViewType(for option: PreferencesOptions) ->
  ContentViewType<PreferencesGeneralViewModel<RunLoop>, PreferencesStorageViewModel> {
    switch option {
    case .general:
      return .general(generalViewModel)
    case .storage:
      return .storages(storageViewModel)
    }
  }
}
