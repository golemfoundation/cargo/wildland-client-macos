//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import WildlandCommon

public protocol PreferencesSceneModelType: ObservableObject {
  var displayPreferences: Bool { get throws }
  var daemonUserService: DaemonUserService { get }
  var logger: Logger { get }
  func fetchAvailabilityState() async
}

public final class PreferencesSceneModel: PreferencesSceneModelType {

  // MARK: - Properties

  public let daemonUserService: DaemonUserService
  public let logger: Logger
  @Published public private(set) var displayPreferences: Bool = false

  // MARK: - Initialization

  public init(daemonUserService: DaemonUserService, logger: Logger) {
    self.daemonUserService = daemonUserService
    self.logger = logger
  }

  // MARK: - Action

  @MainActor
  public func fetchAvailabilityState() async {
    do {
      let availabilityState = try await daemonUserService.availabilityState
      displayPreferences = availabilityState.allowsClosingOnboarding
    } catch {
      logger.logFetchAvailabilityStateError(error)
    }
  }
}

private extension Logger {

  func logFetchAvailabilityStateError(method: String = #function, _ failure: Error?) {
    let details = "Failed to fetch availability state"
    error(method: method, details: details, failure: failure)
  }
}
