//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon
import os

/// A scene that shows the user's preferences, can be triggered from the Cargo app or daemon.
@MainActor
public struct PreferencesScene<SceneModel: PreferencesSceneModelType>: Scene {

  // MARK: - Properties

  private let userApi: UserPublicKeyProviderType & StoragesAccessProvider & UserAvailability
  @ObservedObject private var sceneModel: SceneModel

  // MARK: - Initialization

  public init(
    userApi: UserPublicKeyProviderType & StoragesAccessProvider & UserAvailability,
    sceneModel: SceneModel
  ) {
    self.userApi = userApi
    self.sceneModel = sceneModel
  }

  // MARK: - Body

  public var body: some Scene {
    Window(id: AppSceneType.preferences) {
      if (try? sceneModel.displayPreferences) == true {
        preferencesView
      } else {
        ZStack {
          EmptyView()
        }
        .task {
          await sceneModel.fetchAvailabilityState()
        }
      }
    }
  }

  // MARK: - Properties

  @MainActor
  private var preferencesView: some View {
    PreferencesView(
      viewModel:
        PreferencesViewModel(
          publicKeyProvider: userApi,
          userService: sceneModel.daemonUserService,
          storagesProvider: StoragesProvider(userApi: userApi),
          logger: sceneModel.logger
        )
    )
    .colorScheme()
  }
}
