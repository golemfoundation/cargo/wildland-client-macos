//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct PreferencesView<ViewModel: PreferencesViewModelType>: View {

  // MARK: - Properties

  @ObservedObject private(set) var viewModel: ViewModel

  // MARK: - Views

  var body: some View {
    VStack(spacing: .zero) {
      HStack(alignment: .top, spacing: 40.0) {
        PreferencesOptionsView(
          options: viewModel.options,
          selectedOption: $viewModel.selectedOption
        )
        contentView
          .frame(
            minWidth: .zero,
            maxWidth: .infinity,
            minHeight: .zero,
            maxHeight: .infinity,
            alignment: .topLeading
          )
      }
    }
    .padding(
      EdgeInsets(
        top: 28.0,
        leading: .zero,
        bottom: 24.0,
        trailing: 32.0
      )
    )
    .background(Color.windowBackground)
    .frame(width: 520.0, height: 432.0)
  }

  @ViewBuilder var contentView: some View {
    switch viewModel.contentViewType {
    case .general(let viewModel):
      PreferencesGeneralView(viewModel: viewModel)
    case .storages(let viewModel):
      PreferencesStorageView(viewModel: viewModel)
    }
  }
}

#if DEBUG
struct PreferencesView_Previews: PreviewProvider {
  static var previews: some View {
    PreferencesView(
      viewModel: PreferencesViewModelStub(
        options: PreferencesOptions.allCases,
        selectedOption: .storage,
        contentViewType: .storages(
          PreferencesStorageViewModelStub(
            storages: [],
            isLoading: true
          )
        )
      )
    )
  }
}
#endif
