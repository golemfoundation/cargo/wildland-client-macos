//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct PreferencesOptionsView: View {

  // MARK: - Properties

  let options: [PreferencesOptions]
  @Binding var selectedOption: PreferencesOptions
  private let itemHeight = 26.0

  private var dividerYOffset: CGFloat {
    guard let index = options.firstIndex(of: selectedOption) else { return .zero }
    return itemHeight * Double(index)
  }

  // MARK: - Views

  var body: some View {
    HStack(alignment: .top, spacing: 28.0) {
      verticalDivider
      itemsList
    }
    .frame(width: 112.0)
  }

  private var verticalDivider: some View {
    Divider()
      .frame(width: 4.0, height: itemHeight)
      .overlay(Colors.mint.swiftUIColor)
      .offset(CGSize(width: .zero, height: dividerYOffset))
  }

  private var itemsList: some View {
    VStack(alignment: .leading, spacing: .zero) {
      ForEach(options) { option in
        Text(option.title)
          .font(.textMSemiBold)
          .foregroundColor(textColor(option: option))
          .frame(height: itemHeight)
          .contentShape(Rectangle())
          .onTapGesture {
            guard option != selectedOption else { return }
            selectedOption = option
          }
      }
    }
    .frame(
      minWidth: .zero,
      maxWidth: .infinity,
      alignment: .topLeading
    )
  }

  private func textColor(option: PreferencesOptions) -> Color {
    selectedOption == option
    ? Color(light: Colors.mainText, dark: Colors.cloud)
    : Color(light: Colors.silver, dark: Colors.granite)
  }
}
