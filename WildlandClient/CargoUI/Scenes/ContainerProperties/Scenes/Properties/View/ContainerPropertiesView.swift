//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct ContainerPropertiesView<ViewModel: ContainerPropertiesViewModelType>: View {

  // MARK: - Properties

  @Environment(\.window) private var window
  @ObservedObject private var viewModel: ViewModel
  @FocusState private var changeFocusIfNeeded: Bool

  // MARK: - Initialization

  init(viewModel: ViewModel) {
    self.viewModel = viewModel
  }

  // MARK: - Views

  var body: some View {
    content
      .frame(width: 520.0, alignment: .topLeading)
      .fixedSize()
      .transformEnvironment(\.window, transform: { window in
        transform(window: window())
      })
      .contentShape(Rectangle())
      .onTapGesture {
        changeFocusIfNeeded = true
      }
      // Controls the focus state for the view and its subviews.
      .focused($changeFocusIfNeeded)
      .task {
        await viewModel.fetchContainerInformation()
      }
  }

  private var content: some View {
    VStack(spacing: 24.0) {
      header
        .padding(
          EdgeInsets(top: 16.0, leading: 24.0, bottom: .zero, trailing: 20.0)
        )
      optionsPanel
    }
  }

  private var header: some View {
    HStack(spacing: 16.0) {
      fileIcon
        .frame(width: 31.0, height: 31.0)
      VStack(alignment: .leading, spacing: .zero) {
        Text(viewModel.containerInfo.name)
          .foregroundColor(.text)
          .font(.textLSemiBold)
        HStack(spacing: 1.0) {
          Text(viewModel.containerInfo.properties)
            .foregroundColor(Colors.silver.swiftUIColor)
            .font(.textSRegular)
        }
      }
      Spacer()
    }
    .placeholder(
      animate: viewModel.isLoading,
      isRedacted: viewModel.isHeaderRedacting
    )
  }

  private var fileIcon: some View {
    guard let image = viewModel.containerInfo.icon else {
      return Images.containerPropertiesContainerGreenIcon.swiftUIImage.eraseToAnyView()
    }
    return Image(nsImage: image)
      .resizable()
      .scaledToFit()
      .eraseToAnyView()
  }

  private var optionsPanel: some View {
    OptionPanelView(
      options: viewModel.options,
      selectedOption: $viewModel.selectedOption,
      content: optionView,
      trailingContent: { _ in EmptyView() }
    )
  }

  @ViewBuilder private func optionView(for option: ContainerPropertiesOptions) -> some View {
    switch option {
    case .share:
      ContainerPropertiesShareView(viewModel: viewModel.shareViewModel)
    case .storage:
      ContainerPropertiesStorageView(viewModel: viewModel.storageViewModel)
    }
  }

  private func transform(window: NSWindow?) {
    let windowIdentifier = NSUserInterfaceItemIdentifier(viewModel.identifier)
    guard window?.identifier != windowIdentifier else { return }
    window?.identifier = windowIdentifier
    window?.styleMask.remove([.miniaturizable, .resizable])
    window?.titleVisibility = .hidden
    window?.titlebarSeparatorStyle = .none
    window?.titlebarAppearsTransparent = true
    window?.backgroundColor = NSColor(.windowBackground)
    window?.isMovableByWindowBackground = true
    window?.level = .floating
  }
}
