//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Combine
import WildlandCommon
import os

@MainActor
protocol ContainerPropertiesViewModelType: ObservableObject {
  associatedtype RemoveViewModelType: RemoveViewModelProtocol
  associatedtype ShareViewModel: ShareViewModelType
  associatedtype StorageViewModel: ContainerPropertiesStorageViewModelType

  var options: [ContainerPropertiesOptions] { get }
  var selectedOption: ContainerPropertiesOptions { get set }
  var isLoading: Bool { get }
  var isHeaderRedacting: Bool { get }
  var containerInfo: ContainerInfo { get }
  var identifier: String { get }
  var shareViewModel: ShareViewModel { get }
  var storageViewModel: StorageViewModel { get }
  func fetchContainerInformation() async
}

final class ContainerPropertiesViewModel: ContainerPropertiesViewModelType {
  typealias ShareViewModel = ContainerPropertiesShareViewModel<RemoveViewModelType>
  typealias RemoveViewModelType = RemoveViewModel
  typealias StorageViewModel = ContainerPropertiesStorageViewModel

  // MARK: - Properties
  @Published var selectedOption: ContainerPropertiesOptions = .share
  let options = ContainerPropertiesOptions.allCases
  let identifier: String
  @Published private(set) var isLoading: Bool = false
  @Published private(set) var isHeaderRedacting: Bool = false
  @Published private(set) var containerInfo: ContainerInfo = .placeholder
  private let dependecies: ContainerPropertiesViewModelDependecies
  private let logger: Logger

  let shareViewModel: ContainerPropertiesShareViewModel<RemoveViewModel>
  let storageViewModel: ContainerPropertiesStorageViewModel

  // MARK: - Initialization

  init(
    identifier: String,
    dependecies: ContainerPropertiesViewModelDependecies,
    logger: Logger
  ) {
    self.identifier = identifier
    self.dependecies = dependecies
    self.logger = logger
    self.shareViewModel = ContainerPropertiesShareViewModel(
      identifier: identifier,
      pathResolvable: dependecies.pathResolvable,
      userNotifications: dependecies.userNotifications,
      logger: logger
    )
    self.storageViewModel = ContainerPropertiesStorageViewModel(
      identifier: identifier,
      storagesProvider: dependecies.storagesProvider,
      pathResolvable: dependecies.pathResolvable,
      decimalFormatter: dependecies.formatter,
      logger: logger
    )
  }

  // MARK: - Public

  func fetchContainerInformation() async {
    isLoading = true
    do {
      containerInfo = try await prepareContainerInfo()
    } catch {
      isHeaderRedacting = true
    }
    isLoading = false
  }

  // MARK: - Private

  private func prepareContainerInfo() async throws -> ContainerInfo {
    let fileItem = try await dependecies.fileItemProvider.fileItem(for: identifier, path: nil, metadata: nil)

    let bytes = Measurement<UnitInformationStorage>(value: Double(fileItem.size), unit: .bytes)
    var properties = dependecies.formatter.string(from: bytes)

    if fileItem.isFolder {
      let itemsCount = try await dependecies.fileItemProvider.fileItems(for: fileItem.identifier).count
      properties += itemsCount == 1
      ? WLStrings.ContainerProperties.oneFile
      : String(format: WLStrings.ContainerProperties.filesCount, String(itemsCount))
    }

    return ContainerInfo(
      name: fileItem.name,
      properties: properties,
      icon: dependecies.thumbnailProvider.getThumbnail(from: fileItem)
    )
  }
}

extension ContainerInfo {
  static let placeholder = ContainerInfo(
    name: "example.file",
    properties: "properties",
    icon: nil
  )
}
