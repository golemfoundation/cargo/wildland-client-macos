//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import WildlandCommon

final class ContainerPropertiesShareItem: Identifiable, ObservableObject {

  // MARK: - Properties

  lazy private(set) var publicKeyAddedPublisher = publicKeyAddedSubject.eraseToAnyPublisher()
  private let publicKeyAddedSubject = PassthroughSubject<Void, Never>()
  @Published var name: String
  let id: String
  let publicKey: String
  private let userDefaults: UserDefaultsProviderType
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  convenience init(publicKey: String) {
    self.init(publicKey: publicKey, userDefaults: UserDefaults.standard)
  }

  init(publicKey: String, userDefaults: UserDefaultsProviderType) {
    self.publicKey = publicKey
    self.userDefaults = userDefaults
    self.id = publicKey
    self.name = userDefaults.string(forKey: publicKey) ?? ""
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    $name
      .sink { [weak self] name in
        guard let self else { return }
        self.userDefaults.set(name, forKey: self.publicKey)
      }
      .store(in: &cancellables)
  }

  public func editIfNeeded(publicKey: String?) {
    guard self.publicKey == publicKey else { return }
    publicKeyAddedSubject.send()
  }
}

extension ContainerPropertiesShareItem: Equatable {
  static func == (lhs: ContainerPropertiesShareItem, rhs: ContainerPropertiesShareItem) -> Bool {
    lhs.id == rhs.id
  }
}
