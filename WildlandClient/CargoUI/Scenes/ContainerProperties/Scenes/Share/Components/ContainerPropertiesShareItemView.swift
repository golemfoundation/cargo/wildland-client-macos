//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import Combine
import WildlandCommon

private extension Color {
  static let cellBackground = Color(light: Colors.snow, dark: Colors.onyx)
  static let cellBorder = Colors.mint.swiftUIColor
  static let avatarBorder = Color(light: Colors.pureWhite, dark: Colors.obsidian)
}

enum ContainerPropertiesShare {
  static let itemPrefferedHeight: CGFloat = 56.0
}

struct ContainerPropertiesShareItemView<ViewModel: ContainerPropertiesShareViewRemoveType>: View {

  // MARK: - Environment

  @EnvironmentObject private var viewModel: ViewModel

  // MARK: - Properties

  @State private var isHovering = false
  @State private var isEditing: Bool = false
  @ObservedObject var item: ContainerPropertiesShareItem
  @FocusState private var isFocused: Bool

  private var shouldShowExtras: Bool {
    isHovering || isEditing
  }

  private var fontForPublicKey: SwiftUI.Font {
    (isEditing || isNameSet) ? .textMRegular : .bodyText
  }

  private var isNameSet: Bool {
    !item.name.isEmpty
  }

  // MARK: - Initialization

  init(item: ContainerPropertiesShareItem) {
    self.item = item
  }

  // MARK: - Views

  var body: some View {
    HStack(alignment: .center, spacing: 8.0) {
      avatar
      centerContent
        .padding(.leading, 8.0)
      Spacer(minLength: .zero)
      removeButtonIfNeeded
        .padding(.trailing, 8.0)
    }
    .padding(.all, 8.0)
    .frame(height: ContainerPropertiesShare.itemPrefferedHeight, alignment: .center)
    .background(shouldShowExtras ? Color.cellBackground : .clear)
    .cornerRadius(4.0)
    .overlay { borderOverlayIfNeeded }
    .onHover { hovering in
      isHovering = hovering
    }
    .onChange(of: isFocused) { newValue in
      isEditing = newValue
    }
    .onReceive(item.publicKeyAddedPublisher) {
      isEditing(true)
    }
  }

  private var avatar: some View {
    Images.containerPropertiesAvatar.swiftUIImage
      .padding(10)
      .overlay(
        RoundedRectangle(cornerRadius: 4.0)
          .strokeBorder(Color.avatarBorder, lineWidth: 1.0)
      )
  }

  private var centerContent: some View {
    VStack(alignment: .leading, spacing: isNameSet || isEditing ? 6.0 : .zero) {
      editTextField
      editPublicKey
    }
    .animation(.easeInOut(duration: 0.15), value: isEditing)
  }

  private var editTextField: some View {
    HStack(spacing: 5.0) {
      textFieldIfNeeded
      if isNameSet, shouldShowExtras {
        editButtonIfNeeded
      }
    }
    .animation(isNameSet ? nil : .easeInOut(duration: 0.15), value: isEditing)
  }

  private var editPublicKey: some View {
    HStack(spacing: 5.0) {
      publicKey
      if !isNameSet, shouldShowExtras {
        editButtonIfNeeded
      }
    }
  }

  @ViewBuilder private var removeButtonIfNeeded: some View {
    if shouldShowExtras {
      Button(action: {
        viewModel.remove(publicKey: item.publicKey)
      }) {
        Text(WLStrings.ContainerProperties.Share.Item.remove)
          .font(.textMRegular)
          .foregroundColor(
            Colors.sky.swiftUIColor
          )
          .frame(height: 40.0)
      }
      .buttonStyle(.plain)
    }
  }

  @ViewBuilder private var borderOverlayIfNeeded: some View {
    if shouldShowExtras {
      RoundedRectangle(cornerRadius: 4.0)
        .strokeBorder(Color.cellBorder, lineWidth: 1.0)
    }
  }

  @ViewBuilder private var textFieldIfNeeded: some View {
    if isEditing || isNameSet {
      TextField(text: $item.name)
        .textFieldStyle(.plain)
        .font(.bodyText)
        .foregroundColor(.text)
        .onSubmit {
          isEditing(false)
        }
        .placeholder(isVisible: !isNameSet) {
          Text(WLStrings.ContainerProperties.Share.Item.aliasPlaceholder)
            .foregroundColor(.text)
            .opacity(0.25)
            .fixedSize()
            .allowsHitTesting(false)
        }
        .allowsHitTesting(isEditing)
        .focused($isFocused)
        .fixedSize(horizontal: !isEditing, vertical: true)
    }
  }

  @ViewBuilder private var editButtonIfNeeded: some View {
    if !isEditing {
      Button {
        isEditing(true)
      } label: {
        Images.containerPropertiesEdit.swiftUIImage
      }
      .buttonStyle(.plain)
    }
  }

  private var publicKey: some View {
    Text(item.publicKey)
      .font(fontForPublicKey)
      .truncationMode(.middle)
      .lineLimit(1)
      .foregroundColor(Colors.silver.swiftUIColor)
      .frame(maxWidth: 99.0, alignment: .leading)
  }

  // MARK: - Private

  private func isEditing(_ newValue: Bool) {
    isFocused = newValue
    isEditing = newValue
  }
}
