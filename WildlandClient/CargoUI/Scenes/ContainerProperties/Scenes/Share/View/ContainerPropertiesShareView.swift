//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct ContainerPropertiesShareView<ViewModel: ShareViewModelType>: View {

  // MARK: - Properties

  @ObservedObject var viewModel: ViewModel
  private let shareItemListSpacing = 24.0
  private let maxNumberOfVisibleItems = 2

  // MARK: - Views

  var body: some View {
    VStack(spacing: .zero) {
      PublicKeyShareView<ViewModel>()

      if !viewModel.shareItems.isEmpty {
        Divider().standard
        shareItemList
          .padding(.all, 16.0)
      }

      removeShareViewIfNeeded
    }
    .environmentObject(viewModel)
    .task {
      await viewModel.onAppear()
    }
  }

  @ViewBuilder private var removeShareViewIfNeeded: some View {
    if let removeViewModel = viewModel.removeViewModel {
      RemoveShareView<RemoveViewModel>()
        .padding([.leading, .trailing], 24.0)
        .frame(height: 88.0)
        .background(Color(light: Colors.snow, dark: Colors.onyx))
        .environmentObject(removeViewModel)
    }
  }

  private var shareItemList: some View {
    ScrollView(showsIndicators: viewModel.shareItems.count > maxNumberOfVisibleItems) {
      VStack(spacing: shareItemListSpacing) {
        ForEach(viewModel.shareItems, content: containerPropertiesShareItemView)
      }
    }
    .scrollDisabled(viewModel.shareItems.count <= maxNumberOfVisibleItems)
    .frame(maxHeight: adjustNonScrollableMaxHeight(CGFloat(maxNumberOfVisibleItems)))
  }

  private func containerPropertiesShareItemView(_ shareItem: ContainerPropertiesShareItem) -> some View {
    ContainerPropertiesShareItemView<ViewModel>(item: shareItem)
      .onAppear {
        shareItem.editIfNeeded(publicKey: viewModel.publicKey)
      }
  }

  private func adjustNonScrollableMaxHeight(_ maxVisibleItems: CGFloat) -> CGFloat {
    maxVisibleItems * (ContainerPropertiesShare.itemPrefferedHeight + shareItemListSpacing) - shareItemListSpacing
  }
}
