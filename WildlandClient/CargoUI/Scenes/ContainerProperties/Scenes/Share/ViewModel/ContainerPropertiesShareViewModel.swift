//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit.NSPasteboard
import WildlandCommon
import os
import Combine

@MainActor
protocol ContainerPropertiesShareViewModelType: ObservableObject {
  var publicKey: String { get set }
  var isLoading: Bool { get set }
  var shareItems: [ContainerPropertiesShareItem] { get }
  func copyLinkPressed() async
  func onAppear() async
}

protocol ContainerPropertiesShareViewRemoveType: ObservableObject {
  associatedtype RemoveViewModelType: RemoveViewModelProtocol
  func remove(publicKey: String)
  var removeViewModel: RemoveViewModelType? { get }
}

@MainActor
protocol ShareViewModelType: ContainerPropertiesShareViewModelType, ContainerPropertiesShareViewRemoveType {}

public final class ContainerPropertiesShareViewModel<RemoveViewModelType: RemoveViewModelProtocol>: ShareViewModelType {

  // MARK: - Properties

  private let identifier: String
  private let pathResolvable: PathResolvable
  private let pasteboard: PasteboardProviderType
  private let shareOwnerProvider: ShareOwnerProvider
  private let userNotifications: UserNotificationsType
  private let settings: SettingsType
  private let logger: Logger
  private var cancellables = Set<AnyCancellable>()

  @Published var publicKey: String = ""
  @Published var isLoading: Bool = false
  @Published private(set) var shareItems: [ContainerPropertiesShareItem] = []
  @Published private(set) var removeViewModel: RemoveViewModelType?

  private var path: String {
    get async throws {
      try await Task {
        try self.pathResolvable.path(for: self.identifier)
      }
      .value
    }
  }

  private var userAlias: String? {
    settings.shouldDisplayNameForSharing == true ? settings.displayName : nil
  }

  // MARK: - Initialization

  public init(
    identifier: String,
    pathResolvable: PathResolvable,
    pasteboard: PasteboardProviderType = NSPasteboard.general,
    shareApi: ShareOwnerProvider = UserApiProvider(),
    userNotifications: UserNotificationsType,
    settings: SettingsType = Settings(),
    logger: Logger
  ) {
    self.identifier = identifier
    self.pathResolvable = pathResolvable
    self.pasteboard = pasteboard
    self.shareOwnerProvider = shareApi
    self.userNotifications = userNotifications
    self.settings = settings
    self.logger = logger
  }

  // MARK: - Public

  func copyLinkPressed() async {
    await getShareMessage()
    await getRecipientsList()
  }

  func onAppear() async {
    await getRecipientsList()
  }

  func remove(publicKey: String) {
    let aliasIfExist = shareItems.first { item in
      publicKey == item.publicKey
    }?.name

    removeViewModel = RemoveViewModelType(
      publicKey: publicKey,
      alias: aliasIfExist
    )

    removeViewModel?
      .shouldRemovePublisher
      .handleEvents(receiveOutput: { [weak self] _ in
        self?.removeViewModel = nil
      })
      .filter { $0 == true }
      .sink(receiveValue: { [weak self] _ in
        self?.removeSelectedPublicKey()
      })
      .store(in: &cancellables)
  }

  private func removeSelectedPublicKey() {
    // TODO: - Use share API when available (CARGO-410).
  }

  // MARK: - Private

  private func getShareMessage() async {
    isLoading = true
    do {
      let urlToShare = try await prepareUrlToShare()
      pasteboard.copy(string: urlToShare)
      userNotifications.showNotification(category: .shareLinkCopied)
    } catch {
      logger.error(failure: error)
    }
    isLoading = false
  }

  private func getRecipientsList() async {
    do {
      let allRecipients = try await shareOwnerProvider.getAllRecipients(for: identifier)
      shareItems = allRecipients.map(ContainerPropertiesShareItem.init)
    } catch {
      logger.error(failure: error)
    }
  }

  // MARK: - Private

  private func prepareUrlToShare() async throws -> String {
    let body = try await shareOwnerProvider.share(path: path, publicKey: publicKey)
    let sharingMessage = SharingMessage(body: body, userAlias: userAlias)
    return Deeplink.share(sharingMessage).url.absoluteString
  }
}
