//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Combine
import WildlandCommon
import os

@MainActor
protocol ContainerPropertiesStorageViewModelType: ObservableObject {
  var storageItems: [StorageItem] { get }
  var isLoading: Bool { get }
  func fetchData() async
  func stopFetchingData()
}

public final class ContainerPropertiesStorageViewModel: ContainerPropertiesStorageViewModelType {

  // MARK: - Properties

  @Published private(set) public var storageItems: [StorageItem] = .placeholder
  @Published private(set) public var isLoading: Bool = false
  private let identifier: String
  private let storagesProvider: StoragesProviderType
  private let pathResolvable: PathResolvable
  private let decimalFormatter: ByteCountFormatter
  private var configuration: StoragesFetchingConfiguration
  private let logger: Logger
  private var canFetchData = false

  // MARK: - Initialization

  init(
    identifier: String,
    storagesProvider: StoragesProviderType,
    pathResolvable: PathResolvable,
    decimalFormatter: ByteCountFormatter = .decimalCountFormatter,
    configuration: StoragesFetchingConfiguration = .repeatable(duration: 15),
    logger: Logger
  ) {
    self.identifier = identifier
    self.storagesProvider = storagesProvider
    self.pathResolvable = pathResolvable
    self.decimalFormatter = decimalFormatter
    self.configuration = configuration
    self.logger = logger
  }

  // MARK: - Public

  func fetchData() async {
    canFetchData = true
    repeat {
      await fetchAndSetStorages()
      try? await Task.sleep(for: .seconds(configuration.duration))
    } while configuration.repeatable && canFetchData
  }

  func stopFetchingData() {
    canFetchData = false
  }

  // MARK: - Private

  private func fetchAndSetStorages() async {
    if storageItems == .placeholder { isLoading = true }
    do {
      storageItems = try await fetchStorages()
    } catch {
      logger.error(failure: error)
      storageItems = []
    }
    isLoading = false
  }

  private func fetchStorages() async throws -> [StorageItem] {
    let storages: [Storage] = try await Task.detached(priority: .high) { [weak self] in
      guard let self else { return [] }
      let path = try self.pathResolvable.path(for: self.identifier)
      return try self.storagesProvider.getStorages(path: path)
    }.value
    return storages
      .map { StorageItem($0, formatter: decimalFormatter) }
  }
}
