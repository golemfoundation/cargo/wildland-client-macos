//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct ContainerPropertiesStorageView<ViewModel: ContainerPropertiesStorageViewModelType>: View {

  // MARK: - Properties

  @ObservedObject private(set) var viewModel: ViewModel

  // MARK: - Views

  var body: some View {
    storagesList
      .padding(.all, 20.0)
      .task {
        await viewModel.fetchData()
      }
      .onDisappear {
        viewModel.stopFetchingData()
      }
  }

  private var storagesList: some View {
    VStack(spacing: .zero) {
      ForEach(viewModel.storageItems) { item in
        ContainerPropertiesStorageItemView(item: item)
        if viewModel.storageItems.last != item {
          Divider()
            .standard
            .padding(.vertical, 20.0)
        }
      }
    }
    .placeholder(animate: viewModel.isLoading)
  }
}
