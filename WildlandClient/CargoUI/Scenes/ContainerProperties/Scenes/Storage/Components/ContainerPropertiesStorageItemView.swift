//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct ContainerPropertiesStorageItemView: View {

  // MARK: - Properties

  let item: StorageItem

  // MARK: - Views

  var body: some View {
    HStack(spacing: 16.0) {
      icon
      centerContent
      Spacer()
    }
  }

  private var icon: Image {
    Images.containerPropertiesStorageIcon.swiftUIImage
  }

  private var centerContent: some View {
    VStack(alignment: .leading, spacing: .zero) {
      Text(item.title)
        .font(.textLSemiBold)
        .foregroundColor(.text)
      Text(item.subtitle)
        .font(.textMRegular)
        .foregroundColor(Colors.silver.swiftUIColor)
    }
  }
}
