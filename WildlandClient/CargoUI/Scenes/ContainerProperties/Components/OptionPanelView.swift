//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct OptionPanelView<Content: View, HeaderContent: View, T: OptionPanelItem>: View {

  // MARK: - Properties
  private let options: [T]
  @Binding private var selectedOption: T
  private let horizontalPadding: CGFloat
  private let content: Content
  private let trailingContent: HeaderContent
  private let selectorWidth = 24.0
  @State private var optionsOffsets = [T: CGFloat]()

  // MARK: - Initialization
  init(options: [T],
       selectedOption: Binding<T>,
       horizontalPadding: CGFloat = 24.0,
       @ViewBuilder content: (T) -> Content,
       @ViewBuilder trailingContent: (T) -> HeaderContent) {
    self.options = options
    self._selectedOption = selectedOption
    self.horizontalPadding = horizontalPadding
    self.content = content(selectedOption.wrappedValue)
    self.trailingContent = trailingContent(selectedOption.wrappedValue)
  }

  // MARK: - Views
  var body: some View {
    VStack(alignment: .leading, spacing: .zero) {
      header
        .padding(.horizontal, horizontalPadding)
      divider
        .padding(.top, 9.0)
      content
    }
  }

  @ViewBuilder private var header: some View {
    HStack(spacing: 24.0) {
      ForEach(options, content: text)
      Spacer()
      trailingContent
    }
  }

  @ViewBuilder private func text(for option: T) -> some View {
    Text(option.title)
      .font(.textLSemiBold)
      .foregroundColor(color(for: option))
      .overlay {
        GeometryReader { geometry in
          Color.clear
            .onAppear {
              let frame = geometry.frame(in: .global)
              let offset = frame.midX - selectorWidth / 2.0
              optionsOffsets[option] = offset
            }
        }
      }
      .onTapGesture {
        selectedOption = option
      }
  }

  private func color(for option: T) -> Color {
    option == selectedOption
    ? .text
    : .colors(
      light: Colors.mainText.swiftUIColor.opacity(0.4),
      dark: Colors.cloud.swiftUIColor.opacity(0.5)
    )
  }

  @ViewBuilder private var divider: some View {
    ZStack(alignment: .bottomLeading) {
      Divider().standard
      Divider()
        .frame(width: selectorWidth, height: 2.0)
        .overlay(Colors.mint.swiftUIColor)
        .offset(x: optionsOffsets[selectedOption] ?? .zero)
    }
  }
}
