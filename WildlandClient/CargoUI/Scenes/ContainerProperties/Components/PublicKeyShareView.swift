//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct PublicKeyShareView<ViewModel: ContainerPropertiesShareViewModelType>: View {

  // MARK: - Environment

  @EnvironmentObject private var viewModel: ViewModel

  // MARK: - Views

  var body: some View {
    HStack {
      roundedTextField
        .disabled(viewModel.isLoading)
      copyLinkButton
        .disabled(viewModel.publicKey.isEmpty || viewModel.isLoading)
    }
    .padding(.all, 24.0)
    .pasteboard(text: $viewModel.publicKey)
  }

  private var roundedTextField: some View {
    TextField("", text: $viewModel.publicKey)
      .textFieldStyle(textFieldStyle)
  }

  private var textFieldStyle: SimpleTextFieldStyle {
    SimpleTextFieldStyle(
      placeholder: WLStrings.ContainerProperties.Share.publicKeyPlaceholder,
      isVisiblePlaceholder: viewModel.publicKey.isEmpty,
      overlayColor: .formBorder,
      backgroundColor: .formBackground
    )
  }

  private var copyLinkButton: some View {
    WLButton(
      title: viewModel.isLoading ? WLStrings.button.copying : WLStrings.button.copyLink,
      kind: .main,
      isLoading: $viewModel.isLoading,
      action: {
        Task { await viewModel.copyLinkPressed() }
      }
    )
    .setProperty(\.horizontalPadding, viewModel.isLoading ? 18.5 : 28.5)
  }
}
