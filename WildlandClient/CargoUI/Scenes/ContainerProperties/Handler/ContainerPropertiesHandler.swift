//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit.NSApplication
import Combine
import WildlandCommon
import os

public protocol ContainerHandler {
  var presentSubscriber: AnySubscriber<String, Never> { get }
}

public final class ContainerPropertiesHandler<S: Scheduler>: ContainerHandler {

  // MARK: - Properties

  lazy public private(set) var presentSubscriber = AnySubscriber(identifierSubject)
  private let identifierSubject = PassthroughSubject<String, Never>()
  private var presenters: [Presenter<PanelWindow>] = []
  private var cancellables = Set<AnyCancellable>()
  private let dependecies: ContainerPropertiesViewModelDependecies
  private let scheduler: S
  private let logger: Logger

  // MARK: - Initialization

  public init(
    dependecies: ContainerPropertiesViewModelDependecies,
    scheduler: S = DispatchQueue.main,
    logger: Logger
  ) {
    self.dependecies = dependecies
    self.scheduler = scheduler
    self.logger = logger
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    identifierSubject
      .receive(on: scheduler)
      .sink(receiveValue: { [weak self] identifier in
        self?.presentIfNeeded(for: identifier)
      })
      .store(in: &cancellables)
  }

  // MARK: - Private

  private func presentIfNeeded(for identifier: String) {
    let window = NSApplication.shared.window(identifier: identifier)
    window?.orderFront(nil)
    guard window == nil else { return }
    Task {
      await present(for: identifier)
    }
  }

  @MainActor
  private func present(for identifier: String) {
    let viewModel = ContainerPropertiesViewModel(
      identifier: identifier,
      dependecies: dependecies,
      logger: logger
    )
    let view = ContainerPropertiesView(viewModel: viewModel)
    let presenter = Presenter<PanelWindow>()

    presenter.dismissPublisher
      .sink { [weak self, weak presenter] in
        self?.presenters.removeAll(where: { $0 === presenter })
      }
      .store(in: &cancellables)

    presenters.append(presenter)

    presenter.present {
      view
        .colorScheme()
        .window()
    }
  }
}
