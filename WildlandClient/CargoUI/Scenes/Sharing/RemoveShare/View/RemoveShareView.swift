//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct RemoveShareView<ViewModel: RemoveViewModelProtocol>: View {

  // MARK: - Environment

  @EnvironmentObject private var viewModel: ViewModel

  // MARK: - Views

  var body: some View {
    HStack(spacing: 3.0) {
      message
      Spacer()
      cancelButton
      removeButton
    }
  }

  private var cancelButton: some View {
    WLButton(
      title: WLStrings.button.cancel,
      kind: .fallback,
      action: {
        viewModel.onCancel()
      }
    )
  }

  private var removeButton: some View {
    WLButton(
      title: WLStrings.button.remove,
      kind: .warning,
      action: {
        viewModel.onRemove()
      }
    )
  }

  @ViewBuilder private var message: some View {
    let identity = viewModel.identity
    let format = WLStrings.ContainerProperties.Share.Remove.message
    let formattedString = String(format: format, identity)

    if let range = formattedString.range(of: identity) {
      let prefix = String(formattedString[..<range.lowerBound])
      let suffix = String(formattedString[range.upperBound...])

      (Text(prefix) + Text(identity).fontWeight(.semibold) + Text(suffix))
        .font(.textMRegular)
        .foregroundColor(Color(light: Colors.silver, dark: Colors.cloud))
    }
  }
}
