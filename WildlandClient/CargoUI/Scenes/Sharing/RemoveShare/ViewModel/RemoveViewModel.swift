//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine

public protocol RemoveViewModelProtocol: ObservableObject {
  var shouldRemovePublisher: AnyPublisher<Bool, Never> { get }
  var identity: String { get }
  init(publicKey: String, alias: String?)
  func onCancel()
  func onRemove()
}

final class RemoveViewModel: RemoveViewModelProtocol {

  // MARK: - Properties

  lazy public private(set) var shouldRemovePublisher = shouldRemoveSubject.eraseToAnyPublisher()
  private let shouldRemoveSubject = PassthroughSubject<Bool, Never>()
  let identity: String

  // MARK: - Initialization

  init(publicKey: String, alias: String?) {
    guard let alias, !alias.isEmpty else {
      self.identity = publicKey
      return
    }
    self.identity = alias
  }

  // MARK: - Public

  func onCancel() {
    shouldRemoveSubject.send(false)
  }

  func onRemove() {
    shouldRemoveSubject.send(true)
  }
}
