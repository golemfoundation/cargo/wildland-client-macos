//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit.NSWorkspace
import WildlandCommon
import Combine

@MainActor
protocol ReceiveShareViewModelType: ObservableObject {
  var isContentRedacting: Bool { get }
  var isLoading: Bool { get }
  var infoViewType: InfoViewType? { get }
  var shareInfo: ShareInfo { get }
  var headerTitle: AttributedString { get }
  var closePublisher: AnyPublisher<Void, Never> { get }
  var windowIdentifier: String { get }
  func fetchData() async
  func mainButtonPressed() async
  func cancelAction()
}

final class ReceiveShareViewModel: ReceiveShareViewModelType {

  // MARK: - Properties

  lazy private(set) var closePublisher = closeSubject.eraseToAnyPublisher()
  private let closeSubject = PassthroughSubject<Void, Never>()

  let windowIdentifier: String
  @Published private(set) var isContentRedacting: Bool = true
  @Published private(set) var isLoading: Bool = false
  @Published private(set) var infoViewType: InfoViewType?
  @Published private(set) var shareInfo: ShareInfo = .placeholder
  private let shareReceiverProvider: ShareReceiverProviderType
  private let sharingMessage: SharingMessage
  private let urlProvider: TemporaryUrlProvider
  private let decimalFormatter: ByteCountFormatter
  private let workspace: NSWorkspace
  private let thumbnailProvider: ThumbnailProviderType

  var headerTitle: AttributedString {
    guard shareInfo != .placeholder else { return AttributedString(WLStrings.Share.Receive.headerTitle) }
    var sender = shareInfo.senderPublicKey
    if let senderName = shareInfo.senderName, !senderName.isEmpty {
      sender = senderName
    }
    var result = AttributedString(sender + WLStrings.Share.Receive.headerTitle)
    if let rangeOfBoldedText = result.range(of: sender) {
      result[rangeOfBoldedText].font = .displaySmall
    }
    return result
  }

  // MARK: - Initialization

  init(
    sharingMessage: SharingMessage,
    shareReceiverProvider: ShareReceiverProviderType,
    urlProvider: TemporaryUrlProvider,
    decimalFormatter: ByteCountFormatter = .decimalCountFormatter,
    workspace: NSWorkspace = .shared,
    thumbnailProvider: ThumbnailProviderType = ThumbnailProvider()
  ) {
    self.sharingMessage = sharingMessage
    self.shareReceiverProvider = shareReceiverProvider
    self.urlProvider = urlProvider
    self.decimalFormatter = decimalFormatter
    self.workspace = workspace
    self.thumbnailProvider = thumbnailProvider
    self.windowIdentifier = sharingMessage.body
  }

  // MARK: - Public

  func fetchData() async {
    infoViewType = nil
    isLoading = true
    isContentRedacting = true
    do {
      let previewMessage = try await shareReceiverProvider.prepareSharePreviewMessage(sharingMessage: sharingMessage)
      shareInfo = prepareShareInfo(previewMessage: previewMessage)
      isContentRedacting = false
    } catch {
      infoViewType = .error(error.localizedDescription)
    }
    isLoading = false
  }

  func mainButtonPressed() async {
    infoViewType?.isError == true
    ? await fetchData()
    : await addToCargo()
  }

  func cancelAction() {
    closeSubject.send()
  }

  // MARK: - Private

  private func addToCargo() async {
    infoViewType = .loading(WLStrings.Share.Receive.addingToCargo)
    do {
      let sharedPath = Defaults.Path.root + Defaults.Path.shared
      try await shareReceiverProvider.mountShareContainer(sharingMessage: sharingMessage, path: sharedPath)
      infoViewType = .info(WLStrings.Share.Receive.addedToCargo)
      try await openShared()
      closeSubject.send()
    } catch {
      infoViewType = .error(error.localizedDescription)
    }
  }

  private func prepareShareInfo(previewMessage: PreviewShareMessage) -> ShareInfo {
    let fileNameComponents = previewMessage.fileName.components(separatedBy: ".")
    let fileName = fileNameComponents.count == 2 ? fileNameComponents[0] : previewMessage.fileName
    let fileNameType = fileNameComponents.count == 2 ? fileNameComponents[1] : ""
    let bytes = Measurement<UnitInformationStorage>(value: Double(previewMessage.size), unit: .bytes)
    return ShareInfo(
      senderName: sharingMessage.userAlias,
      senderPublicKey: previewMessage.publicKey,
      sharedContentName: fileName,
      sharedContentSize: decimalFormatter.string(from: bytes),
      sharedContentType: fileNameType,
      image: thumbnailProvider.getThumbnail(from: previewMessage.fileName)
    )
  }

  private func openShared() async throws {
    workspace.selectFile(nil, inFileViewerRootedAtPath: try await urlProvider.rootUrl.path())
  }
}

extension ShareInfo {
  static let placeholder = ShareInfo(
    senderName: "Bob",
    senderPublicKey: "0xc5a...82kcb",
    sharedContentName: "Sponsor Booth details",
    sharedContentSize: "16.56 MB",
    sharedContentType: "JPEG",
    image: nil
  )
}
