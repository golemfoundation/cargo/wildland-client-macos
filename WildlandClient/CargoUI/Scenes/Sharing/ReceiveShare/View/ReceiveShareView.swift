//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct ReceiveShareView<ViewModel: ReceiveShareViewModelType>: View {

  // MARK: - Properties

  @ObservedObject private(set) var viewModel: ViewModel
  @Environment(\.window) var window

  private let sectionInsets = EdgeInsets(
    top: 32.0,
    leading: 24.0,
    bottom: 32.0,
    trailing: 24.0
  )

  private var isError: Bool {
    viewModel.infoViewType?.isError == true
  }

  private var shouldShowPublicKey: Bool {
    viewModel.shareInfo.senderName?.isEmpty == false
  }

  // MARK: - Views

  var body: some View {
    content
      .frame(width: 520.0)
      .background(Color.windowBackground)
      .cornerRadius(8.0)
      .task {
        await viewModel.fetchData()
      }
      .transformEnvironment(\.window) { window in
        configureWindow(window())
      }
      .onReceive(viewModel.closePublisher) {
        window()?.close()
      }
  }

  private var content: some View {
    VStack(spacing: .zero) {
      headerView
      fileInformationView
      bottomView
    }
  }

  private var headerView: some View {
    HStack(spacing: 16.0) {
      Images.shareReceiveAvatar.swiftUIImage
      headerTexts
      Spacer()
    }
    .placeholder(
      animate: viewModel.isLoading,
      isRedacted: viewModel.isContentRedacting
    )
    .padding(sectionInsets)
  }

  private var headerTexts: some View {
    VStack(alignment: .leading, spacing: 2.0) {
      Text(viewModel.headerTitle)
        .font(.displaySmallRegular)
        .foregroundColor(.text)
      if shouldShowPublicKey {
        subtitleText(for: viewModel.shareInfo.senderPublicKey)
      }
    }
  }

  private var fileInformationView: some View {
    HStack(spacing: 24.0) {
      fileIcon
        .resizable()
        .scaledToFit()
        .frame(width: 46.0, height: 46.0)
        .padding(.all, 9.0)
        .background(Color(light: Colors.sleet, dark: Colors.dusk))
        .cornerRadius(4.0)
      fileInformationTexts
        .offset(y: -4.0)
      Spacer()
    }
    .placeholder(
      animate: viewModel.isLoading,
      isRedacted: viewModel.isContentRedacting
    )
    .padding(sectionInsets)
    .background(Color(light: Colors.snow, dark: Colors.gloom))
  }

  private var fileIcon: Image {
    guard let image = viewModel.shareInfo.image else { return Images.shareReceiveFile.swiftUIImage }
    return Image(nsImage: image)
  }

  private var fileInformationTexts: some View {
    VStack(alignment: .leading, spacing: 4.0) {
      Text(viewModel.shareInfo.sharedContentName)
        .font(.displaySmall)
        .foregroundColor(.text)
      subtitleText(for: viewModel.shareInfo.sharedContentSize + " " + viewModel.shareInfo.sharedContentType)
    }
  }

  private var bottomView: some View {
    HStack(spacing: 2.0) {
      if let viewType = viewModel.infoViewType {
        InfoView.view(for: viewType)
      }
      Spacer()
      cancelButton
      mainButton
    }
    .placeholder(animate: viewModel.isLoading)
    .disabled(viewModel.isLoading || viewModel.infoViewType?.isLoading == true)
    .padding(.all, 24.0)
  }

  private var cancelButton: some View {
    WLButton(
      title: WLStrings.button.cancel,
      kind: .fallback,
      action: {
        viewModel.cancelAction()
      }
    )
  }

  private var mainButton: some View {
    WLButton(
      title: isError ? WLStrings.button.retry : WLStrings.Share.Receive.buttonTitle,
      kind: .main,
      action: {
        Task { await viewModel.mainButtonPressed() }
      }
    )
    .setProperty(\.horizontalPadding, 15.0)
  }

  private func subtitleText(for string: String) -> some View {
    Text(string)
      .font(.textMRegular)
      .foregroundColor(Color(light: Colors.silver, dark: Colors.cloud))
      .opacity(0.7)
  }

  private func configureWindow(_ window: NSWindow?) {
    let windowIdentifier = NSUserInterfaceItemIdentifier(viewModel.windowIdentifier)
    guard window?.identifier != windowIdentifier else { return }
    window?.identifier = windowIdentifier
    window?.titleVisibility = .hidden
    window?.titlebarSeparatorStyle = .none
    window?.titlebarAppearsTransparent = true
    window?.backgroundColor = .clear
    window?.isMovableByWindowBackground = true
    window?.level = .modalPanel
    window?.isReleasedWhenClosed = false
  }
}

#if DEBUG
struct ReceiveShareView_Previews: PreviewProvider {
  static var previews: some View {
    ReceiveShareView(viewModel: ReceiveShareViewModelStub())
  }
}
#endif
