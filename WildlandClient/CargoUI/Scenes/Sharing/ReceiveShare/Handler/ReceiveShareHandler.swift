//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import WildlandCommon
import AppKit.NSApplication

public protocol ReceiveShareHandlerType {
  var presentSubscriber: AnySubscriber<SharingMessage, Never> { get }
}

public final class ReceiveShareHandler<S: Scheduler>: ReceiveShareHandlerType {

  // MARK: - Properties

  lazy public private(set) var presentSubscriber = AnySubscriber(presentSubject)
  private let presentSubject = PassthroughSubject<SharingMessage, Never>()

  private let shareReceiverProvider: ShareReceiverProviderType
  private let urlProvider: TemporaryUrlProvider
  private let application: NSApplication
  private let scheduler: S
  private var presenters: [Presenter<PresentableWindow>] = []
  private var cancellables = Set<AnyCancellable>()

  private let windowConfiguration = WindowConfiguration(
    contentRect: .zero,
    styleMask: [.fullSizeContentView],
    defer: false
  )

  // MARK: - Initialization

  public init(
    shareReceiverProvider: ShareReceiverProviderType,
    urlProvider: TemporaryUrlProvider,
    application: NSApplication = .shared,
    scheduler: S = DispatchQueue.main
  ) {
    self.shareReceiverProvider = shareReceiverProvider
    self.urlProvider = urlProvider
    self.application = application
    self.scheduler = scheduler
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    presentSubject
      .receive(on: scheduler)
      .sink(receiveValue: { [weak self] sharingMessage in
        self?.presentIfNeeded(sharingMessage: sharingMessage)
      })
      .store(in: &cancellables)
  }

  // MARK: - Private

  private func presentIfNeeded(sharingMessage: SharingMessage) {
    let window = application.window(identifier: sharingMessage.body)
    window?.orderFront(nil)
    guard window == nil else { return }
    Task { [weak self] in
      await self?.present(sharingMessage: sharingMessage)
    }
  }

  @MainActor
  private func present(sharingMessage: SharingMessage) {
    let viewModel = ReceiveShareViewModel(
      sharingMessage: sharingMessage,
      shareReceiverProvider: shareReceiverProvider,
      urlProvider: urlProvider
    )
    let view = ReceiveShareView(viewModel: viewModel)
    let presenter = Presenter<PresentableWindow>()

    presenter.dismissPublisher
      .sink { [weak self, weak presenter] in
        self?.presenters.removeAll(where: { $0 === presenter })
      }
      .store(in: &cancellables)

    presenters.append(presenter)

    presenter.present(configuration: windowConfiguration) {
      view
        .colorScheme()
        .window()
    }
  }
}
