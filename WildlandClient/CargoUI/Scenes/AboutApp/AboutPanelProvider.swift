//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit
import WildlandCommon

public protocol AboutPanelProviderType {
  func showAboutPanel(appInfo: AppInfo)
}

public struct AboutPanelProvider: AboutPanelProviderType {

  // MARK: - Properties

  private let application: ApplicationAboutPanel

  // MARK: - Initialization

  public init(bundle: Bundle = .main, application: ApplicationAboutPanel = NSApplication.shared) {
    self.application = application
  }

  // MARK: - Public

  public func showAboutPanel(appInfo: AppInfo) {
    application.orderFrontStandardAboutPanel(
      options: [
        NSApplication.AboutPanelOptionKey.credits: NSAttributedString(
          string: appInfo.aboutAppInformation,
          attributes: [
            .paragraphStyle: NSParagraphStyle.paragraph(alignment: .center),
            .font: NSFont.textSRegular
          ]
        ),
        NSApplication.AboutPanelOptionKey(rawValue: appInfo.copyrightKey): appInfo.copyrightValue,
        NSApplication.AboutPanelOptionKey.applicationVersion: appInfo.appVersion,
        NSApplication.AboutPanelOptionKey.version: ""
      ]
    )
  }
}
