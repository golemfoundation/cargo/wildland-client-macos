//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct ActivityMessageView<ViewModel: ActivityViewModelMessage, Content: View>: View {

  // MARK: - Environment

  @EnvironmentObject private var viewModel: ViewModel

  // MARK: - Properties

  let message: ActivityMessage
  @ViewBuilder var centerBottomContent: () -> Content

  private var attributedMessage: AttributedString? {
    guard let subtitle = message.description else { return nil }
    let attributedString = try? AttributedString(markdown: subtitle)
    return attributedString ?? AttributedString(stringLiteral: subtitle)
  }

  // MARK: - Views

  var body: some View {
    content
      .padding(
        EdgeInsets(
          top: 11.0,
          leading: 28.0,
          bottom: 16.0,
          trailing: 13.0
        )
      )
      .background(Color(light: Colors.sleet, dark: Colors.charcoal))
  }

  private var content: some View {
    HStack(alignment: .top, spacing: 8.0) {
      icon
        .padding(iconInsets)
      centerContent
        .padding(.top, 8.0)
      Spacer()
      closeButtonIfVisible
    }
  }

  private var icon: Image {
    switch message.type {
    case .warning:
      return Images.activityViewWarning.swiftUIImage
    case .update:
      return Images.activityViewUpdateIcon.swiftUIImage
    }
  }

  private var iconInsets: EdgeInsets {
    switch message.type {
    case .warning:
      return EdgeInsets(
        top: 7.0,
        leading: .zero,
        bottom: .zero,
        trailing: 20.0
      )
    case .update:
      return EdgeInsets(
        top: 9.0,
        leading: 5.0,
        bottom: .zero,
        trailing: 20.0
      )
    }
  }

  private var centerContent: some View {
    VStack(alignment: .leading, spacing: .zero) {
      Text(message.title)
        .foregroundColor(Color(light: Colors.night, dark: Colors.snow))
        .font(.textMSemiBold)
      subtitleView
      centerBottomContent()
    }
    .frame(alignment: .topLeading)
  }

  @ViewBuilder private var subtitleView: some View {
    if let attributedMessage {
      Text(attributedMessage)
        .fixedSize(horizontal: false, vertical: true)
        .foregroundColor(.text)
        .font(.textMRegular)
        .lineSpacing(2.0)
        .padding(.top, 2.0)
    }
  }

  @ViewBuilder private var closeButtonIfVisible: some View {
    if message.isVisibleButton {
      Button(
        action: { viewModel.messageClosePressed(message: message) },
        label: { Images.activityMessageClose.swiftUIImage }
      )
      .buttonStyle(.borderless)
    }
  }
}
