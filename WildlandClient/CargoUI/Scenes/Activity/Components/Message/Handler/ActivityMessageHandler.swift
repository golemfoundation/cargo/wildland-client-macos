//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import Combine

public protocol ActivityMessageHandlerType {
  var activityMessagePublisher: AnyPublisher<ActivityMessage?, Never> { get }
  var updateStatePublisher: AnyPublisher<UpdateState, Never> { get }

  func update()
  func messageClosePressed(message: ActivityMessage)
}

public final class ActivityMessageHandler: ActivityMessageHandlerType {

  // MARK: - Properties

  lazy public private(set) var activityMessagePublisher = activityMessageSubject.eraseToAnyPublisher()
  private let activityMessageSubject = PassthroughSubject<ActivityMessage?, Never>()

  lazy public private(set) var updateStatePublisher = updateStateSubject.eraseToAnyPublisher()
  private let updateStateSubject = CurrentValueSubject<UpdateState, Never>(.dismiss)

  private let storagesProvider: StoragesProviderType
  private let updaterController: UpdaterControllerType
  private let userNotifications: UserNotificationsType

  private let updateUserChoiceSubject = PassthroughSubject<UpdateUserChoice, Never>()
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  public init(
    storagesProvider: StoragesProviderType,
    updaterController: UpdaterControllerType,
    userNotifications: UserNotificationsType
  ) {
    self.storagesProvider = storagesProvider
    self.updaterController = updaterController
    self.userNotifications = userNotifications
    self.setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    updateUserChoiceSubject
      .subscribe(updaterController.updateUserChoiceSubscriber)

    updaterController
      .updateStatePublisher
      .subscribe(AnySubscriber(updateStateSubject))

    Publishers.CombineLatest(updateStateSubject, storagesProvider.thresholdExceededPublisher)
      .map { [weak self] updateState, percentUsage in
        self?.prepareActivityMessage(updateState: updateState, percentUsage: percentUsage)
      }
      .subscribe(AnySubscriber(activityMessageSubject))

    updateUserChoiceSubject
      .filter { [weak self] updateUserChoice in
        self?.isAcknowledgementDismiss(updateUserChoice: updateUserChoice) == true
      }
      .sink(receiveValue: { [weak self] _ in
        self?.updaterController.acknowledgementDismissed()
      })
      .store(in: &cancellables)

    updaterController
      .updateStatePublisher
      .filter { [weak self] in
        self?.canShowNotification(updateState: $0) == true
      }
      .sink { [weak self] _ in
        self?.userNotifications.showNotification(category: .appUpdate)
      }
      .store(in: &cancellables)

    userNotifications
      .notificationResponsePublisher
      .compactMap { [weak self] response in
        self?.mapNotificationResponse(response)
      }
      .subscribe(AnySubscriber(updateUserChoiceSubject))
  }

  // MARK: - Public

  public func update() {
    updateUserChoiceSubject.send(.install)
  }

  public func messageClosePressed(message: ActivityMessage) {
    switch message.identifier {
    case .storageAlmostFull:
      storagesProvider.warningDismissed()
    case .updateError:
      updaterController.acknowledgementDismissed()
    case .updateAvailable:
      updateUserChoiceSubject.send(.dismiss)
    }
  }

  // MARK: - Private

  private func prepareActivityMessage(updateState: UpdateState, percentUsage: Int?) -> ActivityMessage? {
    if case .error(let error) = updateState {
      return .updateError(errorMessage: error.localizedDescription)
    } else if let percentUsage {
      return .storageAlmostFull(percent: String(percentUsage))
    } else {
      return nil
    }
  }

  private func isAcknowledgementDismiss(updateUserChoice: UpdateUserChoice) -> Bool {
    guard case .upToDate = updateStateSubject.value, updateUserChoice == .dismiss else { return false }
    return true
  }

  private func canShowNotification(updateState: UpdateState) -> Bool {
    guard case .newUpdateAvailable = updateState else { return false }
    return true
  }

  private func mapNotificationResponse(_ response: UserNotificationResponse) -> UpdateUserChoice? {
    guard response.category == .appUpdate else { return nil }
    switch response.action {
    case .approved:
      return .install
    case .declined:
      return .dismiss
    case .select:
      return nil
    }
  }
}
