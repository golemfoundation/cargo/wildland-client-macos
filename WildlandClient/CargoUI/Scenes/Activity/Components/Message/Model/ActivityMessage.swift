//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import WildlandCommon

public enum ActivityMessageIdentifier: String {
  case storageAlmostFull
  case updateError
  case updateAvailable
}

public struct ActivityMessage: Equatable {

  enum MessageType {
    case warning
    case update
  }

  let identifier: ActivityMessageIdentifier
  let title: String
  let description: String?
  let isVisibleButton: Bool
  let type: MessageType
}

extension ActivityMessage {
  static func storageAlmostFull(percent: String) -> ActivityMessage {
    ActivityMessage(
      identifier: .storageAlmostFull,
      title: WLStrings.ActivityView.storageAlmostFullTitle,
      description: String(format: WLStrings.ActivityView.storageAlmostFullDescription, percent),
      isVisibleButton: true,
      type: .warning
    )
  }

  static func updateError(errorMessage: String) -> ActivityMessage {
    ActivityMessage(
      identifier: .updateError,
      title: WLStrings.ActivityView.updateErrorTitle,
      description: errorMessage,
      isVisibleButton: true,
      type: .warning
    )
  }
}
