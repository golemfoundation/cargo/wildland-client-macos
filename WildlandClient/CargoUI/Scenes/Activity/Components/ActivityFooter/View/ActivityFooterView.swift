//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct ActivityFooterView<ViewModel: ActivityFooterViewModel>: View {

  // MARK: - Properties

  @ObservedObject private var viewModel: ViewModel

  // MARK: - Initialization

  init(viewModel: ViewModel) {
    self.viewModel = viewModel
  }

  // MARK: - Views

  var body: some View {
    VStack(alignment: .leading, spacing: .zero) {
      WLLineProgressView(value: viewModel.progress)
        .frame(height: 1)
      headerView
    }
  }

  private var headerView: some View {
    VStack(spacing: 4) {
      HStack(spacing: 0) {
        sychronizationLabel
          .lineLimit(1)
          .foregroundColor(.text)
          .frame(height: 48.0)
        Spacer()

        if let formattedMetrics = viewModel.formattedMetrics {
          Text(formattedMetrics)
            .foregroundColor(Colors.silver.swiftUIColor)
            .font(.textMRegular)
        }

        Button(action: {
          viewModel.isExtended.toggle()
        }) {
          arrowImage
            .foregroundColor(Color(light: Colors.granite, dark: Colors.silver))
        }
        .buttonStyle(PlainButtonStyle())
      }
      .padding(.trailing, 8)
      .padding(.leading, 23)

      storageListViewIfExtended
    }
  }

  private var arrowImage: Image {
    viewModel.isExtended
    ? Images.activityViewArrowBottom.swiftUIImage
    : Images.activityViewArrowTop.swiftUIImage
  }

  private var sychronizationLabel: Text {
    let text = Text(viewModel.synchronizationTitle).font(.textMRegular)
    if let synchronizationFile = viewModel.synchronizationFiles {
      return text + Text(" " + synchronizationFile).font(.textMMedium)
    } else {
      return text
    }
  }

  @ViewBuilder
  private var storageListViewIfExtended: some View {
    if viewModel.isExtended {
      ScrollView(showsIndicators: false) {
        VStack(spacing: 16.0) {
          ForEach(viewModel.storages, content: StorageItemView.init)
        }
        .measureHeight
      }
      .adjustHeight(maxHeight: 96.0)
      .padding(.bottom, 20.0)
    }
  }
}

#if DEBUG

struct MenuFooterView_Previews: PreviewProvider {

  static var previews: some View {
    Group {
      ActivityFooterView(viewModel: ActivityFooterViewModelStub())
      ActivityFooterView(viewModel: ActivityFooterViewModelStub(
        progress: 0.5,
        formattedMetrics: WLStrings.Previews.formattedMetrics,
        synchronizationTitle: WLStrings.ActivityView.synchronizationStatusTitleSyncing,
        synchronizationFiles: nil,
        isExtended: true,
        storages: []
      ))
    }
  }
}

private final class ActivityFooterViewModelStub: ActivityFooterViewModel {

  let progress: Double
  let formattedMetrics: String?
  let synchronizationTitle: String
  let synchronizationFiles: String?
  var isExtended: Bool
  let storages: [StorageItem]

  init(progress: Double = .zero,
       formattedMetrics: String? = nil,
       synchronizationTitle: String = WLStrings.ActivityView.synchronizationStatusTitleSynced,
       synchronizationFiles: String? = nil,
       isExtended: Bool = false,
       storages: [StorageItem] = []) {
    self.progress = progress
    self.formattedMetrics = formattedMetrics
    self.synchronizationTitle = synchronizationTitle
    self.synchronizationFiles = synchronizationFiles
    self.isExtended = isExtended
    self.storages = storages
  }
}

#endif
