//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct StorageItemView: View {

  // MARK: - Properties

  let storage: StorageItem

  // MARK: - Views

  var body: some View {
    HStack {
      Images.popoverDriveIcon.swiftUIImage
      VStack(alignment: .leading, spacing: .zero) {
        Text(storage.title)
          .font(.textMMedium)
          .frame(height: 16.0)
          .foregroundColor(.text)
        Text(storage.subtitle)
          .font(.textMRegular)
          .frame(height: 16.0)
          .foregroundColor(.Activity.subtitle)
      }
      .padding(.top, 4.0)
      Spacer()
    }
    .padding(.horizontal, 20.0)
  }
}
