//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct UpdateView<ViewModel: ActivityViewModelUpdate & ActivityViewModelMessage>: View {

  // MARK: - Environment

  @EnvironmentObject private var viewModel: ViewModel

  // MARK: - Properties

  private var activityMessage: ActivityMessage {
    ActivityMessage(
      identifier: .updateAvailable,
      title: viewModel.updateState.title,
      description: viewModel.updateState.subtitle,
      isVisibleButton: viewModel.updateState.canShowCloseButton,
      type: .update
    )
  }

  // MARK: - Views

  var body: some View {
    ActivityMessageView<ViewModel, _>(
      message: activityMessage,
      centerBottomContent: { downloadButtonIfNeeded }
    )
    .environmentObject(viewModel)
  }

  @ViewBuilder private var downloadButtonIfNeeded: some View {
    if case .newUpdateAvailable = viewModel.updateState {
      downloadButton
        .padding(.top, 16.0)
    }
  }

  private var downloadButton: some View {
    WLButton(
      title: WLStrings.ActivityView.NewVersionAlert.buttonTitle,
      kind: buttonKind,
      action: viewModel.update
    )
    .setProperty(\.height, 28.0)
  }

  private var buttonKind: WLButton.Kind {
    WLButton.Kind(
      font: .textMSemiBold,
      foreground: .primaryForeground,
      background: .primaryBackground,
      border: .clear,
      horizontalPadding: 12.0
    )
  }
}
