//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

enum ActivityHeaderViewActionButtonType {
  case copyPublicKey
}

struct ActivityHeaderViewActionButton: View {

  // MARK: - Properties

  private let type: ActivityHeaderViewActionButtonType
  private let isVisible: Bool
  private let action: (() -> Void)?
  private let onHover: ((Bool) -> Void)?
  private let foregroundColor = Color(light: Colors.cloud, dark: Colors.granite)

  private var popoverBackgroundColor: Color {
    isVisible ? Color.Activity.headerButtonBackground : Color.Activity.background
  }

  // MARK: - Initialization

  init(
    type: ActivityHeaderViewActionButtonType,
    isVisible: Bool,
    action: (() -> Void)? = nil,
    onHover: ((Bool) -> Void)? = nil
  ) {
    self.type = type
    self.isVisible = isVisible
    self.action = action
    self.onHover = onHover
  }

  var body: some View {
    Button(action: {
      action?()
    }) {
      buttonIcon()
        .resized(to: 32)
        .foregroundColor(foregroundColor)
        .background(popoverBackgroundColor)
    }
    .buttonStyle(PlainButtonStyle())
    .cornerRadius(4)
    .onHover(perform: { isOnHover in
      onHover?(isOnHover)
    })
  }

  // MARK: - Private

  private func buttonIcon() -> Image {
    switch type {
    case .copyPublicKey:
      return isVisible ? Images.popoverPublicKeyHovered.swiftUIImage : Images.popoverPublicKeyRegular.swiftUIImage
    }
  }
}
