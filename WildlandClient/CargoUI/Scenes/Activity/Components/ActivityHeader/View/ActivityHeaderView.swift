//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct ActivityHeaderView<ViewModel: ActivityViewModelType>: View {

  // MARK: - Properties

  @ObservedObject private var viewModel: ViewModel

  // MARK: - Initialization

  init(viewModel: ViewModel) {
    self.viewModel = viewModel
  }

  // MARK: - Views

  var body: some View {
    content
  }

  private var content: some View {
    HStack {
      Text(WLStrings.ActivityView.headerTitle)
        .font(.displaySmall)
        .foregroundColor(.text)

      Spacer()

      HStack(spacing: 8) {
        ActivityHeaderViewActionButton(
          type: .copyPublicKey,
          isVisible: viewModel.copyPublicKeyPopoverState.isVisible,
          action: viewModel.copyPublicKey,
          onHover: viewModel.updateCopyPopover(show:))
        .overlay {
          if viewModel.isCopyPopoverPresented {
            VStack(spacing: .zero) {
              Images.popoverTopArrow.swiftUIImage
                .foregroundColor(Color.Activity.popoverBackground)
              Text(viewModel.copyPublicKeyPopoverState.title)
                .font(.textSRegular)
                .foregroundColor(Color.Activity.popoverTitleBackground)
                .padding(.horizontal, 8)
                .frame(height: 24.0)
                .background(
                  Color.Activity.popoverBackground
                    .cornerRadius(4.0)
                )
            }
            .offset(y: 32)
            .frame(width: 120)
          }
        }

        Button(action: {
        }) {
          Images.popoverAvatarPlaceholder.swiftUIImage
            .resized(to: 16)
            .background(Color.Activity.contentBackground)
            .cornerRadius(12)
        }
        .buttonStyle(PlainButtonStyle())
      }
      .buttonStyle(PlainButtonStyle())
    }
    .padding(.all, 24.0)
  }
}

#if DEBUG
struct ActivityHeaderView_Previews: PreviewProvider {
  static var previews: some View {
    ActivityHeaderView(viewModel: ActivityViewModelStub())
  }
}
#endif
