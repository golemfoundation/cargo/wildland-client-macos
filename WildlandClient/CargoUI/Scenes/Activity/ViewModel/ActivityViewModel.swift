//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import os
import Combine
import AppKit.NSPasteboard

public enum ButtonCopyPopoverState: Equatable {
  case invisible
  case visible(String)

  var title: String {
    guard case .visible(let value) = self else { return "" }
    return value
  }
  var isVisible: Bool {
    self != .invisible
  }
}

public protocol ActivityViewModelUpdate: ObservableObject {
  var updateState: UpdateState { get }
  func update()
}

public protocol ActivityViewModelMessage: ObservableObject {
  func messageClosePressed(message: ActivityMessage)
}

public protocol ActivityViewModelType: ActivityViewModelUpdate, ActivityViewModelMessage, ObservableObject {
  var storages: [StorageItem] { get }
  var isExtended: Bool { get set }
  var activityMessage: ActivityMessage? { get }
  var isCopyPopoverPresented: Bool { get set }
  var copyPublicKeyPopoverState: ButtonCopyPopoverState { get }

  func copyPublicKey()
  func updateCopyPopover(show: Bool)
}

public final class ActivityViewModel<S: Scheduler, T: Scheduler>: ActivityViewModelType, ActivityFooterViewModel {

  // MARK: - Properties

  @Published public var isExtended: Bool = false
  @Published private(set) public var activityMessage: ActivityMessage?
  @Published public var isCopyPopoverPresented = false
  @Published public var copyPublicKeyPopoverState: ButtonCopyPopoverState = .invisible {
    didSet {
      isCopyPopoverPresented = copyPublicKeyPopoverState.isVisible
    }
  }
  @Published private(set) public var progress: Double = .zero
  @Published private(set) public var synchronizationTitle = WLStrings.ActivityView.synchronizationStatusTitleSynced
  @Published private(set) public var synchronizationFiles: String?
  @Published private(set) public var formattedMetrics: String?
  @Published private(set) public var storages: [StorageItem] = []
  @Published private(set) public var updateState: UpdateState = .dismiss

  private let synchronizationStateProvider: SynchronizationStateProvider
  private let domainMounter: DomainMounter
  private let countFormatter: ByteCountFormatter
  private let decimalFormatter: ByteCountFormatter
  private let estimateFormatter: DateComponentsFormatter
  private let storagesProvider: StoragesProviderType
  private let pasteboard: PasteboardProviderType
  private let userApi: UserPublicKeyProviderType & UserAvailability
  private let logger: Logger?
  private let scheduler: S
  private let fetchScheduler: T
  private let storageRefreshingPeriod: TimeInterval
  private let activityMessageHandler: ActivityMessageHandlerType

  private var publicKey: String? {
    do {
      return try userApi.userPublicKey
    } catch {
      logger?.fetchPublicKey(failure: error)
    }
    return nil
  }

  private var canReceiveStorages: Bool {
    get throws {
      [.readyForUse, .readyForUseButSomeUnmounted].contains(try userApi.availabilityState)
    }
  }

  // MARK: - Initialization

  public init(
    countFormatter: ByteCountFormatter = .fileCountFormatter,
    decimalFormatter: ByteCountFormatter = .decimalCountFormatter,
    estimateFormatter: DateComponentsFormatter = .estimateFormatter,
    pasteboard: PasteboardProviderType = NSPasteboard.general,
    userApi: UserPublicKeyProviderType & UserAvailability = UserApiProvider(),
    domainMounter: DomainMounter,
    storagesProvider: StoragesProviderType,
    synchronizationStateProvider: SynchronizationStateProvider,
    storageRefreshingPeriod: TimeInterval = 15.0,
    activityMessageHandler: ActivityMessageHandlerType,
    logger: Logger? = nil,
    scheduler: S,
    fetchScheduler: T
  ) {
    self.countFormatter = countFormatter
    self.estimateFormatter = estimateFormatter
    self.decimalFormatter = decimalFormatter
    self.pasteboard = pasteboard
    self.userApi = userApi
    self.domainMounter = domainMounter
    self.storagesProvider = storagesProvider
    self.synchronizationStateProvider = synchronizationStateProvider
    self.storageRefreshingPeriod = storageRefreshingPeriod
    self.activityMessageHandler = activityMessageHandler
    self.logger = logger
    self.scheduler = scheduler
    self.fetchScheduler = fetchScheduler
    self.setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    let synchronizationStatePublisher = synchronizationStateProvider
      .statePublisher
      .throttle(for: .seconds(1), scheduler: scheduler, latest: true)
      .share()

    synchronizationStatePublisher
      .compactMap { [weak self] synchronizationState in
        self?.prepareTitle(for: synchronizationState)
      }
      .receive(on: scheduler)
      .assign(to: &$synchronizationTitle)

    synchronizationStatePublisher
      .compactMap { [weak self] synchronizationState in
        self?.prepareProgress(for: synchronizationState)
      }
      .receive(on: scheduler)
      .assign(to: &$progress)

    synchronizationStatePublisher
      .compactMap { [weak self] synchronizationState in
        self?.prepareSynchronizationFilename(for: synchronizationState)
      }
      .receive(on: scheduler)
      .assign(to: &$synchronizationFiles)

    synchronizationStatePublisher
      .compactMap { [weak self] synchronizationState in
        self?.prepareFormattedMetrics(for: synchronizationState)
      }
      .receive(on: scheduler)
      .assign(to: &$formattedMetrics)

    let syncPublisher = synchronizationStatePublisher
      .removeDuplicates()
      .filter { $0 == .synced }
      .map { _ in }

    let storageRefreshPublisher = Timer.publish(every: storageRefreshingPeriod, on: .main, in: .default)
      .autoconnect()
      .map { _ in }

    let domainMountPublisher = domainMounter
      .isMountDomainPublisher
      .catch { _ in Empty<Void, Never>(completeImmediately: false).eraseToAnyPublisher() }

    Publishers.Merge3(
      syncPublisher,
      domainMountPublisher,
      storageRefreshPublisher
    )
    .tryFilter { [weak self] _ in
      try self?.canReceiveStorages ?? false
    }
    .catch { _ in Empty<Void, Never>(completeImmediately: false).eraseToAnyPublisher() }
    .receive(on: fetchScheduler)
    .compactMap { [weak self] in
      self?.getStorages()
    }
    .switchToLatest()
    .compactMap { [weak self] storages in
      guard let self else { return nil }
      return storages.map { StorageItem($0, formatter: self.decimalFormatter) }
    }
    .receive(on: scheduler)
    .assign(to: &$storages)

    activityMessageHandler
      .activityMessagePublisher
      .throttle(for: .milliseconds(500), scheduler: scheduler, latest: true)
      .receive(on: scheduler)
      .assign(to: &$activityMessage)

    activityMessageHandler
      .updateStatePublisher
      .throttle(for: .milliseconds(500), scheduler: scheduler, latest: true)
      .receive(on: scheduler)
      .assign(to: &$updateState)
  }

  public func messageClosePressed(message: ActivityMessage) {
    activityMessageHandler.messageClosePressed(message: message)
  }

  public func copyPublicKey() {
    copyPublicKeyToPasteboard()
    copyPublicKeyPopoverState = .visible(WLStrings.ActivityView.tooltipPublicKeyCopied)
  }

  public func updateCopyPopover(show: Bool) {
    guard show else { hidePopover(); return }
    copyPublicKeyPopoverState = .visible(WLStrings.ActivityView.tooltipPublicKeyCopy)
  }

  public func update() {
    activityMessageHandler.update()
  }

  // MARK: - Private

  private func getStorages() -> AnyPublisher<[Storage], Never> {
    do {
      return Just(try storagesProvider.getStorages()).eraseToAnyPublisher()
    } catch {
      logger?.fetchStorages(failure: error)
      return Empty(completeImmediately: false).eraseToAnyPublisher()
    }
  }

  private func hidePopover() {
    copyPublicKeyPopoverState = .invisible
  }

  private func copyPublicKeyToPasteboard() {
    pasteboard.copy(string: publicKey ?? "")
  }

  private func prepareTitle(for state: SynchronizationState) -> String {
    switch state {
    case .synced:
      return WLStrings.ActivityView.synchronizationStatusTitleSynced
    case .syncing:
      return WLStrings.ActivityView.synchronizationStatusTitleSyncing
    }
  }

  private func prepareProgress(for state: SynchronizationState) -> Double {
    switch state {
    case .synced:
      return .zero
    case .syncing(let syncProgress):
      guard !syncProgress.totalSize.value.isZero else { return .zero }
      return syncProgress.currentSize.value / syncProgress.totalSize.value
    }
  }

  private func prepareSynchronizationFilename(for state: SynchronizationState) -> String? {
    guard case .syncing(let syncProgress) = state, syncProgress.files.count > .zero else { return nil }
    return syncProgress.files.count == 1
    ? syncProgress.files.first
    : WLStrings.ActivityView.multipleFiles
  }

  private func prepareFormattedMetrics(for state: SynchronizationState) -> String? {
    guard case .syncing(let syncProgress) = state else { return nil }
    var formattedMetrics = countFormatter.string(from: syncProgress.totalSize)
    let estimate = syncProgress.estimate
    let dateComponent = DateComponents(second: Int(estimate.value))
    if estimate.value > .zero, let formattedEstimation = estimateFormatter.string(from: dateComponent) {
      formattedMetrics += " (\(formattedEstimation))"
    }
    return formattedMetrics
  }
}

private extension Logger {

  func fetchStorages(file: String = #file, method: String = #function, failure: Error) {
    error(file: file, method: method, failure: failure)
  }

  func fetchPublicKey(file: String = #file, method: String = #function, failure: Error) {
    error(file: file, method: method, failure: failure)
  }
}
