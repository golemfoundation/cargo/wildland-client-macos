//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

public struct ActivityView<ViewModel: ActivityViewModelType & ActivityFooterViewModel>: View {

  // MARK: - Properties

  @ObservedObject var viewModel: ViewModel

  // MARK: - Initialization

  public init(model: ViewModel) {
    viewModel = model
  }

  // MARK: - Views

  public var body: some View {
    VStack(spacing: .zero) {
      ActivityHeaderView(viewModel: viewModel)
        .zIndex(1)
      messageViews
      ActivityFooterView(viewModel: viewModel)
    }
    .frame(width: 400.0)
    .popover(backgroundColor: .Activity.background)
  }

  @ViewBuilder private var messageViews: some View {
    ForEach(conditionalViews.indices, id: \.self) { index in
      if index > .zero {
        divider
      }
      conditionalViews[index]
    }
  }

  private var divider: some View {
    Divider()
      .frame(height: 1.0)
      .overlay(Color(light: Colors.dust, dark: Colors.dusk))
  }

  private var conditionalViews: [AnyView] {
    var views: [AnyView] = []

    if viewModel.updateState.canShowUpdateView {
      views.append(updateView.eraseToAnyView())
    }

    if let activityMessage = viewModel.activityMessage {
      views.append(messageView(activityMessage: activityMessage).eraseToAnyView())
    }

    return views
  }

  private var updateView: some View {
    UpdateView<ViewModel>()
      .environmentObject(viewModel)
  }

  private func messageView(activityMessage: ActivityMessage) -> some View {
    ActivityMessageView<ViewModel, _>(
      message: activityMessage,
      centerBottomContent: { }
    )
    .environmentObject(viewModel)
  }
}

#if DEBUG
struct ActivityView_Previews: PreviewProvider {
  static var previews: some View {
    ActivityView(model: ActivityViewModelStub())
  }
}
#endif
