//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import os
import SwiftUI
import WildlandCommon

struct EnterSecretPhraseView: View, AccessiblityIdentifiable {

  private let _log = Logger.wildlandLogger()

  @ObservedObject var viewModel: EnterSecretPhraseViewModel

  private let columns: [GridItem] = .init(
    repeating: GridItem(.adaptive(minimum: 160), spacing: 16),
    count: 3
  )

  var body: some View {
    let dropViewDelegate = WLDropViewDelegate { result in
      switch result {
      case .success(let data):
        self.viewModel.dropPerformed(with: data)
      case .failure(let error):
        self._log.error("error: \(error.localizedDescription)")
      }
    }

    ZStack {
      WLDroppableView(dropDelegate: dropViewDelegate)

      OnboardingIntroContainerView {
        LazyVGrid(columns: columns, alignment: .leading, spacing: 16) {
          ForEach(0..<viewModel.words.count, id: \.self) { index in
            wordCell(index)
          }
        }
        .padding(.trailing, 50.0)
      }
      .setProperty(\.leadingPadding, 50.0)
      .setProperty(\.progressValue, viewModel.progressValue)
      .setProperty(\.title, WLStrings.onboarding.enterWildlandWords.title)
      .setProperty(\.text, WLStrings.onboarding.enterWildlandWords.description)
      .setProperty(\.descriptionWidth, nil)
      .setProperty(\.infoViewState, viewModel.activityState)
      .setProperty(\.bottomContent) {
        Spacer()

        WLButton(
          title: WLStrings.button.cancel,
          kind: .fallback,
          action: { viewModel.goBackClicked() }
        )
        .accessibilityIdentifier(identifier(.backButton))
        .opacity(viewModel.activityState.succeded ? 0 : 1)

        if viewModel.activityState.succeded {
          WLButton(
            title: WLStrings.onboarding.enterWildlandWords.buttonGetStarted,
            kind: .main,
            action: { viewModel.nextClicked() }
          )
          .setProperty(\.horizontalPadding, 25.0)
          .accessibilityIdentifier(identifier(.nextButton))
        } else {
          WLButton(
            title: WLStrings.onboarding.enterWildlandWords.buttonVerify,
            kind: .main,
            action: { Task { await viewModel.verifyClicked() } }
          )
          .setProperty(\.horizontalPadding, 14.5)
          .accessibilityIdentifier(identifier(ComponentsIdentifier.verify.rawValue))
          .disabled(!viewModel.canVerify)
        }
      }
    }
  }

  private func infoView(image: String, color: Color, text: String) -> some View {
    HStack {
      Image(systemName: image)
        .resizable()
        .frame(width: 16, height: 16)
        .foregroundColor(color)
      Text(text)
        .font(.bodyText)
        .foregroundColor(.text)
    }
  }

  private func wordCell(_ index: Int) -> some View {
    ZStack {
      let dropTextFieldDelegate = WLDropViewDelegate { result in
        switch result {
        case .success(let data):
          self.viewModel.dropPerformed(with: data, for: index)
        case .failure(let error):
          self._log.error("error: \(error.localizedDescription)")
        }
      }
      WLFocusableTextField(
        stringValue: $viewModel.words[index].value,
        onChange: {
          self.viewModel.wordIsBeingEdited(true, at: index)
        },
        onPaste: { value in
          self.viewModel.pasteString(value: value)
        },
        onTabKeystroke: {
          self.viewModel.selectNextWordForEditing()
        }
      )
      .setProperty(\.placeholder, "--------")
      .setProperty(\.tag, index)
      .setProperty(\.focusTag, $viewModel.currentWordIndex)
      .setProperty(\.font, .bodyText)
      .setProperty(\.textColor, .text)
      .setProperty(\.alignment, .left)
      .setProperty(\.placeholderColor, placeholderColor(index: index))
      .frame(height: 40)
      .padding(.leading)
      .padding(.leading)
      .padding(.leading)
      .background(Color.Onboarding.backgroundForm)
      .cornerRadius(4)
      .accessibilityIdentifier(identifier(.textField, value: String(index)))
      .disabled(viewModel.activityState.succeded)
      HStack {
        ZStack {
          Text("\(viewModel.words.count).").opacity(0)
          Text("\(index + 1).")
            .font(.textLSemiBold)
            .foregroundColor(ordinalNumberColor)
        }
        .padding(.leading)
        Spacer(minLength: 0)
      }
      .padding(.vertical, 12)
      .if(viewModel.currentWordIndex == index, content: { view in
        view.overlay(textFieldActiveOverlay)
      })
      .onDrop(of: [.text, .utf8PlainText], delegate: dropTextFieldDelegate)
    }
  }

  private var ordinalNumberColor: Color {
    Color(light: Colors.mainText, dark: Colors.cloud).opacity(0.5)
  }

  private func placeholderColor(index: Int) -> Color {
    viewModel.currentWordIndex == index
    ? Color(light: Colors.silver, dark: Colors.cloud)
    : Color(light: Colors.mainText, dark: Colors.cloud)
  }

  @ViewBuilder private var textFieldActiveOverlay: some View {
    RoundedRectangle(cornerRadius: 4).stroke(Colors.mint.swiftUIColor, lineWidth: 1)
  }
}

private enum ComponentsIdentifier: String {
  case verify
}

#if DEBUG

struct EnterSecretPhraseView_Previews: PreviewProvider {
  static var previews: some View {
    let viewModel = EnterSecretPhraseViewModel(userService: PreviewDaemonUserService())
    return EnterSecretPhraseView(viewModel: viewModel)
  }
}

#endif
