//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import SwiftUI
import WildlandCommon

struct ChooseCargoStorageView: View, AccessiblityIdentifiable {

  // MARK: - Properties

  @ObservedObject var viewModel: ChooseCargoStorageViewModel

  private let _termsAndConditionsLinkURL = URL(string: "https://wildland.io/")!

  // MARK: - Views

  var body: some View {
    OnboardingIntroContainerView {
      Picker(
        selection: Binding(
          get: { viewModel.type },
          set: {
            $0.flatMap { viewModel.typeChanged($0) }
          }
        ),
        label: EmptyView()
      ) {
        ForEach(ChooseCargoStorageViewModel.ChooseCargoMode.allCases, content: prepareRadioButtonLabel)
      }
      .pickerStyle(.radioGroup)
      .alignmentGuide(.leading) { _ in 7.0 }
      .accentColor(Colors.grass.swiftUIColor)
      .allowsHitTesting(false)
    }
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.image, Images.identityContainers.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.chooseCargoStorage.title)
    .setProperty(\.text, WLStrings.onboarding.chooseCargoStorage.description)
    .setProperty(\.bottomContent) {
      HStack(spacing: 0) {
        Toggle("", isOn: Binding(
          get: { viewModel.userConfirmsTermsAndConditions },
          set: { viewModel.userConfirmsTermsAndConditions = $0 }
        ))
        .padding(.leading, 8.0)
        .labelsHidden()
        .toggleStyle(.checkbox)
        .accessibilityIdentifier(identifier(.checkbox))

        HStack(spacing: 4) {
          Text(WLStrings.onboarding.chooseCargoStorage.agreeCheckboxTitle)
            .font(.bodyText)
            .foregroundColor(.text)
          Link(destination: _termsAndConditionsLinkURL, label: {
            Text(WLStrings.onboarding.chooseCargoStorage.agreeCheckboxLink)
              .underline()
          })
          .font(.bodyText)
          .foregroundColor(Colors.sky.swiftUIColor)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .font(.bodyText)
        .foregroundColor(.text)
        .padding(.horizontal, 10)
        .lineLimit(2)
        .accessibilityIdentifier(identifier(.checkbox))
        .onTapGesture {
          viewModel.userConfirmsTermsAndConditions.toggle()
        }

        Spacer()

        WLButton(
          title: WLStrings.button.next,
          kind: .main,
          action: { viewModel.nextClicked() }
        )
        .accessibilityIdentifier(identifier(.nextButton))
        .disabled(!viewModel.userConfirmsTermsAndConditions)
      }
    }
  }

  // MARK: - Private

  private func prepareRadioButtonLabel(for value: ChooseCargoStorageViewModel.ChooseCargoMode) -> some View {
    WLRadioButtonLabel(
      text: value.title,
      hint: value.hint,
      isDisabled: value.isDisabled
    )
    .tag(value as ChooseCargoStorageViewModel.ChooseCargoMode?)
    .padding(.vertical, 8)
    .padding(.horizontal, 8)
    .accessibilityIdentifier(identifier(.selectOption, value: value.id))
  }
}

private extension ChooseCargoStorageViewModel.ChooseCargoMode {

  // MARK: - Properties

  var title: String {
    switch self {
    case .freeFoundationTier:
      return WLStrings.onboarding.chooseCargoStorage.itemFreeFoundationTier.title
    case .selfSetup:
      return WLStrings.onboarding.chooseCargoStorage.itemSelfSetup.title
    }
  }

  var hint: String {
    switch self {
    case .freeFoundationTier:
      return WLStrings.onboarding.chooseCargoStorage.itemFreeFoundationTier.description
    case .selfSetup:
      return WLStrings.onboarding.chooseCargoStorage.itemSelfSetup.description
    }
  }
}

#if DEBUG

struct ChooseCargoStorageView_Previews: PreviewProvider {
  static var previews: some View {
    return ChooseCargoStorageView(viewModel: ChooseCargoStorageViewModel())
  }
}

#endif
