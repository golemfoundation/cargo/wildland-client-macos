//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon

struct SetupWildlandCargoView: View, AccessiblityIdentifiable {

  @ObservedObject var viewModel: SetupWildlandCargoModel

  var body: some View {
    OnboardingIntroContainerView {
      Picker(
        selection:
          Binding(
            get: { viewModel.type },
            set: {
              $0.flatMap { viewModel.typeChanged($0) }
            }
          ),
        label: EmptyView()
      ) {
        ForEach(SetupWildlandCargoModel.SetupCargoMode.allCases, content: prepareRadioButtonLabel)
      }
      .pickerStyle(.radioGroup)
      .alignmentGuide(.leading) { _ in 7.0 }
      .accentColor(Colors.grass.swiftUIColor)
    }
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.image, Images.setupCargoScrews.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.setupCargo.title)
    .setProperty(\.text, WLStrings.onboarding.setupCargo.description)
    .setProperty(\.bottomContent) {
      Spacer()
      WLButton(
        title: WLStrings.button.next,
        kind: .main,
        action: { viewModel.nextClicked() }
      )
      .accessibilityIdentifier(identifier(.nextButton))
    }
  }

  @ViewBuilder private func prepareRadioButtonLabel(for value: SetupWildlandCargoModel.SetupCargoMode) -> some View {
    WLRadioButtonLabel(text: value.title, hint: value.hint)
      .tag(value as SetupWildlandCargoModel.SetupCargoMode?)
      .padding(
        EdgeInsets(
          top: 9,
          leading: 12,
          bottom: 2,
          trailing: .zero
        )
      )
      .accessibilityIdentifier(identifier(.selectOption, value: value.id))
  }
}

private extension SetupWildlandCargoModel.SetupCargoMode {

  var title: String {
    switch self {
    case .createNew:
      return WLStrings.onboarding.setupCargo.itemCreateNew.title
    case .linkToExisting:
      return WLStrings.onboarding.setupCargo.itemLinkExisting.title
    }
  }

  var hint: String {
    switch self {
    case .createNew:
      return WLStrings.onboarding.setupCargo.itemCreateNew.description
    case .linkToExisting:
      return WLStrings.onboarding.setupCargo.itemLinkExisting.description
    }
  }
}

#if DEBUG

struct SetupWildlandCargoView_Previews: PreviewProvider {

  static var previews: some View {
    SetupWildlandCargoView(viewModel: .init())
  }
}

#endif
