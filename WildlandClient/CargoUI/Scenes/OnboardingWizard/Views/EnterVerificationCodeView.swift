//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct EnterVerificationCodeView: View, AccessiblityIdentifiable {

  @ObservedObject var viewModel: EnterVerificationCodeViewModel

  var body: some View {
    OnboardingIntroContainerView {
      VStack(alignment: .leading, spacing: 24.0) {
        verificationCodeTextField
        resendCodeView
          .disabled(viewModel.isVerified || viewModel.isCompleted)
      }
    }
    .setProperty(\.descriptionWidth, 288.0)
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.title, WLStrings.onboarding.enterVerificationCode.title)
    .setProperty(\.text, WLStrings.onboarding.enterVerificationCode.description)
    .setProperty(\.image, Images.identityCheckmark.swiftUIImage)
    .setProperty(\.infoViewState, viewModel.activityState)
    .setProperty(\.bottomContent) {
      Spacer()
      WLButton(
        title: WLStrings.button.cancel,
        kind: .fallback,
        action: { viewModel.goBackClicked() }
      )
      .accessibilityIdentifier(identifier(.backButton))
      .disabled(viewModel.isVerified)
      WLButton(
        title: WLStrings.onboarding.enterVerificationCode.openFinder,
        kind: .main,
        action: { viewModel.nextClicked() }
      )
      .accessibilityIdentifier(identifier(.nextButton))
      .disabled(!viewModel.isCompleted)
    }
    .frame(maxHeight: 404)
  }

  private var verificationCodeTextField: some View {
    HStack {
      ForEach(0..<viewModel.words.count, id: \.self) { index in
        WLFocusableTextField(
          stringValue: $viewModel.words[index].value,
          onDelete: { string in
            viewModel.deleted(at: index, string: string)
          },
          onPaste: { value in
            viewModel.pasteString(value: value)
          }
        )
        .setProperty(\.tag, index)
        .setProperty(\.focusTag, $viewModel.currentSymbolIndex)
        .setProperty(\.font, .bodyText)
        .setProperty(\.textColor, .text)
        .setProperty(\.alignment, .center)
        .setProperty(\.isEditable, viewModel.isEditableField(at: index))
        .setProperty(\.formatter, viewModel.fieldFormatter)
        .accessibilityIdentifier(identifier(.textField, value: String(index)))
        .frame(width: 40, height: 40)
        .background(Color.Onboarding.backgroundForm)
        .cornerRadius(4)
        .overlay(viewModel.currentSymbolIndex == index ? textFieldActiveOverlay : nil)
        .disabled(viewModel.activityState.loading ||
                  viewModel.isVerified || viewModel.isCompleted)
      }
      .padding(.top, 2)
    }
    .onAppear(perform: viewModel.verificationCodeTextFieldAppeared)
  }

  private var resendCodeView: some View {
    HStack(spacing: 4) {
      Text(WLStrings.onboarding.enterVerificationCode.resendCodeText)
        .font(.textMRegular)
        .foregroundColor(.text)
      Text(WLStrings.onboarding.enterVerificationCode.resendCodeLink)
        .font(.textMRegular)
        .foregroundColor(Colors.sky.swiftUIColor)
        .onTapGesture { Task { await viewModel.resendCodeClicked() } }
        .disabled(viewModel.isVerified)
    }
  }

  private var textFieldActiveOverlay: some View {
    RoundedRectangle(cornerRadius: 4).stroke(Colors.grass.swiftUIColor, lineWidth: 1)
  }
}

#if DEBUG

struct EnterVerificationCodeView_Previews: PreviewProvider {
  static var previews: some View {
    let viewModel = EnterVerificationCodeViewModel(email: "test@mail.com",
                                                   userService: PreviewDaemonUserService(),
                                                   controlService: PreviewDaemonControlService())
    return EnterVerificationCodeView(viewModel: viewModel)
  }
}

#endif
