//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon

struct WildlandSecretPhaseIntroView: View, AccessiblityIdentifiable {
  @ObservedObject var viewModel: WildlandSecretPhraseIntroViewModel

  @State var updater: Bool = false

  var body: some View {
    OnboardingIntroContainerView {
      HStack(alignment: .top, spacing: 24.0) {
        Images.secretPhaseIntroShield.swiftUIImage
          .padding(.leading, 8.0)
          .offset(y: 4.0)
        Text(WLStrings.onboarding.wildlandSecretPhaseIntro.safeText)
          .lineSpacing(2.8)
          .kerning(0.05)
          .fixedSize()
          .font(.bodyText)
          .foregroundColor(.text)
          .lineSpacing(4.0)
      }
      .padding(
        EdgeInsets(
          top: 19.0,
          leading: 16.0,
          bottom: 19.0,
          trailing: 44.0
        )
      )
      .background(Color.Onboarding.backgroundBox)
      .cornerRadius(8.0)
      .padding(.top, 6.0)
    }
    .setProperty(\.horizontalPadding, 33.0)
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.image, Images.identityKeys.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.wildlandSecretPhaseIntro.title)
    .setProperty(\.text, description)
    .setProperty(\.bottomContent) {
      Spacer()

      bottomContent
    }
    .frame(maxHeight: 404)
    .onAppear {
      updater.toggle()
    }
  }

  @ViewBuilder private var bottomContent: some View {
    WLButton(title: WLStrings.button.back, kind: .fallback, action: viewModel.goBackClicked)
      .accessibilityIdentifier(identifier(.backButton))

    switch viewModel.variant {
    case .newIdentity:
      WLButton(title: WLStrings.button.generate, kind: .main, action: viewModel.nextClicked)
        .setProperty(\.horizontalPadding, 20.5)
        .accessibilityIdentifier(identifier(ComponentsIdentifier.generate.rawValue))
    case .recoverIdentity:
      WLButton(title: WLStrings.button.next, kind: .main, action: viewModel.nextClicked)
        .accessibilityIdentifier(identifier(.nextButton))
    }
  }

  private var description: String {
    viewModel.variant == .newIdentity ?
      WLStrings.onboarding.wildlandSecretPhaseIntro.newUserDescription :
      WLStrings.onboarding.wildlandSecretPhaseIntro.existingUserDescription
  }
}

private enum ComponentsIdentifier: String {
  case generate
}

#if DEBUG

struct GenerateSecretPhaseView_Previews: PreviewProvider {
  static var previews: some View {
    WildlandSecretPhaseIntroView(viewModel: .init(variant: .newIdentity, service: PreviewDaemonUserService()))
  }
}

#endif
