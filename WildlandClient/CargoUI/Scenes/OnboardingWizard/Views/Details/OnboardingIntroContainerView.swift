//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

struct OnboardingIntroContainerView<Content: View>: View, AccessiblityIdentifiable {

  let content: () -> Content
  var title: String = ""
  var text: String = ""
  var attributedText: AttributedString?
  var image: Image?
  var bottomContent: AnyView?
  var progressValue: Double = .zero
  var descriptionWidth: Double? = 280.0
  var horizontalPadding: Double = 25.0
  var leadingPadding: Double = 16.0
  var detailsPadding: Double = 27.0
  var infoViewState: ViewActivityState?

  init(@ViewBuilder content: @escaping () -> Content) {
    self.content = content
  }

  var body: some View {
    VStack(alignment: .leading, spacing: 4) {
      HStack(alignment: .top, spacing: horizontalPadding) {
        if let image = image {
          VStack {
            image
              .resizable()
              .scaledToFit()
              .accessibilityIdentifier(identifier(.screenImage))
          }
          .frame(width: 200.0)
          .padding(.top, 3.0)
          .fixedSize()
        }

        VStack(alignment: .leading, spacing: detailsPadding) {
          if !title.isEmpty {
            Text(title)
              .font(.displayLarge)
              .foregroundColor(.text)
              .frame(minHeight: 48)
              .accessibilityIdentifier(identifier(.screenTitle))
          }
          if let descriptionText = descriptionText {
            descriptionText
              .font(.bodyText)
              .foregroundColor(.text)
              .frame(maxWidth: .infinity, alignment: .leading)
              .fixedSize(horizontal: false, vertical: true)
              .lineSpacing(3)
              .accessibilityIdentifier(identifier(.screenSubtitle))
              .ifLet(descriptionWidth, content: { view, value in
                view.frame(width: value)
              })
          }
          content()
        }
        .padding(.top, 19)
      }
      .padding(
        EdgeInsets(
          top: .zero,
          leading: leadingPadding,
          bottom: 8,
          trailing: .zero
        )
      )

      if let bottomContent {
        VStack(spacing: 0) {
          WLLineProgressView(value: progressValue)
            .frame(height: 2.0)
          HStack(spacing: 8.0) {
            infoView
            bottomContent
          }
          .frame(height: 72)
          .padding(.horizontal, 17.0)
        }
        .padding(.top, 6.0)
      }
    }
    .background(Color.Onboarding.background)
  }

  @ViewBuilder private var infoView: some View {
    switch infoViewState {
    case .loading(let text):
      InfoView.view(for: .loading(text))
    case .error(let text, let imageType, _):
      InfoView.view(for: .error(text, image: imageType.image))
    case .success(let text):
      InfoView.view(for: .info(text))
    case .idle, .none:
      EmptyView()
    }
  }

  private var descriptionText: Text? {
    if let attributedText {
      return Text(attributedText)
    } else if !text.isEmpty {
      return Text(text)
    }
    return nil
  }
}

#if DEBUG

struct IntroContainerView_Previews: PreviewProvider {

  static var previews: some View {
    OnboardingIntroContainerView {
      Picker(
        selection: .constant(1),
        content: {
          Text("Sliced").tag(0)
          Text("Sliced").tag(1)
        },
        label: EmptyView.init
      )
      .pickerStyle(.radioGroup)
      .accentColor(Colors.grass.swiftUIColor)
    }
    .setProperty(\.image, Images.identityKeys.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.chooseIdentity.title)
    .setProperty(\.text, WLStrings.onboarding.chooseIdentity.description)
  }
}

#endif
