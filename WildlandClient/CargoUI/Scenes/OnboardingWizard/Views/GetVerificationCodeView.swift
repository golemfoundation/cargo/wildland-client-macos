//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import SwiftUI
import WildlandCommon

struct GetVerificationCodeView: View, AccessiblityIdentifiable {

  @ObservedObject var viewModel: GetVerificationCodeViewModel

  @State private var focusTag = Int.min

  var body: some View {
    OnboardingIntroContainerView {
      VStack(alignment: .leading, spacing: 0) {
        VStack(alignment: .leading, spacing: 0) {
          textField
          invalidEmailLabel
        }
        HStack(alignment: .top, spacing: 4) {
          Text(WLStrings.onboarding.getVerificationCode.tip)
            .font(.textMRegular)
            .foregroundColor(.text)
            .frame(minHeight: 40)
        }
        .padding(.top, 0)
      }
      .padding(.bottom, 16)
      .padding(.trailing, 64.0)
      .onAppear {
        DispatchQueue.main.delay(with: .thirdOfASecond) {
          focusTag = 0
        }
      }

      Spacer()
    }
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.title, WLStrings.onboarding.getVerificationCode.title)
    .setProperty(\.text, WLStrings.onboarding.getVerificationCode.description)
    .setProperty(\.image, Images.identityCheckmark.swiftUIImage)
    .setProperty(\.infoViewState, viewModel.activityState)
    .setProperty(\.bottomContent) {
      Spacer()
      WLButton(
        title: WLStrings.button.back,
        kind: .fallback,
        action: { viewModel.goBackClicked() }
      )
      .accessibilityIdentifier(identifier(.backButton))
      WLButton(
        title: WLStrings.button.getCode,
        kind: .main,
        action: {
          Task {
            await viewModel.nextButtonClicked()
          }
        }
      )
      .setProperty(\.horizontalPadding, 21.0)
      .disabled(viewModel.activityState.loading)
      .accessibilityIdentifier(identifier(.nextButton))
    }
    .frame(maxHeight: 404)
  }

  private var textField: some View {
    TextField(text: $viewModel.email)
      .textFieldStyle(textFieldStyle)
      .accessibilityIdentifier(identifier(.textField))
      .onSubmit {
        Task { await viewModel.nextButtonClicked() }
      }
  }

  private var textFieldStyle: SimpleTextFieldStyle {
    SimpleTextFieldStyle(
      placeholder: WLStrings.onboarding.getVerificationCode.textFieldPlaceholder,
      isVisiblePlaceholder: viewModel.email.isEmpty,
      isInvalid: viewModel.isEmailInvalid
    )
  }

  private var invalidEmailLabel: some View {
    Text(WLStrings.onboarding.getVerificationCode.invalidEmail)
      .font(.textSRegular)
      .foregroundColor(Colors.warning.swiftUIColor)
      .frame(minHeight: 20)
      .opacity(viewModel.isEmailInvalid ? 1 : 0)
  }
}

#if DEBUG

struct GetVerificationCodeView_Previews: PreviewProvider {
  static var previews: some View {
    return GetVerificationCodeView(viewModel: GetVerificationCodeViewModel(userService: PreviewDaemonUserService()))
  }
}

#endif
