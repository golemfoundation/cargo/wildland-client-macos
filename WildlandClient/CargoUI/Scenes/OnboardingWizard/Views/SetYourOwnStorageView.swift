//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import SwiftUI
import WildlandCommon

struct SetYourOwnStorageView: View, AccessiblityIdentifiable {

  @ObservedObject var viewModel: SetYourOwnStorageViewModel
  private let _wildlandLinkURL = URL(string: "https://docs.wildland.io/user-guide/quick-start.html#setup")!

  var body: some View {
    OnboardingIntroContainerView {
      VStack(alignment: .leading, spacing: 5) {
        Text(WLStrings.onboarding.setYourOwnStorage.descriptionBottomTop)
          .font(.bodyText)
          .lineSpacing(5)
          .foregroundColor(.text)
        HStack(spacing: 4) {
          Text(WLStrings.onboarding.setYourOwnStorage.descriptionBottomStart)
            .font(.bodyText)
            .foregroundColor(.text)
          Link(destination: _wildlandLinkURL, label: {
            Text(WLStrings.onboarding.setYourOwnStorage.descriptionBottomLink)
              .underline()
          })
          .font(.bodyText)
          .foregroundColor(.text.opacity(0.8))
          Text(WLStrings.onboarding.setYourOwnStorage.descriptionBottomEnd)
            .font(.bodyText)
            .foregroundColor(.text)
        }
      }
      .padding(.trailing, 30)

      Spacer(minLength: 0)
    }
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.image, Images.identityContainers.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.setYourOwnStorage.title)
    .setProperty(\.bottomContent) {
      Spacer()
      WLButton(
        title: WLStrings.button.back,
        kind: .fallback,
        action: { viewModel.goBackClicked() }
      )
      .accessibilityIdentifier(identifier(.backButton))
      WLButton(
        title: WLStrings.button.quickTour,
        kind: .main,
        action: { viewModel.nextClicked() }
      )
      .accessibilityIdentifier(identifier(.nextButton))
    }
    .frame(maxHeight: 404)
  }
}

#if DEBUG

struct SetYourOwnStorageView_Previews: PreviewProvider {
  static var previews: some View {
    return SetYourOwnStorageView(viewModel: SetYourOwnStorageViewModel())
  }
}

#endif
