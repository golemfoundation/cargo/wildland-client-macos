//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon

struct NewWildlandWordsView: View, AccessiblityIdentifiable {
  @ObservedObject var viewModel: NewWildlandWordsViewModel

  private let columns: [GridItem] = .init(
    repeating: GridItem(.flexible(minimum: 112), spacing: .zero),
    count: 4
  )

  var body: some View {
    OnboardingIntroContainerView {
      VStack(spacing: 24) {
        wordsGrid
        checkboxView
      }
      .padding(
        EdgeInsets(
          top: .zero,
          leading: .zero,
          bottom: 14.0,
          trailing: 50.0
        )
      )
    }
    .setProperty(\.leadingPadding, 50.0)
    .setProperty(\.detailsPadding, 17.0)
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.title, WLStrings.onboarding.newWildlandWords.title)
    .setProperty(\.attributedText, viewModel.attributedText)
    .setProperty(\.descriptionWidth, nil)
    .setProperty(\.infoViewState, viewModel.activityState)
    .setProperty(\.bottomContent) {
      downloadFileButton

      Spacer()

      WLButton(
        title: WLStrings.onboarding.newWildlandWords.buttonGenerate,
        kind: .main,
        action: { viewModel.nextClicked() }
      )
      .setProperty(\.horizontalPadding, 14.0)
      .disabled(!viewModel.userConfirmsStoringWords)
      .accessibilityIdentifier(identifier(.confirmButton))
    }
  }

  @ViewBuilder private var downloadFileButton: some View {
    if case .idle = viewModel.activityState {
      Button(
        action: { Task { await viewModel.downloadFileClicked() } },
        label: {
          Text(WLStrings.onboarding.newWildlandWords.buttonDownload)
            .font(.bodyText)
            .foregroundColor(Colors.sky.swiftUIColor)
        }
      )
      .padding(.leading, 16.0)
      .buttonStyle(.plain)
      .accessibilityIdentifier(identifier(ComponentsIdentifier.downloadAsTextFileButton.rawValue))
    }
  }

  private var wordsGrid: some View {
    LazyVGrid(columns: columns, alignment: .leading) {
      ForEach(0..<viewModel.words.count, id: \.self, content: wordCell)
    }
    .frame(minHeight: 184.0)
    .background(Color.Onboarding.backgroundForm)
    .cornerRadius(8.0)
  }

  private func wordCell(_ index: Int) -> some View {
    HStack {
      ZStack {
        Text("\(index + 1).")
          .font(.textLSemiBold)
          .foregroundColor(
            .colors(light: Colors.cloud.swiftUIColor, dark: Colors.cloud.swiftUIColor.opacity(0.5))
          )
      }
      Text(viewModel.words[index])
        .font(.textLSemiBold)
        .foregroundColor(.text)
        .accessibilityIdentifier(identifier(ComponentsIdentifier.generatedWordWithIndex.rawValue, value: "\(index)"))
    }
    .padding()
  }

  private var checkboxView: some View {
    HStack(spacing: 14.0) {
      Toggle("", isOn: Binding(
        get: { viewModel.userConfirmsStoringWords },
        set: { viewModel.userConfirmsStoringWords = $0 }
      ))
      .labelsHidden()
      .toggleStyle(.checkbox)
      .accessibilityIdentifier(identifier(.writtenDownCheckbox))
      Text(WLStrings.onboarding.newWildlandWords.checkbox)
        .frame(maxWidth: .infinity, alignment: .leading)
        .font(.bodyText)
        .foregroundColor(.text)
        .lineLimit(2)
        .accessibilityIdentifier(identifier(.writtenDownCheckbox))
    }
    .padding(.horizontal, 16.0)
    .padding(.vertical, 12.0)
    .accentColor(Color(light: Colors.cloud, dark: Colors.gloom))
    .background(Color.Onboarding.backgroundBox)
    .cornerRadius(4)
    .onTapGesture(perform: viewModel.checkboxViewTapped)
  }
}

private enum ComponentsIdentifier: String {
  case generatedWordWithIndex
  case downloadAsTextFileButton
}

#if DEBUG

struct NewWildlandWordsView_Previews: PreviewProvider {
  static var previews: some View {
    let mnemonicWords: [MnemonicWord] = [.init(index: 0, value: "six"), .init(index: 1, value: "practice"),
                                         .init(index: 2, value: "skill"), .init(index: 3, value: "grow"),
                                         .init(index: 4, value: "stereo"), .init(index: 5, value: "fame"),
                                         .init(index: 6, value: "tattoo"), .init(index: 7, value: "speed"),
                                         .init(index: 8, value: "sport"), .init(index: 9, value: "old"),
                                         .init(index: 10, value: "tower"), .init(index: 12, value: "tower")]
    let viewModel = NewWildlandWordsViewModel(mnemonic: mnemonicWords,
                                              pathProvider: MacOSDestinationPathProvider())
    return NewWildlandWordsView(viewModel: viewModel)
  }
}

#endif
