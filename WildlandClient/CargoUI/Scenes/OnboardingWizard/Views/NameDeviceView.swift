//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import SwiftUI
import WildlandCommon

struct NameDeviceView: View, AccessiblityIdentifiable {

  @ObservedObject var viewModel: NameDeviceViewModel

  @State private var focusTag = Int.min
  @State var deviceName: String = ""

  var body: some View {
    OnboardingIntroContainerView {
      TextField(text: $viewModel.deviceName)
        .textFieldStyle(textFieldStyle)
        .accessibilityIdentifier(identifier(.textField))
        .padding(EdgeInsets(top: 4.0, leading: .zero, bottom: 103.0, trailing: 65.0))
        .onSubmit {
          viewModel.nextClicked()
        }
        .onAppear {
          viewModel.setSystemDevice()
        }
    }
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.image, Images.identityLaptop.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.nameYourDevice.title)
    .setProperty(\.text, WLStrings.onboarding.nameYourDevice.description)
    .setProperty(\.infoViewState, viewModel.activityState)
    .setProperty(\.bottomContent) {
      Spacer()
      WLButton(
        title: WLStrings.button.next,
        kind: .main,
        action: { viewModel.nextClicked() }
      )
      .disabled(viewModel.activityState.loading)
      .accessibilityIdentifier(identifier(.nextButton))
    }
  }

  private var textFieldStyle: SimpleTextFieldStyle {
    SimpleTextFieldStyle(
      placeholder: WLStrings.onboarding.nameYourDevice.textFieldPlaceholder,
      isVisiblePlaceholder: viewModel.deviceName.isEmpty
    )
  }
}

#if DEBUG

struct NameDeviceView_Previews: PreviewProvider {
  static var previews: some View {
    NameDeviceView(viewModel: NameDeviceViewModel(userService: PreviewDaemonUserService()))
  }
}

#endif
