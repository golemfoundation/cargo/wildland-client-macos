//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/// A view that displays a grid of buttons containing phrases to select.
struct MnemonicPhraseSelector: View, AccessiblityIdentifiable {

  @EnvironmentObject private var viewModel: ConfirmWildlandWordsViewModel<RunLoop>
  private let items: [GridItem]
  ///  Toggled states for buttons - once changed button starts shaking for the moment
  @State private var buttonStates: [Bool] = Array(repeating: true, count: 12)

  init(columnsCount: Int) {
    items = .init(repeating: GridItem(.fixed(80.0), spacing: 8), count: columnsCount)
  }

  var body: some View {
    VStack {
      LazyVGrid(columns: items, alignment: .leading, spacing: 8) {
        ForEach(0..<viewModel.shuffledPhrasesWithIndexes.count, id: \.self) { offset in
          shakeWrapperView(for: offset)
        }
      }
      .buttonStyle(MnemonicButtonStyle())
      .onReceive(viewModel.wrongSelectedIndexPublisher) { index in
        buttonStates[index].toggle()
      }
    }
  }

  private func shakeWrapperView(for offset: Int) -> some View {
    ShakeWrapperView(
      content: {
        Button(action: {
          viewModel.wordClicked(viewModel.shuffledPhrasesWithIndexes[offset].index)
        }) {
          Text("\(viewModel.shuffledPhrasesWithIndexes[offset].phrase.value)")
        }
        .accessibilityIdentifier(identifier(ComponentsIdentifier.wordWithIndex.rawValue, value: "\(offset)"))
      },
      shouldShake: Binding(get: { buttonStates[offset] }, set: { _ in })
    )
  }
}

private enum ComponentsIdentifier: String {
  case wordWithIndex
}

private struct ShakeWrapperView<Content: View>: View {

  @Binding private var shouldShake: Bool
  @State private var shakeCounter: CGFloat = 0
  private var content: () -> Content

  init(@ViewBuilder content: @escaping () -> Content, shouldShake: Binding<Bool>) {
    self.content = content
    self._shouldShake = shouldShake
  }

  var body: some View {
    content()
      .shake(count: $shakeCounter)
      .animation(.linear(duration: 0.6), value: shakeCounter)
      .onChange(of: shouldShake, perform: { _ in
        shakeCounter += 1
      })
  }
}
