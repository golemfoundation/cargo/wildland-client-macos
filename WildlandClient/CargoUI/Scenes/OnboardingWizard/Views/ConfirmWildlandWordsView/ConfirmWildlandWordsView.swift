//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon

struct ConfirmWildlandWordsView: View, AccessiblityIdentifiable {

  // MARK: - Properties

  @ObservedObject var viewModel: ConfirmWildlandWordsViewModel<RunLoop>

  // MARK: - Views

  var body: some View {
    VStack {
      OnboardingIntroContainerView {
        HStack(alignment: .top, spacing: 56.0) {
          VStack {
            wordSection
            forgotButton
          }
          MnemonicPhraseSelector(columnsCount: 3)
            .disabled(viewModel.disabled)
            .padding(.trailing, 8.0)
        }
        .padding(
          EdgeInsets(
            top: 32.0,
            leading: .zero,
            bottom: 56.0,
            trailing: 50.0
          )
        )
      }
      .setProperty(\.leadingPadding, 50.0)
      .setProperty(\.detailsPadding, 17.0)
      .setProperty(\.horizontalPadding, 48.0)
      .setProperty(\.progressValue, viewModel.progressValue)
      .setProperty(\.descriptionWidth, nil)
      .setProperty(\.title, WLStrings.onboarding.comfirmWildlandWords.title)
      .setProperty(\.attributedText, viewModel.description)
    }
    .environmentObject(viewModel)
  }

  private var wordSection: some View {
    HStack(spacing: 8.0) {
      Text("\(viewModel.ordinalNumber).")
        .font(Fonts.Inter.medium.swiftUIFont(size: 18.0))
        .foregroundColor(.text).opacity(0.5)
        .accessibilityIdentifier(identifier(ComponentsIdentifier.requiredWordIndex.rawValue))
      Text(viewModel.selectedWord)
        .font(.displayMedium)
        .foregroundColor(.text)
      Spacer()
    }
    .padding(.horizontal, 16.0)
    .frame(height: 64.0)
    .background(Color.Onboarding.backgroundForm)
    .cornerRadius(6.0)
  }

  private var forgotButton: some View {
    Button(action: viewModel.goBackClicked) {
      Text(WLStrings.onboarding.comfirmWildlandWords.buttonForgot)
        .font(.bodyText)
        .foregroundColor(Colors.sky.swiftUIColor)
    }
    .buttonStyle(.plain)
    .padding(.top, 50.0)
    .accessibilityIdentifier(identifier(ComponentsIdentifier.forgotWordsButton.rawValue))
  }
}

private enum ComponentsIdentifier: String {
  case requiredWordIndex
  case forgotWordsButton
}

#if DEBUG

struct ConfirmWildlandWordsView_Previews: PreviewProvider {
  static var previews: some View {
    let viewModel = ConfirmWildlandWordsViewModel(
      scheduler: RunLoop.main,
      mnemonic: [],
      numberOfAttempts: 3,
      service: PreviewDaemonUserService()
    )
    return ConfirmWildlandWordsView(viewModel: viewModel)
  }
}

#endif
