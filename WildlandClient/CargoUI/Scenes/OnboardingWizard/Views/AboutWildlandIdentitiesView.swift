//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon

struct AboutWildlandIdentitiesView: View, AccessiblityIdentifiable {

  @ObservedObject var viewModel: AboutWildlandIdentitiesModel

  var body: some View {
    OnboardingIntroContainerView {

      Spacer(minLength: 0)
    }
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.image, Images.identityKeys.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.aboutWildlandIdentities.title)
    .setProperty(\.text, WLStrings.onboarding.aboutWildlandIdentities.description)
    .setProperty(\.horizontalPadding, 33.0)
    .setProperty(\.bottomContent) {
      Spacer()
      WLButton(
        title: WLStrings.button.back,
        kind: .fallback,
        action: { viewModel.goBackClicked() }
      )
      .accessibilityIdentifier(identifier(.backButton))
      WLButton(
        title: WLStrings.button.next,
        kind: .main,
        action: { viewModel.nextClicked() }
      )
      .accessibilityIdentifier(identifier(.nextButton))
    }
  }
}

#if DEBUG

struct AboutWildlandIdentitiesView_Previews: PreviewProvider {

  static var previews: some View {
    AboutWildlandIdentitiesView(viewModel: .init())
  }
}

#endif
