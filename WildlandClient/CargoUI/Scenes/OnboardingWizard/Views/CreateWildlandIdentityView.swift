//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon

struct CreateWildlandIdentityView: View, AccessiblityIdentifiable {
  @ObservedObject var viewModel: CreateWildlandIdentityViewModel

  var body: some View {
    OnboardingIntroContainerView {
      Picker(
        selection:
          Binding(
            get: { viewModel.type },
            set: {
              $0.flatMap { viewModel.typeChanged($0) }
            }
          ),
        label: EmptyView()
      ) {
        ForEach(CreateWildlandIdentityViewModel.IdentityMode.allCases, content: prepareRadioButtonLabel)
      }
      .pickerStyle(.radioGroup)
      .alignmentGuide(.leading) { _ in 7.0 }
      .accentColor(Colors.grass.swiftUIColor)
      .allowsHitTesting(false)
    }
    .setProperty(\.progressValue, viewModel.progressValue)
    .setProperty(\.image, Images.identityKeys.swiftUIImage)
    .setProperty(\.title, WLStrings.onboarding.createIdentity.title)
    .setProperty(\.text, WLStrings.onboarding.createIdentity.description)
    .setProperty(\.bottomContent) {
      Spacer()
      WLButton(
        title: WLStrings.button.back,
        kind: .fallback,
        action: { viewModel.goBackClicked() }
      )
      .accessibilityIdentifier(identifier(.backButton))
      WLButton(
        title: WLStrings.button.next,
        kind: .main,
        action: { viewModel.nextClicked() }
      )
      .accessibilityIdentifier(identifier(.nextButton))
    }
  }

  private func prepareRadioButtonLabel(for value: CreateWildlandIdentityViewModel.IdentityMode) -> some View {
    WLRadioButtonLabel(
      text: value.title,
      hint: value.hint,
      isDisabled: value.isDisabled
    )
    .tag(value as CreateWildlandIdentityViewModel.IdentityMode?)
    .padding(
      EdgeInsets(
        top: 9,
        leading: 12,
        bottom: 2,
        trailing: .zero
      )
    )
    .accessibilityIdentifier(identifier(.selectOption, value: value.id))
  }
}

private extension CreateWildlandIdentityViewModel.IdentityMode {

  var title: String {
    switch self {
    case .useWildland:
      return WLStrings.onboarding.createIdentity.itemWildland.title
    case .useEthereum:
      return WLStrings.onboarding.createIdentity.itemEthereum.title
    }
  }

  var hint: String {
    switch self {
    case .useWildland:
      return WLStrings.onboarding.createIdentity.itemWildland.description
    case .useEthereum:
      return WLStrings.onboarding.createIdentity.itemEthereum.description
    }
  }
}

#if DEBUG

struct CreateWildlandIdentityView_Previews: PreviewProvider {
  static var previews: some View {
    CreateWildlandIdentityView(viewModel: .init())
  }
}

#endif
