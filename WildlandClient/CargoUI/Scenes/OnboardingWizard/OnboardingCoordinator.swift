//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import Foundation
import SwiftUI
import os
import WildlandCommon

@MainActor
public class OnboardingCoordinator: BaseCoordinator, ObservableObject {

  enum Stage: Equatable {
    case setupCargo
    case aboutWildlandIdentity
    case createIdentity
    case chooseIdentity
    case wildlandSecretPhraseIntro
    case newSecureWords
    case confirmSecureWords
    case linkWildlandIdentityToDevice
    case setYourOwnStorage
    case nameDevice
    case chooseCargoStorage
    case getVerificationCode
    case enterVerificationCode
  }

  public enum StartStage {
    case storageSetup
  }

  @Published var stage: Stage = .setupCargo

  public var startStage: StartStage?

  private let _log = Logger.wildlandLogger()

  private var stages = [Stage]()
  private var viewModels = [AnyObject]()
  private var vmSinks = [AnyCancellable]()
  private let onboardingProgressProvider = OnboardingProgressProvider()
  private let userService: DaemonUserService & DaemonControlService

  // MARK: - Public

  func currentViewModel<T: BaseOnboardingViewModel>() -> T {
    if let cvm = viewModels.last as? T {
      return cvm
    } else {
      fatalError("currentViewModel type mismatch for \(T.self)")
    }
  }

  // MARK: - Initialization

  public init(userService: DaemonUserService & DaemonControlService) {
    self.userService = userService
  }

  // MARK: - Stage management methods

  private func launchStage<T: BaseOnboardingViewModel>(_ stage: Stage, viewModel: T) { // swiftlint:disable:this cyclomatic_complexity
    stages.append(self.stage)
    self.stage = stage

    // TODO: cleanup sinks when view is popped from stack
    vmSinks.append(viewModel.onGoBack.sink(
      receiveCompletion: { _ in },
      receiveValue: { [weak self] _ in
        guard let self = self else { return }
        self.viewModels.removeLast()
        self.stage = self.stages.popLast()!
      }
    ))

    vmSinks.append(viewModel.onNext.sink(
      receiveCompletion: { _ in },
      receiveValue: { [weak self] _ in
        guard let self = self else { return }
        switch self.stage {
        case .setupCargo: self.handleNext(viewModel: viewModel as! SetupWildlandCargoModel)
        case .aboutWildlandIdentity: self.handleNext(viewModel: viewModel as! AboutWildlandIdentitiesModel)
        case .createIdentity: self.handleNext(viewModel: viewModel as! CreateWildlandIdentityViewModel)
        case .chooseIdentity: self.handleNext(viewModel: viewModel as! ChooseIdentityViewModel)
        case .wildlandSecretPhraseIntro: self.handleNext(viewModel: viewModel as! WildlandSecretPhraseIntroViewModel)
        case .newSecureWords: self.handleNext(viewModel: viewModel as! NewWildlandWordsViewModel)
        case .confirmSecureWords: self.handleNext(viewModel: viewModel as! ConfirmWildlandWordsViewModel)
        case .linkWildlandIdentityToDevice: self.handleNext(viewModel: viewModel as! EnterSecretPhraseViewModel)
        case .setYourOwnStorage: self.handleNext(viewModel: viewModel as! SetYourOwnStorageViewModel)
        case .nameDevice:  self.handleNext(viewModel: viewModel as! NameDeviceViewModel)
        case .chooseCargoStorage: self.handleNext(viewModel: viewModel as! ChooseCargoStorageViewModel)
        case .getVerificationCode: self.handleNext(viewModel: viewModel as! GetVerificationCodeViewModel)
        case .enterVerificationCode: self.handleNext(viewModel: viewModel as! EnterVerificationCodeViewModel)
        }
      }
    ))

    viewModel.progressValue = onboardingProgressProvider.progress(for: stages + [stage])
    viewModels.append(viewModel)
  }

  // MARK: - Handle next actions

  private func handleNext(viewModel: AboutWildlandIdentitiesModel) {
    launchStage(.createIdentity, viewModel: CreateWildlandIdentityViewModel())
  }

  private func handleNext(viewModel: CreateWildlandIdentityViewModel) {
    switch viewModel.type {
    case .useWildland:
      launchStage(
        .wildlandSecretPhraseIntro,
        viewModel: WildlandSecretPhraseIntroViewModel(variant: .newIdentity, service: userService)
      )
    case .useEthereum:
      break
      // TODO: Display web browser for Ethereum identity setup (CARMA-143)
    default: break
    }
  }

  private func handleNext(viewModel: SetupWildlandCargoModel) {
    switch viewModel.type {
    case .some(.createNew):
      launchStage(.aboutWildlandIdentity, viewModel: AboutWildlandIdentitiesModel())
    case .some(.linkToExisting):
      launchStage(.chooseIdentity, viewModel: ChooseIdentityViewModel())
    default: break
    }
  }

  private func handleNext(viewModel: ChooseIdentityViewModel) {
    switch viewModel.type {
    case .wildland:
      launchStage(.wildlandSecretPhraseIntro, viewModel: WildlandSecretPhraseIntroViewModel(variant: .recoverIdentity,
                                                                                            service: userService))
    case .ethereum:
      break
      // TODO: Display web browser for Ethereum identity setup (CARMA-143)
    default: break
    }
  }

  private func handleNext(viewModel: EnterVerificationCodeViewModel) {
    completionHandler?()
  }

  private func handleNext(viewModel: GetVerificationCodeViewModel) {
    Task { @MainActor in
      let viewModel = EnterVerificationCodeViewModel(
        email: viewModel.email,
        userService: userService,
        controlService: userService
      )
      launchStage(.enterVerificationCode, viewModel: viewModel)
    }
  }

  private func handleNext(viewModel: ChooseCargoStorageViewModel) {
    guard let type = viewModel.type else { return }

    Task { @MainActor in
      let model = GetVerificationCodeViewModel(userService: userService)
      switch type {
      case .freeFoundationTier:
        model.type = .freeFoundationTier
      case .selfSetup:
        model.type = .selfSetup
      }
      launchStage(.getVerificationCode, viewModel: model)
    }
  }

  private func handleNext(viewModel: NameDeviceViewModel) {
    launchStage(.chooseCargoStorage, viewModel: ChooseCargoStorageViewModel())
  }

  private func handleNext(viewModel: SetYourOwnStorageViewModel) {
    // TODO: Display quick tour screen (CARMA-17)
  }

  private func handleNext(viewModel: EnterSecretPhraseViewModel) {
    launchStage(.nameDevice, viewModel: NameDeviceViewModel(userService: userService))
  }

  private func handleNext(viewModel: WildlandSecretPhraseIntroViewModel) {
    switch viewModel.variant {
    case .newIdentity:
      // so navigate the user to generate identity view
      guard !viewModel.mnemonicWords.isEmpty else {
        _log.error("No mnemonic was found for the identity screen")
        return
      }
      launchStage(.newSecureWords,
                  viewModel: NewWildlandWordsViewModel(mnemonic: viewModel.mnemonicWords,
                                                       pathProvider: MacOSDestinationPathProvider()))
    case .recoverIdentity:
      launchStage(.linkWildlandIdentityToDevice, viewModel: EnterSecretPhraseViewModel(userService: userService))
    }
  }

  private func handleNext(viewModel: NewWildlandWordsViewModel) {
    assert(viewModel.userConfirmsStoringWords)
    launchStage(
      .confirmSecureWords,
      viewModel: ConfirmWildlandWordsViewModel(
        scheduler: RunLoop.main,
        mnemonic: viewModel.mnemonicWords,
        numberOfAttempts: 3,
        service: userService
      )
    )
  }

  private func handleNext(viewModel: ConfirmWildlandWordsViewModel<RunLoop>) {
    launchStage(.nameDevice, viewModel: NameDeviceViewModel(userService: userService))
  }
}

extension OnboardingCoordinator: Coordinator {

  public var rootView: AnyView? {
    return AnyView(OnboardingView(coordinator: self))
  }

  public func start() {
    guard let startStage = self.startStage else {
      launchStage(.setupCargo, viewModel: SetupWildlandCargoModel())
      return
    }

    switch startStage {
    case .storageSetup:
      launchStage(.chooseCargoStorage, viewModel: ChooseCargoStorageViewModel())
    }
  }
}

private extension Logger {

  func handleOnboardingCompletionError(method: String = #function, _ failure: Error?) {
    if let failure {
      error(method: method, failure: failure)
    } else {
      info(method: method, details: "the domain was mounted")
    }
  }
}
