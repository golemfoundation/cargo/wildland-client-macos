//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

class MacOSDestinationPathProvider: DestinationPathProvider {

  func getDestinationPath() -> String? {
    let sp = NSSavePanel()
    sp.allowedContentTypes = [.text, .plainText]
    sp.canCreateDirectories = true
    sp.isExtensionHidden = false
    sp.allowsOtherFileTypes = false
    sp.title = "Save word list"
    sp.nameFieldStringValue = "wildland_seed_phrase"
    sp.message = "Choose a folder and a name to store the file."
    sp.nameFieldLabel = "File name:"

    let resp = sp.runModal()
    if resp == .OK {
      return sp.url?.path
    } else {
      return .none
    }
  }
}
