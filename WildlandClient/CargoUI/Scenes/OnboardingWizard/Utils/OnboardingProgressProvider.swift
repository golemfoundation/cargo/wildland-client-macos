//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol OnboardingProgressProviderType {
  func progress(for stages: [OnboardingCoordinator.Stage]) -> Double
}

final class OnboardingProgressProvider: OnboardingProgressProviderType {

  enum IdentityType {
    case createNewIdentity
    case linkIdentityToDevice
  }

  func progress(for stages: [OnboardingCoordinator.Stage]) -> Double {
    guard let uniqueStages = Array(NSOrderedSet(array: stages)) as? [OnboardingCoordinator.Stage] else { return .zero }
    let longestPath = buildAvailablePaths()
      .filter { path in
        guard uniqueStages.count <= path.count else { return false }
        return Array(path[0..<uniqueStages.count]) == uniqueStages
      }
      .max(by: { $0.count < $1.count }) ?? []

    let totalCount = longestPath.last == .enterVerificationCode
    ? longestPath.count + 1
    : longestPath.count

    return Double(uniqueStages.count) / Double(totalCount)
  }

  // MARK: - Private
  private func buildAvailablePaths() -> [[OnboardingCoordinator.Stage]] {
    [
      buildPath(identityType: .createNewIdentity, tokenType: .ethereum, storageType: .selfSetup),
      buildPath(identityType: .createNewIdentity, tokenType: .ethereum, storageType: .freeFoundationTier),
      buildPath(identityType: .createNewIdentity, tokenType: .wildland, storageType: .selfSetup),
      buildPath(identityType: .createNewIdentity, tokenType: .wildland, storageType: .freeFoundationTier),
      buildPath(identityType: .linkIdentityToDevice, tokenType: .ethereum, storageType: .selfSetup),
      buildPath(identityType: .linkIdentityToDevice, tokenType: .ethereum, storageType: .freeFoundationTier),
      buildPath(identityType: .linkIdentityToDevice, tokenType: .wildland, storageType: .selfSetup),
      buildPath(identityType: .linkIdentityToDevice, tokenType: .wildland, storageType: .freeFoundationTier),
      [.setupCargo] + buildStoragePath(storageType: .selfSetup),
      [.setupCargo] + buildStoragePath(storageType: .freeFoundationTier)
    ]
  }

  private func buildPath(identityType: OnboardingProgressProvider.IdentityType,
                         tokenType: ChooseIdentityViewModel.IdentityGenerationMode,
                         storageType: ChooseCargoStorageViewModel.ChooseCargoMode) -> [OnboardingCoordinator.Stage] {
    var stages = [OnboardingCoordinator.Stage]()
    switch identityType {
    case .createNewIdentity:
      stages = [.setupCargo, .aboutWildlandIdentity, .createIdentity]
    case .linkIdentityToDevice:
      stages = [.setupCargo, .chooseIdentity]
    }

    switch (identityType, tokenType) {
    case (.createNewIdentity, .wildland):
      stages += [.wildlandSecretPhraseIntro, .newSecureWords, .confirmSecureWords]
    case (.linkIdentityToDevice, .wildland):
      stages += [.wildlandSecretPhraseIntro, .linkWildlandIdentityToDevice]
    default:
      break
    }

    stages += [.nameDevice]
    stages += buildStoragePath(storageType: storageType)

    return stages
  }

  private func buildStoragePath(storageType: ChooseCargoStorageViewModel.ChooseCargoMode) -> [OnboardingCoordinator.Stage] {
    var stages: [OnboardingCoordinator.Stage] = [.chooseCargoStorage]

    switch storageType {
    case .selfSetup:
      stages += [.setYourOwnStorage]
    case .freeFoundationTier:
      stages += [.getVerificationCode, .enterVerificationCode]
    }

    return stages
  }
}
