//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon
import os

/// The class `ConfirmWildlandWordsViewModel` represents the logic for confirming words from a mnemonic phrase.
/// The class uses @Published properties to store the shuffled phrases and their indexes,
/// and the current index to be confirmed by the user.
///
/// The class contains two PassthroughSubjects for emitting events when the user selects a word. `wrongSelectedIndex`
/// is emitted when the user selects a word that is not part of the words to be confirmed, and `correctlySelectedWord`
/// is emitted when the user selects a word that is part of the words to be confirmed.
///
/// The class also has properties for storing the indexes of the words to be confirmed, the max number of attempts, and the number
/// of attempts left. The class has an init method that takes in a mnemonic payload and a `numberOfAttempts` count,
/// and shuffles the phrases.

final class ConfirmWildlandWordsViewModel<S: Scheduler>: BaseOnboardingViewModel {

  @Published private(set) var shuffledPhrasesWithIndexes: [(phrase: MnemonicWord, index: Int)] = []
  @Published private(set) var indexToConfirm: Int?
  @Published private(set) var selectedWord: String = "------"
  @Published private(set) var disabled = false

  var ordinalNumber: Int {
    (indexToConfirm ?? .zero) + 1
  }

  var description: AttributedString {
    let formattedNumber = numberFormatter.string(from: NSNumber(value: ordinalNumber)) ?? String(ordinalNumber)
    let description = String(format: WLStrings.onboarding.comfirmWildlandWords.description, formattedNumber)
    let bolded = String(format: WLStrings.onboarding.comfirmWildlandWords.boldedDescription, formattedNumber)
    var result = AttributedString(description)
    if let rangeOfBoldedText = result.range(of: bolded) {
      result[rangeOfBoldedText].font = .textLSemiBold
    }
    return result
  }

  private(set) lazy var wrongSelectedIndexPublisher = wrongSelectedIndex.eraseToAnyPublisher()

  private let _log = Logger.wildlandLogger()

  // That represents index in shuffled array - not orignal index of the phrase
  private let wrongSelectedIndex = PassthroughSubject<Int, Never>()
  private let correctlySelectedWord = PassthroughSubject<String, Never>()

  private let mnemonicWords: [MnemonicWord]
  private var indexesToConfirm: [Int] = []
  private let userService: DaemonUserService
  private let maxNumberOfAttempts: Int
  private let mnemonicLength: Int

  private var numberOfAttemptsLeft: Int {
    maxNumberOfAttempts - indexesToConfirm.count
  }
  private let delayBetweenAnswer: S.SchedulerTimeType.Stride
  private let numberFormatter: NumberFormatter
  private let scheduler: S

  // MARK: - Initialization

  init(scheduler: S,
       mnemonic: [MnemonicWord],
       numberOfAttempts: Int,
       numberOfWords: Int = 12,
       delayBetweenAnswer: S.SchedulerTimeType.Stride = 1,
       service: DaemonUserService,
       formatter: NumberFormatter = .ordinalNumberFormatter) {
    self.scheduler = scheduler
    self.delayBetweenAnswer = delayBetweenAnswer
    userService = service
    mnemonicWords = mnemonic
    mnemonicLength = numberOfWords
    maxNumberOfAttempts = numberOfAttempts
    shuffledPhrasesWithIndexes = mnemonic.shuffledWithOriginalIndexes()
    numberFormatter = formatter

    super.init()

    setupBindings()
    indexesToConfirm.reserveCapacity(numberOfAttempts)
    prepareNextIndexToConfirm()
  }

  // MARK: - Setup

  private func setupBindings() {

    let wordSetPublisher = correctlySelectedWord
      .handleEvents(receiveOutput: { [weak self] word in
        self?.selectedWord = word
      })

    Publishers.Merge(wrongSelectedIndex.mapToVoid(), wordSetPublisher.mapToVoid())
      .setValue(true, keyPath: \.disabled, on: self)
      .delay(for: delayBetweenAnswer, scheduler: scheduler)
      .handleEvents(receiveOutput: { [weak self] in
        self?.prepareNextIndexToConfirm()
        self?.prepareShuffledPhrasesWithIndexes()
      })
      .setValue(false, keyPath: \.disabled, on: self)
      .map { "------" }
      .assign(to: &$selectedWord)
  }

  // MARK: - Public

  /// Handles a phrase selection by the user
  /// - Parameter index: The selected word's index
  func wordClicked(_ index: Int) {
    var wrongIndex: Int?

    // If the clicked index is not the last from the indexes to confirm
    if indexesToConfirm.last != index {
      wrongIndex = selectedIndex(index)
      indexesToConfirm.removeAll(keepingCapacity: true)
    }

    guard numberOfAttemptsLeft > 0 else {
      nextClicked(); return
    }

    if let wrongIndex {
      wrongSelectedIndex.send(wrongIndex)
    } else {
      correctlySelectedWord.send(mnemonicWords[index].value)
    }
  }

  override func nextClicked() {
    Task { @MainActor in
      switch await createMnemonic(from: mnemonicWords) {
      case .success:
        _log.info("Mnemonic was created for provided words")
        super.nextClicked()
      case .failure(let error):
        _log.error("error: \(error.localizedDescription, privacy: .public)")
      }
    }
  }

  // MARK: - Private

  private func createMnemonic(from words: [MnemonicWord]) async -> Result<Void, Error> {
    let mnemonicWords = words.compactMap { $0.value.isEmpty ? nil : $0.value }
    guard mnemonicWords.count == mnemonicLength else {
      let error = LocalizedViewModelError(WLStrings.onboarding.comfirmWildlandWords.mnemonicFailedEmptyWords)
      return .failure(error)
    }

    return await userService.createMnemonic(using: mnemonicWords)
  }

  private func prepareShuffledPhrasesWithIndexes() {
    shuffledPhrasesWithIndexes = mnemonicWords.shuffledWithOriginalIndexes()
  }

  private func prepareNextIndexToConfirm() {
    nextIndexToConfirm().map({
      indexesToConfirm.append($0)
      indexToConfirm = indexesToConfirm.last
    })
  }

  ///  Converts index of the word in mnemonic into index in shuffled array
  ///  - Parameter index: Index of the phrase in original (not shuffled) mnemonic
  ///  - Returns: Index of the phrase in shuffled array
  private func selectedIndex(_ index: Int) -> Int? {
    shuffledPhrasesWithIndexes.selectedElementIndex(original: index)
  }

  private func nextIndexToConfirm() -> Int? {
    shuffledPhrasesWithIndexes.nextIndexDifferentThan(value: indexesToConfirm)
  }
}

private extension Array where Element == (phrase: MnemonicWord, index: Int) {

  func nextIndexDifferentThan(value: [Int]) -> Int? {
    map({ $0.index }).filter({ !value.contains($0) }).randomElement()
  }

  func selectedElementIndex(original index: Int) -> Int? {
    enumerated().first { $0.element.index == index }.map { $0.offset }
  }
}

private extension Array {
  /// Shuffles the elements of an array and returns an array of tuples,
  /// each containing the shuffled element and its original index.
  func shuffledWithOriginalIndexes() -> [(Element, Index)] {
    enumerated().shuffled().map { ($0.element, $0.offset) }
  }
}
