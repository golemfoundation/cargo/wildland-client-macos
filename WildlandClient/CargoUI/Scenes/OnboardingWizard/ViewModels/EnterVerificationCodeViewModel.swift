//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Combine
import os
import WildlandCommon

@MainActor
final class EnterVerificationCodeViewModel: BaseOnboardingViewModel {

  struct Word: Hashable, Equatable {
    @TruncatedUppercase(maxLength: 1) var value: String = ""
  }

  let fieldFormatter: FocusableTextFieldFormatter
  private let _validCharacters: CharacterSet
  private let _log = Logger.wildlandLogger()
  private let _numberOfWords: Int
  private let _email: String?
  private let userService: DaemonUserService
  private let controlService: DaemonControlService

  @Published var currentSymbolIndex: Int = .zero
  @Published var words: [Word]
  @Published private(set) var isVerified = false
  @Published private(set) var isCompleted = false

  // MARK: Init

  init(numberOfWords: Int = 6,
       email: String,
       userService: DaemonUserService,
       controlService: DaemonControlService,
       validCharacters: CharacterSet = CharacterSet.letters.union(CharacterSet.decimalDigits)) {
    _numberOfWords = numberOfWords
    _email = email
    _validCharacters = validCharacters
    fieldFormatter = FocusableTextFieldFormatter(maxLength: 1, validCharacters: validCharacters)
    words = Array(repeating: Word(), count: _numberOfWords)
    self.userService = userService
    self.controlService = controlService
    super.init()

    setupWordsStream()
  }

  private func setupWordsStream() {
    Task {
      for await words in $words.values {
        currentSymbolIndex = words.firstIndex(where: { $0.value == "" }) ?? words.count - 1
        activityState = .idle
        await verifyEmail(words: words)
      }
    }
  }

  // MARK: - Public

  func verificationCodeTextFieldAppeared() {
    DispatchQueue.main.delay(with: .thirdOfASecond) { [weak self] in
      self?.currentSymbolIndex = .zero
    }
  }

  func isEditableField(at index: Int) -> Bool {
    guard let lastFilledFieldIndex = words.lastIndex(where: { !$0.value.isEmpty }) else {
      return index == .zero
    }
    let nextIndex = min(lastFilledFieldIndex + 1, words.count - 1)
    return index == nextIndex
  }

  func deleted(at index: Int, string: String) {
    guard (1..<words.indices.count).contains(index) else { return }
    let indexToDelete = string.isEmpty ? index - 1 : index
    words[indexToDelete].value = ""
  }

  func resendCodeClicked() async {
    guard let _email else { return }
    do {
      try await userService.requestFreeTierStorage(for: _email)
      activityState = .success(WLStrings.onboarding.enterVerificationCode.infoResend)
    } catch {
      handleError(error)
    }
  }

  func pasteString(value: String?) {
    guard let value else { return }
    guard String(value.unicodeScalars.filter(_validCharacters.contains)) == value else {
      _log.error("Failed to paste value: \(value) characters not valid")
      return
    }
    guard value.count == _numberOfWords else {
      _log.error("Failed to paste content: incorrect number of symbols \(value.count), expected: \(self._numberOfWords)")
      return
    }
    words = value.compactMap { value in
      Word(value: String(value))
    }
  }

  func retryClicked() {
    Task {
      await verifyEmail(words: words)
    }
  }

  // MARK: - Overrides

  override func nextClicked() {
    Task { @MainActor in
      guard isCompleted else { return }
      super.nextClicked()
    }

    // TODO: Tutorial screens are described in task (CARGO-354)
  }

  // MARK: - Private

  private func verifyEmail(words: [Word]) async {
    let code = words.map { $0.value }.joined()
    guard code.count == _numberOfWords else { return }

    do {
      try await verifyCode(code)
      await mountContainers()
    } catch {
      handleError(error)
    }
  }

  private func verifyCode(_ code: String) async throws {
    activityState = .loading(WLStrings.onboarding.enterVerificationCode.infoVerification)
    try await userService.verifyEmail(with: code)
    activityState = .success(WLStrings.onboarding.enterVerificationCode.infoVerified)

    isVerified = true
  }

  private func mountContainers() async {
    activityState = .loading(WLStrings.onboarding.enterVerificationCode.mountingContainers)
    controlService.mountDomain { [weak self] error in
      guard let error else {
        Task { @MainActor in
          self?.isCompleted = true
          self?.activityState = .idle
        }
        return
      }
      self?.handleError(error)
    }
  }

  private func handleError(_ error: Error) {
    let message = WLStrings.Alert.errorGenericMessage
    guard let wildlandError = error as? WildlandError else {
      activityState = .error(message)
      return
    }
    switch wildlandError.type {
    case .auth:
      activityState = .error(WLStrings.onboarding.enterVerificationCode.infoVerificationFailed)
    case .networkConnectivity, .database:
      activityState = .error(
        WLStrings.onboarding.generalError.networkError,
        imageType: .networkConnection,
        timeout: .seconds(3)
      )
    default:
      activityState = .error(message)
    }
  }
}
