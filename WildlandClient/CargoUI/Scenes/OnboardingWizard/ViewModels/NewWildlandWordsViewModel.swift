//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import Foundation
import SwiftUI
import WildlandCommon
import wildlandx

class NewWildlandWordsViewModel: BaseOnboardingViewModel {

  let mnemonicWords: [MnemonicWord]
  @Published var userConfirmsStoringWords = false

  var attributedText: AttributedString {
    var result = AttributedString(WLStrings.onboarding.newWildlandWords.description)
    if let rangeOfBoldedText = result.range(of: WLStrings.onboarding.newWildlandWords.boldedDescription) {
      result[rangeOfBoldedText].font = .textLSemiBold
    }
    return result
  }

  private let destinationPathProvider: DestinationPathProvider

  init(mnemonic: [MnemonicWord], pathProvider: DestinationPathProvider) {
    destinationPathProvider = pathProvider
    mnemonicWords = mnemonic

    super.init()

    assert(mnemonic.count == 12)
  }

  var words: [String] {
    mnemonicWords.map(\.value)
  }

  func checkConfirmStoringWords(_ value: Bool) {
    userConfirmsStoringWords = value
  }

  @MainActor func downloadFileClicked() async {
    guard let path = self.destinationPathProvider.getDestinationPath() else { return }

    let file = self.mnemonicWords.enumerated()
      .map { v in "\(v.offset + 1). \(v.element.value)" }
      .joined(separator: "\n")
    do {
      try file.write(toFile: path, atomically: true, encoding: .ascii)
    } catch {
      activityState = .error(WLStrings.onboarding.nameYourDevice.errorStoringWordFile, timeout: .seconds(3))
    }
  }

  func checkboxViewTapped() {
    userConfirmsStoringWords.toggle()
  }

  override func nextClicked() {
    super.nextClicked()
    userConfirmsStoringWords = false
  }
}
