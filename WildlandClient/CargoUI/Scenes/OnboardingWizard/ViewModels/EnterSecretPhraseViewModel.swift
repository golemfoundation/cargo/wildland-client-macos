//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import os
import WildlandCommon

class EnterSecretPhraseViewModel: BaseOnboardingViewModel {

  private let _log = Logger.wildlandLogger()
  private let _numberOfWords: Int

  private let _userService: DaemonUserService

  init(numberOfWords: Int = 12, userService: DaemonUserService) {
    _numberOfWords = numberOfWords
    _userService = userService

    super.init()

    initializeContent()
  }

  // view
  @Published private(set) var canVerify = false
  @Published var currentWordIndex: Int = .min
  @Published var words: [MnemonicWord] = []

  // MARK: - Public

  @MainActor
  func verifyClicked() async {
    switch await createMnemonic(from: words) {
    case .success:
      _log.info("Mnemonic was created for provided words")
      activityState = .success(WLStrings.onboarding.enterWildlandWords.infoVerified)
    case .failure(let error):
      _log.error("error: \(error.localizedDescription, privacy: .public)")
      activityState = .error(WLStrings.onboarding.enterWildlandWords.infoVerificationFailed)
    }
  }

  func wordIsBeingEdited(_ isEdited: Bool, at index: Int) {
    activityState = .idle
    currentWordIndex = isEdited ? index : Int.min

    wordChanged(index: index)
  }

  func pasteString(value: String?) {
    activityState = .idle

    guard let content = value else {
      activityState = .error(WLStrings.onboarding.enterWildlandWords.fileContentProcessingFailed)
      return
    }

    if let error = processContent(string: content, wordIndex: currentWordIndex) {
      activityState = .error(error.localizedDescription)
    }
  }

  func dropPerformed(with data: Data?, for wordIndex: Int? = nil) {
    activityState = .idle

    guard let data = data, !data.isEmpty else {
      activityState = .error(WLStrings.onboarding.enterWildlandWords.fileContentEmptyData)
      return
    }
    guard let content = String(data: data, encoding: .utf8), !content.isEmpty else {
      activityState = .error(WLStrings.onboarding.enterWildlandWords.fileContentProcessingFailed)
      return
    }

    if let error = processContent(string: content, wordIndex: wordIndex) {
      activityState = .error(error.localizedDescription)
    }
  }

  func selectNextWordForEditing() {
    let nextIndex = currentWordIndex + 1
    currentWordIndex = nextIndex < words.count ? nextIndex : 0
  }

  // MARK: - Private

  private func initializeContent() {
    for i in 0..<_numberOfWords { words.append(MnemonicWord(index: i, value: "")) }
  }

  private func createMnemonic(from words: [MnemonicWord]) async -> Result<Void, Error> {
    let mnemonicWords = words.compactMap { $0.value.isEmpty ? nil : $0.value }
    guard mnemonicWords.count == _numberOfWords else {
      let error = LocalizedViewModelError(WLStrings.onboarding.enterWildlandWords.mnemonicFailedEmptyWords)
      return .failure(error)
    }
    guard areAllItemsUnique(in: words) else {
      let error = LocalizedViewModelError(WLStrings.onboarding.enterWildlandWords.mnemonicFailedDuplicatedWords)
      return .failure(error)
    }

    return await _userService.createMnemonic(using: mnemonicWords)
  }

  private func processContent(string: String, wordIndex: Int? = nil) -> Error? {
    let wordContent = processFile(for: string)
    if wordContent.isEmpty {
      return processPlainStringContent(string: string, index: wordIndex)
    } else {
      return processWordContent(with: wordContent)
    }
  }

  private func processPlainStringContent(string: String, index: Int?) -> Error? {
    guard let index = index else { return LocalizedViewModelError("Text field index is not set") }
    guard index < words.count else { return LocalizedViewModelError("Incorrect word index") }

    words[index].value = string
    return nil
  }

  private func processWordContent(with wordContent: [MnemonicWord]) -> Error? {
    for word in wordContent {
      wordChanged(word.value, at: word.index - 1)
    }
    return nil
  }

  private func wordChanged(index: Int) {
    canVerify = words.compactMap { $0.value.isEmpty ? $0.value : nil }.isEmpty

    if words[index].hasMaxLength {
      selectNextWordForEditing()
    }
  }

  private func wordChanged(_ value: String, at index: Int) {
    guard index >= 0, index < words.count else { return }

    words[index].value = value
    wordChanged(index: index)
  }

  private func areAllItemsUnique(in words: [MnemonicWord]) -> Bool {
    var uniqueWords: Set<String> = []
    words.forEach { word in uniqueWords.insert(word.value) }

    return uniqueWords.count == _numberOfWords
  }

  private func processFile(for content: String) -> [MnemonicWord] {
    let lines = content.components(separatedBy: "\n")
    let words: [MnemonicWord] = lines.compactMap { line in
      let word = line.components(separatedBy: ". ")

      guard word.count == 2 else { return nil }
      guard let index = Int(word[0]) else { return nil }

      return MnemonicWord(index: index, value: word[1])
    }
    return words
  }
}
