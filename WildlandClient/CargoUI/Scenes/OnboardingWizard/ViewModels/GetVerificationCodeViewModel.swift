//
//  Wildland Project
//  GetVerificationCodeViewModel.swift
//  CargoUI
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import wildlandx
import WildlandCommon
import os

@MainActor
final class GetVerificationCodeViewModel: BaseOnboardingViewModel {

  enum CargoMode: Equatable, Hashable, CaseIterable {
    case freeFoundationTier
    case selfSetup
  }

  @Published var email: String = ""
  @Published private(set) var isEmailInvalid: Bool = false

  var type: CargoMode?

  private let userService: DaemonUserService
  private let _log = Logger.wildlandLogger()

  // MARK: - Initialization

  init(userService: DaemonUserService) {
    self.userService = userService
  }

  // MARK: - Public

  func pasteString(value: String?) {
    guard let email = value else { return }
    self.email = email
  }

  func nextButtonClicked() async {
    isEmailInvalid = !isValid(email: email)
    guard let type, !isEmailInvalid else { return }
    switch type {
    case .freeFoundationTier:
      await requestFreeStorage(for: email)
    case .selfSetup:
      break
    }
  }

  // MARK: - Private

  private func isValid(email: String) -> Bool {
    guard !email.isEmpty else { return false }
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPred = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
  }

  private func requestFreeStorage(for email: String) async {
    activityState = .loading(WLStrings.onboarding.getVerificationCode.requestingStorage)
    do {
      try await userService.requestFreeTierStorage(for: email)
      activityState = .idle
      nextClicked()
    } catch {
      handleError(error)
    }
  }

  private func handleError(_ error: Error) {
    _log.error("Code verification failed: \(error.localizedDescription, privacy: .public)")
    let message = WLStrings.Alert.errorGenericMessage
    guard let wildlandError = error as? WildlandError else {
      activityState = .error(message)
      return
    }
    switch wildlandError.type {
    case .networkConnectivity, .database:
      activityState = .error(
        WLStrings.onboarding.generalError.networkError,
        imageType: .networkConnection,
        timeout: .seconds(3)
      )
    default:
      activityState = .error(message)
    }
  }
}
