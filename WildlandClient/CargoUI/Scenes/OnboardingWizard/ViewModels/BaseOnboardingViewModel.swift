//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import Foundation

/*
 A ViewModel base for wizard-like flows
 */
@MainActor
class BaseOnboardingViewModel: ObservableObject {
  @Published var activityState: ViewActivityState = .idle {
    didSet {
      if case .error(_, _, let timeout) = activityState {
        guard let timeout else { return }

        Task.detached { @MainActor in
          try await Task.sleep(for: timeout)
          self.activityState = .idle
        }
      }
    }
  }

  let onGoBack = PassthroughSubject<Void, Error>()
  let onNext = PassthroughSubject<Void, Error>()
  var progressValue = 0.0

  func goBackClicked() {
    onGoBack.send()
  }

  func nextClicked() {
    onNext.send()
  }
}
