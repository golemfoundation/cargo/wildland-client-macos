//
//  Wildland Project
//  NameDeviceViewModel.swift
//  CargoUI
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import WildlandCommon
import SwiftUI
import os
import wildlandx

final class NameDeviceViewModel: BaseOnboardingViewModel {

  @Published var deviceName: String = "" {
    didSet {
      activityState = .idle
    }
  }
  private let _log = Logger.wildlandLogger()
  private let _userService: DaemonUserService

  // MARK: - Initialization

  init(userService: DaemonUserService) {
    _userService = userService
    super.init()
  }

  // MARK: - Overrides

  override func nextClicked() {
    Task { @MainActor in
      activityState = .loading(WLStrings.onboarding.nameYourDevice.loadingCreateUser)
      do {
        try await self.createUserMnemonic()
        activityState = .success(WLStrings.onboarding.nameYourDevice.successCreateUser)
        super.nextClicked()
      } catch {
        handleError(error)
      }
    }
  }

  // MARK: - Public

  func setSystemDevice() {
    deviceName = Host.current().localizedName ?? ""
  }

  // MARK: - Private

  private func createUserMnemonic() async throws {
    guard !deviceName.isEmpty else {
      throw LocalizedViewModelError(WLStrings.onboarding.nameYourDevice.errorDeviceNameEmpty)
    }
    return try await _userService.createUser(with: deviceName)
  }

  private func handleError(_ error: Error) {
    let message = WLStrings.onboarding.nameYourDevice.errorCreatingUser
    guard let wildlandError = error as? WildlandError else {
      activityState = .error(message)
      return
    }
    switch wildlandError.type {
    case .networkConnectivity, .database:
      activityState = .error(
        WLStrings.onboarding.generalError.networkError,
        imageType: .networkConnection,
        timeout: .seconds(3)
      )
    default:
      activityState = .error(message)
    }
  }
}
