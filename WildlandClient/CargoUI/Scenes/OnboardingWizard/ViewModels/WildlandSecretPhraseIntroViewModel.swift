//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import Foundation
import WildlandCommon
import os

class WildlandSecretPhraseIntroViewModel: BaseOnboardingViewModel {

  private let userService: DaemonUserService

  enum Variant {
    case newIdentity
    case recoverIdentity
  }

  let variant: Variant
  var mnemonicWords: [MnemonicWord] = []

  init(variant: Variant, service: DaemonUserService) {
    self.variant = variant
    userService = service

    super.init()
  }

  override func nextClicked() {
    Task {
      switch variant {
      case .newIdentity:
        do {
          let generatedMnemonic = try await userService.generateMnemonic()
          mnemonicWords = generatedMnemonic.enumerated().map(MnemonicWord.init)
        } catch {
          activityState = .error(WLStrings.onboarding.wildlandSecretPhaseIntro.identityGenerationFailed)
          fatalError("Failed generating a new identity from random seed")
        }
      case .recoverIdentity:
        break
      }

      super.nextClicked()
    }
  }
}
