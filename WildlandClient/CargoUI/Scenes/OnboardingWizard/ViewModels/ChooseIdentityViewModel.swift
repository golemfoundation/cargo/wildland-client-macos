//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import Foundation

class ChooseIdentityViewModel: BaseOnboardingViewModel {
  enum IdentityGenerationMode: Equatable, Hashable, CaseIterable, Identifiable {
    case wildland
    case ethereum

    var id: String { "\(self)" }
    var isDisabled: Bool {
      self == .ethereum
    }
  }

  @Published private(set) var type: IdentityGenerationMode?

  // MARK: - Initialization

  override init() {
    self.type = .wildland
  }

  // MARK: - Public

  func typeChanged(_ type: IdentityGenerationMode) {
    guard !type.isDisabled else { return }
    self.type = type
  }
}
