//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine

final class ChooseCargoStorageViewModel: BaseOnboardingViewModel {

  enum ChooseCargoMode: Equatable, Hashable, CaseIterable, Identifiable {
    case freeFoundationTier
    case selfSetup

    var id: String { "\(self)" }
    var isDisabled: Bool {
      self == .selfSetup
    }
  }

  // MARK: - Properties

  @Published private(set) var type: ChooseCargoMode?
  @Published var userConfirmsTermsAndConditions = false

  // MARK: - Initialization

  override init() {
    self.type = .freeFoundationTier
  }

  // MARK: - Public

  func typeChanged(_ type: ChooseCargoMode) {
    guard !type.isDisabled else { return }
    self.type = type
  }
}
