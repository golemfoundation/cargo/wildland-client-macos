//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

struct OnboardingView: View {
  @ObservedObject var coordinator: OnboardingCoordinator

  var body: some View {
    switch coordinator.stage {
    case .setupCargo:
      SetupWildlandCargoView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .aboutWildlandIdentity:
      AboutWildlandIdentitiesView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .createIdentity:
      CreateWildlandIdentityView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .chooseIdentity:
      ChooseIdentityView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .wildlandSecretPhraseIntro:
      WildlandSecretPhaseIntroView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .newSecureWords:
      NewWildlandWordsView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .confirmSecureWords:
      ConfirmWildlandWordsView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .linkWildlandIdentityToDevice:
      EnterSecretPhraseView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .setYourOwnStorage:
      SetYourOwnStorageView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .nameDevice:
      NameDeviceView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .chooseCargoStorage:
      ChooseCargoStorageView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .getVerificationCode:
      GetVerificationCodeView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    case .enterVerificationCode:
      EnterVerificationCodeView(viewModel: coordinator.currentViewModel())
        .colorScheme()
    }
  }
}
