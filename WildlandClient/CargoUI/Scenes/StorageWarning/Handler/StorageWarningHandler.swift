//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa
import SwiftUI
import Combine

public protocol WarningHandler {
  typealias Content = (error: Error, filename: String)
  var dismissPublisher: AnyPublisher<Void, Never> { get }
  var errorSubscriber: AnySubscriber<WarningHandler.Content?, Never> { get }
}

/// `StorageWarningHandler` is responsible for managing storage warning alerts. It observes for
/// storage-related errors and displays an alert to the user, offering potential solutions. Alerts
/// are presented using a `Presenter<PanelWindow>`, which allows for the use of SwiftUI views in
/// AppKit-based applications. It also exposes an interface for observing alert dismissal.
public final class StorageWarningHandler<S: Scheduler>: WarningHandler {

  // MARK: - Properties

  lazy public private(set) var dismissPublisher = presenter.dismissPublisher
  lazy public private(set) var errorSubscriber = AnySubscriber(errorSubject)
  public let presenter = Presenter<PanelWindow>()
  private var cancellables = Set<AnyCancellable>()
  private let errorSubject = PassthroughSubject<WarningHandler.Content?, Never>()
  private let filename = PassthroughSubject<String, Never>()
  private let scheduler: S

  // MARK: - Initialization

  public init(scheduler: S = DispatchQueue.main) {
    self.scheduler = scheduler
    setupBindings()
  }

  // MARK: - Private

  private func setupBindings() {
    errorSubject
      .compactMap { $0?.filename }
      .subscribe(filename)
      .store(in: &cancellables)

    filename
      .filter { [weak self] _ in self?.presenter.isPresented.wrappedValue == false }
      .receive(on: scheduler)
      .sink(receiveValue: { [weak self] in
        self?.present(with: $0)
      })
      .store(in: &cancellables)
  }

  private func present(with filename: String) {
    presenter.present {
      StorageWarningView(filename: filename)
        .frame(width: 520, height: 421)
        .window()
        .colorScheme()
    }
  }
}
