//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WildlandCommon

public struct StorageWarningView: View {

  // MARK: - Environment

  @Environment(\.window) var window

  // MARK: - Properties

  private let filename: String

  // MARK: - Initialization

  public init(filename: String) {
    self.filename = filename
  }

  // MARK: - Views

  public var body: some View {
    VStack(spacing: .zero) {
      header
      Divider().standard
        .padding(.bottom, 36)
      Images.quotaStorage.swiftUIImage
        .padding(.bottom, 23)
      message
      Divider().standard
        .padding(.bottom, 15)
      buttons
      Spacer()
    }
    .background(Color.Modal.windowBackground)
    .ignoresSafeArea()
    .transformEnvironment(\.window) { window in
      transform(window: window())
    }
  }

  private var header: some View {
    Text(WLStrings.quotaModal.title)
      .font(.displayLarge)
      .foregroundColor(Color(light: Colors.mainText, dark: Colors.cloud))
      .padding(
        EdgeInsets(
          top: 32,
          leading: .zero,
          bottom: 31,
          trailing: .zero
        )
      )
  }

  private var message: some View {
    Text(String(format: WLStrings.quotaModal.message, filename))
      .font(.bodyText)
      .foregroundColor(Color(light: Colors.mainText, dark: Colors.cloud))
      .multilineTextAlignment(.center)
      .lineSpacing(2)
      .padding(.bottom, 41)
  }

  private var buttons: some View {
    HStack(spacing: 2) {
      Spacer()
      WLButton(
        title: WLStrings.button.cancel,
        kind: .fallback,
        action: close
      )
      WLButton(
        title: WLStrings.button.gotIt,
        kind: .main,
        action: close
      )
      .padding(.trailing, 18)
    }
  }

  // MARK: - Private

  private func close() {
    window()?.close()
  }

  private func transform(window: NSWindow?) {
    let windowIdentifier = NSUserInterfaceItemIdentifier(filename)
    guard window?.identifier != windowIdentifier else { return }
    window?.identifier = windowIdentifier
    window?.level = .floating
    window?.titleVisibility = .hidden
    window?.titlebarAppearsTransparent = true
    window?.isMovableByWindowBackground = true
    window?.standardWindowButton(.closeButton)?.isHidden = true
    window?.standardWindowButton(.miniaturizeButton)?.isHidden = true
    window?.standardWindowButton(.zoomButton)?.isHidden = true
  }
}

#if DEBUG

struct QuotaView_Previews: PreviewProvider {
  static var previews: some View {
    StorageWarningView(filename: "filename")
      .frame(width: 520, height: 448)
  }
}

#endif
