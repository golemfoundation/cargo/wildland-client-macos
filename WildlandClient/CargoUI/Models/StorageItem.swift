//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

public struct StorageItem: Identifiable, Equatable {
  public var id: String
  let title: String
  let subtitle: String
  let usedBytes: Measurement<UnitInformationStorage>
  let totalBytes: Measurement<UnitInformationStorage>
}

extension StorageItem {
  init(_ storage: Storage, formatter: ByteCountFormatter) {
    let usedBytes = Measurement<UnitInformationStorage>(value: Double(storage.usedSpace), unit: .bytes)
    let totalBytes = Measurement<UnitInformationStorage>(value: Double(storage.totalSpace), unit: .bytes)
    var percent: Int = .zero
    if totalBytes.value > .zero {
      percent = Int(usedBytes.value / totalBytes.value * 100)
    }
    let formattedUsedBytes = formatter.string(from: usedBytes)
    let formattedTotalBytes = formatter.string(from: totalBytes)
    let format = WLStrings.Storages.typeAndSpace
    let subtitle = String(format: format, storage.type, formattedUsedBytes, formattedTotalBytes, String(percent))
    self.init(
      id: storage.identifier,
      title: storage.name ?? "",
      subtitle: subtitle,
      usedBytes: usedBytes,
      totalBytes: totalBytes
    )
  }
}

extension [StorageItem] {
  static let placeholder: [StorageItem] = [
    .placeholder(identifier: "1"),
    .placeholder(identifier: "2")
  ]
}

extension StorageItem {
  static func placeholder(identifier: String) -> StorageItem {
    StorageItem(
      id: identifier,
      title: "title",
      subtitle: "subtitle",
      usedBytes: .init(value: .zero, unit: .bytes),
      totalBytes: .init(value: .zero, unit: .bytes)
    )
  }
}
