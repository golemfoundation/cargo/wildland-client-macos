//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

struct MnemonicWord: Hashable {
  private let maxWordLength = 8

  let index: Int
  var value: String {
    didSet {
      value = String(value.letters().prefix(maxWordLength)).lowercased()
    }
  }

  var hasMaxLength: Bool {
    return value.count == maxWordLength
  }
}
