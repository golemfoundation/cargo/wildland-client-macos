//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os

@propertyWrapper
public struct ClampedProgress {

  private let validRange = 0.0...1.0

  private let log: Logger

  private var value: Double!

  public init(wrappedValue: Double, log: Logger) {
    self.log = log
    self.value = clamp(value: wrappedValue)
  }

  public var wrappedValue: Double {
    get { value }
    set { value = clamp(value: newValue) }
  }

  private func clamp(value: Double) -> Double {
    if !validRange.contains(value) {
      assertionFailure()
      log.errorClamped(value: value)
    }

    return min(max(value, validRange.lowerBound), validRange.upperBound)
  }
}

private extension Logger {
  func errorClamped(file: String = #file, method: String = #function, value: Double) {
    let details = "value: \(value)"
    error(file: file, method: method, details: details, failure: nil)
  }
}
