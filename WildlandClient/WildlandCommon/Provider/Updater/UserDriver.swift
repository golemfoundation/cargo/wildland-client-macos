//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Sparkle
import Combine
import os
import ServiceManagement

public protocol UserDriverType: SPUUserDriver {
  var updateStatePublisher: AnyPublisher<UpdateState, Never> { get }
  var updateUserChoiceSubscriber: AnySubscriber<UpdateUserChoice, Never> { get }
  var canInstallAndRelaunchSubscriber: AnySubscriber<Bool, Never> { get }
  func acknowledgementDismissed()
}

public final class UserDriver: NSObject, UserDriverType {

  // MARK: - Properties

  lazy public private(set) var updateStatePublisher = updateStateSubject.eraseToAnyPublisher()
  private let updateStateSubject = PassthroughSubject<UpdateState, Never>()

  lazy public private(set) var updateUserChoiceSubscriber = AnySubscriber(updateUserChoiceSubject)
  private let updateUserChoiceSubject = PassthroughSubject<UpdateUserChoice, Never>()

  lazy public private(set) var canInstallAndRelaunchSubscriber = AnySubscriber(canInstallAndRelaunchSubject)
  private let canInstallAndRelaunchSubject = PassthroughSubject<Bool, Never>()

  private var totalDataCount: Double = .zero
  private var receivedDataCount: Double = .zero

  private let logger: Logger
  private let decimalFormatter: ByteCountFormatter
  private let upToDateErrorCode = 1001
  private var acknowledgementSet: (() -> Void)?

  private var downloadingState: UpdateState {
    .downloading(
      receivedFormattedBytes: decimalFormatter.string(from: .init(value: receivedDataCount, unit: .bytes)),
      totalFormattedBytes: decimalFormatter.string(from: .init(value: totalDataCount, unit: .bytes))
    )
  }

  // MARK: - Initialization

  public init(logger: Logger = .updater, decimalFormatter: ByteCountFormatter) {
    self.logger = logger
    self.decimalFormatter = decimalFormatter
    super.init()
  }

  // MARK: - Public

  public func acknowledgementDismissed() {
    logger.debug()
    acknowledgementSet?()
  }

  // MARK: - SPUUserDriver

  public func show(_ request: SPUUpdatePermissionRequest) async -> SUUpdatePermissionResponse {
    logger.debug(details: "request: \(request)")
    return SUUpdatePermissionResponse(
      automaticUpdateChecks: true,
      automaticUpdateDownloading: nil,
      sendSystemProfile: false
    )
  }

  public func showUserInitiatedUpdateCheck(cancellation: @escaping () -> Void) {
    logger.debug()
  }

  public func showUpdateFound(with appcastItem: SUAppcastItem, state: SPUUserUpdateState) async -> SPUUserUpdateChoice {
    logger.debug(details: "appcastItem: \(appcastItem) state: \(state)")
    updateStateSubject.send(.newUpdateAvailable)

    let result = await updateUserChoiceSubject
      .map { userChoice -> SPUUserUpdateChoice in
        SPUUserUpdateChoice(rawValue: userChoice.rawValue) ?? .dismiss
      }
      .values
      .first(where: { _ in true })

    return result ?? .dismiss
  }

  public func showUpdateReleaseNotes(with downloadData: SPUDownloadData) {
    logger.debug(details: "downloadData: \(downloadData.data.count)")
  }

  public func showUpdateReleaseNotesFailedToDownloadWithError(_ error: Error) {
    logger.error(failure: error)
  }

  public func showUpdateNotFoundWithError(_ error: Error, acknowledgement: @escaping () -> Void) {
    logger.error(failure: error)
    let state: UpdateState = (error as NSError).code == upToDateErrorCode
    ? .upToDate
    : .error(error)
    updateStateSubject.send(state)
    acknowledgementSet = acknowledgement
  }

  public func showUpdaterError(_ error: Error, acknowledgement: @escaping () -> Void) {
    logger.error(failure: error)
    updateStateSubject.send(.error(error))
    acknowledgementSet = acknowledgement
  }

  public func showDownloadInitiated(cancellation: @escaping () -> Void) {
    logger.debug()
  }

  public func showDownloadDidReceiveExpectedContentLength(_ expectedContentLength: UInt64) {
    logger.debug(details: "expectedContentLength: \(expectedContentLength)")
    totalDataCount = Double(expectedContentLength)
    updateStateSubject.send(downloadingState)
  }

  public func showDownloadDidReceiveData(ofLength length: UInt64) {
    logger.debug(details: "length: \(length)")
    receivedDataCount += Double(length)
    updateStateSubject.send(downloadingState)
  }

  public func showDownloadDidStartExtractingUpdate() {
    updateStateSubject.send(.extracting(percent: .zero))
    logger.debug()
  }

  public func showExtractionReceivedProgress(_ progress: Double) {
    logger.debug(details: "progress: \(progress)")
    updateStateSubject.send(.extracting(percent: UInt(progress * 100)))
  }

  public func showReadyToInstallAndRelaunch() async -> SPUUserUpdateChoice {
    logger.debug()

    let result = await canInstallAndRelaunchSubject
      .handleEvents(receiveOutput: { [weak self] canInstallAndRelaunch in
        self?.updateStateSubject.send(canInstallAndRelaunch ? .dismiss : .waitForOperationsToComplete)
      })
      .filter { $0 }
      .map { _ -> SPUUserUpdateChoice in .install }
      .values
      .first(where: { _ in true })

    return result ?? .dismiss
  }

  public func showInstallingUpdate(withApplicationTerminated applicationTerminated: Bool, retryTerminatingApplication: @escaping () -> Void) {
    logger.debug(details: "applicationTerminated: \(applicationTerminated)")
    guard !applicationTerminated else { return }
    retryTerminatingApplication()
  }

  public func showUpdateInstalledAndRelaunched(_ relaunched: Bool, acknowledgement: @escaping () -> Void) {
    logger.debug(details: "relaunched: \(relaunched)")
    acknowledgement()
  }

  public func showUpdateInFocus() {
    logger.debug()
  }

  public func dismissUpdateInstallation() {
    logger.debug()
    totalDataCount = .zero
    receivedDataCount = .zero
    updateStateSubject.send(.dismiss)
  }
}
