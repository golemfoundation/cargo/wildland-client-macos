//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import Sparkle
import os

public protocol UpdaterControllerType {
  var updateStatePublisher: AnyPublisher<UpdateState, Never> { get }
  var canCheckForUpdatesPublisher: AnyPublisher<Bool, Never> { get }
  var newAppVersionIdentifiedPublisher: AnyPublisher<Void, Never> { get }
  var updateUserChoiceSubscriber: AnySubscriber<UpdateUserChoice, Never> { get }
  func checkForUpdates()
  func acknowledgementDismissed()
}

public final class UpdaterController<S: Scheduler>: UpdaterControllerType {

  // MARK: - Properties

  lazy public private(set) var updateStatePublisher = userDriver.updateStatePublisher
  lazy public private(set) var updateUserChoiceSubscriber = userDriver.updateUserChoiceSubscriber

  lazy public private(set) var canCheckForUpdatesPublisher = canCheckForUpdatesSubject.eraseToAnyPublisher()
  private let canCheckForUpdatesSubject = PassthroughSubject<Bool, Never>()

  lazy public private(set) var newAppVersionIdentifiedPublisher = Publishers.CombineLatest3(
    newAppVersionIdentifiedSubject,
    synchronizationStateProvider.statePublisher,
    updateStatePublisher.prepend(.dismiss)
  )
    .filter { _, synchronizationState, updateState in
      synchronizationState == .synced && !updateState.isUpdating
    }
    .map { _ in }
    .eraseToAnyPublisher()

  private let newAppVersionIdentifiedSubject = PassthroughSubject<Void, Never>()
  private let observedPathChanged = PassthroughSubject<Void, Never>()

  private let updater: UpdaterType
  private let userDriver: UserDriverType
  private let synchronizationStateProvider: SynchronizationStateProvider
  private let logger: Logger
  private let bundle: Bundle
  private var fileWatcher: FileWatcherType
  private let versionChecker: VersionCheckerType
  private let debounceDueTime: Int
  private let queue: DispatchQueue
  private let scheduler: S

  private var mainApplicationPath: String {
    bundle.mainApplication.bundlePath
  }

  private var isNewVersionAvailable: Bool {
    versionChecker.isNewVersionAvailable(bundle: bundle)
  }

  // MARK: - Initialization

  public init(
    updaterType: UpdaterType.Type = SPUUpdater.self,
    bundle: Bundle = Bundle.main,
    userDriver: UserDriverType,
    synchronizationStateProvider: SynchronizationStateProvider,
    logger: Logger = .updater,
    fileWatcher: FileWatcherType = FileWatcher(),
    versionChecker: VersionCheckerType = VersionChecker(logger: .updater),
    debounceDueTime: Int = 3,
    queue: DispatchQueue = DispatchQueue.global(),
    scheduler: S = DispatchQueue.global()
  ) {
    self.userDriver = userDriver
    self.updater = updaterType.init(
      hostBundle: bundle.mainApplication,
      applicationBundle: bundle,
      userDriver: userDriver,
      delegate: nil
    )
    self.synchronizationStateProvider = synchronizationStateProvider
    self.logger = logger
    self.bundle = bundle
    self.fileWatcher = fileWatcher
    self.versionChecker = versionChecker
    self.debounceDueTime = debounceDueTime
    self.queue = queue
    self.scheduler = scheduler

    setupBindings()
    setupFileWatcher()
    startUpdater()
  }

  // MARK: - Setup

  private func setupBindings() {
    updater
      .canCheckForUpdatesPublisher
      .subscribe(AnySubscriber(canCheckForUpdatesSubject))

    synchronizationStateProvider
      .statePublisher
      .map { $0 == .synced }
      .receive(subscriber: userDriver.canInstallAndRelaunchSubscriber)

    observedPathChanged
      .debounce(for: .seconds(debounceDueTime), scheduler: scheduler)
      .filter { [weak self] in self?.isNewVersionAvailable == true }
      .receive(subscriber: AnySubscriber(newAppVersionIdentifiedSubject))
  }

  private func setupFileWatcher() {
    fileWatcher.callback = { [weak self] event in
      guard let mainAppPath = self?.mainApplicationPath,
            event.path.contains(mainAppPath) else { return }
      self?.observedPathChanged.send()
    }
    fileWatcher.start(paths: [mainApplicationPath], queue: queue)
  }

  // MARK: - Public

  public func checkForUpdates() {
    logger.debug()
    updater.checkForUpdates()
  }

  public func acknowledgementDismissed() {
    userDriver.acknowledgementDismissed()
  }

  // MARK: - Private

  private func startUpdater() {
    logger.debug()
    do {
      try updater.start()
    } catch {
      logger.error(failure: error)
    }
  }
}

private extension Bundle {
  var mainApplication: Bundle {
    var applicationUrl = bundleURL
    while applicationUrl.pathComponents.contains(Defaults.App.cargo),
          applicationUrl.lastPathComponent != Defaults.App.cargo {
      applicationUrl.deleteLastPathComponent()
    }
    return Bundle(path: applicationUrl.path()) ?? self
  }
}
