//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public enum UpdateState {
  case dismiss
  case upToDate
  case newUpdateAvailable
  case downloading(receivedFormattedBytes: String, totalFormattedBytes: String)
  case extracting(percent: UInt)
  case waitForOperationsToComplete
  case error(Error)
}

public extension UpdateState {

  var canShowUpdateView: Bool {
    switch self {
    case .dismiss, .error:
      return false
    default:
      return true
    }
  }

  var canShowCloseButton: Bool {
    switch self {
    case .newUpdateAvailable, .upToDate:
      return true
    default:
      return false
    }
  }

  var isUpdating: Bool {
    switch self {
    case .downloading, .extracting:
      return true
    default:
      return false
    }
  }

  var isError: Bool {
    switch self {
    case .error:
      return true
    default:
      return false
    }
  }
}

public extension UpdateState {

  var title: String {
    switch self {
    case .upToDate:
      return WLStrings.ActivityView.NewVersionAlert.latestVersionTitle
    default:
      return WLStrings.ActivityView.NewVersionAlert.title
    }
  }

  var subtitle: String? {
    switch self {
    case .upToDate:
      return WLStrings.ActivityView.NewVersionAlert.latestVersionMessage
    case .dismiss:
      return nil
    case .newUpdateAvailable:
      return WLStrings.ActivityView.NewVersionAlert.subtitle
    case .downloading(let received, let total):
      return String(format: WLStrings.ActivityView.NewVersionAlert.downloading, received, total)
    case .extracting(let percent):
      return String(format: WLStrings.ActivityView.NewVersionAlert.extracting, String(percent))
    case .waitForOperationsToComplete:
      return WLStrings.ActivityView.NewVersionAlert.waitForOperationsToComplete
    case .error:
      return nil
    }
  }
}
