//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

public protocol PathResolvable {
  func path(for identifier: String) throws -> String
}

public protocol FileManagerProtocol: PathResolvable {
  func createDirectory(for path: String) async throws
  func readFile(identifier: String, progressHandler: @escaping ((ProgressType) -> Void)) async throws
  func writeFile(path: String, identifier: String, creationDate: Date?, progressHandler: @escaping ((ProgressType) -> Void)) async throws
  func removeFile(path: String) async throws
  func removeDirectory(path: String, recursive: Bool) async throws
  func rename(path: String, newPath: String) async throws
}
