//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UserNotifications
import os
import Combine

public struct UserNotificationResponse {
  public let category: UserNotifications.Category
  public let action: UserNotifications.Category.Action
}

public protocol UserNotificationsType {
  var notificationResponsePublisher: AnyPublisher<UserNotificationResponse, Never> { get }
  func requestNotificationAuthorization()
  func showNotification(category: UserNotifications.Category)
  func authorizationStatus(completion: @escaping (UNAuthorizationStatus) -> Void)
}

public final class UserNotifications: NSObject, UserNotificationsType {

  public enum Category: String {
    case appUpdate = "WL_Application_update_category"
    case appUninstall = "WL_Application_uninstall_category"
    case shareLinkCopied = "WL_Application_share_link_copied_category"

    public enum Action: String {
      case select
      case approved
      case declined
    }
  }

  public lazy private(set) var notificationResponsePublisher = notificationResponseSubject.eraseToAnyPublisher()
  private let notificationResponseSubject = PassthroughSubject<UserNotificationResponse, Never>()
  private let logger: Logger
  private let userNotificationCenter: UNUserNotificationCenter

  // MARK: - Initialization

  public init(userNotificationCenter: UNUserNotificationCenter = .current(), logger: Logger = .notifications) {
    self.logger = logger
    self.userNotificationCenter = userNotificationCenter
  }

  // MARK: - Public

  public func requestNotificationAuthorization() {
    userNotificationCenter.delegate = self
    userNotificationCenter.requestAuthorization(options: [.alert, .sound]) { [weak self] isAllowed, _ in
      self?.logger.debug(details: "isAllowed: \(isAllowed)")
    }
  }

  public func authorizationStatus(completion: @escaping (UNAuthorizationStatus) -> Void) {
    userNotificationCenter.getNotificationSettings { settings in
      completion(settings.authorizationStatus)
    }
  }

  public func showNotification(category: Category) {
    userNotificationCenter.removeDeliveredNotifications(withIdentifiers: [category.rawValue])

    let request = UNNotificationRequest(
      identifier: UUID().uuidString,
      content: category.notificationContent,
      trigger: nil
    )

    userNotificationCenter.add(request) { [weak self] error in
      self?.logger.error(failure: error)
    }

    let category = UNNotificationCategory(
      identifier: category.rawValue,
      actions: category.actions, intentIdentifiers: [],
      options: UNNotificationCategoryOptions(rawValue: 0)
    )

    userNotificationCenter.setNotificationCategories([category])
  }
}

extension UserNotifications: UNUserNotificationCenterDelegate {
  public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                     didReceive response: UNNotificationResponse,
                                     withCompletionHandler completionHandler: @escaping () -> Void) {
    guard let category = Category(rawValue: response.notification.request.content.categoryIdentifier) else {
      logger.error("notification category identifier is not supported: \(response.notification.request.content.categoryIdentifier)")
      assert(false, "Unsupported category")
      return
    }
    guard let action = Category.Action(rawValue: response.actionIdentifier) else {
      if response.actionIdentifier == UNNotificationDefaultActionIdentifier {
        let userNotificationResponse = UserNotificationResponse(category: category, action: .select)
        notificationResponseSubject.send(userNotificationResponse)
        return
      }
      logger.error("notification action identifier is not supported: \(response.actionIdentifier)")
      assert(false, "Unsupported action")
      return
    }
    let userNotificationResponse = UserNotificationResponse(category: category, action: action)
    notificationResponseSubject.send(userNotificationResponse)
  }
}

private extension UserNotifications.Category {

  var notificationContent: UNMutableNotificationContent {
    let content = UNMutableNotificationContent()
    content.title = title
    content.body = body
    content.sound = .default
    content.categoryIdentifier = rawValue
    return content
  }

  var actions: [UNNotificationAction] {
    switch self {
    case .appUpdate:
      return [
        UNNotificationAction(
          identifier: UserNotifications.Category.Action.approved.rawValue,
          title: WLStrings.appUpdater.appUpdateRequestedNotification.messageApprove,
          options: []
        ),
        UNNotificationAction(
          identifier: UserNotifications.Category.Action.declined.rawValue,
          title: WLStrings.appUpdater.appUpdateRequestedNotification.messageDecline,
          options: []
        )
      ]
    case .appUninstall, .shareLinkCopied:
      return []
    }
  }

  private var title: String {
    switch self {
    case .appUpdate:
      return WLStrings.appUpdater.appUpdateRequestedNotification.title
    case .appUninstall:
      return WLStrings.appUninstaller.appUninstallCompletedAlert.title
    case .shareLinkCopied:
      return WLStrings.ContainerProperties.Share.Notification.title
    }
  }

  private var body: String {
    switch self {
    case .appUpdate:
      return WLStrings.appUpdater.appUpdateRequestedNotification.message
    case .appUninstall:
      return WLStrings.appUninstaller.appUninstallCompletedAlert.message
    case .shareLinkCopied:
      return WLStrings.ContainerProperties.Share.Notification.message
    }
  }
}
