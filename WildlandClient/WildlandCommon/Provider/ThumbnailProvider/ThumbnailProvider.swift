//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UniformTypeIdentifiers
import AppKit.NSImage

public protocol ThumbnailProviderType {
  func getThumbnail(from name: String) -> NSImage?
  func getThumbnail(from fileItem: FileItem) -> NSImage?
}

public struct ThumbnailProvider: ThumbnailProviderType {

  // MARK: - Properties

  private let workspace: NSWorkspace

  // MARK: - Initialization

  public init(workspace: NSWorkspace = .shared) {
    self.workspace = workspace
  }

  // MARK: - Public

  public func getThumbnail(from name: String) -> NSImage? {
    let nameComponents = name.components(separatedBy: ".")
    if nameComponents.count == 2 {
      return getThumbnailForFile(fileExtension: nameComponents.last)
    } else if nameComponents.count == 1 {
      return getThumbnailForFolder()
    }
    return nil
  }

  public func getThumbnail(from fileItem: FileItem) -> NSImage? {
    if fileItem.isFolder {
      return getThumbnailForFolder()
    } else {
      let nameComponents = fileItem.name.components(separatedBy: ".")
      return getThumbnailForFile(fileExtension: nameComponents.last)
    }
  }

  // MARK: - Private

  private func getThumbnailForFile(fileExtension: String?) -> NSImage? {
    guard let fileExtension, let type = UTType(filenameExtension: fileExtension) else {
      guard let publicData = UTType("public.data") else { return nil }
      return workspace.icon(for: publicData)
    }
    return workspace.icon(for: type)
  }

  private func getThumbnailForFolder() -> NSImage? {
    guard let type = UTType("public.folder") else { return nil }
    return workspace.icon(for: type)
  }
}
