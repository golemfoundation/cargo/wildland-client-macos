//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os

public protocol VersionCheckerType {
  func isNewVersionAvailable(bundle: Bundle) -> Bool
}

public struct VersionChecker: VersionCheckerType {

  // MARK: - Properties

  private let logger: Logger

  // MARK: - Initialization

  public init(logger: Logger = .updater) {
    self.logger = logger
  }

  // MARK: - Public

  public func isNewVersionAvailable(bundle: Bundle) -> Bool {
    let infoPlistVersions = getVersionsFromInfoPlist(bundleUrl: bundle.bundleURL)
    let isNewerAppVersion = infoPlistVersions.appVersion?.isNewerThan(bundle.appVersion ?? "") == true
    let isNewerBuildVersion = infoPlistVersions.buildVersion?.isNewerThan(bundle.buildVersion ?? "") == true
    return isNewerAppVersion || isNewerBuildVersion
  }

  // MARK: - Private

  private func getVersionsFromInfoPlist(bundleUrl: URL) -> (appVersion: String?, buildVersion: String?) {
    do {
      let data = try Data(contentsOf: bundleUrl.appending(path: Defaults.Path.infoPlist))
      let infoPlist = try PropertyListSerialization.propertyList(from: data, format: nil) as? [String: Any]
      return (
        appVersion: infoPlist?[InfoPlistKeys.applicationVersion] as? String,
        buildVersion: infoPlist?[InfoPlistKeys.buildVersion] as? String
      )
    } catch {
      logger.error(failure: error)
      return (appVersion: nil, buildVersion: nil)
    }
  }
}
