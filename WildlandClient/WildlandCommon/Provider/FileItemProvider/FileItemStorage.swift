//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

public protocol FileItemStorageType {
  func store(_ fileItem: FileItem) async
  func remove(identifier: String) async
  func fileItem(for identifier: String) async -> FileItem?
  func fileItems(for parentIdentifier: String) async -> [FileItem]
}

public actor FileItemStorage: FileItemStorageType {

  // MARK: - Properties

  private var storage: [FileItem] = []

  // MARK: - Initialization

  public init() { }

  // MARK: - Public

  public func store(_ fileItem: FileItem) async {
    if let index = storage.firstIndex(where: { $0.identifier == fileItem.identifier }) {
      storage[index] = fileItem
    } else {
      storage.append(fileItem)
    }
  }

  public func remove(identifier: String) async {
    storage.removeAll(where: { $0.identifier == identifier })
  }

  public func fileItem(for identifier: String) async -> FileItem? {
    storage.first(where: { $0.identifier == identifier })
  }

  public func fileItems(for parentIdentifier: String) async -> [FileItem] {
    storage.filter { $0.parentIdentifier == parentIdentifier }
  }
}
