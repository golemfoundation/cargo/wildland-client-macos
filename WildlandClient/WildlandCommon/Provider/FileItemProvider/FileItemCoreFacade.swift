//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import wildlandx
import os

public protocol FileItemCoreFacadeType {
  func fileItem(for identifier: String, path: String?, metadata: Metadata?) throws -> FileItem
  func fileItem(path: String) throws -> FileItem
  func fileItems(for parentIdentifier: String) throws -> [FileItem]
}

public struct FileItemCoreFacade: FileItemCoreFacadeType {

  // MARK: - Properties

  private let userApi: UserPublicKeyProviderType
  private let dfs: SharedDfsFrontend
  private let pathComposer: PathComposer
  private let logger: Logger

  private var publicKey: String {
    get throws {
      try userApi.userPublicKey
    }
  }

  // MARK: - Initialization

  public init(
    userApi: UserPublicKeyProviderType,
    dfs: SharedDfsFrontend = WildlandX.cargo.dfsApi(),
    pathComposer: PathComposer = PathComposer(),
    logger: Logger
  ) {
    self.userApi = userApi
    self.dfs = dfs
    self.pathComposer = pathComposer
    self.logger = logger
  }

  // MARK: - Public

  public func fileItem(for identifier: String, path: String?, metadata: Metadata?) throws -> FileItem {
    try measureTimeElapsed(log: logger) {
      let path = try path ?? getPath(for: identifier)
      return try fileItem(identifier: identifier, path: path, metadata: metadata)
    }
  }

  public func fileItem(path: String) throws -> FileItem {
    try measureTimeElapsed(log: logger) {
      let metadata = try Metadata(dfs.metadata(RustString(path)))
      return try fileItem(identifier: metadata.uuid, path: path, metadata: metadata)
    }
  }

  public func fileItems(for parentIdentifier: String) throws -> [FileItem] {
    try measureTimeElapsed(log: logger) {
      let path = try getPath(for: parentIdentifier)
      guard !path.isEmpty else { throw FileManagerError.invalidPath() }
      return try dfs.readDir(RustString(path))
        .map { dirEntry in
          try fileItem(
            for: dirEntry.stat().wildlandObjectId().toString(),
            path: path.appendPathItemName(dirEntry.itemName().toString()),
            metadata: Metadata(dirEntry.stat())
          )
        }
    }
  }

  // MARK: - Private

  private func fileItem(identifier: String, path: String, metadata: Metadata?) throws -> FileItem {
    let name = pathComposer.fileName(from: path)
    let parentPath = pathComposer.parentPath(from: path)
    let metadata = try metadata ?? Metadata(dfs.metadata(RustString(path)))

    let isParentRoot = identifier == Defaults.Identifier.root || parentPath == Defaults.Path.root
    let parentIdentifier = isParentRoot
    ? Defaults.Identifier.root
    : try dfs.metadata(RustString(parentPath)).wildlandObjectId().toString()

    let fileItemEntry = try FileProviderDomainService.Entry(
      identifier: identifier,
      parentIdentifier: parentIdentifier,
      name: name,
      metadata: metadata
    )

    return FileItem(fileItemEntry, userPublicKey: try publicKey)
  }

  private func getPath(for identifier: String) throws -> String {
    identifier != Defaults.Identifier.root
    ? try dfs.getPath(RustString(identifier)).toString()
    : Defaults.Path.root
  }
}
