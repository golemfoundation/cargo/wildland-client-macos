//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

public protocol FileItemProviderType {
  func fileItem(for identifier: String, path: String?, metadata: Metadata?) async throws -> FileItem
  func fileItems(for parentIdentifier: String) async throws -> [FileItem]
  func createFileItem(path: String) async throws -> FileItem
  func updateFileItem(identifier: String) async throws -> FileItem
}

extension FileItemProviderType {
  public func fileItem(for identifier: String, path: String? = nil, metadata: Metadata? = nil) async throws -> FileItem {
    try await fileItem(for: identifier, path: path, metadata: metadata)
  }
}

public final class FileItemProvider: FileItemProviderType {

  // MARK: - Properties

  private let fileItemStorage: FileItemStorageType
  private let fileItemCoreFacade: FileItemCoreFacadeType

  // MARK: - Initialization

  public init(fileItemStorage: FileItemStorageType, fileItemCoreFacade: FileItemCoreFacadeType) {
    self.fileItemStorage = fileItemStorage
    self.fileItemCoreFacade = fileItemCoreFacade
  }

  // MARK: - Public

  public func fileItem(for identifier: String, path: String?, metadata: Metadata?) async throws -> FileItem {
    guard let fileItem = await fileItemStorage.fileItem(for: identifier) else {
      let fileItem = try fileItemCoreFacade.fileItem(for: identifier, path: path, metadata: metadata)
      await fileItemStorage.store(fileItem)
      return fileItem
    }
    return fileItem
  }

  public func fileItems(for parentIdentifier: String) async throws -> [FileItem] {
    let fileItems = try fileItemCoreFacade.fileItems(for: parentIdentifier)
    for fileItem in fileItems {
      await fileItemStorage.store(fileItem)
    }
    return fileItems
  }

  public func createFileItem(path: String) async throws -> FileItem {
    let fileItem = try fileItemCoreFacade.fileItem(path: path)
    await fileItemStorage.store(fileItem)
    return fileItem
  }

  public func updateFileItem(identifier: String) async throws -> FileItem {
    let fileItem = try fileItemCoreFacade.fileItem(for: identifier, path: nil, metadata: nil)
    await fileItemStorage.store(fileItem)
    return fileItem
  }
}
