//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public struct PathComposer {

  // MARK: - Properties

  private let slash = "/"

  // MARK: - Initialization

  public init() { }

  // MARK: - Public

  public func fileName(from path: String) -> String {
    let components = path.components(separatedBy: slash)
    guard let last = components.last, !last.isEmpty else { return Defaults.Identifier.root }
    return last
  }

  public func rename(from path: String, name: String) -> String {
    var components = path.components(separatedBy: slash)
    components.removeLast()
    components.append(name)
    return components.joined(separator: slash)
  }

  public func move(path: String, to newParent: String) -> String {
    let name = fileName(from: path)
    var components = newParent.components(separatedBy: slash)
    components.append(name)
    return components.joined(separator: slash)
  }

  public func parentPath(from path: String) -> String {
    var components = path.components(separatedBy: slash)
    components.removeLast()
    return components.count > 1
    ? components.joined(separator: slash)
    : Defaults.Path.root
  }
}
