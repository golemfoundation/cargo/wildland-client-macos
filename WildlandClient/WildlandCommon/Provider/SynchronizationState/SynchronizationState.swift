//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public enum SynchronizationState: Equatable {
  case syncing(SyncProgress)
  case synced

  public struct SyncProgress: Equatable {
    public var files: [String]
    public var currentSize: Measurement<UnitInformationStorage>
    public var totalSize: Measurement<UnitInformationStorage>
    public var estimate: Measurement<UnitDuration>

    public init(files: [String],
                currentSize: Measurement<UnitInformationStorage>,
                totalSize: Measurement<UnitInformationStorage>,
                estimate: Measurement<UnitDuration>) {
      self.files = files
      self.currentSize = currentSize
      self.totalSize = totalSize
      self.estimate = estimate
    }
  }
}
