//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os

public extension NSXPCListenerDelegate {

  func verifySignature(for connection: NSXPCConnection, log: Logger) -> Bool {
#if DEBUG
    log.warning("Skip verifing XPC connection (allowed for Debug).")
    return true
#else
    do {
      guard let tokenValue = connection.value(forKey: "auditToken") as? NSValue,
            let auditToken = tokenValue.value(of: audit_token_t.self)
      else {
        throw InvalidAuditTokenError()
      }

      try verifyClientConnectionSignature(token: auditToken, developmentTeam: Defaults.codesignTeamID)
      return true
    } catch {
      let errorString = "\(error)"
      log.error("Failed to verify XPC connection. Error: \(errorString, privacy: .public)")
#if DEBUG
      return true
#else
      return false
#endif
    }
#endif
  }

  private func verifyClientConnectionSignature(token: audit_token_t, developmentTeam: String) throws {
    let attributes: [CFString: Any] = [
      kSecGuestAttributeAudit: Data(pod: token)
    ]

    var code: SecCode!
    let status = SecCodeCopyGuestWithAttributes(nil, attributes as CFDictionary, [], &code)
    if let error = NSError("SecCodeCopyGuestWithAttributes", status: status, error: nil) {
      throw error
    }

    let requirement = "anchor apple generic and certificate leaf[subject.OU] = \"\(developmentTeam)\""
    try checkCodeValidity(code, requirement: requirement)
  }

  private func checkCodeValidity(_ code: SecCode, requirement requirementString: String) throws {
    var requirement: SecRequirement!
    var cfError: Unmanaged<CFError>?
    let requirementStatus = SecRequirementCreateWithStringAndErrors(
      requirementString as CFString, [], &cfError, &requirement
    )
    if let error = NSError(
      "SecRequirementCreateWithStringAndErrors", status: requirementStatus, error: cfError?.takeRetainedValue()
    ) {
      throw error
    }

    let flags = kSecCSStrictValidate | SecCSFlags.enforceRevocationChecks.rawValue
    let validateStatus = SecCodeCheckValidityWithErrors(code, SecCSFlags(rawValue: flags), requirement, &cfError)
    if let error = NSError(
      "SecCodeCheckValidityWithErrors", status: validateStatus, error: cfError?.takeRetainedValue()
    ) {
      throw error
    }
  }
}

private struct InvalidAuditTokenError: Error {}

private extension NSError {
  convenience init?(_ name: String, status: OSStatus, error: Error?) {
    guard status != noErr else { return nil }

    var userInfo: [String: Any] = [:]
    error.flatMap { userInfo[NSUnderlyingErrorKey] = $0 }
    self.init(
      domain: NSOSStatusErrorDomain,
      code: Int(status),
      userInfo: [NSDebugDescriptionErrorKey: "SecCodeCheckValidityWithErrors fail"]
    )
  }
}
