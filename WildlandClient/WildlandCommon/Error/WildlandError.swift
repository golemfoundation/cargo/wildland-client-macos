//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import wildlandx
import FileProvider

private let isRustExceptionTypeKey = "isRustExceptionType"

// MARK: - Protocols

protocol FileProviderExtensionErrorAdaptee {
  var fileProviderExtensionError: NSError { get }
}

protocol RustErrorAdaptable {
  associatedtype RustExceptionBaseType: RustExceptionBase
  var rustException: RustExceptionBaseType { get }
  var underlyingError: NSError { get }
  var userInfo: [String: Any] { get }
  var code: Int { get }
}

/**
 `WildlandError`: An Error for the Cargo app and its daemon

 This tool, `WildlandError`, was made mainly for the Wildland project. It helps handle errors that occur when the Rust programming language talks to the Swift programming language, especially in the FileProviderExtension part.

 Why did we make it this way?

 1. **Many Ways to Start (Initializers) - But Why?**:
    You might wonder why `WildlandError` has many starting points (or initializers). Here's why:

    - **One Main Starting Point**: We have one main way to start this tool. You can give it any error, and it will understand. This makes it easy for anyone using it.

    - **Hidden Starting Points**: We also have other hidden ways to start the tool:
        - One understands specific Rust errors related to files.
        - Another understands general Rust errors.
        - And another one understands other common errors not from Rust.

    These hidden starting points help `WildlandError` understand many types of errors without making it hard for the user.

 2. **Making Sure FileProviderExtension Understands**:
    We need to make sure that the FileProviderExtension knows what the error is. So, we change the error into types it knows: `NSFileProviderErrorDomain` or `NSCocoaErrorDomain`.

 3. **Using Rust Errors**:
    Errors coming from Rust parts follow a pattern called `RustExceptionBase`. Some are general, and some are about FileProvider tasks. `WildlandError` changes them into a single type of error but keeps their important information.
*/
public class WildlandError: NSError {

  // MARK: - Properties

  public lazy var type: WildlandErrorCategory = {
    isRustExceptionType
    ? WildlandErrorCategory(ErrorCategory(UInt32(code)))
    : .internalError
  }()

  public override static var supportsSecureCoding: Bool { true }

  // MARK: - Initialization

  public convenience init<T: Error>(_ error: T) {
    // 1. It's rust error, but customized to return error specific for its class.
    if let rustException = error as? RustExceptionBase & FileProviderExtensionErrorAdaptee {
      self.init(rustException)
    // 2. It's rust error and returnis its category related error.
    } else if let rustException = error as? RustExceptionBase {
      self.init(rustException)
    // 3. All of rest.
    } else {
      self.init(error as NSError)
    }
  }

  public required init?(coder: NSCoder) {
    super.init(coder: coder)
  }

  // MARK: - Private

  private init<T>(_ rustException: T) where T: RustExceptionBase & FileProviderExtensionErrorAdaptee {
    let rustExceptionAdapter = FileProviderRustExceptionAdapter(rustException: rustException)
    super.init(
      domain: fileProviderDomainID.rawValue,
      code: rustExceptionAdapter.code,
      userInfo: rustExceptionAdapter.userInfo
    )
  }

  private init<T>(_ rustException: T) where T: RustExceptionBase {
    let rustExceptionAdapter = RustExceptionAdapter(rustException: rustException)
    super.init(
      domain: fileProviderDomainID.rawValue,
      code: rustExceptionAdapter.code,
      userInfo: rustExceptionAdapter.userInfo
    )
  }

  private init(_ nsError: NSError) {
      super.init(domain: nsError.domain, code: nsError.code, userInfo: nsError.userInfo)
  }

  private var isRustExceptionType: Bool {
    (userInfo[isRustExceptionTypeKey] as? Bool) ?? false
  }

  public var underlyingError: NSError {
    (userInfo[NSUnderlyingErrorKey] as? NSError) ?? self
  }
}

extension WildlandError {

  // MARK: - Properties

  var isUserNotAuthenticated: Bool {
    let notAuthenticated = underlyingError.code == NSFileProviderError.notAuthenticated.rawValue
    return type == .logical && notAuthenticated
  }

  public var isInsufficientQuota: Bool {
    underlyingError.code == NSFileProviderError.insufficientQuota.rawValue
  }

  public var isCancelledByUser: Bool {
    underlyingError.domain == NSCocoaErrorDomain && underlyingError.code == NSUserCancelledError
  }
}

private class RustExceptionAdapter<T: RustExceptionBase>: RustErrorAdaptable {

  // MARK: - Properties

  let rustException: T

  var code: Int {
    Int(rustException.category().rawValue)
  }

  var userInfo: [String: Any] {
    [
      NSLocalizedDescriptionKey: rustException.reason().toString(),
      NSUnderlyingErrorKey: underlyingError.verifyDomain,
      isRustExceptionTypeKey: true
    ]
  }

  var underlyingError: NSError {
    rustException.category().fileProviderExtensionError
  }

  // MARK: - Initialization

  init(rustException: T) {
    self.rustException = rustException
  }
}

private final class FileProviderRustExceptionAdapter<T: RustExceptionBase>: RustExceptionAdapter<T>
  where T: FileProviderExtensionErrorAdaptee {

  // MARK: - Properties

  override var userInfo: [String: Any] {
    var userInfo = super.userInfo
    userInfo[NSUnderlyingErrorKey] = rustException.fileProviderExtensionError
    return userInfo
  }
}

private extension NSError {

  // MARK: - Properties

  var verifyDomain: Self {
    let isRelevantDomain = [NSFileProviderErrorDomain, NSCocoaErrorDomain].contains(domain)
    if !ProcessInfo.isRunningForUnitTests {
      assert(isRelevantDomain, "Please make domain relevant for file provider extension.")
    }
    return self
  }
}
