//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

// swiftlint:disable line_length
// swiftlint:disable type_name

import Foundation

public enum WLStrings {
  public enum button {
    public static let cancel = "Cancel"
    public static let next = "Next"
    public static let back = "Back"
    public static let generate = "Generate"
    public static let quickTour = "Quick tour"
    public static let yes = "Yes"
    public static let no = "No"
    public static let ok = "OK"
    public static let getCode = "Get code"
    public static let copyLink = "Copy link"
    public static let copying = "Copying"
    public static let gotIt = "Ok, got it"
    public static let retry = "Retry"
    public static let remove = "Yes, remove"
  }

  public enum fileprovider {
    public static let userNotExists = "User doesn't exist."
    public static let cancelledByUser = "Operation cancelled by user."
    public static let noSuchPath = "The path does not exist in storage."
    public static let defaultError = "Can't complete the operation."
  }

  public enum ColorTheme {
    public static let dark = "Dark theme"
    public static let light = "Light theme"
    public static let automatic = "Follow OS"
  }

  public enum onboarding {
    public enum setupCargo {
      public static let title = "Set up Wildland Cargo"
      public static let description = "To get started, you can create a brand new Wildland identity or add another device to a previously created identity."

      public enum itemCreateNew {
        public static let title = "Create a new Wildland identity"
        public static let description = "Choose this option if you are new to Cargo"
      }

      public enum itemLinkExisting {
        public static let title = "Link device to an existing identity"
        public static let description = "Choose this option to install Cargo on another device"
      }
    }

    public enum aboutWildlandIdentities {
      public static let title = "About Wildland identities"
      public static let description = """
      You can create a Wildland identity
      and securely store a secret phrase
      to manage your identity.\n
      Alternatively, if you're familiar with
      Ethereum, you can use your wallet
      to create and manage your identity.
      """
    }

    public enum createIdentity {
      public static let title = "Create a Wildland identity"
      public static let description = "Generate and securely store a 12 word secret phrase to secure your Wildland Cargo identity."

      public enum itemWildland {
        public static let title = "Use Wildland for identity"
        public static let description = "Generate and store a secret phrase "
      }

      public enum itemEthereum {
        public static let title = "Use Ethereum for identity"
        public static let description = "Derive an identity from a public key"
      }
    }

    public enum chooseIdentity {
      public static let title = "Manage your Wildland identity"
      public static let description = "You can manage Wildland Cargo with a Wildland secret phrase or use your Ethereum wallet for identity."

      public enum itemWildland {
        public static let title = "Use Wildland identity"
        public static let description = "Create a new identity or link this device"
      }

      public enum itemEthereum {
        public static let title = "Use Ethereum identity"
        public static let description = "Create a new identity or link this device"
      }
    }

    public enum wildlandSecretPhaseIntro {
      public static let title = "Keep your secret phrase safe"
      public static let newUserDescription = "On the next screen Cargo will generate and display your 12 word secret phrase."
      public static let existingUserDescription = "On the next screen your 12 word secret phrase may be visible on screen."
      public static let safeText = """
      Please do not display your
      secret phrase somewhere
      others can see your screen
      """
      public static let identityGenerationFailed = "Failed generating a new identity"
    }

    public enum newWildlandWords {
      public static let title = "Secure your 12 word passphrase"
      public static let description = "The secret phrase below is the master key to your Wildland Cargo.\nThe Wildland team cannot recover it, so write it down and keep it safe."
      public static let boldedDescription = "cannot recover it,"
      public static let buttonGenerate = "Confirm words"
      public static let buttonDownload = "Download as text file"
      public static let checkbox = "I have written down or otherwise securely stored my secret phrase"
    }

    public enum comfirmWildlandWords {
      public static let title = "Confirm your 12 word secret phrase"
      public static let description = "Please confirm that you have stored your passphrase securely by entering the words in order below. What was the %@ word in your passphrase?"
      public static let boldedDescription = "%@ word"
      public static let buttonForgot = "I forgot my words"
      public static let mnemonicFailedEmptyWords = "Not all the required words were filled in"
    }

    public enum enterWildlandWords {
      public static let title = "Enter your 12 word secret phrase"
      public static let description = """
      To link this device to your Wildland identity, please enter your
      secret phrase. Paste from the clipboard or enter each word in order.
      """
      public static let infoVerified = "Secret phrase verified"
      public static let infoVerificationFailed = "Secret phrase incorrect. Please try again"
      public static let buttonVerify = "Verify phrase"
      public static let buttonGetStarted = "Get started"
      public static let mnemonicFailedEmptyWords = "Not all the required words were filled in"
      public static let mnemonicFailedDuplicatedWords = "Some of the words are duplicated"
      public static let fileContentProcessingFailed = "Failed to use pasted/dropped content"
      public static let fileContentEmptyData = "File content is empty"
    }

    public enum setYourOwnStorage {
      public static let title = "Set up your own storage"
      public static let descriptionBottomTop = "If you want to use a specific storage\nsolution like S3, IPFS, or Dropbox,\nyou can do so with via the Terminal."
      public static let descriptionBottomStart = "See"
      public static let descriptionBottomLink = "this page"
      public static let descriptionBottomEnd = "in the Wildland docs."
    }

    public enum nameYourDevice {
      public static let title = "Name your device"
      public static let description = "You can give your device a memorable name for use in Cargo or just use the device’s current name."
      public static let errorDeviceNameEmpty = "Device name is empty"
      public static let errorStoringWordFile = "Couldn't save the file to your machine"
      public static let textFieldPlaceholder = "Device name"
      public static let loadingCreateUser = "Creating a user"
      public static let loadingGeneratedWords = "Generating the file"
      public static let successCreateUser = "User was created"
      public static let errorCreatingUser = "Not able to create a user. Please try again"
    }

    public enum chooseCargoStorage {
      public static let title = "Choose your Cargo storage"
      public static let description = "You can either use a free storage tier sponsored by the Golem Foundation or set up your own storage backend."
      public static let agreeCheckboxTitle = "I agree to the free tier"
      public static let agreeCheckboxLink = "terms & conditions"

      public enum itemFreeFoundationTier {
        public static let title = "Free Foundation tier"
        public static let description = "500GB, requires email verification"
      }

      public enum itemSelfSetup {
        public static let title = "Self set up"
        public static let description = "Requires Terminal familiarity"
      }
    }

    public enum getVerificationCode {
      public static let title = "Get a verification code"
      public static let description = "Please enter your email address so we can verify you are human in order to give you the free Cargo storage tier."
      public static let tip = "We won’t use your email for anything except for sending you a code to verify you are not a bot."
      public static let invalidEmail = "This doesn’t look like a valid email address"
      public static let requestingStorage = "Requesting a storage"
      public static let textFieldPlaceholder = "human@example.xyz"
    }

    public enum enterVerificationCode {
      public static let title = "Enter your verification code"
      public static let description = "Please enter the 6 character verification code. If you didn’t get a code, please check your spam folder before requesting a new code."
      public static let infoVerified = "Code verified"
      public static let infoResend = "Resend request was sent"
      public static let infoVerificationFailed = "Code incorrect. Please try again"
      public static let resendCodeText = "Didn’t get an email?"
      public static let resendCodeLink = "Resend verification code"
      public static let infoVerification = "Verifying the code"
      public static let mountingContainers = "Mounting containers"
      public static let openFinder = "Open in Finder"
    }

    public enum alertDomainMountFailed {
      public static let title = "Failed to mount Cargo"
      public static let description = "Please restart the app"
    }

    public enum generalError {
      public static let networkError = "Please go online to continue"
    }
  }

  public enum appUninstaller {
    public enum appUninstallRequestedAlert {
      public static let title = "Do you really want to delete Cargo?"
      public static let message = "This will shut down all Cargo services, but your files will not be affected."
    }

    public enum appUninstallCompletedAlert {
      public static let title = "Application was uninstalled and helper will be closed"
      public static let message = "Would you like to cleanup leftovers?"
    }
  }

  public enum appUpdater {
    public enum appUpdateRequestedNotification {
      public static let title = "New version available"
      public static let message = "A new version of Cargo is available. Please click install to download and install the update."
      public static let messageApprove = "Install"
      public static let messageDecline = "Dismiss"
    }
  }

  public enum appLaunch {
    public enum appInvalidLocationAlert {
      public static let title = "Invalid Location"
      public static let message = "Cargo should be launched only from '/Applications' folder"
    }
  }

  public enum quotaModal {
    public static let title = "Not enough storage space"
    public static let message = """
    Uploading %@ file would exceed your available
    Cargo storage. Please clear some space & try again.
    """
  }

  public enum aboutPanel {
    public enum about {
      public static let copyrightKey = "Copyright"
      public static let copyrightValue = "© 2023 Golem Foundation"
      public static let commitKey = "Commit"
      public static let buildTimeKey = "Build time"
      public static let sdkVersionKey = "SDK version"
      public static let sdkCommitKey = "SDK commit"
    }
  }

  public enum AppCargoMenu {
    public static let about = "About"
    public static let preferences = "Preferences"
    public static let quit = "Quit"
  }

  public enum appQuit {
    public enum domainUnmount {
      public static let title = "Cargo needs to be running in order to sync these files"
    }
  }

  public enum appHelperMenu {
    public enum Item {
      public static let about = "About"
      public static let preferences = "Preferences"
      public static let quit = "Quit Cargo"
      public static let updateHelper = "Check updates"
      public static let startingHelper = "wildland is starting..."
      public static let view = "View"
      public static let window = "Window"
      public static let mountDomain = "Mount domain"
      public static let unmountDomain = "Unmount domain"
    }
  }

  public enum Preferences {
    public enum Tab {
      public static let general = "General"
      public static let storage = "Storage"
    }

    public enum General {
      public static let publicKeyValue = "Public key"
      public static let onStart = "Start Cargo on login"
      public static let uiTheme = "Cargo UI theme"
      public static let filesFolder = "Cargo folder location"
      public static let openFinder = "Open in Finder"
      public static let copy = "Copy Public key"
      public static let copied = "Copied!"
      public static let displayName = "Display name"
      public static let displayNamePlaceholder = "Add a display name (optional)"
      public static let useDisplayNameForSharing = "Use display name for sharing"
      public static let shouldDisplayNameForSharingTooltip =
      """
        If enabled, this option shows your
        display name to people you share files
        with, rather than just your public key
      """
    }
  }

  public enum ContainerProperties {
    public static let oneFile = ", 1 file"
    public static let filesCount = ", %@ files"
    public static let owner = "Owner"
    public static let share = "Share"
    public static let storage = "Storage"

    public enum Share {
      public static let publicKeyPlaceholder = "Enter a Cargo public key"
      public enum Item {
        public static let remove = "Remove"
        public static let aliasPlaceholder = "Enter recipient's alias"
      }

      public enum Notification {
        public static let title = "Share link copied"
        public static let message = "A shareable link to your file has been copied to the clipboard."
      }

      public enum Remove {
        public static let message = "Do you really want to remove %@?"
      }
    }
  }

  public enum Alert {
    public static let errorGenericTitle = "Oops!"
    public static let errorGenericMessage = "Something went wrong."

    public static let connectivityTitle = "Unable to connect"
    public static let connectivityMessage = "We encountered an error while trying to connect, please check your data connection and try again in a few minutes"

    public static let retryButtonTitle = "Retry"
  }

  public enum ActivityView {
    public static let headerTitle = "Activity"
    public static let synchronizationStatusTitleSyncing = "Syncing"
    public static let synchronizationStatusTitleSynced = "Cargo is synced"
    public static let multipleFiles = "multiple files"
    public static let tooltipPublicKeyCopy = "Copy Public key"
    public static let tooltipPublicKeyCopied = "Copied!"
    public static let storageAlmostFullTitle = "Your storage is almost full"
    public static let storageAlmostFullDescription = "You're using **%@%%** of your Cargo storage.\nYou might need to add more storage soon"
    public static let updateErrorTitle = "Oops! Update Unsuccessful"

    public enum NewVersionAlert {
      public static let title = "New version available"
      public static let subtitle = "A new version of Cargo is available. Please click below to download the update."
      public static let buttonTitle = "Download & update"
      public static let downloading = "Downloading %@ of %@"
      public static let extracting = "Extracting %@%%"
      public static let waitForOperationsToComplete = "App will restart after completing ongoing operations."
      public static let latestVersionTitle = "You're up to date!"
      public static let latestVersionMessage = "Your app is at the latest version. Enjoy the latest features and stay tuned for future updates!"
    }
  }

  public enum Share {
    public enum Receive {
      public static let headerTitle = " shared a file with you"
      public static let buttonTitle = "Add to Cargo"
      public static let addingToCargo = "Adding to Cargo..."
      public static let addedToCargo = "Added to Cargo"
    }
  }

  public enum Storages {
    public static let typeAndSpace = "%@, %@ of %@ in use (%@%%)"
  }

  #if DEBUG
  public enum Previews {
    public static let formattedMetrics = "48MB (17 secs)"
  }
  #endif
}

// swiftlint:enable line_length
// swiftlint:enable type_name
