//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public class FileWatcherEvent {
  var id: FSEventStreamEventId
  public var path: String
  var flags: FSEventStreamEventFlags
  init(_ eventId: FSEventStreamEventId, _ eventPath: String, _ eventFlags: FSEventStreamEventFlags) {
    self.id = eventId
    self.path = eventPath
    self.flags = eventFlags
  }
}

public extension FileWatcherEvent {
  var fileChange: Bool { (flags & FSEventStreamEventFlags(kFSEventStreamEventFlagItemIsFile)) != 0 }
  var dirChange: Bool { (flags & FSEventStreamEventFlags(kFSEventStreamEventFlagItemIsDir)) != 0 }

  var created: Bool { (flags & FSEventStreamEventFlags(kFSEventStreamEventFlagItemCreated)) != 0 }
  var removed: Bool { (flags & FSEventStreamEventFlags(kFSEventStreamEventFlagItemRemoved)) != 0 }
  var renamed: Bool { (flags & FSEventStreamEventFlags(kFSEventStreamEventFlagItemRenamed)) != 0 }
  var modified: Bool { (flags & FSEventStreamEventFlags(kFSEventStreamEventFlagItemModified)) != 0 }
}

public extension FileWatcherEvent {
  var fileCreated: Bool { fileChange && created }
  var fileRemoved: Bool { fileChange && removed }
  var fileRenamed: Bool { fileChange && renamed }
  var fileModified: Bool { fileChange && modified }

  var dirCreated: Bool { dirChange && created }
  var dirRemoved: Bool { dirChange && removed }
  var dirRenamed: Bool { dirChange && renamed }
  var dirModified: Bool { dirChange && modified }
}

public extension FileWatcherEvent {
  var description: String {
    var result = "The \(fileChange ? "file":"directory") \(self.path) was"
    if self.created {
      result += " created"
    }
    if self.removed {
      result += " removed"
    }
    if self.renamed {
      result += " renamed"
    }
    if self.modified {
      result += " modified"
    }
    return result
  }
}
