//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

public typealias FileWatcherCallback = (_ fileWatcherEvent: FileWatcherEvent) -> Void

public protocol FileWatcherType {
  var callback: FileWatcherCallback? { get set }
  func start(paths: [String], queue: DispatchQueue)
}

public final class FileWatcher: FileWatcherType {

  // MARK: - Properties

  public var callback: FileWatcherCallback?
  private var streamRef: FSEventStreamRef?
  private var hasStarted: Bool { streamRef != nil }

  // MARK: - Initialization

  public init() { }

  // MARK: - Public

  /// Start file listening
  public func start(paths: [String], queue: DispatchQueue) {
    guard !hasStarted else { return }

    var context = FSEventStreamContext(
      version: 0,
      info: Unmanaged.passUnretained(self).toOpaque(),
      retain: self.retainCallback, release: self.releaseCallback,
      copyDescription: nil
    )
    streamRef = FSEventStreamCreate(
      kCFAllocatorDefault, self.eventCallback, &context,
      paths as CFArray,
      FSEventStreamEventId(kFSEventStreamEventIdSinceNow), 0,
      UInt32(kFSEventStreamCreateFlagUseCFTypes | kFSEventStreamCreateFlagFileEvents)
    )

    guard let streamRef else { return }
    FSEventStreamSetDispatchQueue(streamRef, queue)
    FSEventStreamStart(streamRef)
  }

  // MARK: - Private

  private let eventCallback: FSEventStreamCallback = { (_, callbackInfo, numEvents, eventPaths, eventFlags, eventIds) in
    let fileSystemWatcher = Unmanaged<FileWatcher>.fromOpaque(callbackInfo!).takeUnretainedValue()
    let paths = Unmanaged<CFArray>.fromOpaque(eventPaths).takeUnretainedValue() as! [String]

    for index in 0..<numEvents {
      fileSystemWatcher.callback?(FileWatcherEvent(eventIds[index], paths[index], eventFlags[index]))
    }
  }

  private let retainCallback: CFAllocatorRetainCallBack = {(info: UnsafeRawPointer?) in
    _ = Unmanaged<FileWatcher>.fromOpaque(info!).retain()
    return info
  }

  private let releaseCallback: CFAllocatorReleaseCallBack = {(info: UnsafeRawPointer?) in
    Unmanaged<FileWatcher>.fromOpaque(info!).release()
  }
}
