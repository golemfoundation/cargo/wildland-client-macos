//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine

public protocol StoragesProviderType {
  var thresholdExceededPublisher: AnyPublisher<Int?, Never> { get }
  func getStorages(path: String?) throws -> [Storage]
  func warningDismissed()
}

public extension StoragesProviderType {
  func getStorages(path: String? = nil) throws -> [Storage] {
    try getStorages(path: path)
  }
}

public final class StoragesProvider: StoragesProviderType {

  private enum WarningThresholdState {
    case canShow
    case dismissed
  }

  // MARK: - Properties

  public let warningThreshold: Int = 85
  public var thresholdExceededPublisher: AnyPublisher<Int?, Never> {
    thresholdExceededSubject.eraseToAnyPublisher()
  }
  private let thresholdExceededSubject = PassthroughSubject<Int?, Never>()
  private let userApi: StoragesAccessProvider
  private var warningState: WarningThresholdState = .canShow

  // MARK: - Initialization

  public init(userApi: StoragesAccessProvider) {
    self.userApi = userApi
  }

  // MARK: - Public

  public func getStorages(path: String?) throws -> [Storage] {
    let storages = try userApi.getStorages(path: path)
    sendThreshold(storages: storages)
    return storages
  }

  public func warningDismissed() {
    warningState = .dismissed
    thresholdExceededSubject.send(nil)
  }

  // MARK: - Private

  private func sendThreshold(storages: [Storage]) {
    let percentUsage = getPercentUsageIfNeeded(storages: storages)
    thresholdExceededSubject.send(percentUsage)
  }

  private func getPercentUsageIfNeeded(storages: [Storage]) -> Int? {
    guard let storage = storages.first(where: { $0.percentUsage >= warningThreshold }) else {
      warningState = .canShow
      return nil
    }
    guard warningState == .canShow else { return nil }
    return storage.percentUsage
  }
}
