//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import wildlandx

public struct Storage: Equatable {
  public let identifier: String
  public let type: String
  public let name: String?
  public let usedSpace: UInt64
  public let totalSpace: UInt64
}

public extension Storage {
  var percentUsage: Int {
    guard totalSpace != .zero else { return .zero }
    return Int(Double(usedSpace) / Double(totalSpace) * 100.0)
  }
}

extension Storage {
  init(_ containerStorage: ContainerStorage) throws {
    self.init(
      identifier: containerStorage.uuid().toString().toString(),
      type: try containerStorage.backendType().toString(),
      name: try containerStorage.name().swiftOptional()?.toString(),
      usedSpace: try containerStorage.getSpaceUsage().getUsedSpace(),
      totalSpace: try containerStorage.getSpaceUsage().getTotalSpace()
    )
  }
}
