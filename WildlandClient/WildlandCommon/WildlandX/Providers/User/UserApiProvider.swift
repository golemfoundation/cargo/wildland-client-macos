//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import wildlandx

public protocol UserAvailability {
  var availabilityState: UserAvailabilityState { get throws }
}

public protocol UserApiProviderType: UserAvailability {
  var user: wildlandx.CargoUser { get throws }

  func createMnemonic(words: [RustString]) throws -> MnemonicPayload
  func generateMnemonic() throws -> MnemonicPayload
  func createUser(from mnemonic: MnemonicPayload, deviceName: String) throws -> wildlandx.CargoUser
}

public protocol UserPublicKeyProviderType {
  var userPublicKey: String { get throws }
  var publicKeyShort: String { get throws }
}

public protocol StoragesAccessProvider {
  var isStorageGranted: Bool { get throws }
  func getStorages(path: String?) throws -> [Storage]
}

public protocol ShareOwnerProvider {
  func share(path: String, publicKey: String) async throws -> String
  func getAllRecipients(for identifier: String) async throws -> [String]
}

public struct UserApiProvider {

  // MARK: - Properties

  private let userApi: UserApi

  // MARK: - Initialization

  public init(api: UserApi = WildlandX.cargo.userApi()) {
    userApi = api
  }
}

extension UserApiProvider: UserApiProviderType {

  // MARK: - Properties

  public var user: wildlandx.CargoUser {
    get throws {
      try userApi.getUser()
    }
  }

  public var availabilityState: UserAvailabilityState {
    get throws {
      do {
        return try user.availabilityState
      } catch {
        guard WildlandError(error).isUserNotAuthenticated else { throw error }
        return .notCreatedYet
      }
    }
  }

  // MARK: - Public

  public func createMnemonic(words: [RustString]) throws -> MnemonicPayload {
    try userApi.createMnemonicFromVec(RustVec(words))
  }

  public func generateMnemonic() throws -> MnemonicPayload {
    try userApi.generateMnemonic()
  }

  public func createUser(from mnemonic: MnemonicPayload, deviceName: String) throws -> wildlandx.CargoUser {
    try userApi.createUserFromMnemonic(mnemonic, RustString(deviceName))
  }
}

extension UserApiProvider: UserPublicKeyProviderType {

  // MARK: - Properties

  public var userPublicKey: String {
    get throws {
      try user.getUserPublicKey().toHex
    }
  }

  public var publicKeyShort: String {
    get throws {
      let publicKey = try userPublicKey
      return publicKey.prefix(4) + "..." + publicKey.suffix(5)
    }
  }
}

extension UserApiProvider: StoragesAccessProvider {

  // MARK: - Properties

  public var isStorageGranted: Bool {
    get throws {
      try user.storageGranted
    }
  }

  public func getStorages(path: String?) throws -> [Storage] {
    try user.findContainers(RustOptional(ContainerFilter.createEmptyOptional()), MountState_MountedOrUnmounted)
      .filter { container in
        guard let path else { return true }
        let containerPath = try container.getPath().toString()
        return path.contains(containerPath)
      }
      .reduce(into: [Storage](), { result, container in
        let storages = try container.getStorages()
          .map(Storage.init)
        result.append(contentsOf: storages)
      })
  }
}

extension UserApiProvider: ShareOwnerProvider {

  public func share(path: String, publicKey: String) async throws -> String {
    try await Task.sleep(for: .seconds(2.0))
    // TODO: - Use share API when available (CARGO-410).
//    user.share(RustString(path), PubKey(publicKey))
    return "sharedMessage"
  }

  public func getAllRecipients(for identifier: String) async throws -> [String] {
    return [
      "0f3a5270bc76147f1b287573a0380a07556c9549216b7357999b317a73189035",
      "5449619a4b95dfa023d32bf4746b414fd45d71d67e801146f3fa9bcfd75ec10c",
      "6449619a4b95dfa023d32bf4746b414fd45d71d67e801146f3fa9bcfd75ec10c",
      "7449619a4b95dfa023d32bf4746b414fd45d71d67e801146f3fa9bcfd75ec10c",
      "8449619a4b95dfa023d32bf4746b414fd45d71d67e801146f3fa9bcfd75ec10c"
    ]
    // TODO: - Use share API when available (CARGO-410).
//    try user.getPubkeysHavingAccessToObject(RustString(identifier))
  }
}
