//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import wildlandx

public protocol ShareReceiverProviderType {
  func prepareSharePreviewMessage(sharingMessage: SharingMessage) async throws -> PreviewShareMessage
  func mountShareContainer(sharingMessage: SharingMessage, path: String) async throws
}

public struct ShareReceiverProvider: ShareReceiverProviderType {

  // MARK: - Properties

  private let userApi: UserApiProviderType

  // MARK: - Initialization

  public init(userApi: UserApiProviderType) {
    self.userApi = userApi
  }

  // MARK: - Public

  public func prepareSharePreviewMessage(sharingMessage: SharingMessage) async throws -> PreviewShareMessage {
    #warning("Replaced by preview_sharing_message in CARGO-405")
    try await Task.sleep(for: .seconds(2))
    return PreviewShareMessage(
      fileName: "Sponsor Booth details.zip",
      size: 1677123,
      publicKey: "0xc5a...82kcb"
    )
  }

  public func mountShareContainer(sharingMessage: SharingMessage, path: String) async throws {
    #warning("Replaced by add_shared_container in CARGO-405")
    try await Task.sleep(for: .seconds(2))
  }
}
