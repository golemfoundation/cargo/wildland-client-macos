//
//  DummyCfgProvider.swift
//  WildlandCommon
//
//  Created by Wildland on 07.11.2022.
//

import Foundation
import wildlandx

final class DummyCfgProvider: CargoCfgProvider {

  func getLogLevel() -> RustString {
    RustString("trace")
  }

  func getUseLogger() -> bool {
    true
  }

  func getLogUseAnsi() -> bool {
    true
  }

  func getLogFilePath() -> RustOptional<RustString> {
    Optional.none.toRustOptional()
  }

  func getLogFileEnabled() -> bool {
    true
  }

  func getFoundationCloudEnvMode() -> FoundationCloudMode {
    FoundationCloudMode_Dev
  }

  func getLogFileRotateDirectory() -> RustOptional<RustString> {
    Optional.none.toRustOptional()
  }

  func getOslogCategory() -> RustOptional<RustString> {
    Optional.some(RustString("WildLand")).toRustOptional()
  }

  func getOslogSubsystem() -> RustOptional<RustString> {
    Optional.some(RustString(Defaults.Log.subsystem)).toRustOptional()
  }

  func getRedisUrlForMultideviceState() -> RustString {
    RustString("rediss://default:DHt07bBPk0IHlqINjl9NeeB8GCxTdE@staging.cargo.wildland.dev")
  }

  func getRedisUrlForCatlib() -> RustString {
    RustString("rediss://default:DHt07bBPk0IHlqINjl9NeeB8GCxTdE@staging.cargo.wildland.dev")
  }
}
