//
// Wildland Project
// SecureKeyStorage.swift
// WildlandCommon
//
// Copyright © 2022 Golem Foundation
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import wildlandx
import os

public protocol SecureKeyStorageProtocol {

  init(service: String, accessGroup: String)
  func insert(key: String, value: String) throws
  func get(key: String) throws -> String?
  func contains(key: String) throws -> Bool
  func allKeys() -> [String]
  func remove(key: String) throws
  func removeAll() throws
}

public class SecureKeyStorage<T: SecureKeyStorageProtocol>: LocalSecureStorage {
  public typealias KeychainType = T
  private let _log = Logger.wildlandLogger()
  private let storage: T

  public init(storage: T) {
    self.storage = storage
  }

  // MARK: - LocalSecureStorage protocol methods

  public func insert(_ key: RustString, _ value: RustString) -> OptionalRustStringResultWithLssError {
    do {
      try storage.insert(key: key.toString(), value: value.toString())

      return OptionalRustStringResultWithLssError.from_ok(Optional.some(value).toRustOptional())
    } catch {

      _log.error("Failed inserting a key from the secure storage, \(error.localizedDescription, privacy: .public)")
      return OptionalRustStringResultWithLssError.from_ok(Optional.none.toRustOptional())
    }
  }

  public func get(_ key: RustString) -> OptionalRustStringResultWithLssError {
    do {
      guard let value = try storage.get(key: key.toString()) else {
        return OptionalRustStringResultWithLssError.from_ok(Optional.none.toRustOptional())
      }

      return OptionalRustStringResultWithLssError.from_ok(Optional.some(RustString(value)).toRustOptional())
    } catch {
      _log.error("Failed getting a key from the secure storage, \(error.localizedDescription, privacy: .public)")
      return OptionalRustStringResultWithLssError.from_ok(Optional.none.toRustOptional())
    }
  }

  public func containsKey(_ key: RustString) -> boolResultWithLssError {
    do {
      return boolResultWithLssError.from_ok(try storage.get(key: key.toString()) != nil)
    } catch {
      _log.error("Key was not found, \(error.localizedDescription, privacy: .public)")
      return boolResultWithLssError.from_ok(false)
    }
  }

  public func keysStartingWith(_ prefix: RustString) -> VecRustStringResultWithLssError {
    let keys = RustVec<RustString>(RustString.createNewRustVec())
    let keyPrefix = prefix.toString()
    for key in storage.allKeys() {
      if key.starts(with: keyPrefix) {
        keys.push(RustString(key))
      }
    }
    return VecRustStringResultWithLssError.from_ok(keys)
  }

  public func keys() -> VecRustStringResultWithLssError {
    let vector = RustVec<RustString>(RustString.createNewRustVec())
    for key in storage.allKeys() {
      vector.push(RustString(key))
    }
    return VecRustStringResultWithLssError.from_ok(vector)
  }

  public func remove(_ key: RustString) -> OptionalRustStringResultWithLssError {
    do {
      try storage.remove(key: key.toString())
      return OptionalRustStringResultWithLssError.from_ok(Optional.none.toRustOptional())
    } catch {
      _log.error("Failed removing a key from the secure storage, \(error.localizedDescription, privacy: .public)")
      return OptionalRustStringResultWithLssError.from_ok(Optional.some(RustString(error.localizedDescription))
        .toRustOptional())
    }
  }

  public func len() -> usizeResultWithLssError {
    return usizeResultWithLssError.from_ok((usize)(storage.allKeys().count))
  }

  public func isEmpty() -> boolResultWithLssError {
    return boolResultWithLssError.from_ok(storage.allKeys().isEmpty)
  }

  // MARK: - Public

  public func clearAllKeys() {
    do {
      try storage.removeAll()
    } catch {
      _log.error("Failed removing all keys from the secure storage")
    }
  }
}

extension SecureKeyStorage where T: SecureKeyStorageProtocol {
  public var allKeys: [String] {
    storage.allKeys()
  }
}
