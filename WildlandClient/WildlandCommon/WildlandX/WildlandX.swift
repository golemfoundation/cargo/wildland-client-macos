//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import wildlandx
import os
import KeychainAccess

final public class SecureKeychainAccessKeyStorage: SecureKeyStorage<Keychain> {}

final public class WildlandX {

  static private let shared = WildlandX()

  private let storage: SecureKeychainAccessKeyStorage = {
    let storage = SecureKeychainAccessKeyStorage.KeychainType(
      service: Defaults.BundleID.mainApp,
      accessGroup: Defaults.applicationGroupID
    )
      .synchronizable(false)
      .accessibility(.afterFirstUnlockThisDeviceOnly)
    return SecureKeychainAccessKeyStorage(storage: storage)
  }()

  private let cargo: wildlandx.SharedMutexCargoLib

  class public var cargo: wildlandx.SharedMutexCargoLib {
    shared.cargo
  }

  class public var storage: SecureKeyStorage<Keychain> {
    shared.storage
  }

  init() {
    do {
      let provider = DummyCfgProvider()
      let config = try collectConfig(provider)
      config.overrideEvsUrl(RustString("https://staging.cargo.wildland.dev/"))
      cargo = try createCargoLib(storage, config)
    } catch {
      fatalError("Failed initializing of the core")
    }
  }
}

extension Keychain: SecureKeyStorageProtocol {

  public func remove(key: String) throws {
    try remove(key)
  }

  public func insert(key: String, value: String) throws {
    try set(value, key: key)
  }

  public func get(key: String) throws -> String? {
    try get(key)
  }

  public func contains(key: String) throws -> Bool {
    try get(key) != nil
  }
}
