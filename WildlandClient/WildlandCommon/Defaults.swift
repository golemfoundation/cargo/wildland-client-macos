//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import FileProvider

public enum Defaults {
  public static let codesignTeamID = "S3J4DS9F7R"

  public enum BundleID {
    public static let control = "\(Defaults.codesignTeamID).io.wildland.wlclientd"
    public static let mainApp = "io.wildland.Cargo"
  }

  public enum XPCEndpoints {
    public static let control = BundleID.control
  }

  public enum Path {
    public static let mainApp = URL(fileURLWithPath: "/Applications/Cargo.app")
    public static let root = "/"
    public static let infoPlist = "Contents/Info.plist"
    public static let shared = "Shared"
  }

  public enum Log {
    public enum Category {
      public static let cargo = "Cargo"
      public static let cargoUI = "CargoUI"
      public static let cargoCommon = "CargoCommon"
      public static let daemon = "wlclientd"
      public static let fileProvider = "FileProvider"
      public static let controlService = "CtrlService"
      public static let fileProviderService = "FileProviderService"
      public static let xpcService = "XPC_Server"
      public static let updater = "Updater"
      public static let notifications = "Notifications"
    }
    public static let subsystem = "io.wildland.client"
  }

  public static var applicationGroupID: String {
    #if os(iOS)
      return "group.io.wildland.client"
    #else
      return "\(Defaults.codesignTeamID).group.io.wildland.client"
    #endif
  }

  public enum Identifier {
    public static let root = NSFileProviderItemIdentifier.rootContainer.rawValue
  }

  public enum App {
    public static let cargo = "Cargo.app"
    public static let daemon = "wlclientd.app"
  }

  public enum Scheme {
    public static let client = "wildlandclient://"
  }

  public enum DeeplinkArgument {
    public static let relaunchDaemon = "wlclientd.relaunchDaemon"
  }

  public enum AppURL {
    public static let daemon = "cargo-daemon"
  }
}
