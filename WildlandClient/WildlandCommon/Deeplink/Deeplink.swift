//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public enum DeeplinkType: String {
  case share

  enum ShareQuery: String {
    case body
    case userAlias
  }
}

public enum Deeplink: Equatable {
  case share(SharingMessage)
}

public extension Deeplink {
  var sharingMessage: SharingMessage? {
    guard case .share(let sharingMessage) = self else { return nil }
    return sharingMessage
  }
}

public extension Deeplink {
  init?(urlComponents: URLComponents) {
    guard let host = urlComponents.host,
          let deeplinkType = DeeplinkType(rawValue: host) else { return nil }
    switch deeplinkType {
    case .share:
      guard let body = urlComponents.value(for: DeeplinkType.ShareQuery.body.rawValue) else { return nil }
      let userAlias = urlComponents.value(for: DeeplinkType.ShareQuery.userAlias.rawValue)
      let sharingMessage = SharingMessage(body: body, userAlias: userAlias)
      self = .share(sharingMessage)
    }
  }

  var url: URL {
    var urlComponents = URLComponents()
    urlComponents.scheme = Defaults.AppURL.daemon
    switch self {
    case .share(let sharingMessage):
      createUrlComponents(sharingMessage: sharingMessage, urlComponents: &urlComponents)
    }
    return urlComponents.url ?? URL(filePath: Defaults.AppURL.daemon)
  }

  // MARK: - Private

  private func createUrlComponents(sharingMessage: SharingMessage, urlComponents: inout URLComponents) {
    let body = URLQueryItem(name: DeeplinkType.ShareQuery.body.rawValue, value: sharingMessage.body)
    let userAlias = URLQueryItem(name: DeeplinkType.ShareQuery.userAlias.rawValue, value: sharingMessage.userAlias)
    urlComponents.host = DeeplinkType.share.rawValue
    urlComponents.queryItems = [body]
    guard sharingMessage.userAlias != nil else { return }
    urlComponents.queryItems?.append(userAlias)
  }
}

private extension URLComponents {
  func value(for string: String) -> String? {
    queryItems?.first(where: { $0.name == string })?.value
  }
}
