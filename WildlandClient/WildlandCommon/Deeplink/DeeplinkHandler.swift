//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Combine

public protocol DeeplinkHandlerType {
  var publisher: AnyPublisher<Deeplink, Never> { get }
  func receiveUrl(_ urls: [URL])
}

public final class DeeplinkHandler: DeeplinkHandlerType {

  // MARK: - Properties

  public lazy private(set) var publisher = subject.eraseToAnyPublisher()
  private let subject = PassthroughSubject<Deeplink, Never>()

  // MARK: - Initialization

  public init() { }

  // MARK: - Public

  public func receiveUrl(_ urls: [URL]) {
    urls
      .compactMap(prepareDeeplink)
      .forEach(subject.send)
  }

  // MARK: - Private

  private func prepareDeeplink(from url: URL) -> Deeplink? {
    guard url.scheme == Defaults.AppURL.daemon,
          let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else { return nil }
    return Deeplink(urlComponents: urlComponents)
  }
}
