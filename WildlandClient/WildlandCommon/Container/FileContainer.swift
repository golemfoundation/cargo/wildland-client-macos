//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

@objc(WildlandFileContainer)
public class FileContainer: NSObject, NSSecureCoding {

  public static let supportsSecureCoding = true

  public let name: String

  private enum Keys: String {
    case name
  }

  public init(name: String) {
    self.name = name
  }

  // MARK: - Private

  public required init?(coder: NSCoder) {
    guard let name = coder.decodeObject(of: NSString.self, forKey: Keys.name.rawValue) as String? else {
      coder.failWithError(CocoaError.error(.coderValueNotFound))
      return nil
    }

    self.name = name
  }

  public func encode(with coder: NSCoder) {
    coder.encode(name, forKey: Keys.name.rawValue)
  }
}
