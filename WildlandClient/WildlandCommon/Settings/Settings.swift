//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI

public protocol SettingsType {
  var startCargoOnLogin: Bool? { get }
  var displayName: String? { get }
  var shouldDisplayNameForSharing: Bool? { get }
  var appearanceColorScheme: Settings.ColorThemeOption { get }
  func updateColorScheme(with option: Settings.ColorThemeOption)
  func updateStartCargoOnLogin(_ enable: Bool)
  func updateDisplayName(_ value: String)
  func updateDisplayNameForSharing(_ enable: Bool)
}

public final class Settings: SettingsType {

  public enum ColorThemeOption: String, CaseIterable {
    case dark
    case light
    case automatic
  }

  // MARK: - AppStorage

  @AppStorage(AppStorageIdentifiers.isDarkMode.rawValue, store: .sharedDefaults)
  private var isDarkMode: Bool?

  @AppStorage(AppStorageIdentifiers.startCargoOnLogin.rawValue, store: .sharedDefaults)
  public private(set) var startCargoOnLogin: Bool?

  @AppStorage(AppStorageIdentifiers.displayName.rawValue, store: .sharedDefaults)
  public private(set) var displayName: String?

  @AppStorage(AppStorageIdentifiers.shouldDisplayNameForSharing.rawValue, store: .sharedDefaults)
  public private(set) var shouldDisplayNameForSharing: Bool?

  // MARK: - Properties

  public var appearanceColorScheme: ColorThemeOption {
    guard let isDarkMode = isDarkMode else {
      return .automatic
    }
    return isDarkMode ? .dark : .light
  }

  public let objectWillChange = PassthroughSubject<Void, Never>()
  private let _cancellable: Cancellable

  // MARK: - Initialization

  public init() {
    _cancellable = NotificationCenter.default
      .publisher(for: UserDefaults.didChangeNotification)
      .map { _ in () }
      .subscribe(objectWillChange)
  }

  // MARK: - Public

  public func updateColorScheme(with option: ColorThemeOption) {
    switch option {
    case .dark:
      isDarkMode = true
    case .light:
      isDarkMode = false
    case .automatic:
      isDarkMode = nil
    }
  }

  public func updateStartCargoOnLogin(_ enable: Bool) {
    startCargoOnLogin = enable
  }

  public func updateDisplayName(_ value: String) {
    displayName = value
  }

  public func updateDisplayNameForSharing(_ enable: Bool) {
    shouldDisplayNameForSharing = enable
  }
}
