//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

/// SynchronizedProperty is an actor that provides thread-safe access to a value of type `Value`.
/// It ensures that concurrent accesses to the value are properly synchronized, preventing data races.
public actor SynchronizedProperty<Value> {

  private var value: Value?

  public init() {}

  public func get() -> Value? { value }

  public func set(_ newValue: Value?) { value = newValue }

}
