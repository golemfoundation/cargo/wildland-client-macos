import os
///  Measures the time elapsed for a given process.
///
///  The function logs the start and end of the time measurement, along with the duration of the elapsed time.
///  A unique identifier is generated at the start of the measurement to identify the specific process being timed.
///
/// - Parameters:
///     - name: A string representing the name of the method being timed.
///     - function: closure that performs the process being timed
///
///   Usage:
///   ```
///   measureTimeElapsed {
///     // code to be timed
///   }
///   ```
@discardableResult
public func measureTimeElapsed<T>(log: Logger, method name: String = #function, function: () async throws -> T) async rethrows -> T {
  let identifier = UUID()
  log.logStart(method: name, identifier: identifier)

  let start = CFAbsoluteTimeGetCurrent()
  let result: T

  do {
    result = try await function()
  } catch {
    let duration = measureTimeEnd(start: start)
    log.logError(method: name, duration: duration, error: error, identifier: identifier)
    throw error
  }
  let duration = measureTimeEnd(start: start)
  log.logEnd(method: name, duration: duration, identifier: identifier)

  return result
}

@discardableResult
public func measureTimeElapsed<T>(log: Logger, method name: String = #function, function: () throws -> T) rethrows -> T {
  let identifier = UUID()
  log.logStart(method: name, identifier: identifier)

  let start = CFAbsoluteTimeGetCurrent()
  let result: T

  do {
    result = try function()
  } catch {
    let duration = measureTimeEnd(start: start)
    log.logError(method: name, duration: duration, error: error, identifier: identifier)
    throw error
  }
  let duration = measureTimeEnd(start: start)
  log.logEnd(method: name, duration: duration, identifier: identifier)

  return result
}

private func measureTimeEnd(start: CFAbsoluteTime) -> CFAbsoluteTime {
  let end = CFAbsoluteTimeGetCurrent()
  return end - start
}

private extension Logger {

  func logStart(method name: String, identifier: UUID) {
    log("\u{23F1} measure time elapsed - start, method: \(name, privacy: .public) (id: \(identifier, privacy: .public))")
  }

  func logEnd(method name: String, duration: CFAbsoluteTime, identifier: UUID) {
    log("\u{23F1} measure time elapsed - end, method: \(name, privacy: .public), duration: \(duration, privacy: .public) seconds (id: \(identifier, privacy: .public))")
  }

  func logError(method name: String, duration: CFAbsoluteTime, error: Error, identifier: UUID) {
    log("\u{23F1} \u{2757} measure time elapsed - end with error: \(error, privacy: .public), method: \(name, privacy: .public), duration: \(duration, privacy: .public) seconds (id: \(identifier, privacy: .public))")
  }
}
