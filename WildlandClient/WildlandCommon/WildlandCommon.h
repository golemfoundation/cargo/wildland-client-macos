//
//  WildlandCommon.h
//  WildlandCommon
//
//  Created by Alkenso (Vladimir Vashurkin) on 07.12.2021.
//

#import <Foundation/Foundation.h>

//! Project version number for WildlandCommon.
FOUNDATION_EXPORT double WildlandCommonVersionNumber;

//! Project version string for WildlandCommon.
FOUNDATION_EXPORT const unsigned char WildlandCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WildlandCommon/PublicHeader.h>
