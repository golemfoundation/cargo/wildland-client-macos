//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import FileProvider.NSFileProviderItem

@objc public final class CreateItemInfo: NSObject, Codable, NSSecureCoding {

  // MARK: - Properties

  public let temporaryIdentifier: String
  public let parentIdentifier: String
  public let name: String
  public let type: String
  public let strategy: String
  public let creationDate: Date
  public static var supportsSecureCoding: Bool = true

  // MARK: - Keys

  private enum Keys: String {
    case temporaryIdentifier
    case parentIdentifier
    case name
    case type
    case strategy
    case creationDate
  }

  // MARK: - Initialization

  public init(temporaryIdentifier: String,
              parentIdentifier: String,
              name: String,
              type: String,
              strategy: String,
              creationDate: Date) {
    self.temporaryIdentifier = temporaryIdentifier
    self.parentIdentifier = parentIdentifier
    self.name = name
    self.type = type
    self.strategy = strategy
    self.creationDate = creationDate
  }

  // MARK: - NSSecureCoding

  public required init?(coder: NSCoder) {
    guard let identifier = coder.decodeObject(of: NSString.self, forKey: Keys.temporaryIdentifier.rawValue) as String?,
          let parent = coder.decodeObject(of: NSString.self, forKey: Keys.parentIdentifier.rawValue) as String?,
          let name = coder.decodeObject(of: NSString.self, forKey: Keys.name.rawValue) as String?,
          let type = coder.decodeObject(of: NSString.self, forKey: Keys.type.rawValue) as String?,
          let strategy = coder.decodeObject(of: NSString.self, forKey: Keys.strategy.rawValue) as String?,
          let creationDate = coder.decodeObject(of: NSDate.self, forKey: Keys.creationDate.rawValue) as? Date else {
      coder.failWithError(CocoaError.error(.coderValueNotFound))
      return nil
    }
    self.temporaryIdentifier = identifier
    self.parentIdentifier = parent
    self.name = name
    self.type = type
    self.strategy = strategy
    self.creationDate = creationDate
  }

  public func encode(with coder: NSCoder) {
    coder.encode(temporaryIdentifier, forKey: Keys.temporaryIdentifier.rawValue)
    coder.encode(parentIdentifier, forKey: Keys.parentIdentifier.rawValue)
    coder.encode(name, forKey: Keys.name.rawValue)
    coder.encode(type, forKey: Keys.type.rawValue)
    coder.encode(strategy, forKey: Keys.strategy.rawValue)
    coder.encode(creationDate as NSDate, forKey: Keys.creationDate.rawValue)
  }
}

public extension CreateItemInfo {
  convenience init(providerItem: NSFileProviderItem,
                   options: NSFileProviderCreateItemOptions,
                   type: FileProviderDomainService.Entry.EntryType) {
    let strategy: FileItem.ItemConflictStrategy = options.contains(.mayAlreadyExist)
    ? .updateAlreadyExisting
    : .failOnExisting
    self.init(
      temporaryIdentifier: providerItem.itemIdentifier.rawValue,
      parentIdentifier: providerItem.parentItemIdentifier.rawValue,
      name: providerItem.filename,
      type: type.rawValue,
      strategy: strategy.rawValue,
      creationDate: providerItem.creationDate?.flatMap { $0 } ?? Date()
    )
  }
}
