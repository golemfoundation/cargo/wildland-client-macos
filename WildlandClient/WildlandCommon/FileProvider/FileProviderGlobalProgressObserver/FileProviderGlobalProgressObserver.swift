//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import Combine
import os

public protocol FileProviderGlobalProgressObserverType {
  var inProgressPublisher: AnyPublisher<Bool, Never> { get }
}

public final class FileProviderGlobalProgressObserver: FileProviderGlobalProgressObserverType {

  // MARK: - Properties

  public var inProgressPublisher: AnyPublisher<Bool, Never> {
    Publishers.CombineLatest(
      downloadingProgress.publisher(for: \.isFinished),
      uploadingProgress.publisher(for: \.isFinished)
    )
    .map { !$0 || !$1 }
    .removeDuplicates()
    .eraseToAnyPublisher()
  }

  @objc private let downloadingProgress: Progress
  @objc private let uploadingProgress: Progress

  // MARK: - Initialization

  public init(fileProviderManager: GlobalProgressFileProviderManager) {
    downloadingProgress = fileProviderManager.globalProgress(for: .downloading)
    uploadingProgress = fileProviderManager.globalProgress(for: .uploading)
  }
}
