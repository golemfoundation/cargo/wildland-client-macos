//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

@objc(WildlandFileProviderItemMetadata)
public class FileMetadata: NSObject, NSSecureCoding {
  public static let supportsSecureCoding = true

  private enum Keys: String {
    case fileSystemFlags
    case lastUsedDate
    case creationDate
    case modificationDate
    case validEntries
    case isOwner
    case recipients
  }

  public let fileSystemFlags: FileProviderDomainService.EntryMetadata.FileSystemFlags?
  public let lastUsedDate: Date?
  public let creationDate: Date?
  public let modificationDate: Date?
  public let validEntries: Int
  public let isOwner: Bool
  public let recipients: [String]

  public func encode(with coder: NSCoder) {
    coder.encode(lastUsedDate as Date?, forKey: Keys.lastUsedDate.rawValue)
    coder.encode(creationDate as Date?, forKey: Keys.creationDate.rawValue)
    coder.encode(modificationDate as Date?, forKey: Keys.modificationDate.rawValue)
    coder.encode(validEntries as Int, forKey: Keys.validEntries.rawValue)
    if let flags = fileSystemFlags {
      coder.encode(flags.rawValue as UInt, forKey: Keys.fileSystemFlags.rawValue)
    }
    coder.encode(isOwner, forKey: Keys.isOwner.rawValue)
    coder.encode(recipients, forKey: Keys.recipients.rawValue)
  }

  public init(_ entry: FileProviderDomainService.EntryMetadata, userPublicKey: String) {
    creationDate = entry.creationDate
    lastUsedDate = entry.lastUsedDate
    modificationDate = entry.modificationDate
    validEntries = entry.validEntries.rawValue
    fileSystemFlags = entry.fileSystemFlags
    isOwner = userPublicKey == entry.owner
    recipients = entry.recipients ??  []
  }

  public required init?(coder: NSCoder) {
    lastUsedDate = coder.decodeObject(of: NSDate.self, forKey: Keys.lastUsedDate.rawValue) as Date?
    creationDate = coder.decodeObject(of: NSDate.self, forKey: Keys.creationDate.rawValue) as Date?
    modificationDate = coder.decodeObject(of: NSDate.self, forKey: Keys.modificationDate.rawValue) as Date?

    if let flags = coder.decodeObject(of: NSNumber.self, forKey: Keys.fileSystemFlags.rawValue) as NSNumber? {
      fileSystemFlags = .init(rawValue: flags.uintValue)
    } else {
      fileSystemFlags = nil
    }

    let entries = coder.decodeObject(of: NSNumber.self, forKey: Keys.validEntries.rawValue)
    validEntries = entries?.intValue ?? 0
    isOwner = coder.decodeBool(forKey: Keys.isOwner.rawValue)
    recipients = coder.decodeObject(of: [NSString.self], forKey: Keys.recipients.rawValue) as? [String] ?? []
  }
}
