//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

@objc(WildlandFileProviderItemUpdates)
public class FileItemUpdates: NSObject, NSSecureCoding {

  private enum Keys: String {
    case anchor
    case addedOrUpdatedItems
    case deletedIdentifiers
  }

  public static let supportsSecureCoding = true

  public let anchor: Data
  public let addedOrUpdatedItems: [FileItem]
  public let deletedIdentifiers: [String]

  public init(anchor: Data, updateItems: [FileItem], deletedIdentifiers: [String]) {
    self.anchor = anchor
    self.addedOrUpdatedItems = updateItems
    self.deletedIdentifiers = deletedIdentifiers
  }

  public required init?(coder: NSCoder) {
    let anchorData = coder.decodeObject(
      of: NSData.self,
      forKey: Keys.anchor.rawValue
    ) as Data?
    let updateFileItems = coder.decodeArrayOfObjects(
      ofClass: FileItem.self,
      forKey: Keys.addedOrUpdatedItems.rawValue
    ) as [FileItem]?
    let deletedIdentifiers = coder.decodeArrayOfObjects(
      ofClass: NSString.self, forKey: Keys.deletedIdentifiers.rawValue
    ) as [String]?

    guard let anchorData, let updateFileItems, let deletedIdentifiers else {
      coder.failWithError(CocoaError.error(.coderValueNotFound))
      return nil
    }

    self.anchor = anchorData
    self.addedOrUpdatedItems = updateFileItems
    self.deletedIdentifiers = deletedIdentifiers
  }

  public func encode(with coder: NSCoder) {
    coder.encode(anchor, forKey: Keys.anchor.rawValue)
    coder.encode(addedOrUpdatedItems, forKey: Keys.addedOrUpdatedItems.rawValue)
    coder.encode(deletedIdentifiers, forKey: Keys.deletedIdentifiers.rawValue)
  }
}
