//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

import CoreServices
import UniformTypeIdentifiers
import os

@objc(WildlandFileProviderItem)
public class FileItem: NSObject, NSSecureCoding {

  public enum ItemConflictResult: String {
    case reject
    case bounce
    case merge
  }

  // TODO: The flow for the conflict strategy must be reviewed and improved if needed.
  // Task for this improvement is CARMA-136
  public enum ItemConflictStrategy: String, Codable {
    case failOnExisting
    case updateAlreadyExisting
  }

  public let identifier: String
  public let parentIdentifier: String
  public let name: String
  public let type: String
  public let size: UInt64
  public let contentVersion: UInt64
  public let metadataVersion: UInt64
  public let metadata: FileMetadata

  public var isFolder: Bool {
    type == FileProviderDomainService.Entry.EntryType.folder.rawValue
  }

  public enum SignalSourceType {
    case item
    case parent
  }

  public enum ChangeType {
    case create
    case modifyMetadata
    case modifyContent
  }

  private enum Keys: String {
    case id
    case name
    case type
    case size
    case contentVersion
    case metadataVersion
    case itemIdentifier
    case parentIdentifier
    case metadata
  }

  public override var description: String {
    identifier
  }

  public static let supportsSecureCoding = true

  public init(_ entry: FileProviderDomainService.Entry, userPublicKey: String) {
    name = entry.name
    type = entry.type.rawValue
    size = entry.size
    contentVersion = entry.revision.content
    metadataVersion = entry.revision.metadata
    metadata = FileMetadata(entry.metadata, userPublicKey: userPublicKey)
    parentIdentifier = entry.parentIdentifier
    identifier = entry.identifier
  }

  public required init?(coder: NSCoder) {
    guard let itemName = coder.decodeObject(of: NSString.self, forKey: Keys.name.rawValue) as String?,
          let itemType = coder.decodeObject(of: NSString.self, forKey: Keys.type.rawValue) as String?,
          let itemSize = coder.decodeObject(of: NSNumber.self, forKey: Keys.size.rawValue),
          let itemContentVersion = coder.decodeObject(of: NSNumber.self, forKey: Keys.contentVersion.rawValue),
          let itemMetadataVersion = coder.decodeObject(of: NSNumber.self, forKey: Keys.metadataVersion.rawValue),
          let itemIdentifier = coder.decodeObject(of: NSString.self,
                                                          forKey: Keys.itemIdentifier.rawValue) as String?,
          let itemParentIdentifier = coder.decodeObject(of: NSString.self,
                                                        forKey: Keys.parentIdentifier.rawValue) as String?,
          let itemMetadata = coder.decodeObject(of: FileMetadata.self, forKey: Keys.metadata.rawValue)
    else {
      coder.failWithError(CocoaError.error(.coderValueNotFound))
      return nil
    }

    name = itemName
    type = itemType
    size = itemSize.uint64Value
    contentVersion = itemContentVersion.uint64Value
    metadataVersion = itemMetadataVersion.uint64Value
    metadata = itemMetadata
    identifier = itemIdentifier
    parentIdentifier = itemParentIdentifier
  }

  // MARK: - NSCoding methods

  public func encode(with coder: NSCoder) {
    coder.encode(identifier, forKey: Keys.id.rawValue)
    coder.encode(name, forKey: Keys.name.rawValue)
    coder.encode(type, forKey: Keys.type.rawValue)
    coder.encode(size as UInt64, forKey: Keys.size.rawValue)
    coder.encode(contentVersion as UInt64, forKey: Keys.contentVersion.rawValue)
    coder.encode(metadataVersion as UInt64, forKey: Keys.metadataVersion.rawValue)
    coder.encode(metadata as FileMetadata, forKey: Keys.metadata.rawValue)
    coder.encode(identifier, forKey: Keys.itemIdentifier.rawValue)
    coder.encode(parentIdentifier, forKey: Keys.parentIdentifier.rawValue)
  }
}
