//
// Wildland Project
// FileProviderError.swift
// WildlandCommon
//
// Copyright © 2021 Golem Foundation,
//              Ivan Sinitsa <ivan@wildland.io>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import FileProvider

public enum FileProviderError: Error, LocalizedError {

  internal enum CodingKeys: String, CodingKey {
    case value
    case entry
    case identifier
    case errorDomain
    case errorCode
    case errorLocalizedDescription
  }

  case internalError
  case unsupportedRequest
  case invalidDaemonConnection
  case parameterError
  case itemDecodingFailed
  case itemNotFound(FileProviderDomainService.ItemIdentifier)
  case wrongRevision(FileProviderDomainService.Entry)
  case itemExists(FileProviderDomainService.Entry)
  case itemCreateFailed
  case itemDeleteFailed
  case itemMetadataModifyFailed
  case folderListFailed
  case downloadFailed
  case infoFetchFailed
  case itemContentUpdateFailed
  case unknownItemType

  public var errorDescription: String? {
    return "\(self)"
  }

  public func toError() -> NSError {
    switch self {
    case .itemExists(let entry):
      return NSError.fileProviderErrorForCollision(with: FileItem(entry))
    case .itemNotFound(let identifier):
      return NSError.fileProviderErrorForNonExistentItem(withIdentifier: NSFileProviderItemIdentifier(identifier))
    default:
      let error = self as NSError
      switch (error.domain, error.code) {
      case (NSURLErrorDomain, NSURLErrorCancelled):
        return NSError(domain: NSCocoaErrorDomain, code: NSUserCancelledError, userInfo: nil)
      case (NSURLErrorDomain, _):
        return NSError(domain: NSFileProviderErrorDomain, code: NSFileProviderError.serverUnreachable.rawValue,
                       userInfo: nil)
      case (NSCocoaErrorDomain, NSUserCancelledError):
        return error
      default:
        return NSError(domain: NSCocoaErrorDomain, code: NSXPCConnectionReplyInvalid,
                       userInfo: [NSUnderlyingErrorKey: self])
      }
    }
  }
}
