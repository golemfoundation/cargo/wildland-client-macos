//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public enum FileProviderDomainService {

  public static let fileProviderDomainDisplayName = "FileProvider"

  public struct Entry: Codable {

    public enum EntryType: String, Codable {
      case file
      case folder
      case root
    }

    public let name: String
    public let identifier: String
    public let parentIdentifier: String
    public let revision: Version
    public let deleted: Bool
    public let size: UInt64
    public let type: EntryType
    public let metadata: EntryMetadata

    public init(name: String,
                identifier: String,
                parentIdentifier: String,
                revision: Version,
                deleted: Bool,
                size: UInt64,
                type: EntryType,
                metadata: EntryMetadata) {
      self.name = name
      self.identifier = identifier
      self.parentIdentifier = parentIdentifier
      self.revision = revision
      self.deleted = deleted
      self.size = size
      self.type = type
      self.metadata = metadata
    }
  }

  public struct Version: Codable, Equatable {
    public let content: UInt64
    public let metadata: UInt64

    public static let zero = Version(content: 0, metadata: 0)

    public init(content: UInt64, metadata: UInt64) {
      self.content = content
      self.metadata = metadata
    }
  }

  @objc public class EntryMetadata: NSObject, Codable {

    public struct ValidEntries: OptionSet, Codable {
      public init(rawValue: Int) {
        self.rawValue = rawValue
      }

      public let rawValue: Int

      public static let fileSystemFlags = ValidEntries(rawValue: 1 << 0)
      public static let lastUsedDate = ValidEntries(rawValue: 1 << 1)
      public static let creationDate = ValidEntries(rawValue: 1 << 2)
      public static let contentModificationDate = ValidEntries(rawValue: 1 << 3)
    }

    public struct FileSystemFlags: OptionSet, Codable {
      public init(rawValue: UInt) {
        self.rawValue = rawValue
      }

      public let rawValue: UInt

      public static let userExecutable = FileSystemFlags(rawValue: 1 << 0)
      public static let userWritable = FileSystemFlags(rawValue: 1 << 1)
      public static let userReadable = FileSystemFlags(rawValue: 1 << 2)
      public static let pathExtensionHidden = FileSystemFlags(rawValue: 1 << 3)
    }

    public let fileSystemFlags: FileSystemFlags?
    public let lastUsedDate: Date?
    public let creationDate: Date?
    public let modificationDate: Date?
    public let validEntries: ValidEntries
    public let owner: String?
    public let recipients: [String]?

    public init(fileSystemFlags: FileSystemFlags?,
                lastUsedDate: Date?,
                creationDate: Date?,
                contentModificationDate: Date?,
                validEntries: ValidEntries?,
                owner: String?,
                recipients: [String]?
    ) {
      self.validEntries = validEntries ?? [.fileSystemFlags, .lastUsedDate, .creationDate, .contentModificationDate]
      self.fileSystemFlags = fileSystemFlags
      self.lastUsedDate = lastUsedDate
      self.creationDate = creationDate
      self.modificationDate = contentModificationDate
      self.owner = owner
      self.recipients = recipients
    }
  }
}
