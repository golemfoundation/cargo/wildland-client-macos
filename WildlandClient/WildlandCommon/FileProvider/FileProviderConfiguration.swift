//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import FileProvider

public let bundleID = "\(Defaults.codesignTeamID).io.wildland.wlclientd.WildlandFileProvider"
public let fileProviderDomainID = NSFileProviderDomainIdentifier(rawValue: bundleID)
public let fileProviderDomain: NSFileProviderDomain = {
  let domain = NSFileProviderDomain(identifier: fileProviderDomainID,
                                    displayName: FileProviderDomainService.fileProviderDomainDisplayName)
  if #available(macOSApplicationExtension 13.0, *) {
    domain.supportsSyncingTrash = false
  }
  return domain
}()
