//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider

public protocol TemporaryUrlProvider {
  func temporaryDirectoryURL() throws -> URL
  func getUserVisibleURL(for itemIdentifier: NSFileProviderItemIdentifier) async throws -> URL
}

public extension TemporaryUrlProvider {

  var rootUrl: URL {
    get async throws {
      try await getUserVisibleURL(for: .rootContainer).appending(path: Defaults.Path.shared)
    }
  }

  func url(for identifier: String, bundleId: String? = nil) throws -> URL {
    var temporaryURL = try temporaryDirectoryURL()
    if let bundleId {
      temporaryURL = temporaryURL.appendingPathComponent(bundleId)
    }
    return temporaryURL.appendingPathComponent(identifier)
  }
}

extension NSFileProviderManager: TemporaryUrlProvider { }
