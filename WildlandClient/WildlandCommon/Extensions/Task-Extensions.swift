//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension Task where Failure == Error {
  @discardableResult
  public static func retrying(priority: TaskPriority? = nil,
                              maxRetryCount: Int = 3,
                              retryDelay: Duration = .milliseconds(500),
                              operation: @Sendable @escaping () async throws -> Success) -> Task {
    Task(priority: priority) {
      for _ in 0..<maxRetryCount - 1 {
        do {
          return try await operation()
        } catch {
          try await Task<Never, Never>.sleep(for: retryDelay)
        }
      }
      try Task<Never, Never>.checkCancellation()
      return try await operation()
    }
  }
}
