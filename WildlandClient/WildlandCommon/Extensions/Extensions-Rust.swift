//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
import Foundation
import wildlandx

public extension RustVec where T == UInt8 {
  var toHex: String {
    map { String(format: "%02x", $0) }.joined()
  }
}

extension MnemonicPayload {

  enum MnemonicError: Error {
    case emptyWord
  }

  public var words: [String] {
    get throws {
      let mnemonicValue = self.getVec()
      var words: [String] = []
      for i in 0..<mnemonicValue.size() {
        guard let word = mnemonicValue.at(i) else {
          throw MnemonicError.emptyWord
        }
        let value = word.toString()
        guard !value.isEmpty else {
          throw MnemonicError.emptyWord
        }
        words.append(value)
      }
      return words
    }
  }
}

public extension UnixTimestamp {
  static func create(from date: Date?) -> RustOptional<UnixTimestamp> {
    guard let date else {
      return RustOptional<UnixTimestamp>(UnixTimestamp.createEmptyOptional())
    }
    let nanoseconds = UInt64(date.timeIntervalSince1970.nanoseconds)
    return Optional.some(unixTimestampFromNanos(nanoseconds)).toRustOptional()
  }
}
