//
// Wildland Project
// Wildland WildlandCommon
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine

public extension Publisher {

  func mapToVoid() -> Publishers.Map<Self, Void> {
    map { _ in }
  }

  func setValue<T, Root: AnyObject>(_ value: T, keyPath: ReferenceWritableKeyPath<Root, T>, on object: Root) -> Publishers.HandleEvents<Self> {
    handleEvents(receiveOutput: { [weak object] _ in
      object?[keyPath: keyPath] = value
    })
  }

  func setOutput<Root: AnyObject>(keyPath: ReferenceWritableKeyPath<Root, Output>, on object: Root) -> Publishers.HandleEvents<Self> {
    handleEvents(receiveOutput: { [weak object] value in
      object?[keyPath: keyPath] = value
    })
  }
}

public extension Publisher where Failure == Never {

  func weakAssign<Root: AnyObject>(keyPath: ReferenceWritableKeyPath<Root, Output>, on object: Root) -> AnyCancellable {
    sink { [weak object] value in
      object?[keyPath: keyPath] = value
    }
  }

  func taskSink(_ receiveValue: @escaping (Output) async -> Void) -> AnyCancellable {
    sink { value in
      Task {
        await receiveValue(value)
      }
    }
  }
}
