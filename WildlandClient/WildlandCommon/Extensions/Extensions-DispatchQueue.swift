//
//  Extensions-DispatchQueue.swift
//  WildlandCommon
//
//  Created by Wildland on 25.01.2023.
//

import Foundation

extension DispatchQueue {
  // Delay is specified in milliseconds
  public enum DelayLength: Int {
    case twentiethOfASecond = 50
    case tenthOfASecond = 100
    case thirdOfASecond = 300
    case halfOfASecond = 500
    case second = 1000
    case twoSeconds = 2000
  }

  public func delay(with length: DelayLength, completion: @escaping () -> Void) {
    self.asyncAfter(deadline: .now() + .milliseconds(length.rawValue)) {
      completion()
    }
  }
}
