//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public extension Process {

  static func manuallyUnregisterDaemon() {
    Process.launchedProcess(
      launchPath: "/bin/launchctl",
      arguments: ["bootout", "gui/\(getuid())/S3J4DS9F7R.io.wildland.wlclientd"]
    ).waitUntilExit()
  }

  static func manuallyRemoveDomain() {
    Process.launchedProcess(
      launchPath: "/usr/bin/fileproviderctl",
      arguments: ["domain", "remove", "wlclientd"]
    ).waitUntilExit()
  }
}

public extension ProcessInfo {
  static var isRunningForUnitTests: Bool {
    processInfo.environment["XCTestConfigurationFilePath"] != nil
  }
}
