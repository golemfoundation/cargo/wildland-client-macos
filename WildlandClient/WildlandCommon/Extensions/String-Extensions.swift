//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension String {

  public var firstUppercased: String { prefix(1).uppercased() + dropFirst() }

  /// Filters out all the symbols and numbers to return only characters string
  public func letters() -> String {
    String(unicodeScalars.filter(CharacterSet.letters.contains))
  }
}

extension String {
  func debugIfNeeded(details: String) -> String {
    #if DEBUG
    appending(" (debug: \(details)")
    #else
    self
    #endif
  }

  public func isNewerThan(_ version: String) -> Bool {
    compare(version, options: .numeric) == .orderedDescending
  }
}

extension String {
  public func appendPathItemName(_ name: String) -> String {
    last == "/" ? self + name : self + "/" + name
  }
}
