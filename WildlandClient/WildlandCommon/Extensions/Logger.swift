//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import wildlandx

extension Logger {

  public static var updater: Logger {
    Logger(subsystem: Defaults.Log.subsystem, category: Defaults.Log.Category.updater)
  }

  public static var notifications: Logger {
    Logger(subsystem: Defaults.Log.subsystem, category: Defaults.Log.Category.notifications)
  }

  static func wildlandLogger(category: String = Defaults.Log.Category.cargoCommon) -> Logger {
    return Logger(subsystem: Defaults.Log.subsystem, category: category)
  }

  public static func xpcService() -> Logger {
    Logger(subsystem: Defaults.Log.subsystem, category: Defaults.Log.Category.xpcService)
  }
}

extension Logger {

  public func fault(file name: String = #file, method: String = #function, details: String? = nil, failure: Error?) {
    logMessage(logLevel: .fault, file: name, method: method, details: details, error: failure)
  }

  public func error(file name: String = #file, method: String = #function, details: String? = nil, failure: Error?) {
    logMessage(logLevel: .error, file: name, method: method, details: details, error: failure)
  }

  public func debug(file name: String = #file, method: String = #function, details: String? = nil) {
    logMessage(logLevel: .debug, file: name, method: method, details: details)
  }

  public func info(file name: String = #file, method: String = #function, details: String? = nil) {
    logMessage(logLevel: .info, file: name, method: method, details: details)
  }

  private func logMessage(logLevel: OSLogType, file: String, method: String, details: String? = nil, error: Error? = nil) {
    let combinedMessage = createLogMessage(logLevel: logLevel, file: file, method: method,
                                           details: details, error: error)
    log(level: logLevel, "\(combinedMessage, privacy: .public)")
  }

  private func createLogMessage(logLevel: OSLogType, file: String, method: String, details: String? = nil, error: Error? = nil) -> String {
    let fileName = URL(fileURLWithPath: file).lastPathComponent
    let methodComponents = method.split(separator: "(", maxSplits: 1)
    let methodName = methodComponents.first.map { String($0) + "()" } ?? method

    return buildCombinedMessage(
      logLevel: logLevel,
      details: details,
      error: error,
      fileName: fileName,
      methodName: methodName
    )
  }

  private func buildCombinedMessage(logLevel: OSLogType, details: String?, error: Error?, fileName: String, methodName: String) -> String {
    var combinedMessage = "\(logLevel.toString())"
    details.map { combinedMessage.append(" - \($0)") }

    if let wildlandError = wildlandErrorIfAvailable(for: error) {
      var reason = wildlandError.localizedDescription
      if reason != wildlandError.underlyingError.localizedDescription {
        reason.append(", underlyingError: \(wildlandError.underlyingError.localizedDescription)")
      }
      appendErrorDetails(
        to: &combinedMessage,
        reason: reason,
        category: wildlandError.type.rawValue,
        error: wildlandError
      )
    }

    combinedMessage.append(" [\(fileName)], [method: \(methodName)]")
    return combinedMessage
  }

  private func appendErrorDetails(to message: inout String, reason: String, category: String, error: Error) {
      message.append("""

      - reason: \(reason)
      - category: \(category)
      - error: \(error)

      """)
  }

  private func wildlandErrorIfAvailable(for error: Error?) -> WildlandError? {
    switch error {
    case let error as RustExceptionBase:
      return WildlandError(error)
    case let error as WildlandError:
      return error
    case .some(let error):
      return WildlandError(error)
    case .none:
      return nil
    }
  }
}

private extension OSLogType {
  func toString() -> String {
    switch self {
    case .error:
      return "\u{2757} \u{FE0F} [ERROR]"
    case .fault:
      return "\u{2757} \u{FE0F} \u{2757} [FAULT]"
    case .info:
      return "[INFO]"
    case .debug:
      return "[DEBUG]"
    default:
      return "[DEFAULT]"
    }
  }
}
