//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit

public struct InfoPlistKeys {
  public static let applicationVersion = "CFBundleShortVersionString"
  public static let buildVersion = "CFBundleVersion"
}

extension Bundle {

  /// Bundle version. Value for Info.plist key "CFBundleVersion".
  public var appVersion: String? { value(for: InfoPlistKeys.applicationVersion) }
  public var buildVersion: String? { value(for: InfoPlistKeys.buildVersion) }

  // MARK: - Private

  /// Get value from info dictionary by key
  private func value(for key: String) -> String? {
    return object(forInfoDictionaryKey: key) as? String
  }
}
