//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import wildlandx

public extension Metadata {
  init(_ metadata: NodeStat) {
    self.init(
      uuid: metadata.wildlandObjectId().toString(),
      size: metadata.size(),
      type: metadata.nodeType().rawValue,
      creationTime: metadata.creationTime().swiftOptional()?.sec() ?? .zero,
      changeTime: metadata.changeTime().swiftOptional()?.sec() ?? .zero,
      modificationTime: metadata.modificationTime().swiftOptional()?.sec() ?? 0
    )
  }
}
