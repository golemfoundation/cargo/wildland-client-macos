//
// Wildland Project
// Wildland WildlandCommon
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import wildlandx

extension CargoUser {

  var availabilityState: UserAvailabilityState {
    get throws {
      let isFreeStorageGranted = try isFreeStorageGranted()
      let filter: RustOptional<ContainerFilter> = RustOptional(ContainerFilter.createEmptyOptional())

      let unmountedContainersExists = try findContainers(filter, MountState_Unmounted).first(where: { container in
        container.isMounted() == false
      }) != nil

      let mountedContainersExists = try findContainers(filter, MountState_Mounted).first(where: { container in
        container.isMounted() == true
      }) != nil

      if !isFreeStorageGranted {
        return .storageNotGranted
      }

      if unmountedContainersExists, mountedContainersExists {
        return .readyForUseButSomeUnmounted
      }

      if mountedContainersExists {
        return .readyForUse
      }

      return .storageGrantedButNotMounted
    }
  }

  var storageGranted: Bool {
    get throws {
      try isFreeStorageGranted()
    }
  }
}
