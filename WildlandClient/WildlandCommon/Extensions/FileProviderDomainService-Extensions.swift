//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

public extension FileProviderDomainService.Entry {
  init(identifier: String, parentIdentifier: String, name: String, metadata: Metadata) throws {
    let type = try FileProviderDomainService.Entry.EntryType(identifier: identifier, metadata: metadata)
    let itemIdentifier = identifier == Defaults.Identifier.root ? Defaults.Identifier.root : metadata.uuid

    self.init(
      name: name,
      identifier: itemIdentifier,
      parentIdentifier: parentIdentifier,
      revision: .init(
        content: UInt64(metadata.modificationTime),
        metadata: UInt64(metadata.modificationTime)
      ),
      deleted: false,
      size: type == .file ? UInt64(metadata.size) : 0,
      type: type,
      metadata: .init(
        fileSystemFlags: [.userReadable, .userWritable, .userExecutable],
        lastUsedDate: Date(timeIntervalSince1970: TimeInterval(metadata.changeTime)),
        creationDate: Date(timeIntervalSince1970: TimeInterval(metadata.creationTime)),
        contentModificationDate: Date(timeIntervalSince1970: TimeInterval(metadata.modificationTime)),
        validEntries: nil,
        // TODO: - Use share API when available (CARGO-410). Probably need to update `Metadata` to handle updated `NodeStat`.
        owner: nil,
        recipients: nil
      )
    )
  }
}

public extension FileProviderDomainService.Entry.EntryType {
  init(identifier: String, metadata: Metadata) throws {
    if identifier == Defaults.Identifier.root {
      self = .root
    } else {
      switch metadata.type {
      case .metadataFileType:
        self = .file
      case .metadataDirectoryType:
        self = .folder
      default:
        throw NSError(
          domain: NSErrorDomain(format: "WildlandFileProviderDomain") as String,
          code: NSFeatureUnsupportedError
        )
      }
    }
  }
}
