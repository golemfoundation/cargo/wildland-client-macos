//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import wildlandx
import FileProvider

extension UserRetrievalError_ForestNotFoundException: FileProviderExtensionErrorAdaptee {
  var fileProviderExtensionError: NSError {
    var error: NSError?
    if category() == ErrorCategory_Logical {
      error = NSError(
        domain: NSFileProviderErrorDomain,
        code: NSFileProviderError.Code.notAuthenticated.rawValue,
        userInfo: [
          NSLocalizedDescriptionKey: WLStrings.fileprovider.userNotExists.debugIfNeeded(
            details: reason().toString()
          )
        ]
      )
    }
    return error ?? category().fileProviderExtensionError
  }
}

extension DfsFrontendError_AbortedException: FileProviderExtensionErrorAdaptee {
  var fileProviderExtensionError: NSError {
    NSError(
      domain: NSError.cancellation.domain,
      code: NSError.cancellation.code,
      userInfo: [
        NSLocalizedDescriptionKey: WLStrings.fileprovider.cancelledByUser.debugIfNeeded(
          details: reason().toString()
        )
      ]
    )
  }

}

extension DfsFrontendError_InsufficientQuotaException: FileProviderExtensionErrorAdaptee {
  var fileProviderExtensionError: NSError {
    NSError(
      domain: NSFileProviderErrorDomain,
      code: NSFileProviderError.Code.insufficientQuota.rawValue
    )
  }
}

extension DfsFrontendError_NoSuchPathException: FileProviderExtensionErrorAdaptee {
  var fileProviderExtensionError: NSError {
    NSError(
      domain: NSFileProviderErrorDomain,
      code: NSFileProviderError.Code.noSuchItem.rawValue,
      userInfo: [
        NSLocalizedDescriptionKey: WLStrings.fileprovider.noSuchPath.debugIfNeeded(
          details: reason().toString()
        )
      ]
    )
  }
}

extension DfsFrontendError_ReadOnlyPathException: FileProviderExtensionErrorAdaptee {
  var fileProviderExtensionError: NSError {
    NSError(
      domain: NSCocoaErrorDomain,
      code: NSFileWriteNoPermissionError
    )
  }
}

extension ErrorCategory: FileProviderExtensionErrorAdaptee {
  var fileProviderExtensionError: NSError {
    switch self {
    case ErrorCategory_NetworkConnectivity:
      return NSError(
        domain: NSFileProviderErrorDomain,
        code: NSFileProviderError.Code.serverUnreachable.rawValue
      )
    case ErrorCategory_Auth:
      return NSError(
        domain: NSFileProviderErrorDomain,
        code: NSFileProviderError.Code.notAuthenticated.rawValue
      )
    default:
      return defautFileProviderExtensionCategoryError
    }
  }

  private var defautFileProviderExtensionCategoryError: NSError {
    NSError(
      domain: NSCocoaErrorDomain,
      code: NSXPCConnectionReplyInvalid,
      userInfo: [
        NSLocalizedDescriptionKey: WLStrings.fileprovider.defaultError.debugIfNeeded(
          details: "\(self)"
        )
      ]
    )
  }
}

extension WildlandErrorCategory {

  init(_ base: RustExceptionBase) {
    self.init(base.category())
  }

  init(_ category: ErrorCategory) {
    switch category {
    case ErrorCategory_General:
      self = .general
    case ErrorCategory_NetworkConnectivity:
      self = .networkConnectivity
    case ErrorCategory_Database:
      self = .database
    case ErrorCategory_Logical:
      self = .logical
    case ErrorCategory_Cryptography:
      self = .cryptography
    case ErrorCategory_Auth:
      self = .auth
    case ErrorCategory_ExternalService:
      self = .externalService
    case ErrorCategory_InvalidInputFormat:
      self = .invalidInputFormat
    case ErrorCategory_Dfs:
      self = .dfs
    default:
      self = .unknown
    }
  }
}
