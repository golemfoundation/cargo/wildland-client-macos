//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension Data {

  /// Initializes Data with binary representation of POD (Plain Old Data) value.
  public init<PODType>(pod: PODType) {
    self = Swift.withUnsafeBytes(of: pod) {
      guard let address = $0.baseAddress else { return Data() }
      return Data(bytes: address, count: MemoryLayout<PODType>.size)
    }
  }

  /// Converts data to POD (Plain Old Data) value.
  public func pod<PODType>(exactly type: PODType.Type) -> PODType? {
    guard MemoryLayout<PODType>.size == count else { return nil }
    return withUnsafeBytes { $0.load(fromByteOffset: 0, as: type) }
  }

  /// Converts data to POD (Plain Old Data) value.
  /// If count > PODType size, only 'size' bytes are taken.
  /// If count < PODType size, the data are appended with zeroes to match the size.
  public func pod<PODType>(adopting type: PODType.Type) -> PODType {
    var adopted = self
    let advanceSize = MemoryLayout<PODType>.size - adopted.count
    if advanceSize > 0 {
      adopted += Data(count: advanceSize)
    }

    return adopted.withUnsafeBytes { $0.load(fromByteOffset: 0, as: type) }
  }
}

extension Data {
  public func subdataChunk(at offset: Int, chunkSize: Int) -> Data {
    subdata(in: offset..<offset + chunkSize)
  }
}
