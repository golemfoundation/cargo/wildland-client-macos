//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

extension AsyncThrowingStream.Continuation.Termination: CustomStringConvertible {
  public var description: String {
    switch self {
    case .finished(let error) where error == nil: return "success"
    case .finished(let error) where error != nil: return "finished with error: \(error!)"
    case .cancelled: return "cancelled by user"
    default: return "finished in unknown state"
    }
  }
}

extension AsyncThrowingStream.Continuation {
  public func complete(_ value: Element) {
    yield(value)
    finish()
  }
}
