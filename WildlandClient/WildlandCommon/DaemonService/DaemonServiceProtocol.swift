//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/// Represents the status of a file item async stream (create, download)
public typealias FileItemStreamElement = FileProviderServiceStatus<FileItem>
/// Represents the status of a file item async stream (modification)
public typealias FileItemStreamElementWithStatus = FileProviderServiceStatus<(item: FileItem, status: Bool)>
/// A stream that reports creation or download progress of file and reports back `FileItem` completed. Used on daemon and file provider sides (XPC).
public typealias TransferStream = AsyncThrowingStream<FileItemStreamElement, Error>
/// A stream that reports modification progress of file and reports back `FileItem` with `status` when completed. Used (currently only)  on file provider side (XPC).
public typealias ModifyStream = AsyncThrowingStream<FileItemStreamElementWithStatus, Error>
/// Represents progress in terms of an offset (already downloaded or uploaded data) and a size.
/// The purpose of this type alias is to provide a flexible way to represent progress that can be easily consumed by different clients.
public typealias ProgressType = (offset: Int, size: Int)
/// Defines the status of a file item stream. It has two cases: `.inProgress(K)`, which represents the progress of the operation  and `.completed(T)`, which represents the completion of the operation with a value of type T (please look for types above).
public enum FileProviderServiceStatus<T> {

  case inProgress(ProgressType)

  case completed(T)

}

/// Protocol combines multiple protocols into a single type to simplify the handling of client-side communication in the context of file provider operations.
@objc(WLFileProviderClientProtocol)
public protocol FileProviderClientProtocol: FileProviderCreateProtocol,
                                            FileProviderDeleteProtocol,
                                            FileProviderDownloadProtocol,
                                            FileProviderModifyProtocol,
                                            FileProviderListProtocol {}

@objc(WLDaemonServiceProtocol)
public protocol DaemonServiceProtocol {
  func controlService(_ callback: @escaping ((ControlServiceProtocol) -> Void))
  func fileProviderService(_ callback: @escaping ((FileProviderServiceProtocol) -> Void))
  func userService(_ callback: @escaping ((UserServiceProtocol) -> Void))
  func fileProviderAction(_ callback: @escaping ((FileProviderActionProtocol) -> Void))
}

@objc(WLFileProviderServiceProtocol)
public protocol FileProviderServiceProtocol {
  // swiftlint:disable:next function_parameter_count

  /// The call to create a file item with specified parent, name, date, type, metadata
  func createItem(taskId: String, itemInfo: CreateItemInfo)

  /// The call to delete a file item with specified content and metadata versions, remove strategy
  func deleteItem(taskId: String, identifier: String, contentVersion: UInt64, metadataVersion: UInt64, recursive: Bool)

  /// The call to modify an item content with data for specified identifier
  func modifyItemContent(taskId: String, identifier: String)

  /// The call to get item list with specified fetch strategu and a cursor
  func listFolder(taskId: String, identifier: String, recursive: Bool, startingCursor: UInt64)

  /// The call to download an item content with specified content and metadata versions
  func downloadItem(taskId: String, identifier: String, contentVersion: UInt64, metadataVersion: UInt64)

  /// The call to get an item info
  func itemInfo(taskId: String, identifier: String)

  /// The call to rename a file item using it's identifier and a new name
  func renameItem(taskId: String, identifier: String, name: String)

  /// The call to move a file item from current path to a new parent
  func moveItem(taskId: String, identifier: String, parentIdentifier: String)

  /// The call to get all updated items using anchor data
  func listItemUpdates(taskId: String, anchorData: Data)

  /// The call to get current file sync anchor
  func getCurrentAnchor(taskId: String)

  /// The call to cancel operation in progress, once called, task with identifier `taskId` will be marked as cancelled
  func cancelOperation(taskId: String)

  /// The call to cancel all operation in progress, not used in the File provider extension
  func cancelAllOperations()

  /// The call to update tracked visible folder for provided `identifier`
  func updateTrackingFolder(taskId: String, identifier: String, isObserved: Bool)
}

@objc(WLUserServiceProtocol)
public protocol UserServiceProtocol {
  /// Perform the call to request a free tier storage for provided email
  func requestFreeTierStorage(with email: String, _ callback: @escaping (WildlandError?) -> Void)

  /// Perform the call to request email verification for provided code
  func verifyEmail(with code: String, _ callback: @escaping (WildlandError?) -> Void)

  /// Perform the call to create a mnemonic for provided word list
  func createMnemonic(using words: [String], _ callback: @escaping (Error?) -> Void)

  /// Perform the call to create a mnemonic for provided word list
  func generateMnemonic(_ callback: @escaping (_ words: [String], _ error: Error?) -> Void)

  /// Perform the call to create a user with a provided device name
  func createUser(with deviceName: String, _ callback: @escaping (WildlandError?) -> Void)

  /// Perform the call to get root cargo url, which is path to root folder where file provider stores users files
  func rootCargoUrl(_ callback: @escaping (_ url: URL?, WildlandError?) -> Void)

  /// Perform the call to set theme on the daemon application
  func setTheme(_ theme: String)

  /// Checks the current user state. In case of error, returns `WildlandError` and `error` status (Obj-C compatible).
  func availability(_ callback: @escaping (UserAvailabilityState, WildlandError?) -> Void)
}

@objc(WLFileProviderActionProtocol)
public protocol FileProviderActionProtocol {

  func performAction(action: FileAction, identifiers: [String], completion: @escaping (Error?) -> Void)

}

@objc(WLControlServiceProtocol)
public protocol ControlServiceProtocol {

  /// Mount domain in the daemon
  func mountDomain(_ callback: @escaping (WildlandError?) -> Void)
}

/// `FileProviderClientProxyProtocol` is a protocol that enables bi-directional communication between an XPC service
/// and a client app, specifically for cases where the service needs to send updates to the client. It provides
/// a mechanism for the XPC service to access an object conforming to the `FileProviderClientProtocol`, which
/// combines the various file operation protocols (such as create, delete, and update, etc.).
///
/// By conforming to this protocol, a class or object can provide a closure that returns an object implementing
/// the `FileProviderClientProtocol`. The XPC service can then use this closure to obtain a reference to the client
/// object and invoke the appropriate methods for notifying the client about file operation updates.
public protocol FileProviderClientProxyProtocol {

  /// A closure that returns objects conforming to the `FileProviderClientProtocol`. This closure allows the XPC service
  /// to access the clients object and invoke the appropriate methods to notify the clients about file operation updates.
  ///
  /// The closure may return empty if the client object is unavailable or if the connection should not be established.
  var clientsProxies: (() -> [FileProviderClientProtocol])? { get set }

}

extension NSXPCInterface {
  public static var clientService: NSXPCInterface {
    let fileProviderClientInterface = NSXPCInterface(with: FileProviderClientProtocol.self)
    let selector = #selector(FileProviderListProtocol.receiveListResponseSuccess(taskId:items:cursor:))
    let classSet = fileProviderClientInterface.classes(for: selector, argumentIndex: 1, ofReply: false)
    let classes = setOfClasses(FileItem.self).union(classSet)
    fileProviderClientInterface.setClasses(classes, for: selector, argumentIndex: 1, ofReply: false)

    return fileProviderClientInterface
  }
}

extension NSXPCInterface {

  public static var daemonService: NSXPCInterface {
    let daemonInterface = NSXPCInterface(with: DaemonServiceProtocol.self)
    let fileProviderInterface = NSXPCInterface(with: FileProviderServiceProtocol.self)
    let fileProviderAction = NSXPCInterface(with: FileProviderActionProtocol.self)

    daemonInterface.setInterface(controlInterface, for: #selector(DaemonServiceProtocol.controlService(_:)),
                                 argumentIndex: 0, ofReply: true)
    daemonInterface.setInterface(fileProviderInterface, for: #selector(DaemonServiceProtocol.fileProviderService(_:)),
                                 argumentIndex: 0, ofReply: true)
    daemonInterface.setInterface(userServiceInterface, for: #selector(DaemonServiceProtocol.userService(_:)),
                                 argumentIndex: 0, ofReply: true)
    daemonInterface.setInterface(fileProviderAction, for: #selector(DaemonServiceProtocol.fileProviderAction(_:)),
                                 argumentIndex: 0, ofReply: true)
    return daemonInterface
  }

  private static func setOfClasses(_ classes: AnyClass...) -> Set<AnyHashable> {
    NSSet(array: classes) as Set
  }

  private static var controlInterface: NSXPCInterface {
    let interface = NSXPCInterface(with: ControlServiceProtocol.self)
    [
      #selector(ControlServiceProtocol.mountDomain)
    ]
      .forEach { selector in
        let classSet = interface.classes(for: selector, argumentIndex: 0, ofReply: true)
        let classes = setOfClasses(WildlandError.self, NSError.self).union(classSet)
        interface.setClasses(classes, for: selector, argumentIndex: 0, ofReply: true)
      }
    return interface
  }

  private static var userServiceInterface: NSXPCInterface {
    let interface = NSXPCInterface(with: UserServiceProtocol.self)
    [
      #selector(UserServiceProtocol.verifyEmail),
      #selector(UserServiceProtocol.requestFreeTierStorage),
      #selector(UserServiceProtocol.createUser),
      #selector(UserServiceProtocol.rootCargoUrl),
      #selector(UserServiceProtocol.availability)
    ]
      .forEach { selector in
        let classSet = interface.classes(for: selector, argumentIndex: 0, ofReply: true)
        let classes = setOfClasses(WildlandError.self, NSError.self).union(classSet)
        interface.setClasses(classes, for: selector, argumentIndex: 0, ofReply: true)
      }
    return interface
  }
}
