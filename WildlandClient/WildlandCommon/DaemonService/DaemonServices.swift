//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public protocol DaemonControlService {
  func enableService(_ enable: Bool)
  func restartService()
  func mountDomain(_ callback: @escaping (_ error: Error?) -> Void)
}

public protocol DaemonUserService {
  var rootCargoUrl: URL { get async throws }
  var availabilityState: UserAvailabilityState { get async throws }
  func requestFreeTierStorage(for email: String) async throws
  func verifyEmail(with code: String) async throws
  func createMnemonic(using words: [String]) async -> Result<Void, Error>
  func generateMnemonic() async throws -> [String]
  func createUser(with deviceName: String) async throws
  func setTheme(_ themeOption: Settings.ColorThemeOption) async throws
}
