//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

/// `FileProviderFailureProtocol` is a protocol used in bi-directional communication between a
/// file provider extension and an XPC service, specifically for handling failures.
///
/// This protocol defines a method that is invoked on the XPC service side to notify
/// the client (file provider extension) about the failure of an item operation. All item operations inherit from
/// this protocol to manage error handling.
@objc(WLFileProviderFailureProtocol)
public protocol FileProviderFailureProtocol {

  /// Notifies the client that the item operation has encountered an error.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the item operation.
  ///   - error: An `Error` object describing the reason for the failure.
  func receiveResponseFailure(taskId: String, filename: String?, error: Error)

}
