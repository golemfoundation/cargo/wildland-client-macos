//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

/// `FileProviderProgressProtocol` is a protocol used in bi-directional communication between a
/// file provider extension and an XPC service, specifically for handling operations' progress.
///
/// This protocol defines a method that is invoked on the XPC service side to notify
/// the client (file provider extension) about the progress of an item operation. All item operations inherit from
/// this protocol to manage progress updates.
@objc(WLFileProviderProgressProtocol)
public protocol FileProviderProgressProtocol {

  /// Notifies the client of the progress of an operation that retrieves information about a file creation.
  /// Progress can be represented as `offset/size`.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the item operation.
  ///   - identifier: A unique identifier provided by FileProviderExtension
  ///   - filename: Filename displayed in activity view if nil it's fetched from cache
  ///   - offset: Implicitly represents progress of an operation in relation to its size.
  ///   - size: Total size of file.
  func receiveResponseProgress(taskId: String, identifier: String, offset: Int, size: Int)

}
