//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

/// `FileProviderListProtocol` is a protocol used in bi-directional communication between a
/// file provider extension and an XPC service. It is primarily used when the file provider extension calls the
/// `enumerateItems(for:startingAt:)` method,
/// which initiates implicitly an operation in the XPC service.
///
/// This protocol defines methods that are invoked on the XPC service side to notify
/// the client (file provider extension) about the status of the list folder item operation,
/// whether it succeeded or failed.
@objc(WLFileProviderListProtocol)
public protocol FileProviderListProtocol: FileProviderFailureProtocol {

  /// Notifies the client that the listing folder operation has completed successfully.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the listing operation.
  ///   - items: The resulting `FileItem` array created by the operation.
  ///   - cursor: When used, manage the enumeration of items and to retrieve a subset of the items at a time,
  ///             rather than all of them at once (as it does now as core team do not support multi-pagination yet)
  func receiveListResponseSuccess(taskId: String, items: [FileItem], cursor: UInt64)

  /// Notifies the client that the listing of item updates operation has completed successfully.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the listing operation.
  ///   - itemUpdates: The resulting `FileItemUpdates`  object created by the operation.
  func receiveListUpdatesResponseSuccess(taskId: String, itemUpdates: FileItemUpdates)

  /// Notifies the client that the fetching of sync anchor operation has completed successfully.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the listing operation.
  ///   - anchor: The resulting `Data` object created by the operation.
  func receiveCurrentAnchor(taskId: String, anchor: Data)
}
