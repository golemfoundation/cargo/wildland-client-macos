//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

/// `FileProviderDeleteProtocol` is a protocol used in bi-directional communication between a
/// file provider extension and an XPC service. It is primarily used when the file provider extension calls the
/// `deleteItem(identifier:baseVersion:options:request:completionHandler:)` method,
/// which initiates implicitly an operation in the XPC service.
///
/// This protocol defines methods that are invoked on the XPC service side to notify the client (file provider extension)
/// about the status of the delete item operation, whether it succeeded or failed.
@objc(WLFileProviderDeleteProtocol)
public protocol FileProviderDeleteProtocol: FileProviderFailureProtocol, FileProviderStartProtocol {

  /// Notifies the client that the delete item operation has completed successfully.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the create item operation.
  func receiveDeleteResponseSuccess(taskId: String)

}
