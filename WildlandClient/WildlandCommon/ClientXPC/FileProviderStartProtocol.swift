//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/// `FileProviderStartProtocol` is a protocol used in bi-directional communication between a
/// file provider extension and an XPC service. It is primarily used when an operation was started.
@objc(WLFileProviderStartProtocol)
public protocol FileProviderStartProtocol {

  /// Notifies the client that the delete item operation has completed successfully.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the create item operation.
  ///   - identifier: A unique identifier representing the item.
  ///   - filename: A filename of the item.
  ///   - operationType: Type of operation which will be performed.
  func receiveStart(taskId: String, identifier: String, filename: String?, operationType: OperationType)
}
