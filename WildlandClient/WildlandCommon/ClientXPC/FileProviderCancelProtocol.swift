//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

/// `FileProviderProgressProtocol` is a protocol used in-directional communication between a
/// file provider extension and an XPC service, specifically for handling operations' cancellations.
///
/// This protocol defines a method that is invoked on the XPC service side to notify
/// the client (file provider extension) about the cancellation of an item operation.
/// Currently, it is more like confimation from daemon for client (fp), as cancellation is triggered from fp.
@objc(WLFileProviderCancelProtocol)
public protocol FileProviderCancelProtocol {

  /// Notifies the client of the cancelled an operation.
  ///
  /// - Parameters:
  ///   - taskId: A unique identifier representing the task associated with the item operation.
  ///   - error: An `Error` object describing the reason for the failure.
  func receiveResponseCancelConfirmation(taskId: String, error: Error)

}
