//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import WildlandCommon
import CoreServices
import UniformTypeIdentifiers
import os.log

final class FileProviderEnumerator: NSObject, NSFileProviderEnumerator {

  private let proxy: FileProviderProxyActions
  private let recursive: Bool
  private let enumeratedItemIdentifier: NSFileProviderItemIdentifier

  init(enumeratedItemIdentifier: NSFileProviderItemIdentifier, proxy: FileProviderProxyActions, recursive: Bool = false) {
    _log.enumeratorCreating(identifier: enumeratedItemIdentifier.rawValue)
    self.enumeratedItemIdentifier = enumeratedItemIdentifier
    self.proxy = proxy
    self.recursive = recursive

    super.init()
    Task {
      await setAsObserved(true)
    }
  }

  func invalidate() {
    _log.enumeratorInvalidated()
    Task {
      await setAsObserved(false)
    }
  }

  private func setAsObserved(_ isObserved: Bool) async {
    guard enumeratedItemIdentifier != .workingSet && enumeratedItemIdentifier != .trashContainer else { return }
    _log.setAsObserved(isObserved, identifier: enumeratedItemIdentifier.rawValue)
    do {
      try await proxy.updateTrackingFolder(with: enumeratedItemIdentifier.rawValue, isObserved: isObserved)
    } catch {
      _log.error(failure: error)
    }
  }

  func enumerateItems(for observer: NSFileProviderEnumerationObserver, startingAt page: NSFileProviderPage) {
    _log.enumeratorItems()

    let identifier = enumeratedItemIdentifier == NSFileProviderItemIdentifier.workingSet
    ? Defaults.Identifier.root
    : enumeratedItemIdentifier.rawValue

    Task {
      do {
        let response = try await proxy.listFolder(
          with: identifier,
          recursive: recursive,
          startingCursor: 0
        )

        _log.enumeratorResponseSucceed(count: response.entries.count)

        let items = response.entries.filter { $0.id != NSFileProviderItemIdentifier.trashContainer.rawValue }
        observer.didEnumerate(items)
        observer.finishEnumerating(upTo: nil)
      } catch {
        _log.enumeratorError(failure: error)
        observer.finishEnumeratingWithError(error)
      }
    }
  }

  func currentSyncAnchor(completionHandler: @escaping (NSFileProviderSyncAnchor?) -> Void) {
    Task {
      do {
        let anchorData = try await proxy.getCurrentAnchor()
        if !anchorData.isEmpty {
          let anchor = NSFileProviderSyncAnchor(rawValue: anchorData)
          completionHandler(anchor)
          return
        }
      }
      completionHandler(nil)
    }
  }

  func enumerateChanges(for observer: NSFileProviderChangeObserver, from anchor: NSFileProviderSyncAnchor) {
    Task {
      do {
        let response = try await proxy.listItemUpdates(using: anchor.rawValue)
        let deletedIdentifiers = response.deletedIdentifiers.map { NSFileProviderItemIdentifier($0) }

        if !deletedIdentifiers.isEmpty {
          observer.didDeleteItems(withIdentifiers: deletedIdentifiers)
        }

        if !response.addedOrUpdatedItems.isEmpty {
          observer.didUpdate(response.addedOrUpdatedItems)
        }

        let newAnchor = NSFileProviderSyncAnchor(rawValue: response.anchor)
        observer.finishEnumeratingChanges(upTo: newAnchor, moreComing: false)
      } catch {
        observer.finishEnumeratingWithError(error)
      }
    }
  }
}

private extension Logger {

  func enumeratorResponseSucceed(file: String = #file, method: String = #function, count: Int) {
    let details = "response succeed, number of items to list: \(count)"
    debug(file: file, method: method, details: details)
  }

  func enumeratorInvalidated(file: String = #file, method: String = #function) {
    let details = "enumerator invalidated"
    info(file: file, method: method, details: details)
  }

  func enumeratorCreating(file: String = #file, method: String = #function, identifier: String) {
    let details = "creating enumerator with identifier \(identifier)..."
    debug(file: file, method: method, details: details)
  }

  func enumeratorItems(file: String = #file, method: String = #function) {
    let details = "starting enumerating items..."
    debug(file: file, method: method, details: details)
  }

  func enumeratorError(file: String = #file, method: String = #function, failure: Error) {
    let details = "finish enumerating with error"
    error(file: file, method: method, details: details, failure: failure)
  }

  func setAsObserved(file: String = #file, method: String = #function, _ isObserved: Bool, identifier: String) {
    let details = "set as observed (\(isObserved)) identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }
}
