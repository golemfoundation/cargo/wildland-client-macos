//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import WildlandCommon
import os

let _log = Logger.wildlandLogger()

/// tuple type that represents the result of a remote storage change operation (creation and modification)
typealias RemoteStorageChangesResult = (item: NSFileProviderItem, fields: NSFileProviderItemFields, fetchContent: Bool)
/// Used by `create` and `modify` methods
typealias FileProviderStreamElement = FileProviderServiceStatus<RemoteStorageChangesResult>

 // TODO: - Ticket CARDV-53 introduced bi-directional communication for XPC, which led to a simplification and separation of the code into different files. Initially, methods related to specific actions, such as file creation, were moved to separate files following the naming convention FileProviderExtension+Create.swift. For future improvements, instead of using extensions, consider creating dedicated instances for each action, e.g., FileProviderCreate for file creation. These new instances should then be injected into the FileProviderExtension constructor. CARDV-77.
final class FileProviderExtension: NSObject, NSFileProviderReplicatedExtension {

  let fileProviderProxy: FileProviderProxyActions = FileProviderProxy()
  let temporaryUrlProvider: TemporaryUrlProvider

  required init(domain: NSFileProviderDomain) {
    _log.extensionStarting(domain: domain.debugDescription)
    guard let manager = NSFileProviderManager(for: domain) else {
      fatalError("failed to setup file provider manager")
    }
    temporaryUrlProvider = manager

    super.init()
  }

  func invalidate() {
    _log.extensionInvalidated()
  }
}

extension FileProviderExtension {
  public func enumerator(for containerItemIdentifier: NSFileProviderItemIdentifier,
                         request: NSFileProviderRequest) throws -> NSFileProviderEnumerator {
    _log.enumerator(enumerator: containerItemIdentifier.rawValue)
    switch containerItemIdentifier {
    case .trashContainer:
      _log.enumeratorTrash()
      throw NSFileProviderError(.noSuchItem)
    default:
      return FileProviderEnumerator(
        enumeratedItemIdentifier: containerItemIdentifier,
        proxy: fileProviderProxy
      )
    }
  }
}

extension FileProviderExtension: NSFileProviderIncrementalContentFetching {
  public func fetchContents(for itemIdentifier: NSFileProviderItemIdentifier, // swiftlint:disable:this function_parameter_count
                            version requestedVersion: NSFileProviderItemVersion?,
                            usingExistingContentsAt existingContents: URL,
                            existingVersion: NSFileProviderItemVersion,
                            request: NSFileProviderRequest,
                            completionHandler: @escaping (URL?, NSFileProviderItem?, Error?) -> Void) -> Progress {
    fetchContents(
      for: itemIdentifier,
      version: requestedVersion,
      request: request,
      completionHandler: completionHandler
    )
  }
}

extension FileProviderExtension: NSFileProviderCustomAction {
  func performAction(identifier actionIdentifier: NSFileProviderExtensionActionIdentifier,
                     onItemsWithIdentifiers itemIdentifiers: [NSFileProviderItemIdentifier],
                     completionHandler: @escaping (Error?) -> Void) -> Progress {
    guard let action = FileAction(actionIdentifier: actionIdentifier) else { return Progress() }
    let progress = Progress(totalUnitCount: 100)

    fileProviderProxy.performAction(
      action: action,
      identifiers: itemIdentifiers.map(\.rawValue),
      completion: completionHandler
    )
    progress.cancellationHandler = { completionHandler(NSError.cancellation) }
    return progress
  }
}

private extension Logger {

  func extensionStarting(file: String = #file, method: String = #function, domain: String) {
    let details = "starting file provider extension with domain: \(domain)..."
    info(file: file, method: method, details: details)
  }

  func enumeratorTrash(file: String = #file, method: String = #function) {
    let details = "trashing is not supported"
    info(file: file, method: method, details: details)
  }

  func extensionInvalidated(file: String = #file, method: String = #function) {
    let details = "file provider extension invalidated, clean resources if needed"
    info(file: file, method: method, details: details)
  }

  func enumerator(file: String = #file, method: String = #function, enumerator: String) {
    let details = "enumerator: \(enumerator)"
    info(file: file, method: method, details: details)
  }
}
