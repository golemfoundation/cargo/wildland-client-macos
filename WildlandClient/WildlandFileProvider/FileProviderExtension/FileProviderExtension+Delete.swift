//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import os
import WildlandCommon

extension FileProviderExtension {

  public func deleteItem(identifier itemIdentifier: NSFileProviderItemIdentifier, baseVersion version: NSFileProviderItemVersion,
                         options: NSFileProviderDeleteItemOptions = [], request: NSFileProviderRequest,
                         completionHandler: @escaping (Error?) -> Void) -> Progress {

    _log.logDelete(identifier: itemIdentifier.rawValue)

    let progress = Progress(totalUnitCount: 100)

    Task {
      do {
        try await fileProviderProxy.deleteItem(
          with: itemIdentifier.rawValue,
          version: .init(version),
          recursive: options.contains(.recursive)
        )
        completionHandler(nil)
      } catch {
        _log.errorDownloadingContent(identifier: itemIdentifier.rawValue, failure: error)
        completionHandler(WildlandError(error).underlyingError)
      }
    }

    progress.cancellationHandler = {
      _log.debugCancellationHandler()
      completionHandler(NSError.cancellation)
    }

    return progress
  }
}

private extension Logger {

  func logDelete(identifier: String) {
    log("delete item for identifier: \(identifier, privacy: .public)")
  }

  func debugCancellationHandler() {
    debug("request to delete item was cancelled")
  }

  func errorDownloadingContent(file: String = #file, method: String = #function, identifier: String, failure: Error) {
    let details = "failed deleting item with identifier: \(identifier)"
    error(file: file, method: method, details: details, failure: failure)
  }
}
