//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import WildlandCommon
import os
import UniformTypeIdentifiers

private typealias CreateContentStream = AsyncThrowingMapSequence<TransferStream, FileProviderStreamElement>

extension FileProviderExtension {

  public func createItem(basedOn itemTemplate: NSFileProviderItem, fields: NSFileProviderItemFields, contents url: URL?,
                         options: NSFileProviderCreateItemOptions = [], request: NSFileProviderRequest,
                         completionHandler: @escaping (NSFileProviderItem?, NSFileProviderItemFields, Bool, Error?)
                         -> Void) -> Progress {

    _log.logCreate(identifier: itemTemplate.itemIdentifier.rawValue, filename: itemTemplate.filename)

    let progress = Progress(totalUnitCount: 100)
    // The priority set here is higher than the priority in `receiveResponseSuccess`. All streams under `createItem` inherit
    // this high priority. The priority in `receiveResponseSuccess` is lower to ensure progress (UI) updates occur as quickly as possible.
    // In most cases, the task priority can remain at the default level, only being significant when there are numerous operations.
    let cancelableTask = Task(priority: .high) {
      do {
        let type = try determineType(for: itemTemplate)

        // Not sure if data condition is needed, or not
        if options.contains(.mayAlreadyExist), type != .folder, url == nil {
          completionHandler(nil, [], false, nil)
        }

        try await withTaskCancellationHandler(operation: {
          for try await result in try await perform(
            for: itemTemplate,
            type: type,
            options: options
          ) {
            guard !Task.isCancelled else { return }
            switch result {
            case .inProgress(let value):
              _log.debugProgress(name: itemTemplate.filename)
              progress.completedProgress = value
            case .completed(let changeResult):
              _log.debugCompleted(name: changeResult.item.filename, index: 1)
              completionHandler(
                changeResult.item,
                changeResult.fields,
                changeResult.fetchContent,
                nil
              )
            }
          }
        }, onCancel: {
          _log.debugCancellationHandler(name: itemTemplate.filename)
          completionHandler(itemTemplate, [], false, NSError.cancellation)
        })
      } catch {
        guard !Task.isCancelled else { return }
        _log.error(identifier: itemTemplate.itemIdentifier.rawValue, failure: error, filename: itemTemplate.filename)
        completionHandler(itemTemplate, [], false, WildlandError(error).underlyingError)
      }
    }

    progress.cancellationHandler = {
      cancelableTask.cancel()
    }

    return progress
  }

  private func perform(for providerItem: NSFileProviderItem,
                       type: FileProviderDomainService.Entry.EntryType,
                       options: NSFileProviderCreateItemOptions = []) async throws -> CreateContentStream {
    let itemInfo = CreateItemInfo(providerItem: providerItem, options: options, type: type)
    return try await fileProviderProxy.createItem(with: itemInfo).map { status in
      switch status {
      case .inProgress(let progress):
        return FileProviderStreamElement.inProgress(progress)
      case .completed(let fileItem):
        _log.debugCompleted(name: fileItem.name, index: 2)
        let result: RemoteStorageChangesResult = (fileItem, [], false)
        return FileProviderStreamElement.completed(result)
      }
    }
  }

  func determineType(for providerItem: NSFileProviderItem) throws -> FileProviderDomainService.Entry.EntryType {
    let contentType = providerItem.contentType
    switch contentType {
    case .folder?, .directory?:
      return .folder
    default:
      guard let folderType = UTType("public.directory"), contentType?.isSubtype(of: folderType) == true else {
        return .file
      }
      return .folder
    }
  }
}

private extension Logger {

  func logCreate(file: String = #file, method: String = #function, identifier: String, filename: String) {
    let details = "create item for identifier: \(identifier), filename: \(filename)"
    debug(file: file, method: method, details: details)
  }

  func debugCancellationHandler(file: String = #file, method: String = #function, name: String) {
    let details = "request to create item was cancelled, for: \(name)"
    debug(file: file, method: method, details: details)
  }

  func debugCompleted(file: String = #file, method: String = #function, name: String?, index: Int) {
    // index is just for tracking execution order
    let details = "continuation stream succeed (\(index)), for: \(name ?? "---")"
    debug(file: file, method: method, details: details)
  }

  func debugProgress(file: String = #file, method: String = #function, name: String?, index: Int = 1) {
    // index is just for tracking execution order
    let details = "continuation stream in progress (\(index))..., for: \(name ?? "---")"
    debug(file: file, method: method, details: details)
  }

  func error(file: String = #file, method: String = #function, identifier: String, failure: Error, filename: String) {
    let details = "create item failed for identifier: \(identifier), filename: \(filename)"
    error(file: file, method: method, details: details, failure: failure)
  }
}
