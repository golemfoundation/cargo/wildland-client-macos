//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import WildlandCommon
import os

extension FileProviderExtension {

  func item(for identifier: NSFileProviderItemIdentifier,
            request: NSFileProviderRequest) async throws -> NSFileProviderItem {

    _log.infoFileItem(identifier: identifier.rawValue)

    return try await withCheckedThrowingContinuation { continuation in
      _ = item(for: identifier, request: request) { fileItem, error in
        if let error {
          continuation.resume(throwing: error)
        } else if let fileItem {
          continuation.resume(returning: fileItem)
        } else {
          continuation.resume(throwing: NSFileProviderError(.noSuchItem))
        }
      }
    }
  }

  func item(for identifier: NSFileProviderItemIdentifier, request: NSFileProviderRequest,
            completionHandler: @escaping (NSFileProviderItem?, Error?) -> Void) -> Progress {

    let progress = Progress(totalUnitCount: 100)

    Task {
      do {
        if identifier.rawValue == NSFileProviderItemIdentifier.trashContainer.rawValue {
          throw NSFileProviderError(.noSuchItem)
        }
        let item = try await fileProviderProxy.itemInfo(with: identifier.rawValue)
        completionHandler(item, nil)
      } catch {
        _log.errorFailedGetInfo(identifier: identifier.rawValue, failure: error)
        completionHandler(nil, error)
      }
    }

    progress.cancellationHandler = {
      completionHandler(nil, NSError(domain: NSFileProviderErrorDomain, code: NSUserCancelledError))
    }

    return progress
  }
}

private extension Logger {

  func infoFileItem(file: String = #file, method: String = #function, identifier: String) {
    let details = "start getting info about item with identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func errorFailedGetInfo(file: String = #file, method: String = #function, identifier: String, failure: Error) {
    let details = "failed to get info for identifier: \(identifier)"
    error(file: file, method: method, details: details, failure: failure)
  }
}
