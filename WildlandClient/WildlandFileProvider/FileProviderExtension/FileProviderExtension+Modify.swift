//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import WildlandCommon
import os

private typealias ModifyContentStream = AsyncThrowingMapSequence<ModifyStream, FileProviderStreamElement>

private typealias UpdateChangedFields = (String) async throws -> (FileItem, Bool)

extension FileProviderExtension {

  public func modifyItem(_ item: NSFileProviderItem,
                         baseVersion version: NSFileProviderItemVersion,
                         changedFields: NSFileProviderItemFields,
                         contents newContents: URL?,
                         options: NSFileProviderModifyItemOptions = [],
                         request: NSFileProviderRequest,
                         completionHandler: @escaping (NSFileProviderItem?,
                                                       NSFileProviderItemFields,
                                                       Bool,
                                                       Error?) -> Void) -> Progress {
    _log.logModify(name: item.itemIdentifier.rawValue)

    let progress = Progress(totalUnitCount: 100)
    let cancelableTask = Task(priority: .high) {
      do {
        try await withTaskCancellationHandler(operation: {
          let (item, remainingFields, fetchContent) = try await modifyItem(
            for: item,
            changedFields: changedFields,
            contents: newContents,
            request: request,
            progressHandler: { value in
              progress.completedProgress = value
            }
          )
          guard !Task.isCancelled else { return }
          completionHandler(item, remainingFields, fetchContent, nil)
        }, onCancel: {
          _log.debugCancellationHandler()
          completionHandler(nil, [], false, NSError.cancellation)
        })
      } catch {
        guard !Task.isCancelled else { return }
        _log.errorFailedModifying(identifier: item.filename, failure: error)
        completionHandler(item, changedFields, false, WildlandError(error).underlyingError)
      }
    }

    progress.cancellationHandler = {
      cancelableTask.cancel()
    }

    return progress
  }

  private func modifyItem(for item: NSFileProviderItem,
                          changedFields: NSFileProviderItemFields,
                          contents newContents: URL?,
                          request: NSFileProviderRequest,
                          progressHandler: ((ProgressType) -> Void)) async throws -> RemoteStorageChangesResult {
    let supportedChangedFields = changedFields.removing(.lastUsedDate, .contentModificationDate)
    guard !supportedChangedFields.isEmpty else {
      let fileItem = try await self.item(for: item.itemIdentifier, request: request)
      return (fileItem, changedFields.removing(.lastUsedDate), false)
    }

    if newContents != nil, supportedChangedFields.contains(.contents) {
      for try await status in try await handleContentUpdate(
        item: item,
        changedFields: supportedChangedFields) {
        switch status {
        case .inProgress(let value):
          progressHandler(value)
        case .completed(let result):
          return try await renameIfChanged(result: result)
        }
      }
    }

    if supportedChangedFields.contains(.parentItemIdentifier) {
      if item.parentItemIdentifier == .trashContainer {
        // Handle item trashing
        return try await handleItemTrashing(
          item: item,
          changedFields: supportedChangedFields
        )
      }
      // Handle update item parent
      return try await handleUpdateParent(
        item: item,
        changedFields: supportedChangedFields
      )
    }

    return try await handleDefaultCase(
      item: item,
      changedFields: supportedChangedFields
    )
  }

  private func handleItemTrashing(item: NSFileProviderItem, changedFields: NSFileProviderItemFields) async throws -> RemoteStorageChangesResult {
    _log.errorTrashing()
    let entry = try await fileProviderProxy.itemInfo(with: item.itemIdentifier.rawValue)
    return (entry, changedFields.removing(.contents), false)
  }

  private func handleUpdateParent(item: NSFileProviderItem,
                                  changedFields: NSFileProviderItemFields) async throws -> RemoteStorageChangesResult {
    var result = (item, changedFields, false)
    result = try await moveIfChanged(result: result)
    return result
  }

  private func handleContentUpdate(item: NSFileProviderItem,
                                   changedFields: NSFileProviderItemFields) async throws -> ModifyContentStream {
    return try await fileProviderProxy.modifyItemContent(
      with: item.itemIdentifier.rawValue
    ).map { status in
      switch status {
      case .inProgress(let progress):
        return FileProviderStreamElement.inProgress(progress)
      case .completed(let fileItem):
        let result: RemoteStorageChangesResult = (fileItem.item, changedFields.removing(.contents), false)
        return FileProviderStreamElement.completed(result)
      }
    }
  }

  private func handleDefaultCase(item: NSFileProviderItem,
                                 changedFields: NSFileProviderItemFields) async throws -> RemoteStorageChangesResult {
    let result = (item, changedFields, false)
    return try await renameIfChanged(result: result)
  }

  private func renameIfChanged(result: RemoteStorageChangesResult) async throws -> RemoteStorageChangesResult {
    try await updateIfChanged(result: result, for: .filename) {
      try await fileProviderProxy.rename(identifier: $0, name: result.item.filename)
    }
  }

  private func moveIfChanged(result: RemoteStorageChangesResult) async throws -> RemoteStorageChangesResult {
    try await updateIfChanged(result: result, for: .parentItemIdentifier) {
      try await fileProviderProxy.move(identifier: $0, parentIdentifier: result.item.parentItemIdentifier.rawValue)
    }
  }

  private func updateIfChanged(result: RemoteStorageChangesResult,
                               for field: NSFileProviderItemFields,
                               action: UpdateChangedFields) async throws -> RemoteStorageChangesResult {
    guard result.fields.contains(field) else { return result }
    let response = try await action(result.item.itemIdentifier.rawValue)
    return (response.0, result.fields.removing(field), false)
  }
}

private extension Logger {

  func logModify(file: String = #file, method: String = #function, name: String) {
    let details = "starting file modification, name \(name)..."
    info(file: file, method: method, details: details)
  }

  func debugCancellationHandler(file: String = #file, method: String = #function) {
    let details = "request for item modification was cancelled"
    debug(file: file, method: method, details: details)
  }

  func errorTrashing(file: String = #file, method: String = #function) {
    let details = "trashing disabled, move item back"
    error(file: file, method: method, details: details, failure: nil)
  }

  func errorFailedModifying(file: String = #file, method: String = #function, identifier: String, failure: Error) {
    let details = "failed modifying item with identifier: \(identifier)"
    error(file: file, method: method, details: details, failure: failure)
  }
}
