//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import WildlandCommon
import os

private typealias FileProviderStreamFetchElement = FileProviderServiceStatus<FileItem>

private typealias FetchContentStream = AsyncThrowingMapSequence<TransferStream, FileProviderStreamFetchElement>

extension FileProviderExtension {

  func fetchContents(for itemIdentifier: NSFileProviderItemIdentifier,
                     version requestedVersion: NSFileProviderItemVersion?,
                     request: NSFileProviderRequest,
                     completionHandler: @escaping (URL?, NSFileProviderItem?, Error?) -> Void) -> Progress {

    _log.logFetchStarting(identifier: itemIdentifier.rawValue, line: #line)

    let progress = Progress(totalUnitCount: 100)

    let cancelableTask = Task(priority: .high) {
      do {
        try await withTaskCancellationHandler(operation: {
          try await checkItemVersion(
            for: itemIdentifier,
            requestedVersion: requestedVersion,
            request: request
          )
          let fetchContents = try await perform(for: itemIdentifier, version: requestedVersion)
          let url = try temporaryUrlProvider.url(for: itemIdentifier.rawValue)
          for try await result in fetchContents {
            guard !Task.isCancelled else { return }
            switch result {
            case .inProgress(let value):
              _log.debugProgress(name: itemIdentifier.rawValue)
              progress.completedProgress = value
            case .completed(let fileItem):
              _log.debugCompleted(name: itemIdentifier.rawValue)
              completionHandler(url, fileItem, nil)
            }
          }
        }, onCancel: {
          _log.taskCancellationHandler(identifier: itemIdentifier.rawValue)
          completionHandler(nil, nil, NSError.cancellation)
        })
      } catch {
        guard !Task.isCancelled else { return }
        _log.errorFetchingContent(identifier: itemIdentifier.rawValue, failure: error)
        completionHandler(nil, nil, WildlandError(error).underlyingError)
      }
    }

    progress.cancellationHandler = {
      _log.progressCancellationHandler(identifier: itemIdentifier.rawValue)
      cancelableTask.cancel()
    }

    return progress
  }

  private func perform(for itemIdentifier: NSFileProviderItemIdentifier,
                       version requestedVersion: NSFileProviderItemVersion?) async throws -> FetchContentStream {
    try await fileProviderProxy.downloadItem(
      with: itemIdentifier.rawValue,
      version: requestedVersion.map { FileProviderDomainService.Version($0) }
    ).map { status in
      switch status {
      case .inProgress(let progress):
        return FileProviderStreamFetchElement.inProgress(progress)
      case .completed(let item):
        return FileProviderStreamFetchElement.completed(item)
      }
    }
  }

  @discardableResult
  private func checkItemVersion(for itemIdentifier: NSFileProviderItemIdentifier,
                                requestedVersion: NSFileProviderItemVersion?,
                                request: NSFileProviderRequest) async throws -> NSFileProviderItem {
    let item = try await item(for: itemIdentifier, request: request)
    if let requestedVersion = requestedVersion, requestedVersion.contentVersion != item.itemVersion?.contentVersion {
      throw NSFileProviderError(.versionNoLongerAvailable)
    }
    return item
  }
}

private extension Logger {

  func logFetchStarting(file: String = #file, method: String = #function, identifier: String, line: Int) {
    let details = "FPE: start fetching content (line: \(line), identifier: \(identifier)"
    info(file: file, method: method, details: details)
  }

  func debugCompleted(file: String = #file, method: String = #function, name: String?, index: Int = 1) {
    // index is just for tracking execution order
    let details = "FPE: continuation stream succeed (\(index)), for: \(name ?? "---")"
    debug(file: file, method: method, details: details)
  }

  func debugProgress(file: String = #file, method: String = #function, name: String?, index: Int = 1) {
    // index is just for tracking execution order
    let details = "FPE: continuation stream in progress (\(index))..., for: \(name ?? "---")"
    debug(file: file, method: method, details: details)
  }

  func progressCancellationHandler(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: progress handler - starting cancelling fetch operation, identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func taskCancellationHandler(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: request to fetch item was cancelled, identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func errorFetchingContent(file: String = #file, method: String = #function, identifier: String, failure: Error) {
    let details = "FPE: failed to fetch content, identifier: \(identifier)"
    error(file: file, method: method, details: details, failure: failure)
  }
}
