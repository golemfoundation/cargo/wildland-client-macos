//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import FileProvider.NSFileProviderError
import WildlandCommon

protocol FileProviderManagerClientActions: FileProviderManagerClientStreamActions,
                                           FileProviderManagerClientAsyncActions {
  func setService(_ service: FileProviderServiceProtocol) async
}

// Stream methods are marked as async only because of actor isolation requirements
protocol FileProviderManagerClientStreamActions {
  func createItem(with itemInfo: CreateItemInfo) async -> TransferStream
  func modifyItemContent(with identifier: String) async -> ModifyStream
  func downloadItem(with identifier: String, version: FileProviderDomainService.Version?) async -> TransferStream
}

protocol FileProviderManagerClientAsyncActions {
  func deleteItem(with identifier: String, version: FileProviderDomainService.Version, recursive: Bool) async throws
  func rename(identifier: String, name: String) async throws -> (FileItem, Bool)
  func move(identifier: String, parentIdentifier: String) async throws -> (FileItem, Bool)
  func listFolder(with identifier: String, recursive: Bool, startingCursor: UInt64) async throws -> (entries: [FileItem], cursor: UInt64)
  func itemInfo(with identifier: String) async throws -> FileItem
  func listItemUpdates(using anchor: Data) async throws -> FileItemUpdates
  func updateTrackingFolder(with identifier: String, isObserved: Bool) async throws
  func getCurrentAnchor() async throws -> Data
}

/// `FileProviderServiceProtocol` supports various actions like create, delete, modify, etc.
/// Different actions may return distinct success types (but share the same error type).
/// To accommodate these varying continuation return types while maintaining a consistent type,
/// the `ContinuationWrapper` enum is introduced.
private enum ContinuationWrapper {
  case fileItem(TransferStream.Continuation)
  case modifyItem(ModifyStream.Continuation)
  case itemInfo(CheckedContinuation<FileItem, Error>)
  case void(CheckedContinuation<Void, Error>)
  case modifyItemNoProgressTracking(CheckedContinuation<(FileItem, Bool), Error>)
  case listFolder(CheckedContinuation<([FileItem], UInt64), Error>)
  case listItemUpdates(CheckedContinuation<FileItemUpdates, Error>)
  case getCurrentAnchor(CheckedContinuation<Data, Error>)
}

private typealias TaskStream<T> = AsyncThrowingStream<T, Error>

actor FileProviderClientManager: FileProviderManagerClientActions {

  private var continuationStorage = [UUID: ContinuationWrapper]()

  private var fileProviderService: FileProviderServiceProtocol?

  private var service: FileProviderServiceProtocol {
    get throws {
      guard let fileProviderService else {
        throw NSFileProviderError(.serverUnreachable)
      }
      return fileProviderService
    }
  }

  func setService(_ service: FileProviderServiceProtocol) {
    fileProviderService = service
  }

  private func cancelOperation(taskId: UUID) {
    do {
      try service.cancelOperation(taskId: taskId.uuidString)
      store(for: taskId, continuation: nil)
    } catch {
      serviceNotAvailable(taskId: taskId.uuidString, error: error)
    }
  }

  private func store(for identifier: UUID, continuation: ContinuationWrapper?) {
    continuationStorage[identifier] = continuation
  }

  private func serviceNotAvailable(taskId: String, error: Error) {
    receiveResponseFailure(taskId: taskId, filename: nil, error: error)
  }

  private func continuation(for taskId: String, action: (ContinuationWrapper) -> Void) {
    if let identifier = UUID(uuidString: taskId), let continuation = continuationStorage[identifier] {
      action(continuation)
    } else {
      // Actor is thread-safe, but it is in developer responsibility to prevent re-entrancy error

      // Theoretically, it is possible to encounter a situation, ex. where the file provider crashes and, after relaunching,
      // notifications continue to be sent from the daemon. It is advisable to monitor this behavior and maybe clean up
      // some resources/operations if needed.

      // It also might be that seeing this error is... a requested in some situation. Imagine file provider crashed
      // once creating file -> daemon continue operation -> file provider re-launch. In perfect scenario where everything
      // work as should, you should never get here.
      _log.errorReentrancy(taskId: taskId)
    }
  }

  private func removeTask(taskId: String) {
    guard let identifier = UUID(uuidString: taskId) else { return }
    continuationStorage[identifier] = nil
  }
}

// MARK: Methods which don't report progress

extension FileProviderClientManager {

  func deleteItem(with identifier: String, version: FileProviderDomainService.Version, recursive: Bool) async throws {
    try await performNonProgressTask({ taskId in
      try service.deleteItem(
        taskId: taskId.uuidString,
        identifier: identifier,
        contentVersion: version.content,
        metadataVersion: version.metadata,
        recursive: recursive
      )
    }, continuationHandler: { .void($0) })
  }

  func listFolder(with identifier: String, recursive: Bool, startingCursor: UInt64) async throws -> (entries: [FileItem], cursor: UInt64) {
    try await performNonProgressTask({ taskId in
      try service.listFolder(
        taskId: taskId.uuidString,
        identifier: identifier,
        recursive: recursive,
        startingCursor: startingCursor
      )
    }, continuationHandler: { .listFolder($0) })
  }

  func itemInfo(with identifier: String) async throws -> FileItem {
    try await performNonProgressTask({ taskId in
      try service.itemInfo(
        taskId: taskId.uuidString,
        identifier: identifier
      )
    }, continuationHandler: { .itemInfo($0) })
  }

  func rename(identifier: String, name: String) async throws -> (FileItem, Bool) {
    try await performNonProgressTask({ taskId in
      try service.renameItem(
        taskId: taskId.uuidString,
        identifier: identifier,
        name: name
      )
    }, continuationHandler: { .modifyItemNoProgressTracking($0) })
  }

  func move(identifier: String, parentIdentifier: String) async throws -> (FileItem, Bool) {
    try await performNonProgressTask({ taskId in
      try service.moveItem(
        taskId: taskId.uuidString,
        identifier: identifier,
        parentIdentifier: parentIdentifier
      )
    }, continuationHandler: { .modifyItemNoProgressTracking($0) })
  }

  func listItemUpdates(using anchorData: Data) async throws -> FileItemUpdates {
    try await performNonProgressTask({ taskId in
      try service.listItemUpdates(taskId: taskId.uuidString, anchorData: anchorData)
    }, continuationHandler: { .listItemUpdates($0) })
  }

  func getCurrentAnchor() async throws -> Data {
    try await performNonProgressTask({ taskId in
      try service.getCurrentAnchor(taskId: taskId.uuidString)
    }, continuationHandler: { .getCurrentAnchor($0) })
  }

  func updateTrackingFolder(with identifier: String, isObserved: Bool) async throws {
    try await performNonProgressTask({ taskId in
      try service.updateTrackingFolder(taskId: taskId.uuidString, identifier: identifier, isObserved: isObserved)
    }, continuationHandler: { .void($0) })
  }

  // MARK: - Private

  private func performNonProgressTask<T>(_ action: (UUID) throws -> Void, continuationHandler: (CheckedContinuation<T, Error>) -> ContinuationWrapper) async throws -> T {
    try await withCheckedThrowingContinuation { continuation in
      let taskId = UUID()
      do {
        let continuationWrapper = continuationHandler(continuation)
        _log.debugStart(type: continuationWrapper, taskId: taskId)
        try action(taskId)
        store(for: taskId, continuation: continuationWrapper)
        _log.debugStartContinue(type: continuationWrapper, taskId: taskId)
      } catch {
        serviceNotAvailable(taskId: taskId.uuidString, error: error)
      }
    }
  }
}

// MARK: Methods which report progress

extension FileProviderClientManager {

  func modifyItemContent(with identifier: String) -> ModifyStream {
    performProgressTask({ taskId in
      try self.service.modifyItemContent(
        taskId: taskId.uuidString,
        identifier: identifier
      )
    }, continuationHandler: { .modifyItem($0) })
  }

  func downloadItem(with identifier: String, version: FileProviderDomainService.Version?) -> TransferStream {
    performProgressTask({ taskId in
      try self.service.downloadItem(
        taskId: taskId.uuidString,
        identifier: identifier,
        contentVersion: version?.content ?? 0,
        metadataVersion: version?.metadata ?? 0
        )
    }, continuationHandler: { .fileItem($0) })
  }

  func createItem(with itemInfo: CreateItemInfo) -> TransferStream {
    performProgressTask({ taskId in
      try self.service.createItem(taskId: taskId.uuidString, itemInfo: itemInfo)
    }, continuationHandler: { .fileItem($0) })
  }

  private func performProgressTask<T>(_ action: (UUID) throws -> Void, continuationHandler: (TaskStream<T>.Continuation) -> ContinuationWrapper) -> TaskStream<T> {
    AsyncThrowingStream { continuation in
      let taskId = UUID()
      continuation.onTermination = { termination in
        _log.debugOnTerminate(status: termination.description, taskId: taskId)
        Task {
          await self.cancelOperation(taskId: taskId)
        }
      }
      do {
        let continuationWrapper = continuationHandler(continuation)
        _log.debugStart(type: continuationWrapper, taskId: taskId)
        try action(taskId)
        store(for: taskId, continuation: continuationWrapper)
        _log.debugStartContinue(type: continuationWrapper, taskId: taskId)
      } catch {
        serviceNotAvailable(taskId: taskId.uuidString, error: error)
      }
    }
  }
}

// MARK: - FileProviderClientProtocol

extension FileProviderClientManager: FileProviderClientProtocol {

  /// This method receives response progress updates for a given task.
  /// Progress tracking is needed for .fileItem, .downloadItem, and .modifyItem cases,
  /// as these cases may involve transferring a significant amount of data.
  /// If the `offset` is equal to the `size`, it implies that the operation is completed.
  /// The completion is handled by the `receiveResponseSuccess` method.
  ///
  /// - Parameters:
  ///   - taskId: A string representing the unique identifier for the task.
  ///   - offset: Implicitly represents progress of an operation in relation to its size.
  ///   - size: Total size of file.
  nonisolated func receiveResponseProgress(taskId: String, identifier: String, offset: Int, size: Int) {
    Task {
      await handleResponseProgress(
        taskId: taskId,
        identifier: identifier,
        offset: offset,
        size: size
      )
    }
  }

  nonisolated func receiveInfoResponseSuccess(taskId: String, fileItem: FileItem) {
    receiveResponseSuccess(method: #function, taskId: taskId, result: fileItem)
  }

  nonisolated func receiveDeleteResponseSuccess(taskId: String) {
    receiveResponseSuccess(method: #function, taskId: taskId, result: ())
  }

  nonisolated func receiveModifyResponseSuccess(taskId: String, fileItem: FileItem, fetchContent: Bool) {
    receiveResponseSuccess(method: #function, taskId: taskId, result: (fileItem, fetchContent))
  }

  nonisolated func receiveListResponseSuccess(taskId: String, items: [FileItem], cursor: UInt64) {
    receiveResponseSuccess(method: #function, taskId: taskId, result: (items, cursor))
  }

  nonisolated func receiveListUpdatesResponseSuccess(taskId: String, itemUpdates: FileItemUpdates) {
    receiveResponseSuccess(method: #function, taskId: taskId, result: itemUpdates)
  }

  nonisolated func receiveCurrentAnchor(taskId: String, anchor: Data) {
    receiveResponseSuccess(method: #function, taskId: taskId, result: anchor)
  }

  nonisolated private func receiveResponseSuccess(method name: String, taskId: String, result: Any) {
    Task {
      await handleResponseSuccess(method: name, taskId: taskId, result: result)
    }
  }

  nonisolated func receiveResponseFailure(taskId: String, filename: String?, error: Error) {
    _log.error(failure: error, taskId: taskId)
    Task { await handleResponseFailure(taskId: taskId, error: error) }
  }

  nonisolated func receiveResponseCancelConfirmation(taskId: String, error: Error) {
    _log.debugCancel(taskId: taskId)
    Task { await handleResponseFailure(taskId: taskId, error: error) }
  }

  // MARK: Private

  private func handleResponseProgress(taskId: String, identifier: String, offset: Int, size: Int) {
    let progress: ProgressType = (offset, size)
    _log.debugProgress(identifier: identifier, value: progress, taskId: taskId)

    continuation(for: taskId) {
      guard progress.offset < progress.size else { return }
      switch $0 {
      case .fileItem(let continuation):
        continuation.yield(.inProgress(progress))
      case .modifyItem(let continuation):
        continuation.yield(.inProgress(progress))
      default:
        break
      }
    }
  }

  private func handleResponseSuccess(method name: String, taskId: String, result: Any) {
    _log.debugSuccess(method: name, result: result, taskId: taskId)
    continuation(for: taskId) {
      switch $0 {
      case .itemInfo(let continuation):
        continuation.resume(returning: result as! FileItem)
      case .fileItem(let continuation):
        continuation.complete(.completed(result as! FileItem))
      case .void(let continuation):
        continuation.resume(returning: result as! Void)
      case .modifyItemNoProgressTracking(let continuation):
        continuation.resume(returning: result as! (FileItem, Bool))
      case .modifyItem(let continuation):
        continuation.complete(.completed(result as! (FileItem, Bool)))
      case .listFolder(let continuation):
        continuation.resume(returning: result as! ([FileItem], UInt64))
      case .listItemUpdates(let continuation):
        continuation.resume(returning: result as! FileItemUpdates)
      case .getCurrentAnchor(let continuation):
        continuation.resume(returning: result as! Data)
      }
    }
    removeTask(taskId: taskId)
  }

  private func handleResponseFailure(taskId: String, error: Error) {
    continuation(for: taskId) {
      switch $0 {
      case .itemInfo(let continuation):
        continuation.resume(throwing: error)
      case .fileItem(let continuation):
        continuation.finish(throwing: error)
      case .void(let continuation):
        continuation.resume(throwing: error)
      case .modifyItemNoProgressTracking(let continuation):
        continuation.resume(throwing: error)
      case .modifyItem(let continuation):
        continuation.finish(throwing: error)
      case .listFolder(let continuation):
        continuation.resume(throwing: error)
      case .listItemUpdates(let continuation):
        continuation.resume(throwing: error)
      case .getCurrentAnchor(let continuation):
        continuation.resume(throwing: error)
      }
    }
    removeTask(taskId: taskId)
  }

  // MARK: - operation started

  nonisolated func receiveStart(taskId: String, identifier: String, filename: String?, operationType: OperationType) {
    _log.operationStared(operationType: operationType, taskId: taskId)
  }
}

private extension Logger {

  func errorReentrancy(file: String = #file, method: String = #function, taskId: String) {
    let details = "\(taskId) not exist, operation might be already cancelled, ideally should never get here"
    error(file: file, method: method, details: details, failure: nil)
  }

  func error(file: String = #file, method: String = #function, failure: Error, taskId: String) {
    let details = "XPC response failure: \(taskId)"
    error(file: file, method: method, details: details, failure: failure)
  }

  func debugSuccess(file: String = #file, method: String = #function, result: Any, taskId: String) {
    let details = "XPC response succeed: \(result) \(taskId)"
    debug(file: file, method: method, details: String(details.prefix(250)))
  }

  func debugCancel(file: String = #file, method: String = #function, taskId: String) {
    let details = "XPC cancelation confirmed: \(taskId)"
    debug(file: file, method: method, details: String(details.prefix(250)))
  }

  func debugStart(file: String = #file, method: String = #function, type: ContinuationWrapper? = nil, taskId: UUID) {
    var details = "XPC starting: \(taskId.uuidString)"
    type.map { details.append(", \($0)")}
    debug(file: file, method: method, details: details)
  }

  func debugStartContinue(file: String = #file, method: String = #function, type: ContinuationWrapper? = nil, taskId: UUID) {
    var details = "XPC message sended to daemon: \(taskId.uuidString)"
    type.map { details.append(", \($0)")}
    debug(file: file, method: method, details: details)
  }

  func debugProgress(file: String = #file, method: String = #function, identifier: String, value: ProgressType, taskId: String) {
    let details = "XPC operation in response progress for identifier: \(identifier), progress: \(Double(value.offset) / Double(value.size)), \(taskId)"
    debug(file: file, method: method, details: details)
  }

  func debugOnTerminate(file: String = #file, method: String = #function, status: String, taskId: UUID) {
    let details = "XPC stream completed with status: \(status), taskId: \(taskId.uuidString)"
    debug(file: file, method: method, details: details)
  }

  func operationStared(file: String = #file, method: String = #function, operationType: OperationType, taskId: String) {
    let details = "Operation started: \(operationType), taskId: \(taskId)"
    debug(file: file, method: method, details: details)
  }
}
