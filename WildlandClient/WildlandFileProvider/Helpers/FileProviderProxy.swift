//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import Foundation
import os
import WildlandCommon

protocol FileProviderProxyActions {
  func performAction(action: FileAction, identifiers: [String], completion: @escaping (Error?) -> Void)
  func createItem(with itemInfo: CreateItemInfo) async throws -> TransferStream
  func deleteItem(with identifier: String, version: FileProviderDomainService.Version, recursive: Bool) async throws
  func rename(identifier: String, name: String) async throws -> (FileItem, Bool)
  func move(identifier: String, parentIdentifier: String) async throws -> (FileItem, Bool)
  func modifyItemContent(with identifier: String) async throws -> ModifyStream
  func listFolder(with identifier: String, recursive: Bool, startingCursor: UInt64) async throws -> (entries: [FileItem], cursor: UInt64)
  func downloadItem(with identifier: String, version: FileProviderDomainService.Version?) async throws -> TransferStream
  func itemInfo(with identifier: String) async throws -> FileItem
  func listItemUpdates(using anchor: Data) async throws -> FileItemUpdates
  func updateTrackingFolder(with identifier: String, isObserved: Bool) async throws
  func getCurrentAnchor() async throws -> Data
}

final class FileProviderProxy {
  private let logger = Logger.xpcService()
  private var connection: NSXPCConnection?
  private let clientManager: FileProviderManagerClientActions

  private func connect() -> NSXPCConnection {
    let connection = NSXPCConnection(machServiceName: Defaults.XPCEndpoints.control, options: [])
    connection.remoteObjectInterface = .daemonService
    connection.interruptionHandler = { [weak self] in
      self?.logger.infoConnectionInterrupted(pid: connection.processIdentifier)
      self?.disconnectDomain()
    }

    connection.exportedObject = clientManager
    connection.exportedInterface = .clientService

    connection.invalidationHandler = { [weak self] in
      self?.logger.infoConnectionInvalidated(pid: connection.processIdentifier)
      self?.connection = nil
    }
    logger.resumingConnection()
    connection.resume()
    return connection
  }

  init(manager: FileProviderManagerClientActions = FileProviderClientManager()) {
    clientManager = manager
  }

  deinit {
    connection?.invalidate()
  }

  // MARK: - Private

  private func disconnectDomain() {
    guard let fileProviderManager = NSFileProviderManager(for: fileProviderDomain) else {
      _log.fileManagerCreateFailed()
      return
    }

    fileProviderManager.disconnect(reason: WLStrings.appQuit.domainUnmount.title, options: .temporary) {
      _log.domainDisconnect(failure: $0)
    }
  }

  private var fileProviderService: FileProviderServiceProtocol {
    get async throws {
      let daemonService = try await daemonService
      return await withCheckedContinuation { continuation in
        daemonService.fileProviderService { fileProviderService in
          continuation.resume(returning: fileProviderService)
        }
      }
    }
  }

  private var fileProviderAction: FileProviderActionProtocol {
    get async throws {
      let daemonService = try await daemonService
      return await withCheckedContinuation { continuation in
        daemonService.fileProviderAction { action in
          continuation.resume(returning: action)
        }
      }
    }
  }

  private var daemonService: DaemonServiceProtocol {
    get async throws {
      try await Task.retrying {
        // Consider actor, as theoretically that method might be called simultaneously in more than one place,
        // which might lead to more than one connection with client
        if self.connection == nil {
          self.connection = self.connect()
        }

        self.logger.infoProxy(connection: self.connection)
        let service = self.connection?.remoteObjectProxyWithErrorHandler { [weak self] error in
          self?.logger.errorConnectionRemoteObject(failure: error, pid: self?.connection?.processIdentifier ?? -1)
        } as? DaemonServiceProtocol

        guard let service else {
          throw NSError.invalidXPCConnectionError
        }

        self.logger.debugConnection()
        return service
      }.value
    }
  }
}

extension FileProviderProxy: FileProviderProxyActions {

  func createItem(with itemItem: CreateItemInfo) async throws -> TransferStream {
    _log.debugCreation(itemItem: itemItem)
    return try await performManagerOperation {
      await clientManager.createItem(with: itemItem)
    }
  }

  func deleteItem(with identifier: String, version: FileProviderDomainService.Version, recursive: Bool) async throws {
    _log.debugDeletion(identifier: identifier, recursive: recursive)
    try await performManagerOperation {
      try await clientManager.deleteItem(
        with: identifier,
        version: version,
        recursive: recursive
      )
    }
  }

  func rename(identifier: String, name: String) async throws -> (FileItem, Bool) {
    _log.debugRenameItem(identifier: identifier)
    return try await performManagerOperation {
      try await clientManager.rename(identifier: identifier, name: name)
    }
  }

  func move(identifier: String, parentIdentifier: String) async throws -> (FileItem, Bool) {
    _log.debugMoveItem(identifier: identifier)
    return try await performManagerOperation {
      try await clientManager.move(identifier: identifier, parentIdentifier: parentIdentifier)
    }
  }

  func modifyItemContent(with identifier: String) async throws -> ModifyStream {
    _log.debugModification(identifier: identifier)
    return try await performManagerOperation {
      await clientManager.modifyItemContent(with: identifier)
    }
  }

  func listFolder(with identifier: String, recursive: Bool, startingCursor: UInt64) async throws -> (entries: [FileItem], cursor: UInt64) {
    _log.debugListingItem(identifier: identifier,
                          startingCursor: startingCursor, recursive: recursive)
    return try await performManagerOperation {
      try await clientManager.listFolder(
        with: identifier,
        recursive: recursive,
        startingCursor: startingCursor
      )
    }
  }

  func downloadItem(with identifier: String, version: FileProviderDomainService.Version? = .zero) async throws -> TransferStream {
    _log.debugDownloadItem(identifier: identifier)
    return try await performManagerOperation {
      await clientManager.downloadItem(with: identifier, version: version)
    }
  }

  func itemInfo(with identifier: String) async throws -> FileItem {
    _log.debugInfoItem(identifier: identifier)
    return try await performManagerOperation {
      try await clientManager.itemInfo(with: identifier)
    }
  }

  func listItemUpdates(using anchor: Data) async throws -> FileItemUpdates {
    _log.debugItemUpdates(anchor: anchor)
    return try await performManagerOperation {
      try await clientManager.listItemUpdates(using: anchor)
    }
  }

  func getCurrentAnchor() async throws -> Data {
    _log.debugCurrentAnchor()
    return try await performManagerOperation {
      try await clientManager.getCurrentAnchor()
    }
  }

  func updateTrackingFolder(with identifier: String, isObserved: Bool) async throws {
    _log.updateTrackingFolder(identifier: identifier)
    return try await performManagerOperation {
      try await clientManager.updateTrackingFolder(
        with: identifier,
        isObserved: isObserved
      )
    }
  }

  func performAction(action: FileAction, identifiers: [String], completion: @escaping (Error?) -> Void) {
    logger.performAction(identifiers: identifiers)
    Task {
      do {
        try await fileProviderAction.performAction(
          action: action,
          identifiers: identifiers,
          completion: completion
        )
      } catch {
        completion(nil)
      }
    }
  }

  @discardableResult
  private func performManagerOperation<T>(function: () async throws -> T) async throws -> T {
    let service = try await fileProviderService
    await clientManager.setService(service)
    return try await function()
  }
}

private extension Logger {

  func infoConnectionInvalidated(file: String = #file, method: String = #function, pid: pid_t) {
    let details = "FPE: control connection is invalidated: \(pid)"
    info(file: file, method: method, details: details)
  }

  func infoConnectionInterrupted(file: String = #file, method: String = #function, pid: pid_t) {
    let details = "FPE: control connection is interrupted: \(pid)"
    info(file: file, method: method, details: details)
  }

  func infoProxy(file: String = #file, method: String = #function, connection: NSXPCConnection?) {
    let details = "FPE: connection: \(connection?.debugDescription ?? "---"), processIdentifier \(connection?.processIdentifier ?? -1)"
    info(file: file, method: method, details: details)
  }

  func errorConnectionRemoteObject(file: String = #file, method: String = #function, failure: Error, pid: pid_t) {
    let details = "FPE: processIdentifier: \(pid)"
    error(file: file, method: method, details: details, failure: failure)
  }

  func resumingConnection(file: String = #file, method: String = #function) {
    let details = "FPE: resuming connection..."
    debug(file: file, method: method, details: details)
  }

  func debugConnection(file: String = #file, method: String = #function) {
    let details = "FPE: SUCCESS TO OBTAIN PROXY INSTANCE"
    debug(file: file, method: method, details: details)
  }

  func performAction(file: String = #file, method: String = #function, identifiers: [String]) {
    let details = "FPE: perform action - identifiers: \(identifiers)"
    debug(file: file, method: method, details: details)
  }

  func debugDownloadItem(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: item download - identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func debugInfoItem(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: item info - identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func debugListingItem(file: String = #file, method: String = #function, identifier: String, startingCursor: UInt64, recursive: Bool) {
    let details =
        """
        FPE: item list - identifier:\(identifier),
        startingCursor:\(startingCursor)
        recursive:\(recursive)
        """
    debug(file: file, method: method, details: details)
  }

  func debugRenameItem(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: item rename - identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func debugMoveItem(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: item move - identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func debugCreation(file: String = #file, method: String = #function, itemItem: CreateItemInfo) {
    let details =
      """
      FPE: item create - type:\(itemItem.type),
      identifier:\(itemItem.temporaryIdentifier),
      name:\(itemItem.name),
      parentIdentifier:\(itemItem.parentIdentifier),
      strategy:\(itemItem.strategy),
      creationDate:\(itemItem.creationDate)
      """
    debug(file: file, method: method, details: details)
  }

  func debugDeletion(file: String = #file, method: String = #function, identifier: String, recursive: Bool) {
    let details = "FPE: item delete - identifier: \(identifier), recursive: \(recursive)"
    debug(file: file, method: method, details: details)
  }

  func debugModification(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: item modify - identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func debugItemUpdates(file: String = #file, method: String = #function, anchor: Data) {
    let details = "FPE: items update - anchor: \(String(data: anchor, encoding: .utf8) ?? "Empty anchor data")"
    debug(file: file, method: method, details: details)
  }

  func debugCurrentAnchor(file: String = #file, method: String = #function) {
    let details = "FPE: get current anchor"
    debug(file: file, method: method, details: details)
  }

  func updateTrackingFolder(file: String = #file, method: String = #function, identifier: String) {
    let details = "FPE: updating folder  - identifier: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func domainDisconnect(file: String = #file, method: String = #function, failure: Error?) {
    if let failure {
      error(failure: failure)
    } else {
      let details = "FPE: domain disconnected"
      info(file: file, method: method, details: details)
    }
  }

  func fileManagerCreateFailed(file: String = #file, method: String = #function) {
    let details = "FPE: failed to create NSFileProviderManager for domain \(fileProviderDomain)"
    error(file: file, method: method, details: details, failure: nil)
  }
}
