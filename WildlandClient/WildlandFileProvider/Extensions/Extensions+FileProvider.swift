//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import FileProvider
import WildlandCommon

extension FileProviderDomainService.Version {
  public init(_ version: NSFileProviderItemVersion) {
    self = FileProviderDomainService.Version(content: version.contentVersion.pod(exactly: UInt64.self) ?? 0,
                                             metadata: version.metadataVersion.pod(exactly: UInt64.self) ?? 0)
  }
}

extension NSFileProviderItemVersion {
  public convenience init(_ version: FileProviderDomainService.Version) {
    let contentVersion = withUnsafeBytes(of: version.content) { Data($0) }
    let metadataVersion = withUnsafeBytes(of: version.metadata) { Data($0) }
    self.init(contentVersion: contentVersion, metadataVersion: metadataVersion)
  }
}

extension FileProviderDomainService.EntryMetadata.ValidEntries {
  public init(_ fields: NSFileProviderItemFields) {
    var result = FileProviderDomainService.EntryMetadata.ValidEntries(rawValue: 0)
    if fields.contains(.fileSystemFlags) {
      result.insert(.fileSystemFlags)
    }
    if fields.contains(.lastUsedDate) {
      result.insert(.lastUsedDate)
    }
    if fields.contains(.creationDate) {
      result.insert(.creationDate)
    }
    if fields.contains(.contentModificationDate) {
      result.insert(.contentModificationDate)
    }

    self = result
  }
}

extension FileProviderDomainService.EntryMetadata.FileSystemFlags {
  public init(_ fields: NSFileProviderFileSystemFlags) {
    var result = FileProviderDomainService.EntryMetadata.FileSystemFlags(rawValue: 0)
    if fields.contains(.userWritable) {
      result.insert(.userWritable)
    }
    if fields.contains(.userReadable) {
      result.insert(.userReadable)
    }
    if fields.contains(.userExecutable) {
      result.insert(.userExecutable)
    }
    if fields.contains(.pathExtensionHidden) {
      result.insert(.pathExtensionHidden)
    }
    self = result
  }
}
