//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

extension Progress {
  var completedProgress: ProgressType {
    get {
      (Int(completedUnitCount), Int(totalUnitCount))
    }
    set {
      let denominator = max(max(Double.leastNormalMagnitude, Double(newValue.size)), Double(newValue.offset))
      @ClampedProgress(log: _log) var progress = Double(newValue.offset) / denominator
      completedUnitCount = Int64(progress * Double(totalUnitCount))
    }
  }
}
