//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import CoreServices
import UniformTypeIdentifiers
import WildlandCommon
import os

extension FileItem: NSFileProviderItemProtocol, NSFileProviderItemDecorating {

  public var itemVersion: NSFileProviderItemVersion {
    NSFileProviderItemVersion(.init(content: contentVersion, metadata: metadataVersion))
  }

  public var itemIdentifier: NSFileProviderItemIdentifier {
    NSFileProviderItemIdentifier(rawValue: identifier)
  }

  private var shareIconIdentifier: NSFileProviderItemDecorationIdentifier {
    NSFileProviderItemDecorationIdentifier(bundleID + ".share")
  }

  public var parentItemIdentifier: NSFileProviderItemIdentifier {
    NSFileProviderItemIdentifier(rawValue: parentIdentifier)
  }

  public var id: String {
    identifier
  }

  public var filename: String {
    name
  }

  public var isShared: Bool {
    !metadata.recipients.isEmpty
  }

  public var isSharedByCurrentUser: Bool {
    metadata.isOwner && isShared
  }

  public var revision: FileProviderDomainService.Version {
    .init(content: contentVersion, metadata: metadataVersion)
  }

  // Only root, folders and files are used. symlink or alias can be added in the future
  public var contentType: UTType {
    switch FileProviderDomainService.Entry.EntryType(rawValue: type) {
    case .folder, .root:
      return .folder
    case .file:
      return .item
    default:
      assert(false, "Not supported type")
      return .item
    }
  }

  public func type() throws -> FileProviderDomainService.Entry.EntryType {
    guard let value = FileProviderDomainService.Entry.EntryType(rawValue: type) else {
      _log.error("unable to determine capabilities: not supported object type: \(self.type, privacy: .public)")
      throw NSError(domain: NSFileProviderErrorDomain, code: NSFeatureUnsupportedError)
    }
    return value
  }

  public var capabilities: NSFileProviderItemCapabilities {
    switch FileProviderDomainService.Entry.EntryType(rawValue: type) {
    case .folder, .root:
      return .folderCapabilities
    case .file:
      return .fileCapabilities
    default:
      _log.error("unable to determine capabilities: not supported object type: \(self.type, privacy: .public)")
      assert(false, "Not supported type")
      return []
    }
  }

  public var documentSize: NSNumber? {
    size as NSNumber
  }

  public var lastUsedDate: Date? {
    metadata.lastUsedDate
  }

  public var creationDate: Date? {
    metadata.creationDate
  }

  public var contentModificationDate: Date? {
    metadata.modificationDate
  }

  public var fileSystemFlags: NSFileProviderFileSystemFlags {
    NSFileProviderFileSystemFlags(rawValue: metadata.fileSystemFlags?.rawValue ?? 0)
  }

  public var decorations: [NSFileProviderItemDecorationIdentifier]? {
    guard case .folder = FileProviderDomainService.Entry.EntryType(rawValue: type) else { return nil }
    guard shouldDecorateFolderByShare else { return nil }
    return [
      shareIconIdentifier
    ]
  }

  // TODO: - It will be implemented in CARGO-341
  private var shouldDecorateFolderByShare: Bool {
    false
  }

  public var contentPolicy: NSFileProviderContentPolicy {
    .downloadLazily
  }
}

extension NSFileProviderItemCapabilities {

  static let folderCapabilities: NSFileProviderItemCapabilities = [
    .allowsAddingSubItems,
    .allowsContentEnumerating,
    .allowsDeleting,
    .allowsReading,
    .allowsRenaming,
    .allowsWriting,
    .allowsReparenting
  ]

  static let fileCapabilities: NSFileProviderItemCapabilities = [
    .allowsDeleting,
    .allowsReading,
    .allowsRenaming,
    .allowsWriting,
    .allowsReparenting
  ]
}
