//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon

import Foundation

func main() {

}

func obtainMountPoint() throws -> String {
  let connection = NSXPCConnection(machServiceName: Defaults.XPCEndpoints.control, options: [])
  connection.remoteObjectInterface = .daemonService
  connection.resume()
  defer { connection.invalidate() }

  let group = DispatchGroup()

  var result: Result<String, Error>?
  let proxy = connection.remoteObjectProxyWithErrorHandler {
    result = .failure($0)
    group.leave()
  } as! DaemonServiceProtocol // swiftlint:disable:this force_cast

  group.enter()

  proxy.controlService { _ in
    group.leave()
  }

  let timeout: TimeInterval = 5
  _ = group.wait(timeout: .now() + timeout)

  if let result = result {
    return try result.get()
  } else {
    throw CLIError("connection timeout exceeded. Control connection unavailable or daemon is not running")
  }
}

private struct CLIError: LocalizedError {
  var errorDescription: String?

  init(_ value: String) {
    errorDescription = value
  }
}

main()
