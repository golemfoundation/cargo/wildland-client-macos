//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit
import Foundation
import os
import ServiceManagement
import WildlandCommon

private let _log = Logger.xpcService()

/**
 This class is responsible for controlling the NFS server/Wildand daemon (so essentially
 the wlclientd process.
 */
class DaemonControl {

  static let shared = DaemonControl()

  enum ErrorType: Error {
    case proxyNotAvailable
  }

  var proxy: DaemonServiceProtocol {
    get async throws {
      try await Task.retrying { [weak self] in
        if self?.connection == nil {
          self?.connection = self?.prepareConnection()
        }
        _log.proxy(identifier: self?.connection?.processIdentifier)
        let service = self?.connection?.remoteObjectProxyWithErrorHandler { [weak self] error in
          _log.proxy(identifier: self?.connection?.processIdentifier, failure: error)
        } as? DaemonServiceProtocol

        guard let service else {
          throw ErrorType.proxyNotAvailable
        }

        return service
      }.value
    }
  }

  private var connection: NSXPCConnection?

  // MARK: - Initialization

  deinit {
    connection?.invalidate()
  }

  // MARK: - Private

  private func prepareConnection() -> NSXPCConnection {
    _log.prepareConnection()
    let connection = NSXPCConnection(machServiceName: Defaults.XPCEndpoints.control, options: [])
    connection.remoteObjectInterface = .daemonService
    connection.interruptionHandler = { [identifier = connection.processIdentifier] in
      _log.prepareConnectionInterruptionHandler(identifier: identifier)
    }
    connection.invalidationHandler = { [weak self] in
      _log.prepareConnectionInvalidationHandler(identifier: self?.connection?.processIdentifier)
      self?.connection = nil
    }
    connection.resume()
    return connection
  }
}

extension DaemonControl: DaemonControlService {

  // MARK: - Service management

  func enableService(_ enable: Bool) {
    do {
      try SMAppService.enableService(identifier: Defaults.BundleID.control, enable)
      guard !enable else { return }
      connection?.invalidate()
    } catch {
      _log.enableService(failure: error)
    }
  }

  func restartService() {
    enableService(false)
    enableService(true)
  }

  func mountDomain(_ callback: @escaping (_ error: Error?) -> Void) {
    Task {
      do {
        try await proxy.controlService { controlService in
          controlService.mountDomain { error in
            callback(error)
          }
        }
      } catch {
        callback(error)
      }
    }
  }
}

private extension Logger {

  func proxy(file: String = #file, method: String = #function, identifier: pid_t?) {
    let details = "cargo proxy, pid: \(identifier ?? .zero)"
    debug(file: file, method: method, details: details)
  }

  func proxy(file: String = #file, method: String = #function, identifier: pid_t?, failure: Error) {
    let details = """
    remoteObjectProxyWithErrorHandler: \(failure.localizedDescription)
    processIdentifier: \(identifier ?? .zero)
    """
    error(details: details, failure: failure)
  }

  func enableService(file: String = #file, method: String = #function, failure: Error) {
    error(details: "SMAppService", failure: failure)
  }

  func prepareConnection(file: String = #file, method: String = #function) {
    let details = "cargo preparing connection..."
    debug(file: file, method: method, details: details)
  }

  func prepareConnectionInterruptionHandler(file: String = #file, method: String = #function, identifier: pid_t) {
    let details = "interruption handler, pid: \(identifier)"
    debug(file: file, method: method, details: details)
  }

  func prepareConnectionInvalidationHandler(file: String = #file, method: String = #function, identifier: pid_t?) {
    let details = "invalidation handler, pid: \(identifier ?? .zero)"
    debug(file: file, method: method, details: details)
  }
}
