//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import ServiceManagement
import WildlandCommon

extension DaemonControl: DaemonUserService {

  var rootCargoUrl: URL {
    get async throws {
      let userService = try await proxy.userService

      return try await withCheckedThrowingContinuation { continuation in
        userService.rootCargoUrl { url, error in
          if let error {
            continuation.resume(throwing: error)
          } else if let url {
            continuation.resume(returning: url)
          }
        }
      }
    }
  }

  var availabilityState: UserAvailabilityState {
    get async throws {
      let userService = try await proxy.userService

      return try await withCheckedThrowingContinuation { continuation in
        userService.availability { state, error in
          if let error {
            continuation.resume(throwing: error)
          } else {
            continuation.resume(returning: state)
          }
        }
      }
    }
  }

  func createMnemonic(using words: [String]) async -> Result<Void, Error> {
    guard let userService = try? await proxy.userService else { return .failure(ErrorType.proxyNotAvailable) }

    return await withCheckedContinuation { continuation in
      userService.createMnemonic(using: words) { error in
        if let error { continuation.resume(returning: .failure(error)); return }

        continuation.resume(returning: .success(()))
      }
    }
  }

  func generateMnemonic() async throws -> [String] {
    let userService = try await proxy.userService

    return try await withCheckedThrowingContinuation { continuation in
      userService.generateMnemonic { words, error in
        if let error { continuation.resume(throwing: error); return }

        continuation.resume(returning: words)
      }
    }
  }

  func createUser(with deviceName: String) async throws {
    let userService = try await proxy.userService

    return try await withCheckedThrowingContinuation { continuation in
      userService.createUser(with: deviceName) { error in
        if let error {
          continuation.resume(throwing: error)
        } else {
          continuation.resume()
        }
      }
    }
  }

  // MARK: - User API management

  func requestFreeTierStorage(for email: String) async throws {
    let userService = try await proxy.userService

    return try await withCheckedThrowingContinuation { continuation in
      userService.requestFreeTierStorage(with: email) { error in
        if let error {
          continuation.resume(throwing: error)
        } else {
          continuation.resume()
        }
      }
    }
  }

  func verifyEmail(with code: String) async throws {
    let userService = try await proxy.userService

    return try await withCheckedThrowingContinuation { continuation in
      userService.verifyEmail(with: code) { error in
        if let error {
          continuation.resume(throwing: error)
        } else {
          continuation.resume()
        }
      }
    }
  }

  // MARK: - Settings

  func setTheme(_ themeOption: Settings.ColorThemeOption) async throws {
    let userService = try await proxy.userService
    userService.setTheme(themeOption.rawValue)
  }
}
