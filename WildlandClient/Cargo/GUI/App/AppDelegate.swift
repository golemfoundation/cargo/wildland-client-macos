//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa
import SwiftUI
import os
import WildlandCommon
import CargoUI

private let _log = Logger.wildlandLogger()

final class AppDelegate: NSObject, NSApplicationDelegate, ObservableObject {

  @Published private(set) var deeplink: URL?

  var shouldPostponeTermination: Bool = false

  func applicationShouldTerminate(_ sender: NSApplication) -> NSApplication.TerminateReply {
    if shouldPostponeTermination {
      shouldPostponeTermination = false
      return .terminateLater
    }
    return .terminateNow
  }

  func applicationDidFinishLaunching(_ notification: Notification) {
    NSApp.removeMenuItems()

    if !launchedFromValidLocation() {
      notifyInvalidLocation()
      exit(0)
    }
  }

  func application(_ application: NSApplication, open urls: [URL]) {
    deeplink = urls.first
  }

  // MARK: - Private

  private func launchedFromValidLocation() -> Bool {
    guard !isDeveloperRun else { return true }
    return Bundle.main.bundlePath.starts(with: "/Applications") && Bundle.main.bundleURL.pathComponents.count == 3
  }

  private func notifyInvalidLocation() {
    let alert = NSAlert()
    alert.messageText = WLStrings.appLaunch.appInvalidLocationAlert.title
    alert.informativeText = WLStrings.appLaunch.appInvalidLocationAlert.message

    _ = alert.runModal()
  }

  private var isDeveloperRun: Bool {
#if DEBUG
    return true
#else
    // Check if run from the Xcode build folder.
    let processPath = Bundle.main.bundlePath
    guard !processPath.contains("Library/Developer/Xcode/DerivedData") else { return true }

    return false
#endif
  }
}
