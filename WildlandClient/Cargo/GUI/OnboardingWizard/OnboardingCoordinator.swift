//
// Wildland Project
//  OnboardingCoordinator.swift
//  WildlandClient
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import CargoUI
import Combine
import Foundation
import SwiftUI

class OnboardingCoordinator: BaseCoordinator, ObservableObject {

  enum WildlandOnboardingStage {
    case mnemonicGenerationInfo
    case mnemonicDisplay
    case mnemonicConfirm
  }

  enum OnboardingStage {
    case identityChooser
    case wildlandOnboarding(WildlandOnboardingStage)
  }

  @Published var stage: OnboardingStage = .identityChooser

  let identityChooserVM = IdentityChooserViewModel()
  let mnemonicGenerationInfoVM = MnemonicGenerationInfoViewModel()

  private var vmSinks = [AnyCancellable]()

  override init() {
    super.init()
    vmSinks.append(identityChooserVM.onShowNext.sink { _ in } receiveValue: { [weak self] _ in
      guard let ss = self else { return }
      switch ss.identityChooserVM.mode {
      case .wildland:
        ss.stage = .wildlandOnboarding(.mnemonicGenerationInfo)
      case .ethereum:
        fatalError("ethereum mode is not implemented yet")
      default: fatalError("invalid state reached")
      }
    })

    vmSinks.append(mnemonicGenerationInfoVM.onAction.sink { _ in } receiveValue: { [weak self] action in
      guard let ss = self else { return }
      switch action {
      case .goBack: ss.stage = .identityChooser
      case .generate:
        /* mnemonicGenerationInfoVM has already generated the words, so
         we can initialize displayVM with them */
        fatalError("next view is not implemented yet")
      }
    })
  }
}

extension OnboardingCoordinator: Coordinator {

  var title: String? { return "Onboarding" }

  var rootView: AnyView? {
    return AnyView(OnboardingView(coordinator: self)
      .modifier(ColorSchemeViewModifier()))
  }

  func start() {
    stage = .identityChooser
  }
}
