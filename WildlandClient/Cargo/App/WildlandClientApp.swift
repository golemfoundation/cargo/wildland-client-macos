//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import CargoUI
import WildlandCommon
import os

@main
struct WildlandClientApp: App {

  // MARK: - Properties

  @StateObject private var appCoordinator = ApplicationCoordinator()
  @NSApplicationDelegateAdaptor(AppDelegate.self) private var appDelegate
  @Environment(\.openWindow) private var openWindow

  private let userApiProvider = UserApiProvider()
  private var showPreferences: Bool {
    (try? userApiProvider.availabilityState.allowsClosingOnboarding) == true
  }

  // MARK: - Body

  var body: some Scene {
    Group {
      OnboardingScene(coordinator: appCoordinator.onboardingCoordinator)
      PreferencesScene(
        userApi: userApiProvider,
        sceneModel: PreferencesSceneModel(
          daemonUserService: DaemonControl.shared,
          logger: Logger.wildlandLogger()
        )
      )
      EmptyScene()
    }
    .onChange(of: appCoordinator.activeScenes) { identifiers in
      identifiers.forEach { openWindow(id: $0) }
    }
    .onChange(of: appCoordinator.inactiveScenes) { sceneIdentifiers in
      appCoordinator.closeWindows(for: sceneIdentifiers)
    }
    .onChange(of: appDelegate.deeplink, perform: { url in
      appCoordinator.handleDeeplink(url: url)
    })
    .defaultPosition(.center)
    .windowStyle(.hiddenTitleBar)
    .windowResizability(.contentSize)
    .commands {
      CargoMenu(showPreferences: showPreferences, commandHandler: appCoordinator)
    }
  }
}
