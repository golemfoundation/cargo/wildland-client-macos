//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import CargoUI
import SwiftUI
import WildlandCommon

protocol CommandMenuHandler: AnyObject {
  func handleQuit()
}

struct CargoMenu: Commands {

  // MARK: - Initialization

  init(showPreferences: Bool, commandHandler: CommandMenuHandler? = nil) {
    self.showPreferences = showPreferences
    self.commandHandler = commandHandler
  }

  // MARK: - Environment

  @Environment(\.openWindow) var openWindow
  @Environment(\.appInfo) var appInfo
  private weak var commandHandler: CommandMenuHandler?

  // MARK: - Body

  var body: some Commands {
    CommandGroup(replacing: .newItem, addition: { EmptyView() })
    CommandGroup(replacing: .help, addition: { EmptyView() })
    CommandGroup(replacing: .appInfo) { appInfoButton }
    if showPreferences {
      CommandGroup(replacing: .appSettings) { preferencesButton }
    }
    CommandGroup(replacing: .appTermination) { appTerminationButton }
  }

  // MARK: - Properties

  private let aboutPanelProvider = AboutPanelProvider()
  private let showPreferences: Bool

  private var appInfoButton: some View {
    Button("\(WLStrings.AppCargoMenu.about) \(appInfo.appName)") {
      aboutPanelProvider.showAboutPanel(appInfo: appInfo)
    }
  }

  private var preferencesButton: some View {
    Button(WLStrings.AppCargoMenu.preferences) {
      openWindow(id: AppSceneType.preferences)
    }
  }

  private var appTerminationButton: some View {
    Button("\(WLStrings.AppCargoMenu.quit) \(appInfo.appName)") {
      commandHandler?.handleQuit()
    }
    .keyboardShortcut("q")
  }
}
