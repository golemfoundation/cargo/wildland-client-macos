//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import CargoUI

/// Represents the onboarding process for the app.
/// The scene is launched when a user's status is either `.storageNotGranted` or `.notCreatedYet`. 
struct OnboardingScene: Scene {

  // MARK: - Properties

  private var coordinator: AppOnboardingCoordinator?

  // MARK: - Initialization

  init(coordinator: AppOnboardingCoordinator?) {
    self.coordinator = coordinator
  }

  // MARK: - Body

  var body: some Scene {
    Window(id: AppSceneType.onboarding) {
      if let coordinator {
        OnboardingCoordinatorView()
          .environmentObject(coordinator)
      }
    }
  }
}

private struct OnboardingCoordinatorView: View {

  // MARK: - Properties

  @EnvironmentObject private var coordinator: AppOnboardingCoordinator
  @EnvironmentObject private var appDelegate: AppDelegate

  // MARK: - Body

  var body: some View {
    coordinator.rootView
      .frame(idealWidth: 608.0)
      .fixedSize(horizontal: true, vertical: true)
      .transformEnvironment(\.window, transform: { window in
        window()?.isMovableByWindowBackground = true
        window()?.styleMask.remove([.miniaturizable, .resizable])
      })
      .onWindowClose { window in
        window?.center()
        coordinator.onOnboardingCloseWindow()
      }
      .onAppear {
        appDelegate.shouldPostponeTermination = true
      }
  }
}
