//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import SwiftUI
import WildlandCommon
import CargoUI
import os

private let _log = Logger.wildlandLogger()

@MainActor
final class ApplicationCoordinator: ObservableObject {

  // MARK: - Properties

  @Published private(set) var activeScenes: [AppSceneType] = [.empty]

  var inactiveScenes: Set<AppSceneType> {
    Set(AppSceneType.allCases).subtracting(activeScenes)
  }

  var onboardingCoordinator: AppOnboardingCoordinator? {
    onboardingCoordinatorSubject.value
  }

  private let onboardingCoordinatorSubject = CurrentValueSubject<AppOnboardingCoordinator?, Never>(nil)
  private let daemonService: DaemonUserService & DaemonControlService
  private let bundle: Bundle
  private let finishOnboardingSubject = PassthroughSubject<Void, Never>()
  private var cancellables = Set<AnyCancellable>()
  private let checkUserStateSubject = PassthroughSubject<Void, Never>()
  private let userStateSubject = PassthroughSubject<UserAvailabilityState, Never>()

  // MARK: - Initialization

  init(daemonService: DaemonUserService & DaemonControlService = DaemonControl.shared, bundle: Bundle = .main) {
    self.daemonService = daemonService
    self.bundle = bundle
    setupDaemon()
    setupBindings()
    checkUserStateSubject.send()
  }

  // MARK: - Setup

  private func setupBindings() {
    onboardingCoordinatorSubject
      .map { $0 != nil }
      .sink { [weak self] isActive in
        self?.setActiveScene(isActive, scene: .onboarding)
      }
      .store(in: &cancellables)

    userStateSubject
      .sink { [weak self] _ in
        self?.setActiveScene(false, scene: .empty)
      }
      .store(in: &cancellables)

    userStateSubject
      .filter { state in
        !state.allowsClosingOnboarding
      }
      .map { ($0, AppSceneType.onboarding) }
      .sink { [weak self] userAvailabilityState, sceneType in
        self?.setupViewModel(
          userAvailabilityState: userAvailabilityState,
          for: sceneType
        )
      }
      .store(in: &cancellables)

    finishOnboardingSubject
      .sink { [weak self] _ in
        Task {
          await self?.openFinderWhenAppReady()
          self?.onboardingCoordinatorSubject.send(nil)
        }
      }
      .store(in: &cancellables)

    checkUserStateSubject
      .sink { [weak self] _ in
        self?.checkAvailabilityState()
      }
      .store(in: &cancellables)
  }

  // MARK: - Public

  func handleDeeplink(url: URL?) {
    guard let host = url?.host, let sceneType = AppSceneType(rawValue: host) else { return }
    activeScenes.append(sceneType)
  }

  // MARK: - Private

  private func setActiveScene(_ isActive: Bool, scene: AppSceneType) {
    if isActive, !activeScenes.contains(scene) {
      activeScenes.append(scene)
    } else {
      activeScenes.removeFirst(scene)
    }
  }

  private func checkAvailabilityState() {
    Task { [weak self] in
      do {
        guard let availabilityState = try await self?.daemonService.availabilityState else { return }
        self?.userStateSubject.send(availabilityState)
      } catch {
        _log.error(failure: error)
      }
    }
  }

  private func openFinderWhenAppReady() async {
    guard let path = try? await daemonService.rootCargoUrl.path else { return }
    NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: path)
  }

  private func setupViewModel(userAvailabilityState: UserAvailabilityState, for state: AppSceneType) {
    switch state {
    case .empty, .preferences:
      break
    case .onboarding:
      let appOnboardingCoordinator = AppOnboardingCoordinator(
        finishOnboardingSubscriber: AnySubscriber(finishOnboardingSubject),
        checkUserStateSubscriber: AnySubscriber(checkUserStateSubject)
      )

      userStateSubject.prepend(userAvailabilityState)
        .subscribe(appOnboardingCoordinator.userStateSubscriber)

      onboardingCoordinatorSubject.send(appOnboardingCoordinator)
    }
  }

  private func setupDaemon() {
    if bundle.isNewDaemonVersionAvailable {
      DaemonControl.shared.restartService()
    } else {
      DaemonControl.shared.enableService(true)
    }
  }
}

extension ApplicationCoordinator {

  func closeWindows(for sceneIdentifiers: Set<AppSceneType>) {
    sceneIdentifiers
      .compactMap(window)
      .forEach { $0.close() }
  }

  private func window(id: AppSceneType) -> NSWindow? {
    NSApplication.shared.windows.first(where: { $0.identifier == NSUserInterfaceItemIdentifier(id.rawValue) })
  }
}

extension ApplicationCoordinator: CommandMenuHandler {

  func handleQuit() {
    closeWindows(for: inactiveScenes.union(activeScenes))
  }
}
