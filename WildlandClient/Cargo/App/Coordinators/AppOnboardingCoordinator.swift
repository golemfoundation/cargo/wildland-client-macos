//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import CargoUI
import Combine
import SwiftUI
import WildlandCommon
import os

private let _log = Logger.wildlandLogger()

@MainActor
final class AppOnboardingCoordinator: ObservableObject {

  // MARK: - Properties

  @Published private(set) var rootView: AnyView?
  lazy private(set) var userStateSubscriber = AnySubscriber(userStateSubject)
  private let daemonService: DaemonUserService & DaemonControlService
  private var cancellables = Set<AnyCancellable>()
  private let userStateSubject = PassthroughSubject<UserAvailabilityState, Never>()
  private let finishOnboardingSubscriber: AnySubscriber<Void, Never>
  private let finishOnboardingSubject = PassthroughSubject<Void, Never>()
  private let checkUserStateSubscriber: AnySubscriber<Void, Never>
  private let checkUserStateSubject = PassthroughSubject<Void, Never>()

  // MARK: - Initialization

  init(daemonService: DaemonUserService & DaemonControlService = DaemonControl.shared,
       finishOnboardingSubscriber: AnySubscriber<Void, Never>,
       checkUserStateSubscriber: AnySubscriber<Void, Never>) {
    self.daemonService = daemonService
    self.finishOnboardingSubscriber = finishOnboardingSubscriber
    self.checkUserStateSubscriber = checkUserStateSubscriber
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    finishOnboardingSubject
      .subscribe(finishOnboardingSubscriber)

    checkUserStateSubject
      .subscribe(checkUserStateSubscriber)

    userStateSubject
      .prefix(1)
      .sink(receiveValue: { [weak self] userAvailabilityState in
        self?.startOnboarding(state: userAvailabilityState)
      })
      .store(in: &cancellables)
  }

  // MARK: - Public

  func onOnboardingCloseWindow() {
    userStateSubject
      .prefix(1)
      .sink { [weak self] userAvailabilityState in
        defer { NSApplication.shared.reply(toApplicationShouldTerminate: true) }
        guard !userAvailabilityState.allowsClosingOnboarding else { return }
        self?.daemonService.enableService(false)
      }
      .store(in: &cancellables)

    checkUserStateSubject.send()
  }

  // MARK: - Private

  private func startChildCoordinator(_ coordinator: Coordinator, completion: (() -> Void)? = nil) {
    coordinator.completionHandler = completion
    coordinator.start()
    rootView = coordinator.rootView
  }

  private func startOnboarding(state: UserAvailabilityState) {
    switch state {
    case .storageNotGranted:
      showUserOnboarding(stage: .storageSetup)
    case .notCreatedYet:
      showUserOnboarding()
    case .error:
      assertionFailure("Should never get here as it is just for Obj-C (XPC) compatibility.")
    case .readyForUse, .readyForUseButSomeUnmounted, .storageGrantedButNotMounted:
      break
    }
  }

  private func showUserOnboarding(stage: OnboardingCoordinator.StartStage? = nil) {
    let coordinator = OnboardingCoordinator(userService: daemonService)
    coordinator.startStage = stage
    startChildCoordinator(coordinator) { [weak self] in
      self?.finishOnboardingSubject.send()
    }
  }
}
