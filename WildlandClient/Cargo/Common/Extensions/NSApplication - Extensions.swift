//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import AppKit.NSApplication
import WildlandCommon

extension NSApplication {
  func removeMenuItems(_ items: [String] = .disabledDefaultMenuTitles) {
    mainMenu?.items.forEach { item in
      guard items.contains(item.title) else { return }
      mainMenu?.removeItem(item)
    }
  }

  func window(identifier: String) -> NSWindow? {
    windows.first(where: { $0.identifier == NSUserInterfaceItemIdentifier(identifier) })
  }
}

private extension [String] {
  static let disabledDefaultMenuTitles = [
    WLStrings.appHelperMenu.Item.view,
    WLStrings.appHelperMenu.Item.window
  ]
}
