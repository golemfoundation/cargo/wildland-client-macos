//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import AppKit.NSAlert
import os
import Combine

protocol ApplicationUninstallerType {
  var uninstallDaemonPublisher: AnyPublisher<Void, Never> { get }
}

final class ApplicationUninstaller: ApplicationUninstallerType {

  private var watcher: FileWatcher {
    let filewatcher = FileWatcher()
    filewatcher.callback = { [weak self] (event: FileWatcherEvent) in
      guard let self else { return }
      guard event.dirRenamed || event.dirRemoved else { return }
      guard !FileManager.default.fileExists(atPath: event.path) else { return }

      guard let trashURL = self.trashURL()  else { return }
      let appInTrashURL = trashURL.appendingPathComponent(self._appURL.lastPathComponent, isDirectory: true)
      guard self.isInTrash(appInTrashURL) else { return }

      self.showUninstallDialog()
    }
    return filewatcher
  }

  private let _appURL = Defaults.Path.mainApp

  lazy private(set) var uninstallDaemonPublisher = uninstallDaemonSubject.eraseToAnyPublisher()
  private let uninstallDaemonSubject = PassthroughSubject<Void, Never>()
  private let userNotifications: UserNotificationsType
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  init(userNotifications: UserNotificationsType) {
    self.userNotifications = userNotifications
    setupBindings()
    watcher.start(paths: [_appURL.path], queue: .global())
  }

  // MARK: - Setup

  private func setupBindings() {
    userNotifications
      .notificationResponsePublisher
      .filter { response in
        response.category == .appUninstall && response.action == .select
      }
      .sink { [weak self] _ in
        self?.showUninstallAlert()
      }
      .store(in: &cancellables)
  }

  // MARK: - Private

  private func showUninstallAlert() {
    let alert = NSAlert()
    alert.messageText = WLStrings.appUninstaller.appUninstallRequestedAlert.title
    alert.informativeText = WLStrings.appUninstaller.appUninstallRequestedAlert.message
    alert.addButton(withTitle: WLStrings.button.yes)
    alert.addButton(withTitle: WLStrings.button.no)

    if alert.runModal() == .alertFirstButtonReturn {
      self.openFileStoragePath()
      uninstallDaemonSubject.send()
    } else {
      _log.info("user rejected to perform manual cleanup")
    }
  }

  private func showUninstallDialog() {
    userNotifications.authorizationStatus { [weak self] status in
      DispatchQueue.main.async { [weak self] in
        if status == .authorized {
          self?.userNotifications.showNotification(category: .appUninstall)
        } else {
          self?.showUninstallAlert()
        }
      }
    }
  }

  private func openFileStoragePath() {
    let filePath = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)[0]
      .appendingPathComponent(Defaults.BundleID.mainApp)
    do {
      guard (try filePath.resourceValues(forKeys: [.isDirectoryKey])).isDirectory == true else { return }
      NSWorkspace.shared.selectFile(nil, inFileViewerRootedAtPath: filePath.path)
    } catch {
      _log.error("Failed opening file path: \(filePath)")
    }
  }

  private func isInTrash(_ file: URL) -> Bool {
    var relationship: FileManager.URLRelationship = .other
    try? FileManager.default.getRelationship(&relationship, of: .trashDirectory, in: .allDomainsMask, toItemAt: file)
    return relationship == .contains
  }

  public func trashURL() -> URL? {
    return try? FileManager.default.url(for: .trashDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
  }
}
