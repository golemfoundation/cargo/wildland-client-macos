//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import os
import Combine

protocol FileProviderActionHandler {
  var showContainerPropertiesPublisher: AnyPublisher<String, Never> { get }
  var evictItemsPublisher: AnyPublisher<[String], Never> { get }
}

typealias FileProviderActionType = FileProviderActionHandler & FileProviderActionProtocol

final class FileProviderAction: FileProviderActionType {

  // MARK: - Properties

  lazy private(set) var showContainerPropertiesPublisher = showContainerPropertiesSubject.eraseToAnyPublisher()
  private let showContainerPropertiesSubject = PassthroughSubject<String, Never>()

  lazy private(set) var evictItemsPublisher = evictItemsSubject.eraseToAnyPublisher()
  private let evictItemsSubject = PassthroughSubject<[String], Never>()

  // MARK: - Public

  func performAction(action: FileAction, identifiers: [String], completion: @escaping (Error?) -> Void) {
    _log.performAction(action: action, identifiers: identifiers)
    switch action {
    case .containerProperties:
      guard let identifier = identifiers.first else { return }
      showContainerPropertiesSubject.send(identifier)
    case .makeOnlineOnly:
      evictItemsSubject.send(identifiers)
    }

    completion(nil)
  }
}

private extension Logger {
  func performAction(file: String = #file, method: String = #function, action: FileAction, identifiers: [String]) {
    let details = "action performed: \(action), identifiers: \(identifiers.joined(separator: ", "))"
    debug(file: file, method: method, details: details)
  }
}
