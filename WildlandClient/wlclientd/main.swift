//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa
import WildlandCommon

private final class AppDelegateUnitTests: NSObject, NSApplicationDelegate {}

autoreleasepool {
  let appDelegate: NSApplicationDelegate
  switch ProcessInfo.isRunningForUnitTests {
  case true:
    appDelegate = AppDelegateUnitTests()
  case false:
    guard let dependencyContainer = DependencyContainer() else { fatalError("Application cannot be configured") }
    appDelegate = MainDelegate(dependencyContainer: dependencyContainer)
  }
  NSApplication.shared.delegate = appDelegate
  NSApp.run()
}
