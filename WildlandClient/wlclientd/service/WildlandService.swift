//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import AppKit
import Foundation
import os

private let logger = Logger.xpcService()

protocol WildlandServiceType {
  func start()
  func stop()
  func stopAllTasks()
}

typealias FileProviderServiceType = FileProviderServiceProtocol & FileProviderClientProxyProtocol

final class WildlandService: NSObject, WildlandServiceType {
  private let _listener: NSXPCListener
  private var _fileProviderService: FileProviderServiceType
  private let _controlService: ControlServiceProtocol
  private let _userService: UserServiceProtocol
  private let _fileProviderAction: FileProviderActionProtocol
  private var biDirectionalConnections: [NSXPCConnection] = []

  init(with fileProviderService: FileProviderServiceType,
       controlService: ControlServiceProtocol,
       userService: UserServiceProtocol,
       fileProviderAction: FileProviderActionProtocol) {
    _listener = NSXPCListener(machServiceName: Defaults.XPCEndpoints.control)
    _controlService = controlService
    _fileProviderService = fileProviderService
    _userService = userService
    _fileProviderAction = fileProviderAction

    super.init()

    _listener.delegate = self
    _fileProviderService.clientsProxies = { [weak self] in
      self?.clientsProxies ?? []
    }
  }

  func start() {
    _listener.resume()
    logger.log("daemon: _listener service started")
  }

  func stop() {
    _listener.invalidate()
    logger.log("daemon: _listener invalidate")
  }

  func stopAllTasks() {
    _fileProviderService.cancelAllOperations()
  }
}

extension NSXPCConnection {
  var clientName: String {
    NSRunningApplication(processIdentifier: processIdentifier)?.bundleIdentifier ?? "---"
  }
}

extension WildlandService: NSXPCListenerDelegate {

  func listener(_ listener: NSXPCListener, shouldAcceptNewConnection c: NSXPCConnection) -> Bool {
    guard verifySignature(for: c, log: logger) else {
      c.invalidate()
      return false
    }
    logger.log("daemon: shouldAcceptNewConnection: \(c.processIdentifier, privacy: .public)")

    c.exportedInterface = .daemonService
    c.exportedObject = self

    c.interruptionHandler = { [weak c] in
      guard let c else { return }
      logger.interruptionHandler(client: c.clientName, pid: c.processIdentifier)
    }

    c.invalidationHandler = { [weak self, weak c] in
      guard let c else { return }
      logger.invalidationHandler(client: c.clientName, pid: c.processIdentifier)
      self?.biDirectionalConnections.removeAll { $0.processIdentifier == c.processIdentifier }
      logger.activeBidirectionalConnections(count: self?.biDirectionalConnections.count ?? 0)
    }

    // As bi-directional is not used in main app, do not care about it
    if c.clientName != Defaults.BundleID.mainApp {
      biDirectionalConnections.removeAll { $0.processIdentifier == c.processIdentifier }
      c.remoteObjectInterface = .clientService
      biDirectionalConnections.append(c)
    }

    c.resume()

    logger.establishedNewConnection(client: c.clientName)
    logger.activeBidirectionalConnections(count: biDirectionalConnections.count)
    return true
  }

  var clientsProxies: [FileProviderClientProtocol] {
    let proxies = biDirectionalConnections.compactMap { connection in
      connection.remoteObjectProxyWithErrorHandler({
        logger.errorConnectionRemoteObject(failure: $0)
      }) as? FileProviderClientProtocol
    }

    logger.fileProviderClients(count: proxies.count)

    return proxies
  }
}

extension WildlandService: DaemonServiceProtocol {

  func controlService(_ callback: @escaping ((ControlServiceProtocol) -> Void)) {
    DispatchQueue.main.async {
      callback(self._controlService)
    }
  }

  func fileProviderService(_ callback: @escaping ((FileProviderServiceProtocol) -> Void)) {
    callback(self._fileProviderService)
  }

  func fileProviderAction(_ callback: @escaping ((FileProviderActionProtocol) -> Void)) {
    DispatchQueue.main.async {
      callback(self._fileProviderAction)
    }
  }

  func userService(_ callback: @escaping ((UserServiceProtocol) -> Void)) {
    DispatchQueue.main.async {
      callback(self._userService)
    }
  }
}

private extension Logger {

  func establishedNewConnection(file: String = #file, method: String = #function, client: String) {
    let details = "XPC (from daemon) established new connection with client: \(client)"
    debug(file: file, method: method, details: details)
  }

  func interruptionHandler(file: String = #file, method: String = #function, client: String, pid: pid_t) {
    let details = "XPC (from daemon) interruption handler, client: \(client), \(pid)"
    error(file: file, method: method, details: details, failure: nil)
  }

  func invalidationHandler(file: String = #file, method: String = #function, client: String, pid: pid_t) {
    let details = "\u{2757} \u{FE0F} XPC (from daemon) invalidation handler for client: \(client), \(pid)"
    debug(file: file, method: method, details: details)
  }

  func activeBidirectionalConnections(file: String = #file, method: String = #function, count: Int) {
    let details = "XPC (from daemon) active bi-directional connections: \(count)"
    debug(file: file, method: method, details: details)
  }

  func fileProviderClients(file: String = #file, method: String = #function, count: Int) {
    let details = "XPC (from daemon) clients supporting bi-directional: \(count)"
    debug(file: file, method: method, details: details)
  }

  func errorConnectionRemoteObject(file: String = #file, method: String = #function, failure: Error) {
    let details = "XPC (from daemon) Remote object failure"
    error(file: file, method: method, details: details, failure: failure)
  }
}
