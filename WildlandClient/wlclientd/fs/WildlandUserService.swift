//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import WildlandCommon
import wildlandx

enum UserServiceError: Error {
  case emailVerificationEmptyHandle
  case userMnemonicDoNotExist
}

final class WildlandUserService: NSObject, UserServiceProtocol {

  // MARK: - Properties

  private var userMnemonic: MnemonicPayload?
  private var freeStorageHandle: FreeTierProcessHandle?
  private let userApi: UserApiProviderType
  private let containerProvider: ContainerProviderType
  private let urlProvider: TemporaryUrlProvider
  private let settings: SettingsType

  private var user: CargoUser {
    get throws {
      try userApi.user
    }
  }

  // MARK: - Initialization

  init(
    userApi: UserApiProviderType = UserApiProvider(),
    containerProvider: ContainerProviderType,
    urlProvider: TemporaryUrlProvider,
    settings: SettingsType
  ) {
    self.userApi = userApi
    self.containerProvider = containerProvider
    self.urlProvider = urlProvider
    self.settings = settings
  }

  // MARK: - Public

  func requestFreeTierStorage(with email: String, _ callback: @escaping (WildlandError?) -> Void) {
    do {
      freeStorageHandle = try userApi.user.requestFreeTierStorage(RustString(email))
      callback(nil)
    } catch {
      _log.requestFreeTierStorage(failure: error)
      callback(WildlandError(error))
    }
  }

  func verifyEmail(with code: String, _ callback: @escaping (WildlandError?) -> Void) {
    do {
      guard let handle = freeStorageHandle else {
        callback(WildlandError(UserServiceError.emailVerificationEmptyHandle))
        return
      }
      let template = try user.verifyEmail(handle, RustString(code))
      try containerProvider.createContainer(
        from: template,
        name: "Main",
        path: "/"
      )
      freeStorageHandle = nil
      callback(nil)
    } catch {
      _log.verifyEmail(failure: error)
      callback(WildlandError(error))
    }
  }

  func createMnemonic(using words: [String], _ callback: @escaping (Error?) -> Void) {
    let mnemonicWords = words.compactMap { $0.isEmpty ? nil : RustString($0) }
    do {
      userMnemonic = try userApi.createMnemonic(words: mnemonicWords)
      _log.createMnemonic()
      callback(nil)
    } catch {
      _log.createMnemonic(failure: error)
      callback(error)
    }
  }

  func generateMnemonic(_ callback: @escaping ([String], Error?) -> Void) {
    do {
      userMnemonic = try userApi.generateMnemonic()
      guard let words = try userMnemonic?.words else {
        _log.error("Failed generating mnemonic")
        callback([], UserServiceError.userMnemonicDoNotExist)
        return
      }
      _log.info("Mnemonic generating mnemonic")
      callback(words, nil)
    } catch {
      _log.error("Failed generating mnemonic: \(error.localizedDescription, privacy: .public)")
      callback([], error)
    }
  }

  func createUser(with deviceName: String, _ callback: @escaping (WildlandError?) -> Void) {
    guard let userMnemonic else {
      callback(WildlandError(UserServiceError.userMnemonicDoNotExist))
      return
    }
    do {
      let user = try userApi.createUser(from: userMnemonic, deviceName: deviceName)
      _log.info("User created: \(user.stringify().toString())")
      callback(nil)
    } catch {
      _log.createUser(failure: error)
      callback(WildlandError(error))
    }
  }

  func rootCargoUrl(_ callback: @escaping (URL?, WildlandError?) -> Void) {
    Task {
      do {
        let url = try await urlProvider.getUserVisibleURL(for: .rootContainer)
        callback(url, nil)
      } catch {
        callback(nil, WildlandError(error))
      }
    }
  }

  func setTheme(_ theme: String) {
    guard let theme = Settings.ColorThemeOption(rawValue: theme) else { return }
    settings.updateColorScheme(with: theme)
  }

  func availability(_ callback: @escaping (UserAvailabilityState, WildlandError?) -> Void) {
    Task {
      do {
        let availabilityState = try userApi.availabilityState
        callback(availabilityState, nil)
      } catch {
        callback(.error, WildlandError(error))
      }
    }
  }
}

private extension Logger {

  func createUser(file: String = #file, method: String = #function, failure: Error) {
    let details = "Failed creating user"
    error(file: file, method: method, details: details, failure: failure)
  }

  func requestFreeTierStorage(file: String = #file, method: String = #function, failure: Error) {
    let details = "Failed requesting free storage"
    error(file: file, method: method, details: details, failure: failure)
  }

  func verifyEmail(file: String = #file, method: String = #function, failure: Error) {
    let details = "Email verification failed"
    error(file: file, method: method, details: details, failure: failure)
  }

  func createMnemonic(file: String = #file, method: String = #function, failure: Error) {
    let details = "Failed creating mnemonic for the provided words"
    error(file: file, method: method, details: details, failure: failure)
  }

  func createMnemonic(file: String = #file, method: String = #function) {
    let details = "Mnemonic created for provided words"
    info(file: file, method: method, details: details)
  }
}
