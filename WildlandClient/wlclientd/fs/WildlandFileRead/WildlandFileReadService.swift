//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import WildlandCommon

/// WildlandFileReadService provides functionality for reading files in a remote file system.
struct WildlandFileReadService {

  let fileManager: FileManagerProtocol
  let fileItemProvider: FileItemProviderType

  func perform(_ identifier: String,
               contentVersion: UInt64,
               metadataVersion: UInt64,
               onCancel: @escaping (Error) -> Void) -> TransferStream {
    return AsyncThrowingStream { continuation in
      let cancelableTask = Task(priority: .background) {
        do {
          try await fileManager.readFile(identifier: identifier) { progress in
            continuation.yield(.inProgress(progress))
          }
          let fileItem = try await fileItemProvider.fileItem(for: identifier)
          continuation.complete(.completed(fileItem))
        } catch {
          let error = WildlandError(error)
          if error.isCancelledByUser {
            onCancel(error)
          } else {
            continuation.finish(throwing: error)
          }
        }
      }
      continuation.onTermination = { _ in
        cancelableTask.cancel()
      }
    }
  }
}
