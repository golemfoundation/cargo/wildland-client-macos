//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import wildlandx
import os

private final class DataOutputStream: OStream {

  // MARK: - Properties

  private let url: URL
  private let fileManager: FileManager
  private var fileHandle: Foundation.FileHandle?

  // MARK: - Initialization

  init(url: URL, fileManager: FileManager) {
    self.fileManager = fileManager
    self.url = url
  }

  func write(_ bytes: RustVec<u8>) -> OStreamResult {
    do {
      if fileHandle == nil, !fileManager.fileExists(atPath: url.path) {
        fileManager.createFile(atPath: url.path, contents: nil, attributes: nil)
        fileHandle = try FileHandle(forWritingTo: url)
      }
      try fileHandle?.write(contentsOf: bytes)
      return okOstreamResult()
    } catch {
      let code = Int32((error as NSError).code)
      let message = RustString(error.localizedDescription)
      return errOstreamResult(code, message)
    }
  }

  func close() {
    try? fileHandle?.close()
  }
}

extension WildlandFileManager {
  func readFile(identifier: String, progressHandler: @escaping ((ProgressType) -> Void)) async throws {
    try measureTimeElapsed(log: _log) {
      let path = try path(for: identifier)
      try checkPathValidity(path)

      let temporaryUrl = try temporaryUrlProvider.url(for: identifier, bundleId: bundleID)
      let outputStream = DataOutputStream(url: temporaryUrl, fileManager: fileManager)
      defer { outputStream.close() }

      let abortFlag = newAbortFlag()
      let metadata = try dfs.metadata(RustString(path))
      let fileSize = Int(metadata.size())

      progressHandler((0, fileSize))

      let reporter = FileProgressReporter { [weak abortFlag] type in
        if Task.isCancelled { abortFlag?.set() }
        progressHandler(type)
      }

      _ = try dfs.download(RustString(path), outputStream, reporter, abortFlag)
    }
  }
}
