//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import wildlandx
import WildlandCommon
import os

typealias FileManagerType = FileManagerProtocol &
                            FileProviderClientProtocol &
                            WildlandFileManagerTrackerProtocol

final class WildlandFileManager: FileManagerType {

  // MARK: - Properties

  let dfs = WildlandX.cargo.dfsApi()
  let fileItemProvider: FileItemProviderType
  let fileManager: FileManager
  let temporaryUrlProvider: TemporaryUrlProvider
  private let operationListener: FileOperationListener
  private var identifierTracker: IdentifierTrackerProtocol

  // MARK: - Initialization

  init(operationListener: FileOperationListener,
       identifierTracker: IdentifierTrackerProtocol,
       fileItemProvider: FileItemProviderType,
       fileManager: FileManager = .default,
       temporaryUrlProvider: TemporaryUrlProvider) {
    self.operationListener = operationListener
    self.fileItemProvider = fileItemProvider
    self.identifierTracker = identifierTracker
    self.fileManager = fileManager
    self.temporaryUrlProvider = temporaryUrlProvider

    Task {
      await identifierTracker.startContentUpdate()
    }
  }
}

extension WildlandFileManager {

  func path(for identifier: String) throws -> String {
    guard identifier != Defaults.Identifier.root else { return Defaults.Path.root }
    return try dfs.getPath(RustString(identifier)).toString()
  }

  func checkPathValidity(_ path: String, method: String = #function) throws {
    guard !path.isEmpty else {
      throw FileManagerError.invalidPath(method: method)
    }
  }
}

// MARK: - FileProviderClientProtocol

extension WildlandFileManager {
  // `operationListener` might not be interested in every of the below notifications,
  // but all are forwarded anyway

  func receiveInfoResponseSuccess(taskId: String, fileItem: FileItem) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveDeleteResponseSuccess(taskId: String) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveModifyResponseSuccess(taskId: String, fileItem: FileItem, fetchContent: Bool) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveListResponseSuccess(taskId: String, items: [FileItem], cursor: UInt64) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveResponseFailure(taskId: String, filename: String?, error: Error) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid, input: .init(filename: filename, error: error))
  }

  func receiveResponseProgress(taskId: String, identifier: String, offset: Int, size: Int) {
    let metadata = FileOperationMetadata(completedUnitCount: offset, totalUnitCount: size)
    operationListener.setOperationMetadata(metadata, identifier: identifier)
  }

  func receiveListUpdatesResponseSuccess(taskId: String, itemUpdates: FileItemUpdates) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveCurrentAnchor(taskId: String, anchor: Data) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveResponseCancelConfirmation(taskId: String, error: Error) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid, input: .init(filename: nil, error: error))
  }

  func receiveStart(taskId: String, identifier: String, filename: String?, operationType: OperationType) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    Task {
      operationListener.startOperation(
        taskId: uuid,
        identifier: identifier,
        filename: await prepareFilename(identifier: identifier, filename: filename),
        type: operationType
      )
    }
  }

  private func prepareFilename(identifier: String, filename: String?) async -> String {
    if let filename {
      return filename
    }
    do {
      return try await fileItemProvider.fileItem(for: identifier, path: nil, metadata: nil).name
    } catch {
      return identifier
    }
  }
}

extension WildlandFileManager {

  func anchor() async -> Data {
    await identifierTracker.anchor()
  }

  func startObserving(identifier: String) async {
    await identifierTracker.startObserving(identifier: identifier)
  }

  func stopObserving(identifier: String) async {
    await identifierTracker.stopObserving(identifier: identifier)
  }

  func itemUpdates(anchorData: Data) async -> FileItemUpdates {
    await identifierTracker.itemUpdates(anchorData: anchorData)
  }
}
