//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import WildlandCommon

typealias TaskType = Task<(), Never>

typealias ProviderServiceProtocol = FileProviderServiceProtocol & FileProviderClientProxyProtocol

final class WildlandFileProviderService: ProviderServiceProtocol {

  var clientsProxies: (() -> [FileProviderClientProtocol])?
  let fileManager: FileManagerType
  let fileItemProvider: FileItemProviderType
  let fileItemStorage: FileItemStorageType

  init(fileManager: FileManagerType,
       fileItemProvider: FileItemProviderType,
       fileItemStorage: FileItemStorageType) {
    self.fileManager = fileManager
    self.fileItemProvider = fileItemProvider
    self.fileItemStorage = fileItemStorage
  }

  // TODO: Leave it for now, better to go with actor with (non-actor) handler property (remove SynchronizedProperty afterwards)
  var taskManager = SynchronizedProperty<[UUID: TaskType]>()

  // Computed property as client proxy might change dynamically
  var listeners: [FileProviderClientProtocol] {
    var proxies = clientsProxies?() ?? []
    proxies.append(fileManager)
    return proxies
  }
}

extension SynchronizedProperty where Value == [UUID: TaskType] {

  func store(taskId: UUID, with task: Value.Value?) {
    var storage = get() ?? [:]
    storage[taskId] = task
    set(storage)
  }

  func stored(for taskId: UUID) -> Value.Value? {
    get()?[taskId]
  }

  func cancel(taskId: UUID) {
    var tasks = get()
    tasks?[taskId]?.cancel()
    tasks?[taskId] = nil
    set(tasks)
  }

  func cancelAllTasks() {
    var tasks = get() ?? [:]
    for uuid in tasks.keys {
      tasks[uuid]?.cancel()
      tasks[uuid] = nil
    }
    set(tasks)
  }
}

extension Array where Element == FileProviderClientProtocol {

  func receiveInfoResponseSuccess(taskId: String, fileItem: FileItem) {
    forEach { $0.receiveInfoResponseSuccess(taskId: taskId, fileItem: fileItem) }
  }

  func receiveDeleteResponseSuccess(taskId: String) {
    forEach { $0.receiveDeleteResponseSuccess(taskId: taskId) }
  }

  func receiveModifyResponseSuccess(taskId: String, fileItem: FileItem, fetchContent: Bool) {
    forEach { $0.receiveModifyResponseSuccess(taskId: taskId, fileItem: fileItem, fetchContent: fetchContent) }
  }

  func receiveListResponseSuccess(taskId: String, items: [FileItem], cursor: UInt64) {
    forEach { $0.receiveListResponseSuccess(taskId: taskId, items: items, cursor: cursor) }
  }

  func receiveResponseFailure(taskId: String, filename: String?, error: Error) {
    forEach { $0.receiveResponseFailure(taskId: taskId, filename: filename, error: error) }
  }

  func receiveResponseProgress(taskId: String, identifier: String, offset: Int, size: Int) {
    forEach { $0.receiveResponseProgress(
      taskId: taskId,
      identifier: identifier,
      offset: offset,
      size: size)
    }
  }

  func receiveItemUpdateResponseSuccess(taskId: String, itemUpdates: FileItemUpdates) {
    forEach { $0.receiveListUpdatesResponseSuccess(taskId: taskId, itemUpdates: itemUpdates) }
  }

  func receiveCurrentAnchor(taskId: String, anchor: Data) {
    forEach { $0.receiveCurrentAnchor(taskId: taskId, anchor: anchor) }
  }

  func receiveResponseCancelConfirmation(taskId: String, error: Error) {
    forEach { $0.receiveResponseCancelConfirmation(taskId: taskId, error: error) }
  }

  func receiveStart(taskId: String, identifier: String, filename: String? = nil, operationType: OperationType) {
    forEach {
      $0.receiveStart(taskId: taskId, identifier: identifier, filename: filename, operationType: operationType)
    }
  }
}

extension Logger {

  func infoProgressProviderService(taskId: String, method: String = #function, file: String = #file, value: ProgressType) {
    let details = "daemon: identifier: \(taskId), in progress: \(Double(value.offset) / Double(value.size))"
    info(file: file, method: method, details: details)
  }

  func infoCompletedProviderService(taskId: String, method: String = #function, file: String = #file) {
    let details = "daemon: identifier: \(taskId), completed"
    info(file: file, method: method, details: details)
  }

  func errorProviderService(taskId: String, method: String = #function, file: String = #file, failure: Error) {
    let details = "daemon: identifier: \(taskId)"
    error(file: file, method: method, details: details, failure: failure)
  }
}
