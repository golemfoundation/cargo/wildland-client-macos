//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

protocol WildlandFilePathManagerProtocol {
  @discardableResult func insert(path: String, isFolder: Bool) -> Bool
  func insertObserved(path: String)
  func children(for path: String) -> Set<String>
  @discardableResult func remove(path: String) -> Bool
  func searchForParentsAndChildren(path: String) -> (parents: Set<String>, children: Set<String>)
  func allObservedPath() -> Set<String>
  func updateAnchor()
  func itemExist(at path: String) -> Bool
  func getMetadata(for path: String) -> Metadata?
  func set(metadata: Metadata, for path: String)
  func getContent(for path: String) -> Data?
  func set(content: Data, for path: String)
  func removeContent(for path: String)

  var anchor: Data? { get }
}

final class WildlandFilePathManager: WildlandFilePathManagerProtocol {

  final class FilePathItem: Equatable, Hashable, CustomStringConvertible {
    enum ItemType {
      case file
      case folder
    }

    let name: String
    var children: Set<FilePathItem>
    let type: ItemType

    init(name: String, type: ItemType, children: Set<FilePathItem>) {
      self.name = name
      self.children = children
      self.type = type
    }

    var description: String {
      let childrenString = children.isEmpty ? "" : ":\(children)"
      return "\"\(name)\"" + childrenString
    }

    // MARK: - Protocol confirmation
    static func == (lhs: FilePathItem, rhs: FilePathItem) -> Bool {
      lhs.name == rhs.name
    }

    func hash(into hasher: inout Hasher) {
      hasher.combine(name)
    }
  }

  private var pathRoot: FilePathItem = FilePathItem(name: "/", type: .folder, children: [])
  private var pathDataContent: [String: Data] = [:]
  private var pathMetadata: [String: Metadata] = [:]
  private var observedItemPaths: Set<String> = [ "/" ]

  private(set) var anchor: Data? = "0".data(using: .utf8)

  // Anchor management

  func updateAnchor() {
    anchor = "\(Date().timeIntervalSince1970)".data(using: .utf8)
  }

  // Path management

  @discardableResult
  func insert(path: String, isFolder: Bool) -> Bool {
    guard !path.isEmpty else { return false }

    let destinationPath = URL(fileURLWithPath: path).deletingLastPathComponent()
    let type: FilePathItem.ItemType = isFolder ? .folder : .file
    let newItem = FilePathItem(name: path, type: type, children: [])

    return insert(item: newItem, to: destinationPath.path, node: pathRoot)
  }

  func children(for path: String) -> Set<String> {
    children(for: path, node: pathRoot)
  }

  @discardableResult
  func remove(path: String) -> Bool {
    guard !path.isEmpty else { return false }

    let parentPath = URL(fileURLWithPath: path).deletingLastPathComponent()
    observedItemPaths.remove(path)
    return remove(path: path, parentPath: parentPath.path, parent: pathRoot)
  }

  func searchForParentsAndChildren(path: String) -> (parents: Set<String>, children: Set<String>) {
    searchForChildTypeNodes(for: path, node: pathRoot)
  }

  func insertObserved(path: String) {
    observedItemPaths.insert(path)
  }

  // Metadata management

  func getMetadata(for path: String) -> Metadata? {
    pathMetadata[path]
  }

  func set(metadata: Metadata, for path: String) {
    pathMetadata[path] = metadata
  }

  // Content management

  func getContent(for path: String) -> Data? {
    pathDataContent[path]
  }

  func set(content: Data, for path: String) {
    pathDataContent[path] = content
  }

  func removeContent(for path: String) {
    pathDataContent.removeValue(forKey: path)
  }

  // Path management

  func allObservedPath() -> Set<String> {
    observedItemPaths
  }

  func itemExist(at path: String) -> Bool {
    itemExist(at: path, node: pathRoot)
  }

  // MARK: - Private

  private func insert(item: FilePathItem, to path: String, node: FilePathItem) -> Bool {
    guard !path.isEmpty else { return false }

    if node.name == path {
      var children = node.children
      children.insert(item)
      node.children = children
      return true
    } else {
      for child in node.children {
        if insert(item: item, to: path, node: child) {
          return true
        }
      }
    }
    return false
  }

  private func itemExist(at path: String, node: FilePathItem) -> Bool {
    guard !path.isEmpty else { return false }

    if node.name == path {
      return true
    } else {
      for child in node.children {
        if itemExist(at: path, node: child) {
          return true
        }
      }
    }
    return false
  }

  private func searchForChildTypeNodes(for path: String, node: FilePathItem) -> (parents: Set<String>,
                                                                                 children: Set<String>) {
    guard !path.isEmpty else { return ([], []) }

    var branches: Set<String> = []
    var leaves: Set<String> = []
    if node.name == path {
      if node.children.isEmpty {
        leaves.insert(path)
      } else {
        branches.insert(path)
        for child in node.children {
          let (subBranches, subLeaves) = searchForChildTypeNodes(for: child.name, node: node)
          branches.formUnion(subBranches)
          leaves.formUnion(subLeaves)
        }
      }
    } else {
      for child in node.children {
        let (subBranches, subLeaves) = searchForChildTypeNodes(for: path, node: child)
        branches.formUnion(subBranches)
        leaves.formUnion(subLeaves)
      }
    }
    return (branches, leaves)
  }

  private func remove(path: String, parentPath: String, parent: FilePathItem) -> Bool {
    guard !path.isEmpty else { return false }

    if parent.name == parentPath {
      if let item = parent.children.first(where: { $0.name == path }) {
        parent.children.remove(item)
        removeContent(for: path)
        return true
      }
    } else {
      for child in parent.children {
        if remove(path: path, parentPath: parentPath, parent: child) {
          return true
        }
      }
    }
    return false
  }

  private func children(for path: String, node: FilePathItem) -> Set<String> {
    guard !path.isEmpty else { return [] }

    var nodes: Set<String> = []
    if node.name == path {
      let pathItems = node.children.map { $0.name }
      nodes.formUnion(pathItems)
    } else {
      for child in node.children {
        let pathItems = children(for: path, node: child)
        nodes.formUnion(pathItems)

        if !pathItems.isEmpty { break }
      }
    }
    return nodes
  }
}
