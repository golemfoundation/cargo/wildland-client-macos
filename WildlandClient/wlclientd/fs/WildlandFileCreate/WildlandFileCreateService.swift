//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import WildlandCommon

/// WildlandFileCreateService provides functionality for creating files and folders in a remote file system.
struct WildlandFileCreateService {

  let fileManager: FileManagerProtocol
  let fileItemProvider: FileItemProviderType

  // MARK: Public

  func perform(itemInfo: CreateItemInfo, onCancel: @escaping (Error) -> Void) -> TransferStream {
    createItem(itemInfo: itemInfo, onCancel: onCancel)
  }

  private func createItem(itemInfo: CreateItemInfo, onCancel: @escaping (Error) -> Void) -> TransferStream {
    AsyncThrowingStream { continuation in
      let cancelableTask = Task(priority: .background) {
        do {
          guard let entryType = FileProviderDomainService.Entry.EntryType(rawValue: itemInfo.type) else {
            throw FileManagerError.nonSupportedType
          }

          guard let conflictStrategy = FileItem.ItemConflictStrategy(rawValue: itemInfo.strategy) else {
            throw FileManagerError.nonSupportedConflictStrategy
          }

          let fullPath = try fileManager.path(for: itemInfo.parentIdentifier).appendPathItemName(itemInfo.name)

          if entryType != .root,
             conflictStrategy == .updateAlreadyExisting,
             FileManager.default.fileExists(atPath: fullPath) {
            let url = URL(string: fullPath) ?? URL(fileURLWithPath: fullPath)
            try FileManager.default.removeItem(at: url)
          }

          switch entryType {
          case .file:
            try await fileManager.writeFile(
              path: fullPath,
              identifier: itemInfo.temporaryIdentifier,
              creationDate: itemInfo.creationDate) { progress in
                _log.debugYield(value: progress)
                continuation.yield(.inProgress(progress))
              }
          case .folder:
            try await fileManager.createDirectory(for: fullPath)
          case .root:
            break
          }

          let item = try await fileItemProvider.createFileItem(path: fullPath)
          _log.debugYield()
          continuation.complete(.completed(item))
        } catch {
          let error = WildlandError(error)
          if error.isCancelledByUser {
            onCancel(error)
          } else {
            continuation.finish(throwing: error)
          }
        }
      }
      continuation.onTermination = { _ in
        cancelableTask.cancel()
      }
    }
  }
}

private extension Logger {

  func debugYield(file: String = #file, method: String = #function, value: ProgressType) {
    let details = "stream yield in progress: \(Double(value.offset) / Double(value.size))"
    debug(file: file, method: method, details: details)
  }

  func debugYield(file: String = #file, method: String = #function) {
    let details = "stream yield completed"
    debug(file: file, method: method, details: details)
  }
}
