//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import WildlandCommon
import wildlandx
import os
import FileProvider.NSFileProviderItem

private final class DataInputStream: IStream {

  private let fileUrl: URL
  private var fileHandle: Foundation.FileHandle?
  private let fileSize: UInt64

  init(fileUrl: URL, fileSize: UInt64) {
    self.fileUrl = fileUrl
    self.fileSize = fileSize
  }

  func read(_ bytes_count: usize) -> IStreamResult {
    do {
      if fileHandle == nil {
        fileHandle = try FileHandle(forReadingFrom: fileUrl)
      }
      guard let fileHandle else { return errIstreamResult(0, RustString("fileHandle not set")) }

      return autoreleasepool {
        let chunk = fileHandle.readData(ofLength: Int(bytes_count))
        let result = RustVec<UInt8>(chunk)

        guard result.count == chunk.count else { return errIstreamResult(0, RustString("Failed converting data")) }

        return okIstreamResult(result)
      }
    } catch {
      let code = Int32((error as NSError).code)
      let message = RustString(error.localizedDescription)
      return errIstreamResult(code, message)
    }
  }

  func totalSize() -> usize {
    UInt(fileSize)
  }

  func close() {
    try? fileHandle?.close()
  }
}

extension WildlandFileManager {

  func writeFile(path: String,
                 identifier: String,
                 creationDate: Date?,
                 progressHandler: @escaping ((ProgressType) -> Void)) async throws {
    try await measureTimeElapsed(log: _log) {
      try checkPathValidity(path)
      let itemIdentifier = NSFileProviderItemIdentifier(rawValue: identifier)
      let fileUrl = try await temporaryUrlProvider.getUserVisibleURL(for: itemIdentifier)
      try await performWriteFile(
        path: path,
        fileUrl: fileUrl,
        creationDate: creationDate,
        progressHandler: progressHandler
      )
    }
  }

  func createDirectory(for path: String) async throws {
    try checkPathValidity(path)
    _ = try dfs.createDir(RustString(path))
  }

  private func performWriteFile(path: String,
                                fileUrl: URL,
                                creationDate: Date?,
                                progressHandler: @escaping (ProgressType) -> Void) async throws {
    let abortFlag = newAbortFlag()
    let fileSize = try fileSize(of: fileUrl)
    let creationTime = UnixTimestamp.create(from: creationDate)
    let inputStream = DataInputStream(fileUrl: fileUrl, fileSize: fileSize)
    defer { inputStream.close() }

    let reporter = FileProgressReporter { [weak self] type in
      if Task.isCancelled || self?.fileManager.fileExists(atPath: fileUrl.path) == false {
        abortFlag.set()
      }
      progressHandler(type)
    }

    guard !Task.isCancelled else { return }

    progressHandler((0, Int(inputStream.totalSize())))
    _ = try dfs.upload(RustString(path), inputStream, reporter, abortFlag, creationTime)
  }

  private func fileSize(of url: URL) throws -> UInt64 {
    let fileAttributes = try fileManager.attributesOfItem(atPath: url.path)
    return fileAttributes[.size] as? UInt64 ?? .zero
  }
}
