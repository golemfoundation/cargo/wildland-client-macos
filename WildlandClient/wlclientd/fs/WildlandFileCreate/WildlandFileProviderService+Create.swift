//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import WildlandCommon

extension WildlandFileProviderService {

  func createItem(taskId: String, itemInfo: CreateItemInfo) {
    Task {
      guard let uuid = UUID(uuidString: taskId),
            await taskManager.stored(for: uuid) == nil else { return }

      _log.startCreate(taskId: taskId, name: itemInfo.parentIdentifier + "/" + itemInfo.name, type: itemInfo.type)

      let cancelableTask = Task(priority: .high) {
        await perform(taskId: taskId, itemInfo: itemInfo)
      }
      await taskManager.store(taskId: uuid, with: cancelableTask)
    }
  }

  private func perform(taskId: String, itemInfo: CreateItemInfo) async {
    do {
      let operation = WildlandFileCreateService(fileManager: fileManager, fileItemProvider: fileItemProvider)
      // If operation throws error, it is only caused because task is cancelled.
      listeners.receiveStart(
        taskId: taskId,
        identifier: itemInfo.temporaryIdentifier,
        filename: itemInfo.name,
        operationType: .transfer
      )
      for try await status in operation.perform(itemInfo: itemInfo, onCancel: { [weak self] in
        self?.listeners.receiveResponseCancelConfirmation(taskId: taskId, error: $0)
      }) {
        switch status {
        case .inProgress(let progress):
          _log.infoProgressProviderService(taskId: taskId, value: progress)
          listeners.receiveResponseProgress(
            taskId: taskId,
            identifier: itemInfo.temporaryIdentifier,
            offset: progress.offset,
            size: progress.size
          )
        case .completed(let item):
          _log.infoCompletedProviderService(taskId: taskId)
          listeners.receiveInfoResponseSuccess(taskId: taskId, fileItem: item)
        }
      }
    } catch {
      _log.errorProviderService(taskId: taskId, failure: error)
      listeners.receiveResponseFailure(taskId: taskId, filename: itemInfo.name, error: error)
    }
  }
}

private extension Logger {
  func startCreate(file: String = #file, method: String = #function, taskId: String, name: String, type: String) {
    let details = "starting creating item..., identifier: \(taskId), name: \(name), type: \(type)"
    debug(file: file, method: method, details: details)
  }
}
