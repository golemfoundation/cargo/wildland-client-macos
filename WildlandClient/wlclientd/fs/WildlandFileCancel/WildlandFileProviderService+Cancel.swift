//
// Wildland Project
//
// Copyright © 2023 Golem Foundation,
//              Vladimir Vashurkin <vladimir@wildland.io>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import os

extension WildlandFileProviderService {

  func cancelOperation(taskId: String) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    Task {
      _log.cancelOperation(taskId: taskId)
      await taskManager.cancel(taskId: uuid)
    }
  }

  func cancelAllOperations() {
    Task {
      _log.cancelAllOperation()
      await taskManager.cancelAllTasks()
    }
  }
}

private extension Logger {

  func cancelOperation(taskId: String, method: String = #function, file: String = #file) {
    let details = "daemon: cancelling operation with taskId: \(taskId)"
    debug(file: file, method: method, details: details)
  }

  func cancelAllOperation(method: String = #function, file: String = #file) {
    let details = "daemon: cancelling all operations"
    debug(file: file, method: method, details: details)
  }
}
