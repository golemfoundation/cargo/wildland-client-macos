//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import WildlandCommon
import os

protocol IdentifierTrackerProtocol {
  func anchor() async -> Data
  func startObserving(identifier: String) async
  func stopObserving(identifier: String) async
  func itemUpdates(anchorData: Data) async -> FileItemUpdates

  func startContentUpdate(with duration: Int, repeatable: Bool) async
}

extension IdentifierTrackerProtocol {
  func startContentUpdate(with duration: Int = 10, repeatable: Bool = true) async {
    await startContentUpdate(with: duration, repeatable: repeatable)
  }
}

actor IdentifierTracker {

  // MARK: - Properties

  private var anchor: Data = "0".data(using: .utf8) ?? Data()
  private let fileProviderManager: FileProviderSignalEnumerator
  private var scheduledItemsForUpdate: FileItemUpdates?
  private var observed: [String] = []
  private let fileItemProvider: FileItemProviderType
  private let fileItemStorage: FileItemStorageType

  // MARK: - Initialization

  init(fileProviderManager: FileProviderSignalEnumerator,
       fileItemProvider: FileItemProviderType,
       fileItemStorage: FileItemStorageType) {
    self.fileProviderManager = fileProviderManager
    self.fileItemProvider = fileItemProvider
    self.fileItemStorage = fileItemStorage
  }

  // MARK: - Public

  func startContentUpdate(with duration: Int, repeatable: Bool) async {
    repeat {
      try? await Task.sleep(for: .seconds(duration))
      await reloadContent()
    } while repeatable
  }

  // MARK: - Private

  private func reloadContent() async {
    guard let updates = await checkForItemUpdates() else { return }
    refreshScheduledUpdates(using: updates)

    fileProviderManager.signalEnumerator(for: .workingSet, completionHandler: { error in
      _log.signalEnumerator(failure: error)
    })
  }

  // MARK: - Public

  public func refreshScheduledUpdates(using updates: FileItemUpdates) {
    if scheduledItemsForUpdate == nil {
      scheduledItemsForUpdate = updates
    }

    var deletedIdentifiers = Set(updates.deletedIdentifiers)
    deletedIdentifiers.formUnion(scheduledItemsForUpdate?.deletedIdentifiers ?? [])

    var updatedItems = Set(updates.addedOrUpdatedItems)
    updatedItems.formUnion(scheduledItemsForUpdate?.addedOrUpdatedItems ?? [])

    scheduledItemsForUpdate = FileItemUpdates(
      anchor: anchor,
      updateItems: Array(updatedItems),
      deletedIdentifiers: Array(deletedIdentifiers)
    )
  }

  private func checkForItemUpdates() async -> FileItemUpdates? {
    let result = await prepareDifferences()
    guard !result.isEmpty else { return nil }
    return FileItemUpdates(
      anchor: anchor,
      updateItems: Array(result.updated),
      deletedIdentifiers: Array(result.deleted)
    )
  }

  // MARK: - Private

  private func prepareDifferences() async -> TrackerResult {
    var result = TrackerResult()
    for identifier in Set(observed) {
      let cachedItems = await fileItemStorage.fileItems(for: identifier)
      if let fetchedItems = await fileItems(for: identifier) {
        await applyChanges(cachedItems: cachedItems, fetchedItems: fetchedItems, result: &result)
      } else {
        await untrackItems(cachedItems.map(\.identifier))
        await stopObserving(identifier: identifier)
      }
    }
    return result
  }

  private func fileItems(for parentIdentifier: String) async -> [FileItem]? {
    do {
      return try await fileItemProvider.fileItems(for: parentIdentifier)
    } catch {
      return nil
    }
  }

  private func applyChanges(cachedItems: [FileItem], fetchedItems: [FileItem], result: inout TrackerResult) async {
    let localItems = Set(cachedItems.map(TrackedItem.init))
    let serverItems = Set(fetchedItems.map(TrackedItem.init))

    let deleted = localItems.subtracting(serverItems)
    await untrackItems(deleted.map(\.identifier))

    let added = serverItems.subtracting(localItems)
    let updated = serverItems.intersection(localItems).filter { serverItem in
      localItems.contains {
        $0.identifier == serverItem.identifier && $0.modificationDate != serverItem.modificationDate
      }
    }
    let unionItems = updated.union(added)
    let updatedItems = fetchedItems
      .filter { fileItem in
        unionItems.contains(where: { $0.identifier == fileItem.identifier })
      }
    await trackItems(updatedItems)

    result.deleted = result.deleted.union(deleted.map(\.identifier))
    result.updated = result.updated.union(updatedItems)
  }

  private func untrackItems(_ identifiers: [String]) async {
    for identifier in identifiers {
      await fileItemStorage.remove(identifier: identifier)
    }
  }

  private func trackItems(_ items: [FileItem]) async {
    for item in items {
      await fileItemStorage.store(item)
    }
  }

  private func itemUpdates(for anchorData: Data) -> FileItemUpdates {
    guard let updates = scheduledItemsForUpdate else {
      return .init(anchor: anchorData, updateItems: [], deletedIdentifiers: [])
    }
    scheduledItemsForUpdate = nil
    updateAnchor()
    return updates
  }

  private func updateAnchor() {
    anchor = "\(Date().timeIntervalSince1970)".data(using: .utf8) ?? Data()
  }
}

extension IdentifierTracker: IdentifierTrackerProtocol {

  func anchor() async -> Data {
    anchor
  }

  func startObserving(identifier: String) async {
    observed.append(identifier)
  }

  func stopObserving(identifier: String) async {
    observed.removeFirst(identifier)
  }

  func itemUpdates(anchorData: Data) async -> FileItemUpdates {
    itemUpdates(for: anchorData)
  }
}

private struct TrackerResult {
  var deleted: Set<String> = []
  var updated: Set<FileItem> = []

  var isEmpty: Bool {
    deleted.isEmpty && updated.isEmpty
  }
}

private extension Logger {

  func signalEnumerator(method: String = #function, failure: Error?) {
    if let failure {
      error(method: method, details: "Failed scheduling update for observed folders", failure: failure)
    } else {
      debug(method: method, details: "Scheduled update for observed folders")
    }
  }
}
