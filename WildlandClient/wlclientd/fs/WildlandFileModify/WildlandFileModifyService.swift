//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import WildlandCommon

/// WildlandFileModifyService provides functionality for modifying files in a remote file system.
struct WildlandFileModifyService {

  let fileManager: FileManagerProtocol

  func renameItem(path: String, newPath: String) async throws {
    try await fileManager.rename(path: path, newPath: newPath)
    _log.modify(path: path)
  }

  /// Modifies the content of the item with the specified identifier and path.
  /// Updates the file with the specified data and reports the progress of the update operation\
  /// through the progressHandler closure.
  func modifyContent(_ path: String,
                     identifier: String,
                     progressHandler: @escaping ((ProgressType) -> Void)) async throws {
    try await fileManager.writeFile(
      path: path,
      identifier: identifier,
      creationDate: nil,
      progressHandler: progressHandler
    )
    _log.modify(path: path)
  }
}

private extension Logger {
  func modify(file: String = #file, method: String = #function, path: String) {
    let details = "modify operation success, path: \(path)"
    debug(file: file, method: method, details: details)
  }
}
