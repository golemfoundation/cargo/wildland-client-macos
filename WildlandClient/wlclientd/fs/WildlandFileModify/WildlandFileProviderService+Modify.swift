//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import WildlandCommon

extension WildlandFileProviderService {

  func renameItem(taskId: String, identifier: String, name: String) {
    performOperation(taskId: taskId, identifier: identifier, operation: { service, path in
      let newPath = PathComposer().rename(from: path, name: name)
      try await service.renameItem(path: path, newPath: newPath)
    })
  }

  func moveItem(taskId: String, identifier: String, parentIdentifier: String) {
    performOperation(taskId: taskId, identifier: identifier, operation: { service, path in
      let parentPath = try self.fileManager.path(for: parentIdentifier)
      let newPath = PathComposer().move(path: path, to: parentPath)
      try await service.renameItem(path: path, newPath: newPath)
    })
  }

  func modifyItemContent(taskId: String, identifier: String) {
    listeners.receiveStart(taskId: taskId, identifier: identifier, operationType: .transfer)
    performOperation(taskId: taskId, identifier: identifier, operation: { service, path in
      try await service.modifyContent(path, identifier: identifier) { [weak self] progress in
        _log.infoProgressProviderService(taskId: taskId, value: progress)
        self?.listeners.receiveResponseProgress(
          taskId: taskId,
          identifier: identifier,
          offset: progress.offset,
          size: progress.size
        )
      }
    })
  }

  private func performOperation(taskId: String, identifier: String, operation: @escaping (WildlandFileModifyService, String) async throws -> Void) {
    Task {
      guard let uuid = UUID(uuidString: taskId),
            await taskManager.stored(for: uuid) == nil else { return }

      _log.startPerformOperation(taskId: taskId)

      let cancelableTask = Task {
        do {
          let service = WildlandFileModifyService(fileManager: fileManager)
          let path = try fileManager.path(for: identifier)
          try await operation(service, path)
          let item = try await fileItemProvider.updateFileItem(identifier: identifier)
          _log.infoCompletedProviderService(taskId: taskId)
          listeners.receiveModifyResponseSuccess(taskId: taskId, fileItem: item, fetchContent: true)
        } catch {
          _log.errorProviderService(taskId: taskId, failure: error)
          let error = WildlandError(error)
          if error.isCancelledByUser {
            listeners.receiveResponseCancelConfirmation(taskId: taskId, error: error)
          } else {
            listeners.receiveResponseFailure(taskId: taskId, filename: nil, error: error)
          }
        }
      }
      await taskManager.store(taskId: uuid, with: cancelableTask)
    }
  }
}

private extension Logger {
  func startPerformOperation(taskId: String, method: String = #function) {
    let details = "starting file modification..., identifier: \(taskId)"
    debug(method: method, details: details)
  }
}
