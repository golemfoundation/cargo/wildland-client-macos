//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Combine
import WildlandCommon

final class WildlandSynchronizationStateProvider: SynchronizationStateProvider {

  // MARK: - Properties

  private(set) lazy var statePublisher = stateSubject.eraseToAnyPublisher()
  private let stateSubject = CurrentValueSubject<SynchronizationState, Never>(.synced)
  private let globalProgressObserver: FileProviderGlobalProgressObserverType
  private let fileOperationListener: FileOperationListener

  // MARK: - Initialization

  init(globalProgressObserver: FileProviderGlobalProgressObserverType,
       fileOperationListener: FileOperationListener) {
    self.globalProgressObserver = globalProgressObserver
    self.fileOperationListener = fileOperationListener
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    Publishers.CombineLatest(
      globalProgressObserver.inProgressPublisher,
      fileOperationListener.operationsPublisher
    )
    .map(SynchronizationState.init)
    .subscribe(AnySubscriber(stateSubject))
  }
}

private extension SynchronizationState {
  init(inProgress: Bool, operations: [FileOperationStorage]) {
    guard inProgress || !operations.isEmpty else { self = .synced; return }
    let syncProgress = SyncProgress(
      files: operations.filenames,
      currentSize: Measurement(value: operations.currentSize, unit: .bytes),
      totalSize: Measurement(value: operations.totalSize, unit: .bytes),
      estimate: Measurement(value: operations.estimation, unit: .seconds)
    )
    self = .syncing(syncProgress)
  }
}
