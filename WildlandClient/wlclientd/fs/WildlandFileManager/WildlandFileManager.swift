//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
import Foundation
import wildlandx
import WildlandCommon
import os

public enum FileManagerError: Error {
  case userNotFound
  case userCancelled
  case containerPathAddFailed
  case containerPathRenameFailed
  case directoryReadFailed
  case directoryRemoveFailed
  case directoryRemoveFailedNotEmpty // TODO: Add after handling of deleting a populated folder is added
  case fileWriteFailed
  case invalidPath(method: String = #function)
  case fileUpdateFailed(path: String)
  case fileUpdateFailedInvalidContent(identifier: String, path: String)
  case fileRemoveFailed
  case metadataReadFailed
  case nonSupportedConflictStrategy
  case nonSupportedType
  case pathOpenTimeUpdateFailed(path: String)
  case pathContentTimeUpdateFailed(path: String)
  case pathRenameFailed(path: String)
}

public struct Item: Hashable {
  let name: String
}

public struct Metadata {
  let size: UInt
  let type: UInt32
  let accessTime: UInt64
  let changeTime: UInt64
  let modificationTime: UInt64
}

final class FileProgressReporter: ProgressReporter {

  private let progressHandler: (ProgressType) -> Void

  init(handler: @escaping (ProgressType) -> Void) {
    progressHandler = handler
  }

  func report(_ completed: usize, _ total: usize, _ unit: ProgressUnit) {
    progressHandler((offset: Int(completed), size: Int(total)))
  }
}

public protocol WildlandFileManagerProtocol {
  func readDirectory(for path: String) -> Result<[Item], FileManagerError>
  func createDirectory(for path: String) async throws
  func removeDirectory(path: String, recursive: Bool) async throws
  func readFile(item: Item, progressHandler: @escaping ((ProgressType) -> Void)) async throws -> Data
  func writeFile(path: String, data: Data, progressHandler: @escaping ((ProgressType) -> Void)) async throws
  func removeFile(path: String) async throws
  func addPath(for container: FileContainer, path: String) -> Result<Bool, FileManagerError>
  func metadata(for path: String) -> Result<Metadata, FileManagerError>
  func rename(path: String, newPath: String) async throws
  func modifyOpenTime(path: String) async throws
  func modifyContentModificationTime(path: String) async throws
}

public protocol WildlandListenerProtocol {

  associatedtype ListenerType

  init(operationListener: ListenerType)
}

typealias FileManagerType = WildlandFileManagerProtocol & WildlandListenerProtocol

// TODO: Split WildlandFileManager responsibility (currently it is used by both WildlandFileProviderService and WildlandContainerService) CARDV-115
final class WildlandFileManager: FileManagerType {

  let dfs = WildlandX.cargo.dfsApi()

  private let operationListener: FileOperationListener

  // MARK: - Initialization
  init(operationListener: FileOperationListener) {
    self.operationListener = operationListener
  }

  // MARK: - Private

  private func dfsContainer(for name: String, user: wildlandx.CargoUser) -> Container? {
    do {
      let filter: RustOptional<ContainerFilter> = RustOptional(ContainerFilter.createEmptyOptional())
      let dfsContainers = try user.findContainers(filter, MountState_Unmounted)
      return try dfsContainers.filter { (try $0.name().toString()) == name }.first
    } catch {
      _log.error("Failed searching for dfs container: \(error.localizedDescription)")
    }
    return nil
  }
}

extension WildlandFileManager {

  func metadata(for path: String) -> Result<Metadata, FileManagerError> {
    guard !path.isEmpty else {
      return .failure(.invalidPath())
    }
    do {
      let metadata = try dfs.metadata(RustString(path))
      let fileMetadata = Metadata(size: metadata.size(),
                                  type: metadata.nodeType().rawValue,
                                  accessTime: metadata.accessTime().swiftOptional()?.sec() ?? 0,
                                  changeTime: metadata.changeTime().swiftOptional()?.sec() ?? 0,
                                  modificationTime: metadata.modificationTime().swiftOptional()?.sec() ?? 0)
      return .success(fileMetadata)
    } catch {
      return .failure(.metadataReadFailed)
    }
  }

  func addPath(for container: FileContainer, path: String) -> Result<Bool, FileManagerError> {
    do {
      try checkPathValidity(path)
      guard let user = try? WildlandX.cargo.userApi().getUser() else {
        return .failure(.userNotFound)
      }
      let dfsContainer = dfsContainer(for: container.name, user: user)
      let added = try dfsContainer?.addPath(RustString(path)) ?? false
      return .success(added)
    } catch {
      _log.error("error: \(error)")
    }
    return .failure(.containerPathAddFailed)
  }

}

extension WildlandFileManager {

  func checkPathValidity(_ path: String, method: String = #function) throws {
    guard !path.isEmpty else {
      throw FileManagerError.invalidPath(method: method)
    }
  }

}

extension WildlandFileManager: FileProviderClientProtocol {
  // `operationListener` might not be interested in every of the below notifications,
  // but all are forwarded anyway

  func receiveInfoResponseSuccess(taskId: String, fileItem: FileItem) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveDeleteResponseSuccess(taskId: String) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveDownloadResponseSuccess(taskId: String, fileItem: WildlandCommon.FileItem, data: Data) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveModifyResponseSuccess(taskId: String, fileItem: WildlandCommon.FileItem, fetchContent: Bool) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveListResponseSuccess(taskId: String, items: [WildlandCommon.FileItem], cursor: UInt64) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveResponseFailure(taskId: String, error: Error) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveResponseCancelConfirmation(taskId: String, error: Error) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    operationListener.finishOperation(uuid)
  }

  func receiveResponseProgress(taskId: String, identifier: String, offset: Int, size: Int) {
    guard let uuid = UUID(uuidString: taskId) else { return }
    let metadata = FileOperationMetadata(completedUnitCount: offset, totalUnitCount: size)
    if offset == 0 {
      operationListener.startOperation(taskId: uuid, path: identifier, metadata: metadata)
    } else {
      operationListener.setOperationMetadata(metadata, path: identifier)
    }
  }

}
