//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import Combine
import WildlandCommon
import FileProvider

protocol FileProviderManagerType {
  static func add(_ domain: NSFileProviderDomain, completionHandler: @escaping (Error?) -> Void)
}

extension NSFileProviderManager: FileProviderManagerType { }

final class WildlandDomainMounter: NSObject, DomainMounter, ControlServiceProtocol {

  // MARK: - Properties

  lazy var isMountDomainPublisher = isMountDomainSubject.eraseToAnyPublisher()
  private let isMountDomainSubject = PassthroughSubject<Void, Error>()

  private let fileProviderManager: FileProviderManagerType.Type
  private let userAvailabilityProvider: UserAvailability
  private let containerProvider: ContainerProviderType

  // MARK: - Initialization

  init(fileProviderManagerType: FileProviderManagerType.Type = NSFileProviderManager.self,
       userAvailabilityProvider: UserAvailability,
       containerProvider: ContainerProviderType) {
    self.fileProviderManager = fileProviderManagerType
    self.userAvailabilityProvider = userAvailabilityProvider
    self.containerProvider = containerProvider
  }

  // MARK: - Public

  func mountDomain(_ callback: @escaping (WildlandError?) -> Void) {
    Task {
      do {
        if case .storageGrantedButNotMounted = try userAvailabilityProvider.availabilityState {
          try await mountContainersAndFileProvider()
        }
        if case .readyForUse = try userAvailabilityProvider.availabilityState {
          isMountDomainSubject.send()
        }
        callback(nil)
      } catch {
        let wildlandError = WildlandError(error)
        callback(wildlandError)
        isMountDomainSubject.send(completion: .failure(wildlandError))
        _log.error(failure: wildlandError)
      }
    }
  }

  func mountIfAvailable() {
    mountDomain { _ in }
  }

  func disconnectDomain() async throws {
    guard let fileProviderManager = NSFileProviderManager(for: fileProviderDomain) else {
      _log.fileManagerCreateFailed()
      return
    }

    try await fileProviderManager.disconnect(
      reason: WLStrings.appQuit.domainUnmount.title,
      options: .temporary
    )
    _log.domainDisconnect()
  }

  private func mountContainersAndFileProvider() async throws {
    try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) in
      do {
        try containerProvider.mountAllContainers()
        fileProviderManager.add(fileProviderDomain) { error in
          if let error {
            continuation.resume(throwing: error)
          } else {
            continuation.resume()
          }
        }
      } catch {
        continuation.resume(throwing: error)
      }
    }
  }
}

private extension Logger {

  func domainMount(file: String = #file, method: String = #function, failure: Error?) {
    if let failure {
      let details = "daemon: failed to mount domain"
      error(file: file, method: method, details: details, failure: failure)
    } else {
      let details = "daemon: domain mount"
      info(file: file, method: method, details: details)
    }
  }

  func domainDisconnect(file: String = #file, method: String = #function) {
    let details = "daemon: domain disconnected"
    info(file: file, method: method, details: details)
  }

  func fileManagerCreateFailed(file: String = #file, method: String = #function) {
    let details = "daemon: failed to create NSFileProviderManager for domain \(fileProviderDomain)"
    error(file: file, method: method, details: details, failure: nil)
  }
}
