//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import WildlandCommon

extension WildlandFileProviderService {

  func deleteItem(taskId: String, identifier: String, contentVersion: UInt64, metadataVersion: UInt64, recursive: Bool) {
    Task {
      guard let uuid = UUID(uuidString: taskId),
            await taskManager.stored(for: uuid) == nil else { return }

      _log.startDelete(taskId: taskId)

      let cancelableTask = perform(
        taskId: taskId,
        identifier: identifier,
        contentVersion: contentVersion,
        metadataVersion: metadataVersion,
        recursive: recursive
      )

      await taskManager.store(taskId: uuid, with: cancelableTask)
    }
  }

  private func perform(taskId: String, identifier: String, contentVersion: UInt64, metadataVersion: UInt64, recursive: Bool) -> TaskType {
    Task {
      do {
        let operation = WildlandFileDeleteService(fileManager: fileManager, fileItemStorage: fileItemStorage)
        let item = try await fileItemProvider.fileItem(for: identifier)
        listeners.receiveStart(taskId: taskId, identifier: identifier, operationType: .delete)
        try await operation.deleteItem(
          identifier,
          contentVersion: contentVersion,
          type: .init(rawValue: item.type) ?? .file,
          metadataVersion: metadataVersion,
          recursive: recursive
        )

        _log.infoCompletedProviderService(taskId: taskId)
        listeners.receiveDeleteResponseSuccess(taskId: taskId)
      } catch {
        _log.errorProviderService(taskId: taskId, failure: error)
        listeners.receiveResponseFailure(taskId: taskId, filename: nil, error: error)
      }
    }
  }
}

private extension Logger {
  func startDelete(file: String = #file, method: String = #function, taskId: String) {
    let details = "starting deleting item..., identifier: \(taskId)"
    debug(file: file, method: method, details: details)
  }
}
