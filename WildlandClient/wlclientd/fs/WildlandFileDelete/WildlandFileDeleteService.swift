//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import WildlandCommon

/// WildlandFileDeleteService provides functionality for  files removal in a remote file system.
struct WildlandFileDeleteService {

  let fileManager: FileManagerProtocol
  let fileItemStorage: FileItemStorageType

  func deleteItem(_ identifier: String,
                  contentVersion: UInt64,
                  type: FileProviderDomainService.Entry.EntryType,
                  metadataVersion: UInt64,
                  recursive: Bool) async throws {
    switch type {
    case .folder:
      try await deleteFolder(
        identifier: identifier,
        contentVersion: contentVersion,
        metadataVersion: metadataVersion,
        recursive: recursive
      )
    case .file:
      try await deleteFile(
        identifier: identifier,
        contentVersion: contentVersion,
        metadataVersion: metadataVersion
      )
    case .root:
      return
    }

    _log.itemRemoved(identifier: identifier)
  }

  private func deleteFile(identifier: String,
                          contentVersion: UInt64,
                          metadataVersion: UInt64) async throws {
    try await performDeletion(identifier: identifier) {
      try await fileManager.removeFile(path: $0)
      await fileItemStorage.remove(identifier: identifier)
    }
  }

  private func deleteFolder(identifier: String,
                            contentVersion: UInt64,
                            metadataVersion: UInt64,
                            recursive: Bool) async throws {
    try await performDeletion(identifier: identifier) {
      try await fileManager.removeDirectory(path: $0, recursive: recursive)
      await fileItemStorage.remove(identifier: identifier)
    }
  }

  private func performDeletion(identifier: String, deletionAction: (String) async throws -> Void) async throws {
    let path = try fileManager.path(for: identifier)
    try await deletionAction(path)
  }
}

private extension Logger {
  func itemRemoved(file: String = #file, method: String = #function, identifier: String) {
    let details = "item removal succeed: \(identifier)"
    debug(file: file, method: method, details: details)
  }
}
