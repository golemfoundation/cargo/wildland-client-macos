//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import os
import WildlandCommon

struct WildlandFileListService {

  let tracker: WildlandFileManagerTrackerProtocol
  let fileItemProvider: FileItemProviderType

  func anchor() async -> Data {
    await tracker.anchor()
  }

  func listFolder(_ identifier: String, recursive: Bool, startingCursor: UInt64) async throws -> (items: [FileItem], cursor: UInt64) {
    let fileItems = try await fileItemProvider.fileItems(for: identifier)
    return (fileItems, 0)
  }

  func listItemUpdates(_ identifier: String, anchorData: Data) async -> FileItemUpdates {
    await tracker.itemUpdates(anchorData: anchorData)
  }

  func updateTrackingFolder(taskId: String, identifier: String, isObserved: Bool) async {
    isObserved
    ? await tracker.startObserving(identifier: identifier)
    : await tracker.stopObserving(identifier: identifier)
  }
}
