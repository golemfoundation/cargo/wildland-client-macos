//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import WildlandCommon

extension WildlandFileProviderService {

  func listFolder(taskId: String, identifier: String, recursive: Bool, startingCursor: UInt64) {
    Task {
      let operation = WildlandFileListService(tracker: fileManager, fileItemProvider: fileItemProvider)
      do {
        let (items, cursor) = try await operation.listFolder(
          identifier,
          recursive: recursive,
          startingCursor: startingCursor
        )
        listeners.receiveListResponseSuccess(taskId: taskId, items: items, cursor: cursor)
      } catch {
        listeners.receiveResponseFailure(taskId: taskId, filename: nil, error: error)
      }
    }
  }

  func listItemUpdates(taskId: String, anchorData: Data) {
    let operation = WildlandFileListService(tracker: fileManager, fileItemProvider: fileItemProvider)
    Task {
      let updates = await operation.listItemUpdates(taskId, anchorData: anchorData)
      listeners.receiveItemUpdateResponseSuccess(taskId: taskId, itemUpdates: updates)
    }
  }

  func getCurrentAnchor(taskId: String) {
    let operation = WildlandFileListService(tracker: fileManager, fileItemProvider: fileItemProvider)
    Task {
      let anchor = await operation.anchor()
      listeners.receiveCurrentAnchor(taskId: taskId, anchor: anchor)
    }
  }

  func updateTrackingFolder(taskId: String, identifier: String, isObserved: Bool) {
    let operation = WildlandFileListService(tracker: fileManager, fileItemProvider: fileItemProvider)
    Task {
      await operation.updateTrackingFolder(taskId: taskId, identifier: identifier, isObserved: isObserved)
    }
  }
}
