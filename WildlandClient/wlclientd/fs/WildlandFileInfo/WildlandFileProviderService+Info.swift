//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import os
import WildlandCommon

extension WildlandFileProviderService {
  func itemInfo(taskId: String, identifier: String) {
    Task {

      _log.startInfo(taskId: taskId)

      do {
        let item = try await fileItemProvider.fileItem(for: identifier)
        _log.infoCompletedProviderService(taskId: taskId)
        listeners.receiveInfoResponseSuccess(taskId: taskId, fileItem: item)
      } catch {
        _log.errorProviderService(taskId: taskId, failure: error)
        listeners.receiveResponseFailure(taskId: taskId, filename: nil, error: error)
      }
    }
  }
}

private extension Logger {
  func startInfo(file: String = #file, method: String = #function, taskId: String) {
    let details = "starting getting item info..., identifier: \(taskId)"
    debug(file: file, method: method, details: details)
  }
}
