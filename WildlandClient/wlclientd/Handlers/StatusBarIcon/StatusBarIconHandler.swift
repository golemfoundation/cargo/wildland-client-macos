//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Combine
import CargoUI
import WildlandCommon

protocol StatusBarIconHandlerType {
  var popover: PopoverIconConfiguration? { get set }
  var errorSubscriber: AnySubscriber<Error?, Never> { get }
}

final class StatusBarIconHandler<S: Scheduler>: StatusBarIconHandlerType {

  // MARK: - Properties

  weak var popover: PopoverIconConfiguration?

  private(set) lazy var errorSubscriber = AnySubscriber(errorSubject)
  private let storagesProvider: StoragesProviderType
  private let synchronizationStateProvider: SynchronizationStateProvider
  private let updaterController: UpdaterControllerType
  private let errorSubject = PassthroughSubject<Error?, Never>()
  private let scheduler: S
  private var cancellables = Set<AnyCancellable>()

  // MARK: - Initialization

  init(storagesProvider: StoragesProviderType,
       synchronizationStateProvider: SynchronizationStateProvider,
       updaterController: UpdaterControllerType,
       scheduler: S = DispatchQueue.main) {
    self.storagesProvider = storagesProvider
    self.synchronizationStateProvider = synchronizationStateProvider
    self.updaterController = updaterController
    self.scheduler = scheduler
    self.setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {

    let isInsufficientQuotaErrorPublisher = errorSubject
      .map { error in
        guard let error else { return false }
        return WildlandError(error).isInsufficientQuota
      }

    let thresholdExceededPublisher = storagesProvider
      .thresholdExceededPublisher

    let statePublisher = synchronizationStateProvider
      .statePublisher

    Publishers.CombineLatest4(
      thresholdExceededPublisher,
      statePublisher,
      isInsufficientQuotaErrorPublisher,
      updaterController.updateStatePublisher.prepend(.dismiss)
    )
    .compactMap(StatusAppIcon.init)
    .receive(on: scheduler)
    .sink { [weak self] status in
      self?.updateStatusBarIcon(status: status)
    }
    .store(in: &cancellables)
  }

  // MARK: - Private

  private func updateStatusBarIcon(status: StatusAppIcon) {
    popover?.statusBarIconImage = status.icon
  }
}
