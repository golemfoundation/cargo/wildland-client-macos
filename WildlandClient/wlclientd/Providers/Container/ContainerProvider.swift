//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WildlandCommon
import wildlandx

enum ContainerProviderError: Error {
  case containerCreationFailedInvalidName
  case containerNotExist
}

protocol ContainerProviderType {
  @discardableResult
  func createContainer(from template: StorageTemplate, name: String, path: String) throws -> FileContainer
  func getContainers() throws -> [FileContainer]
  func mountAllContainers() throws
  func mount(container: FileContainer) throws
  func unmount(container: FileContainer) throws
}

struct ContainerProvider: ContainerProviderType {

  // MARK: - Properties

  private let userApi: UserApiProviderType
  private var user: CargoUser {
    get throws {
      try userApi.user
    }
  }

  // MARK: - Initialization

  init(userApi: UserApiProviderType = UserApiProvider()) {
    self.userApi = userApi
  }

  // MARK: - Public

  func createContainer(from template: StorageTemplate, name: String, path: String) throws -> FileContainer {
    guard !name.isEmpty else {
      throw ContainerProviderError.containerCreationFailedInvalidName
    }

    guard !path.isEmpty else {
      throw FileManagerError.invalidPath()
    }
    let container = try user.createContainer(RustString(name), template, RustString(path), true)
    let name = try container.name().toString()
    return FileContainer(name: name)
  }

  func getContainers() throws -> [FileContainer] {
    try user.findContainers(RustOptional(ContainerFilter.createEmptyOptional()), MountState_MountedOrUnmounted)
      .compactMap {
        FileContainer(name: try $0.name().toString())
      }
  }

  func mountAllContainers() throws {
    for container in try getContainers() {
      try mount(container: container)
    }
  }

  func mount(container: FileContainer) throws {
    guard let container = try user.container(for: container.name, mountState: MountState_Unmounted) else {
      throw ContainerProviderError.containerNotExist
    }
    _ = try container.mount(RustOptional(Persistency.createEmptyOptional()))
  }

  func unmount(container: FileContainer) throws {
    guard let container = try user.container(for: container.name, mountState: MountState_Mounted) else {
      throw ContainerProviderError.containerNotExist
    }
    _ = try container.unmount(RustOptional(Persistency.createEmptyOptional()))
  }
}
