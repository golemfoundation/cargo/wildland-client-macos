//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Combine
import os
import WildlandCommon

public protocol FileOperationHandler {
  var operationsPublisher: AnyPublisher<[FileOperationStorage], Never> { get }
  func startOperation(taskId: UUID, identifier: String, filename: String, type: OperationType)
  func setOperationMetadata(_ metadata: FileOperationMetadata, identifier: String)
  func finishOperation(_ taskId: UUID, input: FileOperationFinishInput)
}

extension FileOperationHandler {
  func finishOperation(_ taskId: UUID) {
    finishOperation(taskId, input: .init(filename: nil, error: nil))
  }
}

public struct FileOperationFinishInput {
  let filename: String?
  let error: Error?
}

public protocol FileOperationListenerErrors {
  typealias Content = (error: Error, filename: String)
  var errorPublisher: AnyPublisher<Content?, Never> { get }
}

public typealias FileOperationListener = FileOperationHandler & FileOperationListenerErrors

/// This class aggregates, stores and publishes current operations added by startOperation.
/// As long as some operation is in progress, operations are aggregated
/// If all operations are finished and finishedOperation was called for each of then, then operation list is cleaned
final class WildlandFileOperationListener: FileOperationListener {

  // MARK: - Properties

  private let operationsSubject = CurrentValueSubject<[FileOperationStorage], Never>([])
  private let currentOperationsSubject = CurrentValueSubject<[UUID], Never>([])
  private let latestErrorSubject = PassthroughSubject<FileOperationListenerErrors.Content?, Never>()
  lazy private(set) var operationsPublisher = operationsSubject.eraseToAnyPublisher()
  lazy private(set) var errorPublisher = latestErrorSubject.eraseToAnyPublisher()

  // MARK: - Initialization

  init() {
    setupBindings()
  }

  // MARK: - Setup

  private func setupBindings() {
    currentOperationsSubject
      .filter { $0.isEmpty }
      .map { _ in [] }
      .subscribe(AnySubscriber(operationsSubject))
  }

  // MARK: - Public

  func startOperation(taskId: UUID, identifier: String, filename: String, type: OperationType) {
    _log.startOperation(identifier: identifier, filename: filename, taskId: taskId)
    currentOperationsSubject.value.append(taskId)
    let storage = FileOperationStorage(
      identifier: identifier,
      filename: filename,
      type: type
    )
    operationsSubject.value.append(storage)
  }

  func setOperationMetadata(_ metadata: FileOperationMetadata, identifier: String) {
    guard let index = operationsSubject.value.firstIndex(where: { $0.identifier == identifier }) else { return }
    operationsSubject.value[index].setOperationMetadata(metadata)
  }

  func finishOperation(_ taskId: UUID, input: FileOperationFinishInput) {
    _log.finishOperation(taskId: taskId)
    currentOperationsSubject.value.removeAll(where: { $0 == taskId })
    guard let error = input.error, let filename = input.filename else { return }
    latestErrorSubject.send((error, filename))
  }
}

private extension Logger {

  func startOperation(file: String = #file, method: String = #function, identifier: String, filename: String, taskId: UUID) {
    let details = """
      daemon: file opeartion listener starting..., identifier: \(identifier), filename: \(filename), taskId: \(taskId.uuidString)
    """
    debug(file: file, method: method, details: details)
  }

  func finishOperation(file: String = #file, method: String = #function, taskId: UUID) {
    let details = "daemon: file opeartion listener finishing..., identifier: \(taskId.uuidString)"
    debug(file: file, method: method, details: details)
  }
}
