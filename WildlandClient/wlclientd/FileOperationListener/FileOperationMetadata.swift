//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

public struct FileOperationMetadata: Equatable {

  // MARK: - Properties

  public let completedUnitCount: Int
  public let totalUnitCount: Int
  public let time: CFAbsoluteTime

  // MARK: - Initialization

  public init(completedUnitCount: Int = .zero,
              totalUnitCount: Int,
              time: CFAbsoluteTime = CFAbsoluteTimeGetCurrent()) {
    self.completedUnitCount = completedUnitCount
    self.totalUnitCount = totalUnitCount
    self.time = time
  }
}
