//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension [FileOperationStorage] {

  var filenames: [String] {
    map(\.filename)
  }

  var currentSize: Double {
    reduce(into: Double.zero, { result, storage in
      let metadata = storage.lastMetadata ?? storage.firstMetadata
      result += Double(metadata?.completedUnitCount ?? .zero)
    })
  }

  var totalSize: Double {
    reduce(into: Double.zero, { result, storage in
      let metadata = storage.lastMetadata ?? storage.firstMetadata
      result += Double(metadata?.totalUnitCount ?? .zero)
    })
  }

  var estimation: Double {
    let bytesPerSecond = bytesPerSecond
    guard bytesPerSecond > .zero else { return .zero }
    let remainingSize = totalSize - currentSize
    return remainingSize / bytesPerSecond
  }

  private var bytesPerSecond: Double {
    let allBytesPerSecond = map { $0.bytesPerSecond }
      .filter { $0 != .zero }
    return allBytesPerSecond.reduce(Double.zero, +) / Double(allBytesPerSecond.count)
  }
}
