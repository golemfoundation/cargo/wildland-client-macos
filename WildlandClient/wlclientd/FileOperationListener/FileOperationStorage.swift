//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import WildlandCommon

/// This struct stores information about file operation
/// - Parameters:
///   - `identifier`: Identifies file
///   - `filename` is used to display filename
///   - `type` can by transfer or delete
///   - `firstMetadata` stores first uploaded or downloaded item of metadata
///   - `lastMetadata` stores last uploaded or downloaded item of metadata
public struct FileOperationStorage: Equatable {

  // MARK: - Properties

  public let identifier: String
  public let filename: String
  public let type: OperationType
  public var firstMetadata: FileOperationMetadata?
  public var lastMetadata: FileOperationMetadata?

  // MARK: - Initialization

  public init(
    identifier: String,
    filename: String,
    type: OperationType
  ) {
    self.identifier = identifier
    self.filename = filename
    self.type = type
  }

  public mutating func setOperationMetadata(_ metadata: FileOperationMetadata) {
    if firstMetadata == nil {
      firstMetadata = metadata
    } else {
      lastMetadata = metadata
    }
  }
}

extension FileOperationStorage {
  var bytesPerSecond: Double {
    guard let firstMetadata, let lastMetadata else {
      return .zero
    }
    let totalByteCount = Double(lastMetadata.completedUnitCount - firstMetadata.completedUnitCount)
    let totalTime = lastMetadata.time - firstMetadata.time
    guard totalTime > .zero else {
      return .zero
    }
    return totalByteCount / totalTime
  }
}
