//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2022 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import CargoUI
import WildlandCommon
import Cocoa
import os
import SwiftUI
import Combine

let _log = Logger.wildlandLogger()

final class MainDelegate: NSObject, NSApplicationDelegate {

  // MARK: - Properties

  private let application: NSApplication
  private let storageWarningHandler: any WarningHandler
  private let serviceProvider: WildlandServiceType
  private var fileProviderAction: FileProviderActionType
  private let domainMounter: DomainMounter
  private var popover: Popover?
  private var activityViewModel: (any ActivityViewModelType)?
  private let dependencyContainer: DependencyContainerType
  private var statusBarIconHandler: StatusBarIconHandlerType
  private let workspace: NSWorkspace
  private var cancellables = Set<AnyCancellable>()

  private lazy var updateAppItem = NSMenuItem(
    title: WLStrings.appHelperMenu.Item.updateHelper,
    action: #selector(updateApp),
    keyEquivalent: ""
  )

  // MARK: - Initialization

  init(dependencyContainer: DependencyContainerType, application: NSApplication = .shared, workspace: NSWorkspace = .shared) {
    self.application = application
    self.workspace = workspace
    self.dependencyContainer = dependencyContainer
    self.serviceProvider = dependencyContainer.wildlandService
    self.fileProviderAction = dependencyContainer.fileProviderExtension.fileProviderAction
    self.domainMounter = dependencyContainer.domainMounter
    self.statusBarIconHandler = dependencyContainer.application.statusBarIconHandler
    self.storageWarningHandler = dependencyContainer.handlers.storageWarning
    super.init()
    setupBindings()
  }

  // MARK: - Public

  func applicationDidFinishLaunching(_ notification: Notification) {
    _log.info("wlclientd is starting")
    application.activate(ignoringOtherApps: true)

    setupStatusItem()

    serviceProvider.start()
    mountDomainIfAvailable()

    setupUserNotifications()

    statusBarIconHandler.popover = popover
  }

  func applicationWillTerminate(_ notification: Notification) {
    _log.info("daemon is terminating")
  }

  // MARK: - Setup

  private func mountDomainIfAvailable() {
    domainMounter
      .isMountDomainPublisher
      .receive(on: RunLoop.main)
      .sink(receiveCompletion: { completion in
        if case .failure = completion {
          NSAlert.showDomainMountErrorAlert()
          NSApp.terminate(nil)
        }
      }, receiveValue: { _ in })
      .store(in: &cancellables)
    domainMounter.mountIfAvailable()
  }

  // MARK: - Private

  @objc private func quitApp() {
    dependencyContainer.wildlandService.stopAllTasks()

    Task {
      let isTerminated = try workspace.terminateApp(with: Defaults.BundleID.mainApp)
      _log.mainAppTerminated(isTerminated: isTerminated)

      try await domainMounter.disconnectDomain()
      unloadDaemon()
    }
  }

  @objc private func showAbout() {
    let appInfo = AppInfo()
    dependencyContainer.application.aboutPanelProvider.showAboutPanel(appInfo: appInfo)
  }

  @objc private func updateApp() {
    dependencyContainer.application.updaterController.checkForUpdates()
  }

  @objc private func openWildlandPrefs() {
    launchCargoClientApp(appSceneType: .preferences)
  }

  private func setupBindings() {
    let errorPublisher = dependencyContainer.fileOperationListener.errorPublisher
      .map { errorContent -> FileOperationListenerErrors.Content? in
        guard let errorContent else { return nil }
        return WildlandError(errorContent.error).isInsufficientQuota ? errorContent : nil
      }
      .share()
      .prepend(nil)

    errorPublisher
      .compactMap { $0 }
      .subscribe(storageWarningHandler.errorSubscriber)

    Publishers.Merge(
      errorPublisher.map(\.?.error),
      storageWarningHandler.dismissPublisher.map { nil }
    )
    .subscribe(statusBarIconHandler.errorSubscriber)

    dependencyContainer.application.updaterController
      .canCheckForUpdatesPublisher
      .map { !$0 }
      .weakAssign(keyPath: \.isHidden, on: updateAppItem)
      .store(in: &cancellables)

    dependencyContainer.application.uninstaller
      .uninstallDaemonPublisher
      .sink { [weak self] in
        self?.uninstallDaemon()
      }
      .store(in: &cancellables)

    let upToDatePublisher = dependencyContainer.application.updaterController
      .updateStatePublisher
      .map { updateState in
        guard case .upToDate = updateState else { return false }
        return true
      }

    let notificationSelectedPublisher = dependencyContainer.application.userNotifications
      .notificationResponsePublisher
      .map { response in
        response.category == .appUpdate && response.action == .select
      }

    Publishers.Merge(upToDatePublisher, notificationSelectedPublisher)
      .filter { $0 }
      .sink { [weak self] _ in
        self?.popover?.showContentIfPossible()
      }
      .store(in: &cancellables)

    fileProviderAction.showContainerPropertiesPublisher
      .subscribe(dependencyContainer.handlers.containerProperties.presentSubscriber)

    fileProviderAction.evictItemsPublisher
      .taskSink { [weak self] identifiers in
        await self?.dependencyContainer.fileProviderExtension.evictItemHandler.evictItems(identifiers, logger: _log)
      }
      .store(in: &cancellables)

    dependencyContainer.application.updaterController
      .newAppVersionIdentifiedPublisher
      .sink(receiveValue: { [weak self] in
        self?.launchCargoClientApp(arguments: [Defaults.DeeplinkArgument.relaunchDaemon])
      })
      .store(in: &cancellables)

    dependencyContainer.application.deeplinkHandler
      .publisher
      .compactMap(\.sharingMessage)
      .receive(subscriber: dependencyContainer.handlers.receiveShare.presentSubscriber)
  }

  private func setupStatusItem() {
    let viewModel = ActivityViewModel(
      domainMounter: dependencyContainer.domainMounter,
      storagesProvider: dependencyContainer.providers.storages,
      synchronizationStateProvider: dependencyContainer.synchronizationStateProvider,
      activityMessageHandler: dependencyContainer.handlers.message,
      logger: _log,
      scheduler: DispatchQueue.main,
      fetchScheduler: DispatchQueue.global()
    )
    activityViewModel = viewModel

    let menuItems: [NSMenuItem] = [
      NSMenuItem.separator(),
      NSMenuItem(title: WLStrings.appHelperMenu.Item.about, action: #selector(showAbout), keyEquivalent: ""),
      NSMenuItem.separator(),
      NSMenuItem(title: WLStrings.appHelperMenu.Item.preferences, action: #selector(openWildlandPrefs),
                 keyEquivalent: ""),
      updateAppItem,
      NSMenuItem.separator(),
      NSMenuItem(title: WLStrings.appHelperMenu.Item.quit, action: #selector(quitApp), keyEquivalent: "")
    ]
    popover = Popover(
      with: menuItems,
      statusBarIconImage: StatusAppIcon.synchronized.icon,
      contentController: NSHostingController(
        rootView: ActivityView(model: viewModel)
          .colorScheme()
      )
    )
    popover?.isPopoverEnabled = { [weak self] in
      (try? self?.dependencyContainer.storagesAccessProvider.isStorageGranted) ?? false
    }
  }

  private func setupUserNotifications() {
    dependencyContainer.application.userNotifications.requestNotificationAuthorization()
  }

  private func unloadDaemon() {
    serviceProvider.stop()

    Process.manuallyUnregisterDaemon()

    NSApp.terminate(self)
  }

  private func uninstallDaemon() {
    serviceProvider.stop()

    // Application is in the Trash and system does not allow connection to "com.apple.FileProvider".
    // In this case removing domains using "NSFileProviderManager.removeAllDomains" always fails.
    Process.manuallyRemoveDomain()

    terminateApp(after: .second)
  }

  private func terminateApp(after delay: DispatchQueue.DelayLength) {
    DispatchQueue.main.delay(with: delay) {
      NSApp.terminate(self)
    }
  }

  private func launchCargoClientApp(appSceneType: AppSceneType? = nil, arguments: [String] = []) {
    guard let url = URL(string: Defaults.Scheme.client + (appSceneType?.rawValue ?? "")) else { return }
    let configuration = NSWorkspace.OpenConfiguration()
    configuration.arguments = arguments
    workspace.openApplication(at: url, configuration: configuration)
  }

  func application(_ application: NSApplication, open urls: [URL]) {
    dependencyContainer.application.deeplinkHandler.receiveUrl(urls)
  }
}

private extension Logger {
  func mainAppTerminated(file: String = #file, method: String = #function, isTerminated: Bool) {
    let details = "daemon: main app was \(isTerminated ? "" : "NOT") terminated"
    info(file: file, method: method, details: details)
  }
}
