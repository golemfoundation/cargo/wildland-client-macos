//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import FileProvider
import WildlandCommon
import CargoUI

protocol DependencyContainerType {
  var wildlandService: WildlandServiceType { get }
  var fileOperationListener: FileOperationListener { get }
  var domainMounter: DomainMounter { get }
  var storagesAccessProvider: StoragesAccessProvider { get }
  var synchronizationStateProvider: SynchronizationStateProvider { get }
  var application: DependencyContainer.Application { get }
  var providers: DependencyContainer.Providers { get }
  var handlers: DependencyContainer.Handlers { get }
  var fileProviderExtension: DependencyContainer.FileProviderExtension { get }
}

extension DependencyContainer {
  struct Handlers {
    let storageWarning: any WarningHandler
    let containerProperties: any ContainerHandler
    let receiveShare: any ReceiveShareHandlerType
    let message: ActivityMessageHandlerType
  }
}

extension DependencyContainer {
  struct Application {
    let aboutPanelProvider: AboutPanelProviderType
    let userNotifications: UserNotificationsType
    let uninstaller: ApplicationUninstallerType
    let statusBarIconHandler: StatusBarIconHandlerType
    let updaterController: UpdaterControllerType
    let deeplinkHandler: DeeplinkHandlerType
  }
}

extension DependencyContainer {
  struct Providers {
    let storages: StoragesProviderType
  }
}

extension DependencyContainer {
  struct FileProviderExtension {
    let fileProviderAction: FileProviderActionType
    let globalProgressObserver: FileProviderGlobalProgressObserverType
    let evictItemHandler: EvictItemHandler
  }
}

struct DependencyContainer: DependencyContainerType {

  // MARK: - Properties

  let wildlandService: WildlandServiceType
  let fileManager: FileManagerProtocol
  let fileOperationListener: FileOperationListener
  let domainMounter: DomainMounter
  let storagesAccessProvider: StoragesAccessProvider
  let synchronizationStateProvider: SynchronizationStateProvider
  let application: Application
  let providers: Providers
  let handlers: Handlers
  let fileProviderExtension: FileProviderExtension

  // MARK: - Public

  init?() {
    let userAPI = UserApiProvider()
    let fileOperationListener = WildlandFileOperationListener()
    let containerProvider = ContainerProvider()
    let fileProviderAction = FileProviderAction()
    let domainMounter = WildlandDomainMounter(
      userAvailabilityProvider: userAPI,
      containerProvider: containerProvider
    )
    guard let fileProviderManager = NSFileProviderManager(for: fileProviderDomain) else { return nil }

    let globalProgressObserver = FileProviderGlobalProgressObserver(
      fileProviderManager: fileProviderManager
    )

    let storagesProvider = StoragesProvider(userApi: userAPI)
    let userNotifications = UserNotifications()

    self.synchronizationStateProvider = WildlandSynchronizationStateProvider(
      globalProgressObserver: globalProgressObserver,
      fileOperationListener: fileOperationListener
    )

    let updaterController = UpdaterController<DispatchQueue>(
      userDriver: UserDriver(decimalFormatter: .decimalCountFormatter),
      synchronizationStateProvider: synchronizationStateProvider
    )

    let fileItemStorage = FileItemStorage()
    let fileItemCoreFacade = FileItemCoreFacade(userApi: userAPI, logger: _log)

    let fileItemProvider = FileItemProvider(
      fileItemStorage: fileItemStorage,
      fileItemCoreFacade: fileItemCoreFacade
    )

    let identifierTracker = IdentifierTracker(
      fileProviderManager: fileProviderManager,
      fileItemProvider: fileItemProvider,
      fileItemStorage: fileItemStorage
    )

    let fileManager = WildlandFileManager(
      operationListener: fileOperationListener,
      identifierTracker: identifierTracker,
      fileItemProvider: fileItemProvider,
      temporaryUrlProvider: fileProviderManager
    )

    let fileProviderService = WildlandFileProviderService(
      fileManager: fileManager,
      fileItemProvider: fileItemProvider,
      fileItemStorage: fileItemStorage
    )

    self.wildlandService = WildlandService(
      with: fileProviderService,
      controlService: domainMounter,
      userService: WildlandUserService(
        containerProvider: containerProvider,
        urlProvider: fileProviderManager,
        settings: Settings()
      ),
      fileProviderAction: fileProviderAction
    )

    self.fileManager = fileManager
    self.domainMounter = domainMounter
    self.fileOperationListener = fileOperationListener
    self.storagesAccessProvider = userAPI
    self.application = DependencyContainer.Application(
      aboutPanelProvider: AboutPanelProvider(),
      userNotifications: userNotifications,
      uninstaller: ApplicationUninstaller(
        userNotifications: userNotifications
      ),
      statusBarIconHandler: StatusBarIconHandler(
        storagesProvider: storagesProvider,
        synchronizationStateProvider: synchronizationStateProvider,
        updaterController: updaterController
      ),
      updaterController: updaterController,
      deeplinkHandler: DeeplinkHandler()
    )
    self.providers = Providers(
      storages: storagesProvider
    )
    self.handlers = Handlers(
      storageWarning: StorageWarningHandler(),
      containerProperties: ContainerPropertiesHandler(
        dependecies: ContainerPropertiesViewModelDependecies(
          pathResolvable: fileManager,
          fileItemProvider: fileItemProvider,
          userNotifications: userNotifications,
          storagesProvider: storagesProvider
        ),
        logger: _log
      ),
      receiveShare: ReceiveShareHandler<DispatchQueue>(
        shareReceiverProvider: ShareReceiverProvider(userApi: userAPI),
        urlProvider: fileProviderManager
      ),
      message: ActivityMessageHandler(
        storagesProvider: storagesProvider,
        updaterController: updaterController,
        userNotifications: userNotifications
      )
    )
    self.fileProviderExtension = FileProviderExtension(
      fileProviderAction: fileProviderAction,
      globalProgressObserver: FileProviderGlobalProgressObserver(
        fileProviderManager: fileProviderManager
      ),
      evictItemHandler: fileProviderManager
    )
  }
}
