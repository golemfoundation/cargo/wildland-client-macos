//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
@testable import CargoUI

@MainActor
final class PreferencesViewTests: XCTestCase {

  // MARK: - Properties

  private func items(for count: Int) -> [DropdownItem] {
    (1...count).map { DropdownItem(id: "\($0)", title: "example\($0)") }
  }

  private func storageItems(for count: Int) -> [StorageItem] {
    (1...count).map {
      StorageItem(
        id: "\($0)",
        title: "GF Free tier \($0)",
        subtitle: "S3SC, 10 bytes of 20 bytes in use (50%)",
        usedBytes: Measurement<UnitInformationStorage>(value: 10, unit: .bytes),
        totalBytes: Measurement<UnitInformationStorage>(value: 20, unit: .bytes)
      )
    }
  }

  // MARK: - Tests

  func testPreferencesViewGeneralSelectedLaunchCargoOnDisplayNameSet() throws {
    let generalViewModel = prepareGeneralViewModel(
      selectedItem: DropdownItem(id: "2", title: "example2"),
      items: items(for: 2),
      launchCargoOnLogin: true,
      displayName: "Display name"
    )
    let viewModel = prepareViewModel(
      selectedOption: .general,
      contentViewType: .general(generalViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewGeneralSelectedDisplayNameSetShouldDisplayNameForSharingTrue() throws {
    let generalViewModel = prepareGeneralViewModel(
      selectedItem: DropdownItem(id: "2", title: "example2"),
      items: items(for: 2),
      displayName: "Display name",
      shouldDisplayNameForSharing: true
    )
    let viewModel = prepareViewModel(
      selectedOption: .general,
      contentViewType: .general(generalViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewGeneralSelectedDisplayNameDisplayNameSetForSharingDisabledTrue() throws {
    let generalViewModel = prepareGeneralViewModel(
      selectedItem: DropdownItem(id: "2", title: "example2"),
      items: items(for: 2),
      displayName: "Display name",
      displayNameForSharingDisabled: true
    )
    let viewModel = prepareViewModel(
      selectedOption: .general,
      contentViewType: .general(generalViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewGeneralSelectedIsDropdownShownTrueTwoItems() throws {
    let generalViewModel = prepareGeneralViewModel(
      selectedItem: DropdownItem(id: "2", title: "example2"),
      items: items(for: 2),
      isDropdownShown: true
    )
    let viewModel = prepareViewModel(
      selectedOption: .general,
      contentViewType: .general(generalViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewGeneralSelectedIsDropdownShownTrueThreeItems() throws {
    let generalViewModel = prepareGeneralViewModel(
      selectedItem: DropdownItem(id: "2", title: "example2"),
      items: items(for: 3),
      isDropdownShown: true
    )
    let viewModel = prepareViewModel(
      selectedOption: .general,
      contentViewType: .general(generalViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewGeneralSelectedPublicKeyTooltipStatePressed() throws {
    let generalViewModel = prepareGeneralViewModel(
      selectedItem: DropdownItem(id: "2", title: "example2"),
      items: items(for: 2),
      publicKeyTooltipState: .pressed("pressed text")
    )
    let viewModel = prepareViewModel(
      selectedOption: .general,
      contentViewType: .general(generalViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewGeneralSelectedPublicKeyTooltipStateHover() throws {
    let generalViewModel = prepareGeneralViewModel(
      selectedItem: DropdownItem(id: "2", title: "example2"),
      items: items(for: 2),
      publicKeyTooltipState: .hover("hover text")
    )
    let viewModel = prepareViewModel(
      selectedOption: .general,
      contentViewType: .general(generalViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewStorageSelectedIsLoadingTrue() throws {
    let storageViewModel = prepareStorageViewModel(
      isLoading: true,
      storages: []
    )
    let viewModel = prepareViewModel(
      selectedOption: .storage,
      contentViewType: .storages(storageViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewStorageSelectedOneStorageSet() throws {
    let storageViewModel = prepareStorageViewModel(
      isLoading: false,
      storages: storageItems(for: 1)
    )
    let viewModel = prepareViewModel(
      selectedOption: .storage,
      contentViewType: .storages(storageViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  func testPreferencesViewStorageSelectedTwoStorageSet() throws {
    let storageViewModel = prepareStorageViewModel(
      isLoading: false,
      storages: storageItems(for: 2)
    )
    let viewModel = prepareViewModel(
      selectedOption: .storage,
      contentViewType: .storages(storageViewModel)
    )
    let view = PreferencesView(viewModel: viewModel)
    try validateView(view)
  }

  // MARK: - Helpers

  private func prepareViewModel(
    selectedOption: PreferencesOptions,
    contentViewType: ContentViewType<PreferencesGeneralViewModelStub, PreferencesStorageViewModelStub>
  ) -> PreferencesViewModelStub {
    PreferencesViewModelStub(
      options: PreferencesOptions.allCases,
      selectedOption: selectedOption,
      contentViewType: contentViewType
    )
  }

  private func prepareGeneralViewModel(
    selectedItem: DropdownItem,
    items: [DropdownItem],
    launchCargoOnLogin: Bool = false,
    displayName: String = "",
    shouldDisplayNameForSharing: Bool = false,
    isDropdownShown: Bool = false,
    publicKeyTooltipState: TooltipState = .dismiss,
    displayNameForSharingDisabled: Bool = false
  ) -> PreferencesGeneralViewModelStub {
    PreferencesGeneralViewModelStub(
      publicKey: "0xc57a...82kcb",
      rootPath: "file://example/root/path",
      selectedItem: selectedItem,
      items: items,
      launchCargoOnLogin: launchCargoOnLogin,
      displayName: displayName,
      shouldDisplayNameForSharing: shouldDisplayNameForSharing,
      isDropdownShown: isDropdownShown,
      publicKeyTooltipState: publicKeyTooltipState,
      displayNameForSharingDisabled: displayNameForSharingDisabled
    )
  }

  private func prepareStorageViewModel(
    isLoading: Bool,
    storages: [StorageItem]
  ) -> PreferencesStorageViewModelStub {
    PreferencesStorageViewModelStub(
      storages: storages,
      isLoading: isLoading
    )
  }
}
