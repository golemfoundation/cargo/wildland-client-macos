//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
@testable import CargoUI

@MainActor
final class ReceiveShareViewTests: XCTestCase {

  // MARK: - Properties

  private let shareInfo = ShareInfo(
    senderName: "Bob",
    senderPublicKey: "0xc5a...82kcb",
    sharedContentName: "Sponsor Booth details",
    sharedContentSize: "16.56 MB",
    sharedContentType: "PDF",
    image: nil
  )

  // MARK: - Tests

  func testReceiveShareViewIsLoadingTrue() throws {
    let viewModel = prepareViewModel(
      shareInfo: shareInfo,
      isLoading: true
    )
    let view = ReceiveShareView(viewModel: viewModel)
    try validateView(view)
  }

  func testReceiveShareViewInfoViewTypeInfo() throws {
    let viewModel = prepareViewModel(
      shareInfo: shareInfo,
      infoViewType: .info("example info")
    )
    let view = ReceiveShareView(viewModel: viewModel)
    try validateView(view)
  }

  func testReceiveShareViewInfoViewTypeErrorContentRedacted() throws {
    let viewModel = prepareViewModel(
      shareInfo: shareInfo,
      isContentRedacting: true,
      infoViewType: .error("example error message")
    )
    let view = ReceiveShareView(viewModel: viewModel)
    try validateView(view)
  }

  func testReceiveShareViewInfoViewTypeLoading() throws {
    let viewModel = prepareViewModel(
      shareInfo: shareInfo,
      infoViewType: .loading("loading message")
    )
    let view = ReceiveShareView(viewModel: viewModel)
    try validateView(view)
  }

  func testReceiveShareViewWithoutSenderName() throws {
    let viewModel = prepareViewModel(
      shareInfo: ShareInfo(
        senderName: nil,
        senderPublicKey: "0xc5a...82kcb",
        sharedContentName: "Sponsor Booth details",
        sharedContentSize: "16.56 MB",
        sharedContentType: "PDF",
        image: nil
      ),
      headerTitle: "0xc5a...82kcb shared a file with you"
    )
    let view = ReceiveShareView(viewModel: viewModel)
    try validateView(view)
  }

  // MARK: - Helpers

  private func prepareViewModel(
    shareInfo: ShareInfo,
    headerTitle: AttributedString = AttributedString("Bob shared a file with you"),
    isContentRedacting: Bool = false,
    isLoading: Bool = false,
    infoViewType: InfoViewType? = nil
  ) -> ReceiveShareViewModelStub {
    ReceiveShareViewModelStub(
      shareInfo: shareInfo,
      headerTitle: headerTitle,
      isContentRedacting: isContentRedacting,
      isLoading: isLoading,
      infoViewType: infoViewType
    )
  }
}
