//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import WildlandCommon
@testable import CargoUI

@MainActor
final class ActivityViewTests: XCTestCase {

  private enum UpdateError: Error {
    case updateError
  }

  // MARK: - Properties

  private func items(for count: Int) -> [StorageItem] {
    (1...count).map {
      StorageItem(
        id: "\($0)",
        title: "StorageItem \($0)",
        subtitle: "S3SC, 50 bytes of 100 bytes in use (50%)",
        usedBytes: Measurement<UnitInformationStorage>(value: 50, unit: .bytes),
        totalBytes: Measurement<UnitInformationStorage>(value: 100, unit: .bytes)
      )
    }
  }

  // MARK: - Tests

  func testActivityView() throws {
    let viewModel = prepareViewModel()
    let view = ActivityView(model: viewModel)
    try validateView(view)
  }

  func testActivityViewIsCopyPopoverPresentedTrueCopyPublicKeyPopoverStateVisible() throws {
    let viewModel = prepareViewModel(
      isCopyPopoverPresented: true,
      copyPublicKeyPopoverState: .visible("message")
    )
    let view = ActivityView(model: viewModel)
    try validateView(view)
  }

  func testActivityViewProgressWithFormattedMetricsTitleAndFiles() throws {
    let viewModel = prepareViewModel(
      progress: 0.5,
      formattedMetrics: "48MB (17 secs)",
      synchronizationTitle: "Syncing",
      synchronizationFiles: "file.png"
    )
    let view = ActivityView(model: viewModel)
    try validateView(view)
  }

  func testActivityViewStoragesSeTwotIsExtendedTrue() throws {
    let viewModel = prepareViewModel(
      storages: items(for: 2),
      isExtended: true
    )
    let view = ActivityView(model: viewModel)
      .frame(height: 249.0)
    try validateView(view)
  }

  func testActivityViewStoragesThreeSetIsExtendedTrue() throws {
    let viewModel = prepareViewModel(
      storages: items(for: 3),
      isExtended: true
    )
    let view = ActivityView(model: viewModel)
      .frame(height: 249.0)
    try validateView(view)
  }

  func testActivityViewUpdateState() throws {
    let updateStates: [UpdateState] = [
      .dismiss,
      .upToDate,
      .newUpdateAvailable,
      .downloading(receivedFormattedBytes: "100 bytes", totalFormattedBytes: "1 KB"),
      .extracting(percent: 15),
      .waitForOperationsToComplete,
      .error(UpdateError.updateError)
    ]

    try updateStates.forEach {
      let viewModel = prepareViewModel(
        updateState: $0
      )
      let view = ActivityView(model: viewModel)
      try validateView(view, testName: #function + "-" + $0.description)
    }
  }

  func testActivityViewActivityMessage() throws {
    let activityMessages = [
      ActivityMessage(
        identifier: .storageAlmostFull,
        title: "example title",
        description: "example description ",
        isVisibleButton: true,
        type: .warning
      ),
      ActivityMessage(
        identifier: .storageAlmostFull,
        title: "example title",
        description: "example description ",
        isVisibleButton: false,
        type: .update
      )
    ]

    try activityMessages.forEach {
      let viewModel = prepareViewModel(
        activityMessage: $0
      )
      let view = ActivityView(model: viewModel)
      try validateView(view, testName: #function + "-" + String(describing: $0.type))
    }
  }

  // MARK: - Helpers

  private func prepareViewModel(
    storages: [StorageItem] = [],
    isExtended: Bool = false,
    isCopyPopoverPresented: Bool = false,
    progress: Double = .zero,
    formattedMetrics: String? = nil,
    synchronizationTitle: String = "",
    synchronizationFiles: String? = nil,
    activityMessage: ActivityMessage? = nil,
    copyPublicKeyPopoverState: ButtonCopyPopoverState = .invisible,
    updateState: UpdateState = .dismiss
  ) -> ActivityViewModelStub {
    ActivityViewModelStub(
      storages: storages,
      isExtended: isExtended,
      isCopyPopoverPresented: isCopyPopoverPresented,
      progress: progress,
      formattedMetrics: formattedMetrics,
      synchronizationTitle: synchronizationTitle,
      synchronizationFiles: synchronizationFiles,
      activityMessage: activityMessage,
      copyPublicKeyPopoverState: copyPublicKeyPopoverState,
      updateState: updateState
    )
  }
}

private extension UpdateState {
  var description: String {
    guard case .error = self else { return String(describing: self) }
    return "error"
  }
}
