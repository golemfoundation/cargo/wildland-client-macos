//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
@testable import CargoUI

@MainActor
final class ContainerPropertiesViewTests: XCTestCase {

  // MARK: - Properties

  private let shareItem = ContainerPropertiesShareItem(
    publicKey: "df2AvzsSSwafdgsdsCCAsa412AS12221dasda",
    userDefaults: UserDefaults(suiteName: "SnapshotTests") ?? .standard
  )

  // MARK: - Tests

  func testContainerPropertiesViewLoadedSelectedOptions() throws {
    let options = ContainerPropertiesOptions.allCases
    try options.forEach {
      let viewModel = prepareViewModel(isLoading: false, isHeaderRedacting: false, selectedOption: $0)
      let view = ContainerPropertiesView(viewModel: viewModel)
      try validateView(view, testName: #function + $0.id)
    }
  }

  func testContainerPropertiesViewIsLoading() throws {
    let viewModel = prepareViewModel(isLoading: true, isHeaderRedacting: false)
    let view = ContainerPropertiesView(viewModel: viewModel)
    try validateView(view)
  }

  func testContainerPropertiesViewIsHeaderRedacting() throws {
    let viewModel = prepareViewModel(isLoading: false, isHeaderRedacting: true)
    let view = ContainerPropertiesView(viewModel: viewModel)
    try validateView(view)
  }

  func testContainerPropertiesViewIsLoadedWithShareTabAndOneShareItem() throws {
    for index in 1..<4 {
      let viewModel = prepareViewModel(
        isLoading: false,
        isHeaderRedacting: false,
        selectedOption: .share,
        shareItems: Array(repeating: shareItem, count: index)
      )
      let view = ContainerPropertiesView(viewModel: viewModel)
      try validateView(view, testName: #function + "-shareItemCount-\(index)")
    }
  }

  func testContainerPropertiesViewIsLoadedWithShareAndRemoveNoAlias() throws {
    let removeViewModel = RemoveViewModel(publicKey: "public key", alias: nil)
    let viewModel = prepareViewModel(
      isLoading: false,
      isHeaderRedacting: false,
      selectedOption: .share,
      removeViewModel: removeViewModel
    )
    let view = ContainerPropertiesView(viewModel: viewModel)
    try validateView(view)
  }

  func testContainerPropertiesViewIsLoadedWithShareAndRemoveAlias() throws {
    let removeViewModel = RemoveViewModel(publicKey: "public key", alias: "alias")
    let viewModel = prepareViewModel(
      isLoading: false,
      isHeaderRedacting: false,
      selectedOption: .share,
      removeViewModel: removeViewModel
    )
    let view = ContainerPropertiesView(viewModel: viewModel)
    try validateView(view)
  }

  // MARK: - Helpers

  private func prepareViewModel(
    isLoading: Bool,
    isHeaderRedacting: Bool,
    selectedOption: ContainerPropertiesOptions = .storage,
    shareItems: [ContainerPropertiesShareItem] = [],
    removeViewModel: RemoveViewModel? = nil
  ) -> ContainerPropertiesViewModelStub {
    ContainerPropertiesViewModelStub(
      options: [.storage, .share],
      selectedOption: selectedOption,
      isLoading: isLoading,
      isHeaderRedacting: isHeaderRedacting,
      containerInfo: ContainerInfo(name: "Public Documents", properties: "256.6MB, 25 files", icon: nil),
      identifier: "identifier",
      shareViewModel: ContainerPropertiesShareViewModelStub(
        publicKey: "0xfcQ1...4a92cL",
        isLoading: false,
        shareItems: shareItems,
        removeViewModel: removeViewModel
      ),
      storageViewModel: ContainerPropertiesStorageViewModelStub(
        storageItems: [],
        isLoading: false
      )
    )
  }
}
