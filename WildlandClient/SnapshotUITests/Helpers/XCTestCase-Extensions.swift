//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import SwiftUI
import SnapshotTesting
@testable import CargoUI

extension XCTestCase {
  func validateView(_ view: some View, file: StaticString = #file, testName: String = #function) throws {
    try ColorScheme.allCases.forEach { schema in
      let window = NSWindow(
        view.preferredColorScheme(schema)
      )
      let nsView = try XCTUnwrap(window.contentView)
      assertSnapshot(of: nsView, as: .image(perceptualPrecision: 0.98), file: file, testName: testName + schema.name)
    }
  }
}

private extension ColorScheme {
  var name: String {
    switch self {
    case .light:
      return "-light"
    case .dark:
      return "-dark"
    @unknown default:
      return ""
    }
  }
}
