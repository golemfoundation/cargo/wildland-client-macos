#!/bin/sh

set -ex

DMG=${1:?"need dmg path"} # /path/to/Cargo.dmg
IDENTITY=$2

if [ "$IDENTITY" == "" ]; then
  # If a code signing identity is not specified, use ad hoc signing
  IDENTITY="-"
fi
codesign --verbose --force --timestamp -o runtime --sign "$IDENTITY" "$DMG"
