#!/bin/sh

set -ex

SRC_APP=${1:?"need application bundle path"} # /path/to/Cargo.app
DST_DMG=${2:?"need dmg output path"} # /path/to/Cargo.dmg

CURRENT_DIR=`dirname $0`
SUPPORTS_DIR="$CURRENT_DIR/../../Documentation/tech/support"

APP_NAME="Cargo"

# Build DMG using built app
create-dmg \
  --volname "$APP_NAME" \
  --background "$CURRENT_DIR/Resources/backgroundImage.jpg" \
  --window-pos 200 120 \
  --window-size 768 588 \
  --icon-size 100 \
  --icon "$APP_NAME.app" 234 180 \
  --hide-extension "$APP_NAME.app" \
  --app-drop-link 525 180 \
  --no-internet-enable \
  --codesign "6E2412AE749A806331F2D5FD38E438CC778A669E" \
  --add-file "lightCleanup" "$SUPPORTS_DIR/lightCleanup" 234 400 \
  --add-file "completeCleanup" "$SUPPORTS_DIR/completeCleanup" 525 400 \
  --volicon "$CURRENT_DIR/Resources/containerIcon.icns" \
  --format UDBZ \
  "$DST_DMG" "$SRC_APP"
