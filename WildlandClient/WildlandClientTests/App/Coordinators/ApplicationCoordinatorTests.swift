//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
import CargoUI
import WildlandCommon
@testable import Cargo

@MainActor
final class ApplicationCoordinatorTests: XCTestCase {

  // MARK: - Private

  private var sut: ApplicationCoordinator!

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    let bundleStub = BundleStub()
    bundleStub.expectedBundleURL = URL(filePath: "somePath/somePath/Cargo.app/somePath")
    sut = ApplicationCoordinator(
      daemonService: DaemonUserServiceMock(),
      bundle: bundleStub
    )
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakAppCoordinator = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakAppCoordinator)
  }

  func testActiveScenesStartsWithEmpty() {
    XCTAssertEqual(sut.activeScenes, [.empty])
  }

  func testInactiveScenesComputedCorrectly() {
    // given
    let inactiveScenes = sut.inactiveScenes

    // then
    XCTAssertEqual(inactiveScenes, Set(AppSceneType.allCases).subtracting(sut.activeScenes))
  }

  func testHandleDeeplinkAppendsSceneType() {
    // given
    let url = URL(string: "wildlandclient://onboarding")!

    // when
    sut.handleDeeplink(url: url)

    // then
    XCTAssertTrue(sut.activeScenes.contains(.onboarding))
  }
}
