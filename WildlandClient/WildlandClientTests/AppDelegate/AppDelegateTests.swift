//
// Wildland Project
// Wildland Cargo
//
//
// Copyright © 2023 Golem Foundation
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import XCTest
@testable import Cargo

final class AppDelegateTests: XCTestCase {

  // MARK: - Private

  private var sut: AppDelegate!

  // MARK: - Setup

  override func setUp() {
    super.setUp()
    sut = AppDelegate()
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  // MARK: - Tests

  func testDeinitSuccess() {
    // given
    weak var weakAppDelegate = sut

    // when
    sut = nil

    // then
    XCTAssertNil(weakAppDelegate)
  }

  func testApplicationOpenURLs() {
    // given
    let url = URL(string: "https://example.com")!

    // when
    sut.application(NSApplication.shared, open: [url])

    // then
    XCTAssertEqual(sut.deeplink, url)
  }
}
