#!/bin/bash

set -eo pipefail

WL_BUILD_ROOT=${WL_BUILD_ROOT:-"build"}

xcodebuild test -workspace WildlandClient.xcworkspace \
  -scheme GenericUnitTests \
  -configuration Debug \
  -enableCodeCoverage YES \
  -derivedDataPath "$WL_BUILD_ROOT" \
  | xcpretty

slather coverage -xv
slather coverage -s
