#!/bin/bash
# Maintainers:
#     Michał Kluczek  (michal@wildland.io)
#     Piotr Isajew    (pisajew@wildland.io)
#     Marek Słomnicki (marek@wildland.io)
# Variables to provide:
# - GCP_CREDENTIALS - Service Account credentials (base64 encoded)
# - ARTIFACT_SRC_PATH - source path to artifact
# - ARTIFACT_DST_PATH - destination path to artifact
# - ARTIFACT_SRC_NAME - source artifact name
# - ARTIFACT_DST_NAME - destination artifact name (use {{PACKAGE_SUFFIX}} as unique placeholder)
# - FETCH_URL - URL prefix where DMG files are stored
# - WL_BUILD_ROOT - build root directory
# - SPARKLE_PRIVATE_KEY - Sparkle private key to sign updates

set -e

SPARKLE_LINK=${SPARKLE_LINK:-"https://wildland.io/"}
WL_BUILD_ROOT=${WL_BUILD_ROOT:?"need build root path. Usually 'build' folder or another path to keep build intermediates"}

sync_bucket() {
  if [ "$1" = "download" ]; then
    echo "Sync bucket with local copy"
    gsutil -m rsync -r -d "$ARTIFACT_DST_PATH" "$DST_PATH"
  elif [ "$1" = "upload" ]; then
    echo "Sync data with remote bucket"
    gsutil -m rsync -r -d "$DST_PATH" "$ARTIFACT_DST_PATH"
  fi
}

generate_sparkle() {
  echo "Generating Sparkle appcast.xml"
  echo "$SPARKLE_PRIVATE_KEY" | "$WL_BUILD_ROOT/SourcePackages/artifacts/sparkle/Sparkle/bin/generate_appcast" \
    --ed-key-file - \
    --link "$SPARKLE_LINK" \
    --download-url-prefix "$FETCH_URL/" \
    "$DST_PATH"
}

copy_artifact() {
  # Generate PACKAGE_SUFFIX
  if [ -z ${CI_COMMIT_TAG+x} ]; then
    # If CI_COMMIT_TAG var is unset (ie. it's *not* a tag pipeline)
    PACKAGE_SUFFIX=$CI_COMMIT_SHORT_SHA
    echo "Detected development commit $PACKAGE_SUFFIX"
  else
    # Otherwise... (ie. tag-triggered pipeline)
    PACKAGE_SUFFIX=$CI_COMMIT_TAG
    echo "Detected a tag commit $PACKAGE_SUFFIX"
  fi

  # Copy artifact
  ARTIFACT_NAME="${ARTIFACT_DST_NAME/\{\{PACKAGE_SUFFIX\}\}/$PACKAGE_SUFFIX}"
  echo "Copy artifact from '$ARTIFACT_SRC_NAME' to '${DST_PATH}/${ARTIFACT_NAME}'"
  cp "${ARTIFACT_SRC_PATH}/${ARTIFACT_SRC_NAME}" "${DST_PATH}/${ARTIFACT_NAME}"
}

TARGET_BRANCH=${TARGET_BRANCH:?"Target branch needs to be defined"}
DST_PATH="$HOME/builds/cargo/binaries/$TARGET_BRANCH"

if [ -n "${GCP_CREDENTIALS+x}" ]; then
  echo "$GCP_CREDENTIALS" | base64 -d | gcloud auth activate-service-account --key-file=-
fi

mkdir -p "$DST_PATH"

set -x

sync_bucket download
copy_artifact
generate_sparkle
sync_bucket upload
bash ci/generate_and_publish_releases_page.sh
