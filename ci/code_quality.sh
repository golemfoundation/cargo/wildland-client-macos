#!/bin/sh

mkdir -p reports

swiftlint lint --reporter codeclimate Wildland WildlandClient > reports/swiftlint.json
