#!/bin/sh

WL_BUILD_ROOT=${WL_BUILD_ROOT:-"build"}

rm -rf "$WL_BUILD_ROOT"

xcodebuild -resolvePackageDependencies \
  -workspace WildlandClient.xcworkspace \
  -scheme Cargo \
  -configuration Release \
  -derivedDataPath "$WL_BUILD_ROOT"

xcodebuild build \
  -workspace WildlandClient.xcworkspace \
  -scheme Cargo \
  -configuration Release \
  -derivedDataPath "$WL_BUILD_ROOT" 
