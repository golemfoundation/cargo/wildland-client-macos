#!/usr/bin/env bash

if [ ! -f "${DMG_FILE}" ]; then
  echo "The following input file does not exists or is not accessible: ${DMG_FILE}"
  exit 1
fi

if [[ "${DMG_FILE: -4}" != ".dmg" ]]; then
  echo "The input file was not recognized. Only dmg files are supported"
  exit 1
fi

xcrun notarytool submit "${DMG_FILE}" \
  -v \
  --team-id "$ADP_TEAMID" \
  --apple-id "$ADP_USERID" \
  --password "$ADP_APP_PASS" \
  --wait

if [[ $? != 0 ]]; then
  echo "Notarization request has failed"
  exit 1
fi

# This will automatically pick up the right notarization for the file, if it is available.
# If not, it will fail and you need to rerun the script again later.
echo "Running the stapling procedure..."
xcrun stapler staple "${DMG_FILE}" || exit 1
xcrun stapler validate "${DMG_FILE}" || exit 1
