#!/bin/bash
# Maintainers:
#     Marek Słomnicki (marek@wildland.io)
#
# Variables to provide:
# - PUBLISH_BUCKET_NAME - bucket with published images
# - VERSION_REGEX - regex to get commit value
# - FETCH_URL - URL to fetch artifacts
# - GIT_HISTORY_SINCE - older entries will be shown as "[...]"
# - REMOVE_MERGE_COMMITS - remove merge commits from changelog
# - MD_HEADER - page header
# - HACKMD_TOKEN - access token to HackMD
# - HACKMD_NOTEID - note id in HackMD
# - HACKMD_TAGS - tags to set (watch on backticks!)
#
# TODO:
# - tag support

set -e

GIT_HISTORY_SINCE=${GIT_HISTORY_SINCE:-"1 year"}
REMOVE_MERGE_COMMITS=${REMOVE_MERGE_COMMITS:-true}
MD_HEADER=${MD_HEADER:-"# Wildland Cargo macOS client releases page"}
HACKMD_TAGS=${HACKMD_TAGS:-"\`clients/macos\`, \`release_notes\`"}

echo "Fetching builds"
# Get all releases from bucket
builds=$(gsutil ls "gs://$PUBLISH_BUCKET_NAME/**/*.dmg" | sed "s#gs://$PUBLISH_BUCKET_NAME/##")
commits=$(git log --pretty=format:"%h|%cs|%s" --since="$GIT_HISTORY_SINCE")

TMPFILE=$(mktemp)
echo "$MD_HEADER" >"$TMPFILE"

echo "$commits" | while read -r logline; do
  IFS='|' read -ra fields <<<"$logline"
  HASH=${fields[0]}
  DATE=${fields[1]}
  MESSAGE=${fields[2]}
  if grep -q "$HASH" <<<"$builds"; then
    ARTIFACT_NAME=$(grep "$HASH" <<<"$builds")
    {
      echo "## Build $HASH ($DATE)"
      echo "### Assets"
      echo "- [$ARTIFACT_NAME]($FETCH_URL/$ARTIFACT_NAME)"
      echo "### Changelog"
    } >>"$TMPFILE"
  fi
  if [[ $REMOVE_MERGE_COMMITS = true && ! $MESSAGE =~ ^Merge\ branch ]]; then
    echo "- $DATE - $MESSAGE" >>"$TMPFILE"
  fi
done
{
  echo "- [...]"
  echo "###### tags: $HACKMD_TAGS"
} >>"$TMPFILE"
echo "Changelog generated"

echo "Uploading file to hackmd"
BODY=$(jq --null-input --arg content "$(cat "$TMPFILE")" '{"content": $content}')
curl -q -H "Authorization: Bearer $HACKMD_TOKEN" -H 'Content-Type: application/json' -X PATCH "https://api.hackmd.io/v1/notes/$HACKMD_NOTEID" -d "$BODY"
rm "$TMPFILE"
