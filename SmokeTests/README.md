# Integration tests environment

Smoke test suite used to perform UI test using pytest and appium.
Tests are performed in VM which is reverted to known state after running test suite.

## Usage

```shell
./test.sh [path_to_DMG_file]
```

## Main files
- `test.sh` - run test suite
  - run VM in UTM
  - copy Cargo.dmg
  - install Cargo application
  - copy test suite
  - start Appium server
  - run test suite
  - shutdown VM
  - rollback VM to state in saved in _backup_ directory
- `config.sh` - common functions + variables to use in scripts
- `TestSuite/runnerpy.sh` - script running test suite inside VM
- `TestSuite/main.py` - pytest test suite
- `ssh/demo` - ssh key used to connect to UTM VM 

## Main environment variables

- `VM_NO_ROLLBACK` - set to any value to disable rollback of VM after tests
- `UTM_DIR` - location of VM files (default: `~/UTM/`)
- `VM_NAME` - name of the VM to start

# Developer notes

Current configuration has the following specials:

- set appearance to "Light"
- Make fixed highlight color (Blue)
- Disable wallpaper tinting
- Always show scroll bars

Details in [VM creation docs](docs/VM_creation_steps.md)
