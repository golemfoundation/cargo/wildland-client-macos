STDIR=${STDIR=$(pwd)}
SSHKEY="${STDIR}/ssh/demo"
VM_NAME=${VM_NAME:-"macOS_13.3.1"}
VM_IP="192.168.64.2"
VM_USERNAME=demo
UTM_DIR=${UTM_DIR:-"${HOME}/UTM"}

vm_check_status() {
  VM_STATUS=$(utmctl status "$VM_NAME")
  if [ "$VM_STATUS" != "$1" ]; then
    return 1
  fi
}

vm_start() {
  open -a UTM
  until pgrep -q UTM; do
    sleep 1
  done
  sleep 5 # Dirty hack to be sure that UTM is up and running
  echo "Import VM to UTM"
  open "$UTM_DIR/${VM_NAME}.utm"
  vm_check_status "stopped" || return 1
  echo "Start VM"
  utmctl start "$VM_NAME"
}

vm_stop() {
  vm_check_status "started" || return 1
  ssh -i "$SSHKEY" $VM_USERNAME@$VM_IP 'sudo shutdown -h now'
  until vm_check_status "stopped"; do
    sleep 1
  done
}

vm_rollback() {
  vm_check_status "stopped"
  pushd "$UTM_DIR" || return 1
  echo "Deleting of old VM"
  rm -Rf "${VM_NAME}.utm"
  echo "Copying VM image"
  cp -cR "${VM_NAME}.utm.backup" "${VM_NAME}.utm"
  echo "Rollback completed"
  popd || return 1
}

wait_for_vm() {
  echo "Waiting for VM to start"
  gotit=0
  set -x
  while [ "$gotit" != "1" ]; do
    sleep 1
    gotit=$(ssh -i "$SSHKEY" -oConnectTimeout=10 $VM_USERNAME@$VM_IP echo 1 2>/dev/null)
  done
  set +x
}

wait_for_no_vm() {
  echo "Waiting for VM to terminate"
  stillthere=1
  while [ $stillthere -ne 0 ]; do
    ping -c1 -q $VM_IP >/dev/null 2>&1
    if [ $? -eq 2 ]; then
      stillthere=0
    else
      sleep 1
    fi
  done
}
