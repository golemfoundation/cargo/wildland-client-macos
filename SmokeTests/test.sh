#!/bin/bash

. ./common.sh
CARGO_DMG=${1:-"Cargo.dmg"}

if [ ! -f "$CARGO_DMG" ]; then
  echo "Cargo DMG file not exists"
  exit 101
fi

echo "Remove old output directory"
rm -Rf out

echo "Fix SSH key privileges"
chmod 0600 "$SSHKEY"

echo "Starting VM"
vm_start || (echo "VM is running, exiting..."; exit 102)
wait_for_vm

# WARNING ---== ACTUAL TEST CODE STARTS HERE ==----
echo "Installing Cargo"
scp -i "$SSHKEY" "$CARGO_DMG" "$VM_USERNAME@$VM_IP:Cargo.dmg"
ssh -i "$SSHKEY" "$VM_USERNAME@$VM_IP" "hdiutil mount Cargo.dmg && sudo cp -R /Volumes/Cargo/Cargo.app /Applications && hdiutil unmount /Volumes/Cargo"

echo "Copying test suite"
rsync -e "ssh -i $SSHKEY" -r TestSuite/* "$VM_USERNAME@$VM_IP:TestSuite/"

echo "Running test suite"
ssh -i "$SSHKEY" "$VM_USERNAME@$VM_IP" TestSuite/runnerpy.sh
EXITCODE=$?

rsync -e "ssh -i $SSHKEY" -r "$VM_USERNAME@$VM_IP:TestSuite/out/*" out/

# WARNING ---== NO TEST LOGIC SHOULD HAPPEN BEYOND THIS POINT ==---

echo "Shutting down VM"
vm_stop || echo "VM already stopped, omiting stop command"
wait_for_no_vm
[ -n "$VM_NO_ROLLBACK" ] || vm_rollback
echo "All done"

if [ $EXITCODE -eq 0 ]; then
  echo "Tests Succeeded"
else
  echo "Tests Failed"
fi
exit $EXITCODE
