# Gitlab runner configration

## Input data

- gitlab-runner tag: `utm`
- name of UTM VM: `macOS_13.3.1`

## UTM configuration

- unpack VM image to `~/UTM/`
- `mv macOS_13.3.1.utm macOS_13.3.1.utm.backup`
- `cp -cR macOS_13.3.1.utm.backup macOS_13.3.1.utm`
- `brew install --cask utm`
- start UTM
- click `Create a New Virtual Machine`
- click `Open`
- open `~/UTM/macOS_13.3.1.utm` directory
- run VM
- login to VM using ssh to save fingerprint
- shutdown VM
- test if command `utmctl list` works properly

## Gitlab runner configuration

- run command `gitlab-runner register` and register runner with tag `utm`
- edit file `~/.gitlab-runner/config.toml` and add `concurrent = 1` to new runner
- `gitlab-runner restart`
- `automationmodetool enable-automationmode-without-authentication`
- run pipeline once and look at screen to click popups
