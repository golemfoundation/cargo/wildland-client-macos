# VM installation and config steps

- Install macOS
- Language English
- Account `demo` with password `password`
- disable Siri
- set appearance to Light
- Settings
    - Network
        - DHCP with manual ip address 192.168.64.2 (or else, depending on config)
        - when VMs are started bridge100 is started and used
    - General/Sharing
        - Screen sharing: On for All users
        - Remote login: On for All users, Allow full disk access for remote users
    - Appearance
        - Appearance: Light
        - Accent colour: Blue
        - Allow wallpaper tinting in windows: Off
        - Show scroll bars: always
    - Accessibility/Display
        - Reduce motion: On
        - Reduce transparency: On
    - Lock screen
        - Start Screen Saver when inactive: Never
        - Turn display off when inactive: Never
    - Users and groups
        - Create user `demo` with password `password`
        - Enable autologin for user `demo`
- install ssh key for demo user
- install homebrew (/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)")
- `(echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> /Users/demo/.zprofile`
- `(echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> /Users/demo/.profile`
- `brew install python`
- `echo "demo ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/demo` as root
- install appium
  ```shell
  brew install node
  npm install -g appium@next
  appium driver install mac2
  ```
- install XCode
  - download installer xip
  - `open XCode_14.3.xip`
  - `mv XCode.app /Applications`
  - `sudo xcode-select --switch /Applications/Xcode.app`
  - Run XCode, agree with license, install frameworks
  - `open /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/Library/Xcode/Agents/`
  - drag & drop the Xcode Helper app to Security & Privacy -> Privacy -> Accessibility list of your System Preferences
  - `automationmodetool enable-automationmode-without-authentication`
