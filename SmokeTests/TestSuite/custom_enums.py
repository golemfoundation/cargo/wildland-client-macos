from enum import Enum


class Components(str, Enum):
    SCREEN_TITLE = "screenTitle"
    CREATE_NEW_IDENTITY = "CreateNew"
    NEXT_BTN = "NextButton"
    CHECKBOX_PASSPHRASE = "WrittenDownCheckbox"
    USE_WL = "UseWildland"
    GENERATE_WORDS = "GeneratedWordWithIndex"
    CONFIRM_BUTTON = "ConfirmButton"
    CHECKBOX = "Checkbox"
    SELECT_OPTION = "SelectOption"
    FFT = "FreeFoundationTier"
    TEXT_FIELD = "TextField"


class Views(str, Enum):
    INTRO_CONTAINER = "OnboardingIntroContainerView"
    SETUP_CARGO = "SetupWildlandCargoView"
    ONBOARDING_INTRO_CONTAINER_VIEW = "OnboardingIntroContainerView"
    ABOUT_WL_IDENTITIES = "AboutWildlandIdentitiesView"
    MNEMONIC_PHRASE_SELECTOR = "MnemonicPhraseSelector"
    CREATE_WL_IDENTITY = "CreateWildlandIdentityView"
    NEW_WL_PASSPHRASE = "NewWildlandWordsView"
    GENERATE_PASSPHRASE = "NewWildlandWordsView"
    CONFIRM_WORDS = "ConfirmWildlandWordsView"
    NAME_DEVICE = "NameDeviceView"
    CHOOSE_CARGO_STORAGE = "ChooseCargoStorageView"
    GET_VERIFICATION_CODE = "GetVerificationCodeView"
    ENTER_VERIFICATION_CODE = "EnterVerificationCodeView"
