from pathlib import Path
from typing import Union
from dataclasses import dataclass
from logger import logger
import subprocess
import time
import psutil
import requests
import secrets


@dataclass
class AppPredicates:
    radio_setup_new_identity: str = 'label CONTAINS "Create a new Wildland identity"'
    next_btn: str = 'label CONTAINS "Next"'
    about_wl_identities: str = 'label CONTAINS "About Wildland identities"'
    get_code_btn: str = 'label CONTAINS "Get code"'
    fft: str = 'label CONTAINS "Free foundation tier"'
    radio_use_wl: str = 'label CONTAINS "Use Wildland for identity"'
    confirm_btn: str = 'label CONTAINS "Confirm words"'
    radio_fft: str = 'label CONTAINS "Free Foundation tier"'

    @staticmethod
    def generated_word(index):
        return f'value CONTAINS "generated-word-{index}"'


@dataclass
class AppConfig:
    base_dir: Path = Path.home()
    test_folder: Path = base_dir / "Documents/TestFiles"
    destination: Path = base_dir / "Library/CloudStorage/Cargo-FileProvider"
    number_of_default_items: int = 1
    email: str = f"{secrets.token_hex(12)}@{secrets.token_hex(6)}.xyz"
    str_to_format_type1: str = "{0}.{1}.{2}-{3}.{4}.{5}"
    log_file: Path = Path('mylogs.txt')
    evs_url: str = "https://staging.cargo.wildland.dev"


config = AppConfig()
predicates = AppPredicates()


def run_log_stream(log_file: Path):
    with log_file.open('w') as f:
        process = subprocess.Popen(
            ['log', 'stream', '--predicate', 'subsystem == "io.wildland.client"', '--level', 'debug'], stdout=f)
        process.wait()


def wait_for_session_id(file_name: Path, timeout=10):
    start_time = time.time()  # Save the start time

    while True:
        session_id = parse_log_file(file_name)
        if session_id:
            return session_id  # Return the session id as soon as it's found

        if time.time() - start_time > timeout:
            raise TimeoutError(
                f"Timeout of {timeout}s exceeded while waiting for session id")  # Raise an error if the timeout is exceeded

        time.sleep(0.1)  # Sleep for a short amount of time to avoid hogging the CPU


def get_and_verify_token(evs_url: str, email: str, session_id: str):
    verification_token = request_verification_token(evs_url, email, session_id)
    confirm_verification_token(evs_url, email, verification_token, session_id)
    return verification_token


def request_verification_token(evs_url: str, email: str, session_id: str):
    get_token_url = f"{evs_url}/debug_get_token"
    payload = {"email": email, "session_id": session_id}
    response = requests.get(get_token_url, params=payload)
    response.raise_for_status()

    verification_token = response.json().get("tokens", [])[0]
    print(verification_token)

    return verification_token


def confirm_verification_token(evs_url: str, email: str, verification_token: str, session_id: str):
    confirm_token_url = f"{evs_url}/confirm_token"
    payload = {
        "email": email,
        "verification_token": verification_token,
        "session_id": session_id,
    }
    response = requests.put(confirm_token_url, json=payload)
    response.raise_for_status()
    return response


def initiate_token_verification(log_file: Path, evs_url: str, email: str):
    # Although at this point, request to /get_storage already called by Cargo, we want to get session_id in
    # order to be able to confirm token without checking email
    session_id = wait_for_session_id(log_file)

    return get_and_verify_token(evs_url, email, session_id)


def parse_log_file(file_name: Union[str, Path]):
    file_name = Path(file_name)  # This ensures file_name is a Path object
    with file_name.open('r') as file:
        for line in file:
            if 'session id:' in line:
                session_id = line.split('session id: ')[1].split()[
                    0]  # The session id appears to be before the first space after "session id: "
                return session_id
    return None  # return None if no session id was found


def check_if_process_running(processName: str):
    """
    Check if there is any running process that contains the given name processName.
    """
    # Iterate over the all the running processes
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False


def find_element_with_retry(driver, find_method, find_args, max_retries=10, retry_interval=1):
    """
    Find an element using the given method and arguments, retrying up to `max_retries` times with an interval of
    `retry_interval` seconds between each retry if the element is not found.
    """
    for i in range(max_retries):
        try:
            element = find_method(*find_args)
            return element
        except Exception as e:
            if isinstance(e, TimeoutError):
                logger.error(f"Timed out while waiting for element on attempt {i + 1}/{max_retries}. Retrying...")
            else:
                if i == max_retries - 1:
                    raise  # re-raise the exception if we've exceeded the maximum number of retries
                else:
                    logger.error(f"Failed to find element on attempt {i + 1}/{max_retries}. Retrying...")
            time.sleep(retry_interval)
