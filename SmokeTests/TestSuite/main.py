from multiprocessing import Process
import time
import pytest
from appium import webdriver
from appium.options.mac import Mac2Options
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.mobileby import MobileBy
from selenium.common.exceptions import NoSuchElementException

from custom_enums import Views, Components
from logger import logger

from helpers import (
    config,
    predicates,
    run_log_stream,
    find_element_with_retry,
    check_if_process_running,
    initiate_token_verification
)


@pytest.fixture(scope='module', autouse=True)
def log_setup_teardown(request):
    global log_process
    log_process = Process(target=run_log_stream, args=(config.log_file,))
    log_process.start()  # Start the log process
    time.sleep(2)

    def kill_log_process():
        if log_process and log_process.is_alive():  # Check if the process is running
            log_process.terminate()  # Send a signal to the log process to terminate it
            log_process.join()  # Wait for the process to actually terminate

    # This is the teardown. It will be called after the last test in the module has finished execution.
    request.addfinalizer(kill_log_process)


@pytest.fixture()
def driver():
    options = Mac2Options()
    options.bundle_id = 'io.wildland.Cargo'
    options.show_server_logs = 'true'

    logger.info("Initializing a driver")

    for attempt in range(5):
        try:
            drv = webdriver.Remote('http://0.0.0.0:4723', options=options)
            drv.capabilities['appium:showServerLogs'] = 'true'
            break  # If the driver was initialized successfully, break the loop
        except Exception as e:
            logger.error(f"Error while trying to connect to Appium server: {e}")
            logger.info("Retrying in 5 seconds...")
            time.sleep(5)
    else:
        raise Exception("Could not connect to Appium server after 5 attempts")

    yield drv

    logger.info("Stopping a driver")
    drv.quit()


def click_next_button(driver):
    try:
        next_button = find_element_with_retry(driver, driver.find_element,
                                              (MobileBy.IOS_PREDICATE, predicates.next_btn))
        next_button.click()
        return True
    except NoSuchElementException:
        return False


def test_onboarding(driver):
    def element_id_finder(elem):
        return driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, value=elem)

    logger.info("Starting to test onboarding flow: ")
    create_new_identity = find_element_with_retry(driver, driver.find_element,
                                                  (MobileBy.IOS_PREDICATE, predicates.radio_setup_new_identity))
    create_new_identity.click()
    click_next_button(driver)
    click_next_button(driver)
    use_wl_identity = find_element_with_retry(driver, driver.find_element,
                                              (MobileBy.IOS_PREDICATE, predicates.radio_use_wl))
    use_wl_identity.click()
    click_next_button(driver)
    generate = driver.find_element(by=AppiumBy.NAME, value="Generate")
    generate.click()
    seed_phrase_checkbox = driver.find_element(by=AppiumBy.NAME,
                                               value="I have written down or otherwise securely stored my secret phrase")
    confirm_words = find_element_with_retry(driver, driver.find_element,
                                            (MobileBy.IOS_PREDICATE, predicates.confirm_btn))
    seed_phrase = {}
    for i in range(1, 13):
        seed_phrase[i] = element_id_finder(
            "{0}.{1}".format(Views.NEW_WL_PASSPHRASE.value,
                             Components.GENERATE_WORDS + '.' + str(i - 1))).get_attribute('value')

    seed_phrase_checkbox.click()

    def select_word(num):
        return seed_phrase.get(num)

    confirm_words.click()
    for i in range(3):
        required_word = element_id_finder(Views.CONFIRM_WORDS + ".RequiredWordIndex").get_attribute('value').replace(
            ".", "")
        required_word = int(required_word)
        driver.find_element(by=AppiumBy.NAME, value=str(select_word(required_word))).click()
        time.sleep(1)

    click_next_button(driver)
    fft = find_element_with_retry(driver, driver.find_element, (MobileBy.IOS_PREDICATE, predicates.radio_fft))
    terms_conditions_cb = driver.find_element(by=AppiumBy.NAME, value="I agree to the free tier")
    fft.click()
    terms_conditions_cb.click()
    click_next_button(driver)
    enter_email = element_id_finder("{0}.{1}".format(Views.GET_VERIFICATION_CODE.value, Components.TEXT_FIELD.value))
    enter_email.send_keys(config.email)

    get_code = find_element_with_retry(driver, driver.find_element, (MobileBy.IOS_PREDICATE, predicates.get_code_btn))
    get_code.click()

    verification_token = initiate_token_verification(config.log_file, config.evs_url, config.email)
    text_fields = [
        find_element_with_retry(driver, element_id_finder, ("{0}.{1}.{2}".format(Views.ENTER_VERIFICATION_CODE.value,
                                                                                 Components.TEXT_FIELD.value, i),))
        for i in range(len(verification_token))
    ]
    for i, char in enumerate(verification_token):
        text_fields[i].send_keys(char)
    click_next_button(driver)


def test_daemon():
    logger.info("Checking if daemon is running: ")
    if check_if_process_running('wlclientd'):
        assert True
        logger.info('Yes a wlclientd process was running')
    else:
        logger.info('No wlclientd process was running')
        assert False
