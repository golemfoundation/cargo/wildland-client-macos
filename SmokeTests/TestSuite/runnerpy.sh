#!/bin/bash -l
cd TestSuite || exit 201

echo "Create output directory"
rm -Rf out
mkdir -p out

# This is workaround which fixes launching tests
# Task for improve and more details: CAQD-252
open /Applications/Cargo.app
sleep 5
killall Cargo

echo "Starting Appium"
appium server --log out/appium.log &
APPIUM_PID=$!

echo "Wait until Appium is up and running"
until nc -z localhost 4723; do
  sleep 1
done

echo "Install requirements"
pip3 install -r requirements.txt

echo "Run Test Suite"
pytest main.py --junitxml=out/report.xml
EXIT_CODE=$?

echo "Cleanup"
kill $APPIUM_PID

if [ $EXIT_CODE -ne 0 ]; then
  echo "Copy crash logs to output directory"
  cp ~/Library/Logs/DiagnosticReports/*.crash out/
fi

exit $EXIT_CODE
