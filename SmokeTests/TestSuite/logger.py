import logging

logging.basicConfig(filename='debug.log', level=logging.DEBUG, force=True)
logger = logging.getLogger(__name__)
