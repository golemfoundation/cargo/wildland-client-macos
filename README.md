Wildland macOS client
=====================

This is the Wildland Cargo macOS client source code. 

Status
------

This project is under intensive development and, due to the early
stage is not ready for any usage yet. You may expect to build the
project, but unless you are interested in
[contributing](https://docs.wildland.io/contributor-agreement.html),
you probably won't find a use for it.


More info
---------

- you can learn more about Wildland project on
  [wildland.io](https://wildland.io)
- to find more about Wildland's cross-platform core, which is
  what this project uses see
  [CoreX](https://gitlab.com/wildland/corex/wildland-core)
  repository
- for more information about macOS Cargo, see our
  internal [documentation](Documentation) directory

Build environment
-----------------

Our current CI setup uses:
- macOS Monterey 12.6 running on x86/ARM CPU 
- Xcode 14.0 (14A309)
- swiftlint
- swiftformat (not required for building)
- create-dmg (required for building DMG)

Building
--------

`swiftlint` utility is needed for the build. It can be installed
from [Homebrew](https://brew.sh) with the following command:

```
brew install swiftlint 
```

Building using Xcode should be straightforward: just open
the WildandClient Xcode project, make sure that _Cargo_ target is
selected and press Cmd+R. 

If you want to build a DMG image, use _CargoDMG_
target instead and press Cmd+B. Before doing so, you want to
ensure, that you have valid installation of _create-dmg_ tool on your machine.

```
brew install create-dmg
```

Build can also be performed using command line. The script:

```
./scripts/build_all.sh build
```

should build the project and put the resulting artifacts in `build/Products`

Note on macOS targets
---------------------

The application uses an updated version of the `KeychainAccess` library as a dependency to make 
possible keychain item sharing between `Cargo` and `wlclientd` targets.
The code in the original repository [OwnerRepo](https://github.com/kishikawakatsumi/KeychainAccess)
does not support using of the `kSecUseDataProtectionKeychain` key while fetching 
shared keys in the keychain sharing group.
The source is available by the link: [KeychainAccess](https://gitlab.com/wildland/varia/keychainaccess.git)

Note on iOS targets
-----------------

iOS targets do not work yet. Don't expect them to even build.

Problems
--------

Before reporting build-related problems, make sure that your build
environment (macOS and Xcode versions) is up to date.

When reporting problems provide detailed description of your
environment and steps to reproduce the problem.

First launch
------------

With a single exception of launching the app from Xcode,
application should be copied to /Applications folder before
starting it.

You start the client by opening Cargo.app. This installs
the daemon and will enable file provider extension. The
daemon will launch automatically once you log in.

Development
------------

The code in the project should follow internal coding conventions and standards.
To keep code consistent, `swiftlint` and `swiftformat` tools are used.
When some piece of work is done, update code format by calling
```./scripts/format_code.sh```


Using
-------

The project is not ready for usage yet, but we are working hard to
make it happen.

Copyright and licensing
-------------------

In order to be able to contribute to any Wildland repository, you will
need to agree to the terms of the Wildland Contributor Agreement,
available at (https://docs.wildland.io/contributor-agreement.html). By
contributing to any such repository, you agree that your contributions
will be licensed under the [GPLv3 License](LICENSE).
